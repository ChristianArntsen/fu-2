<?php
//print_r($teetime_info);
echo form_open('teesheets/repeat/'.$teetime_id, array('id'=>'repeat_form'));
?>
<style>
	.ui-button-text-only .ui-button-text {
		padding:.1em 0.6em;
	}
</style>
<fieldset id="repeat_basic_info">
<legend><?php echo 'Repeat Details'; ?></legend>
<div class="field_row clearfix">
<?php echo form_label('Repeats:', 'name',array('class'=>'wide label-override')); ?>
	<div class='form_field'>
		<div id='repeats_buttons' class='repeats_buttons'>
			<?php echo form_radio(array('id'=>'once','name'=>'repeats','value'=>'once')).form_label('Once', 'once', array()); ?>
			<?php echo form_radio(array('id'=>'daily','name'=>'repeats','value'=>'daily')).form_label('Daily', 'daily', array()); ?>
			<?php echo form_radio(array('id'=>'weekly','name'=>'repeats','value'=>'weekly','checked'=>true)).form_label('Weekly', 'weekly', array()); ?>
			<?php //echo form_dropdown('repeats', array('daily'=>'Daily','weekly'=>'Weekly'), 'weekly'); ?>
		</div>
	</div>
</div>
<div class="field_row clearfix day_options">
<?php echo form_label('Repeats every:', 'name',array('class'=>'wide label-override')); ?>
	<div class='form_field'>
		<div id='day_buttons' class='day_buttons'>
			<?php echo form_checkbox(array('id'=>'sunday','name'=>'sunday','value'=>1)).form_label('S', 'sunday', array()); ?>
			<?php echo form_checkbox(array('id'=>'monday','name'=>'monday','value'=>1)).form_label('M', 'monday', array()); ?>
			<?php echo form_checkbox(array('id'=>'tuesday','name'=>'tuesday','value'=>1)).form_label('T', 'tuesday', array()); ?>
			<?php echo form_checkbox(array('id'=>'wednesday','name'=>'wednesday','value'=>1)).form_label('W', 'wednesday', array()); ?>
			<?php echo form_checkbox(array('id'=>'thursday','name'=>'thursday','value'=>1)).form_label('T', 'thursday', array()); ?>
			<?php echo form_checkbox(array('id'=>'friday','name'=>'friday','value'=>1)).form_label('F', 'friday', array()); ?>
			<?php echo form_checkbox(array('id'=>'saturday','name'=>'saturday','value'=>1)).form_label('S', 'saturday', array()); ?>
		</div>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label('Between:', 'name',array('id'=>'range_text', 'class'=>'wide label-override')); ?>
	<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'start_date',
			'id'=>'start_date',
			'value'=> date('m/d/Y', strtotime($teetime_info->start+1000000)))
			); 
		?>
		-
		<?php echo form_input(array(
			'name'=>'end_date',
			'id'=>'end_date',
			'value'=> date('m/d/Y', strtotime($teetime_info->start+1000000)))
			); 
		?>
	</div>
</div>
<?php
echo form_submit(array(
	 'name'=>'submitButton',
 	'id'=>'submitButton',
	'value'=> lang('common_save'),
	'class'=>'submit_button')
);
?>
</fieldset>
</form>
<script>
	var repeat = {
		toggle_days:function(){
			var repeats = $('input[name=repeats]:checked').val();
			if (repeats == 'daily')
			{
				$('#end_date').attr('disabled', false).show();
				$('#range_text').html('Between:');
				$('.day_options').hide();
			}
			else if (repeats == 'weekly')
			{
				$('#end_date').attr('disabled', false).show();
				$('#range_text').html('Between:');
				$('.day_options').show();
			}
			else if (repeats == 'once')
			{
				$('#end_date').attr('disabled', true).hide().val($('#start_date').val());
				$('#range_text').html('On:');
				$('.day_options').hide();
			}
			$.colorbox.resize();
		}
	};
	$(document).ready(function(){
		$('.repeats_buttons, .day_buttons').buttonset();
    
		$.colorbox.resize();
		$("#start_date, #end_date").datepicker({
			'minDate': new Date(),
			'onSelect':function(dateText, inst){
				var repeats = $('input[name=repeats]:checked').val();
				if (repeats == 'once')
					$('#end_date').val(dateText);
				if ($('#start_date').val() > $('#end_date').val())
					$('#end_date').val($('#start_date').val());
			}
		});
		$('input[name=repeats]').change(function(){
			repeat.toggle_days();
		})
		//$("#new_time").timepicker();
		var submitting = false;
	    $('#repeat_form').validate({
			submitHandler:function(form)
			{
		        if (submitting) return;
					submitting = true;
				$(form).mask("<?php echo lang('common_wait'); ?>");
				//var d = new Date($('#new_date').val());
				//var t = $('#new_time').val();
				//var et = parseInt(t, 10) + parseInt($('#difference').val(), 10);
				//et = parseInt(et, 10)%100>59?parseInt(et,10)+40:et;
				//console.log('<?php echo $teetime_info->end.' - '.$teetime_info->start?>');
				//console.log(parseInt(t, 10)+' + '+parseInt($('#difference').val(), 10));
				//console.log(t+' - '+et+' OR '+(parseInt(et, 10)+40));
				//var new_start = d.getFullYear()+(d.getMonth()<10?'0':'')+d.getMonth()+(d.getDate()<10?'0':'')+d.getDate()+(parseInt(t, 10)<1000?'0':'')+parseInt(t, 10);
				//var new_end = d.getFullYear()+(d.getMonth()<10?'0':'')+d.getMonth()+(d.getDate()<10?'0':'')+d.getDate()+(parseInt(et, 10)<1000?'0':'')+parseInt(et, 10);
				//et = et%100>59?parseInt(et,2)+40:et;
				//$('#start').val(new_start);
				//$('#end').val(new_end);
				//console.log('submitting');
				$(form).ajaxSubmit({
			        success:function(response)
			        {
						Calendar_actions.update_teesheet(response.teetimes);
                    	console.dir(response);
                        $.colorbox.close();
			            submitting = false;
			        },
			        dataType:'json'
			    });
		
			},
			errorLabelContainer: "#error_message_box",
	 		wrapper: "li",
			rules:
			{
			
			},
			messages:
			{
			
			}
		});
	});
</script>
