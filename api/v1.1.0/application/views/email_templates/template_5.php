<html lang="en"><head>
<title>Live Preview High Definition by PremiumStuff</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body bgcolor="#ebecf2" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<!-- The beginning of our E-mail -->

<!-- Let's start with the very top where we add our logo -->
<table width="652" border="0" cellpadding="0" cellspacing="0" align="center">
<tbody><tr>
	<td height="80">
		<a href="#" style="font-family: Helvetica, arial, sans-serif; font-size: 24px; font-weight: bold; font-style: italic; text-decoration: none; color: #000000">
			<img src="http://premiumstuff.net/demo/highdefinition/images/logo.jpg" border="0" width="224" height="59" alt="HighDefiniton"></a>
	</td>
</tr>

<!-- Second, we like to have a Hat on the main content page. -->
</tbody></table><table width="652" border="0" cellpadding="0" cellspacing="0" align="center">
<tbody><tr>
	<td height="8">
		<img src="http://premiumstuff.net/demo/highdefinition/images/top.jpg" width="652" height="8" style="display: block;">
	</td>
</tr>

<!-- Here is where we create our catchy Headline -->
</tbody></table><table width="652" border="0" cellpadding="0" cellspacing="0" style="border-left: 1px solid #c5c6cb; border-right: 1px solid #c5c6cb;" align="center">
<tbody><tr>
	<td height="110" bgcolor="#FFFFFF" align="center" style="font-family: Helvetica, arial, sans-serif; font-size: 22px; color: #000000; font-weight: bold; line-height: 29px;">
	
		<!-- Headline -->
		Our Mac OSX App now 50% off.<br>
		<span style="color: #b6b6b6; font-weight: normal;">
		
		<!-- Second Headline -->
		Rise yourself to an Oreo. Get your app.</span>

	</td>
</tr>
</tbody></table>

<!-- This is where we show our product as a Screen -->
<table width="652" border="0" cellpadding="0" cellspacing="0" align="center" style="border-left: 1px solid #c5c6cb; border-right: 1px solid #c5c6cb;">
	<tbody><tr>
		<td width="51" height="310" bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF" style="text-align: center;">
			
			<!-- Screen Image, remember to change the alt tag in case the images are turned off -->
			<a href="#" style="font-family: Helvetica, arial, sans-serif; font-size: 24px; text-decoration: none; color: #b6b6b6"><img src="http://premiumstuff.net/demo/highdefinition/images/screen.jpg" border="0" width="551" height="310" style="display: block;" alt="Turning on images might be a good idea."></a></td>
			
		<td width="50" height="310" bgcolor="#FFFFFF"></td>
	</tr>
</tbody></table>

<!-- And of course add shadow beneath. Also, here we create a subtile Bottom Border-->
<table width="652" border="0" cellpadding="0" cellspacing="0" align="center" style="border-left: 1px solid #c5c6cb; border-right: 1px solid #c5c6cb; border-bottom: 1px solid #c5c6cb;">
<tbody><tr>
	<td height="46" bgcolor="#FFFFFF">
		
		<!-- Shadow Image -->
		<img src="http://premiumstuff.net/demo/highdefinition/images/shadow_screen.jpg" width="650" height="46" style="display: block;">
	</td>
</tr>
</tbody></table>

<!-- Create some space between the Screen and the upcoming Column -->
<table width="652" border="0" cellpadding="0" cellspacing="0" align="center" style="border-left: 1px solid #c5c6cb; border-right: 1px solid #c5c6cb;">
<tbody><tr>
	<td width="650" height="35" bgcolor="#FFFFFF">
	</td>
</tr>
</tbody></table>

<!-- Column Images, remember to replace the alt tag in case the images are turned off -->
<table width="652" border="0" cellpadding="0" cellspacing="0" align="center" style="border-left: 1px solid #c5c6cb; border-right: 1px solid #c5c6cb;">
	<tbody><tr>
		<td width="49" height="91" bgcolor="#FFFFFF"></td>
		<td width="148" bgcolor="#FFFFFF" style="text-align: center;">
			
			<!-- Image 1 -->
			<a href="#" style="font-family: Helvetica, arial, sans-serif; font-size: 18px; text-decoration: none; color: #b6b6b6"><img src="http://premiumstuff.net/demo/highdefinition/images/col_image_1.jpg" border="0" width="148" height="91" style="display: block;" alt="Pretty Image"></a></td>
		<td bgcolor="#FFFFFF" style="text-align: center; color: #b6b6b6; font-weight: bold;">
		
			<!-- Arrow 1 -->
			<img src="http://premiumstuff.net/demo/highdefinition/images/arrow.jpg" width="53" height="91" style="display: block;" alt="&gt;"></td>
		<td width="148" bgcolor="#FFFFFF" style="text-align: center;">
		
			<!-- Image 2 -->
			<a href="#" style="font-family: Helvetica, arial, sans-serif; font-size: 18px; text-decoration: none; color: #b6b6b6"><img src="http://premiumstuff.net/demo/highdefinition/images/col_image_2.jpg" border="0" width="149" height="91" style="display: block;" alt="Pretty Image"></a></td>
		<td bgcolor="#FFFFFF" style="text-align: center; color: #b6b6b6; font-weight: bold;">
		
			<!-- Arrow 2 -->
			<img src="http://premiumstuff.net/demo/highdefinition/images/arrow.jpg" width="53" height="91" style="display: block;" alt="&gt;"></td>
		<td width="148" bgcolor="#FFFFFF" style="text-align: center;">
		
			<!-- Image 3 -->
			<a href="#" style="font-family: Helvetica, arial, sans-serif; font-size: 18px; text-decoration: none; color: #b6b6b6"><img src="http://premiumstuff.net/demo/highdefinition/images/col_image_3.jpg" border="0" width="148" height="91" style="display: block;" alt="Pretty Image"></a></td>
		<td width="50" height="91" bgcolor="#FFFFFF"></td>
	</tr>
</tbody></table>

<!-- Some Padding between the Images and the upcoming Text -->
<table width="652" border="0" cellpadding="0" cellspacing="0" align="center" style="border-left: 1px solid #c5c6cb; border-right: 1px solid #c5c6cb;">
<tbody><tr>
	<td width="650" height="7" bgcolor="#FFFFFF">
	</td>
</tr>
</tbody></table>

<!-- Column Text -->
<table width="652" border="0" cellpadding="0" cellspacing="0" align="center" style="border-left: 1px solid #c5c6cb; border-right: 1px solid #c5c6cb;">
	<tbody><tr>
		<td width="49" height="109" bgcolor="#FFFFFF"></td>
		<td width="148" height="109" bgcolor="FFFFFF" valign="top" align="center">
			<span style="font-family: Helvetica, arial, sans-serif; font-size: 18px; font-weight: bold; color: #3d3d3d; line-height: 32px;">
			
			<!-- Headline -->
			Interface</span><br>
			
			<span style="font-family: Helvetica, arial, sans-serif; font-size: 14px; font-weight: normal; color: #787878; line-height: 21px;">
			
			<!-- Text -->
			Product X comes with a slick and slim design specially designed to work fast.</span>
		
		</td>
		<td width="53" height="109" bgcolor="FFFFFF"></td>
		<td width="149" height="109" bgcolor="FFFFFF" valign="top" align="center">
		
			<!-- Headline -->
			<span style="font-family: Helvetica, arial, sans-serif; font-size: 18px; font-weight: bold; color: #3d3d3d; line-height: 32px;">
			
			<!-- Headline -->
			Lightweight</span><br>
			
			<span style="font-family: Helvetica, arial, sans-serif; font-size: 14px; font-weight: normal; color: #787878; line-height: 21px;">
			
			<!-- Text -->
			One click install. We worked hard to let the App automate the installing process.</span>
			
		</td>
		<td width="53" height="109" bgcolor="FFFFFF"></td>
		<td width="148" height="109" bgcolor="FFFFFF" valign="top" align="center">
			<!--Headline -->
			<span style="font-family: Helvetica, arial, sans-serif; font-size: 18px; font-weight: bold; color: #3d3d3d; line-height: 32px;">
			
			<!-- Headline -->
			Experiment</span><br>
			
			<span style="font-family: Helvetica, arial, sans-serif; font-size: 14px; font-weight: normal; color: #787878; line-height: 21px;">
			
			<!-- Text -->
			When your ready, experiment with the hundreds of elements we build in.</span>
			
		</td>
		<td width="50" height="109" bgcolor="FFFFFF"></td>
	</tr>
</tbody></table>

<!-- Padding and subtile Bottom Border-->
<table width="652" border="0" cellpadding="0" cellspacing="0" align="center" style="border-left: 1px solid #c5c6cb; border-right: 1px solid #c5c6cb; border-bottom: 1px solid #c5c6cb;">
<tbody><tr>
	<td width="650" height="35" bgcolor="#FFFFFF">
	</td>
</tr>

<!-- Padding -->
</tbody></table><table width="652" border="0" cellpadding="0" cellspacing="0" align="center" style="border-left: 1px solid #c5c6cb; border-right: 1px solid #c5c6cb;">
<tbody><tr>
	<td width="650" height="35" bgcolor="#FFFFFF" style="display: block;">
	</td>
</tr>
</tbody></table>

<!-- Price table Top -->
<table width="650" border="0" cellpadding="0" cellspacing="0" align="center" style="border-left: 1px solid #c5c6cb; border-right: 1px solid #c5c6cb;">
<tbody><tr>
	<td height="6" bgcolor="#FFFFFF">
		<img src="http://premiumstuff.net/demo/highdefinition/images/price_top.jpg" width="650" height="6" style="display: block;">
	</td>
</tr>
</tbody></table>

<!-- Price Table Content-->
<table width="652" border="0" cellpadding="0" cellspacing="0" align="center" style="border-left: 1px solid #c5c6cb; border-right: 1px solid #c5c6cb;">
	<tbody><tr>
		<td width="49" height="60" bgcolor="#FFFFFF"></td>
		<td width="124" height="60" bgcolor="#f4f3f3" align="center" style="font-family: Helvetica, arial, sans-serif; font-size: 33px; color: #8a8a8a;">
		
			<!-- Price Tag -->
			$8,99</td>
		<td bgcolor="#FFFFFF">
			<img src="http://premiumstuff.net/demo/highdefinition/images/divider.jpg" width="30" height="60" style="display: block;"></td>
		<td width="189" height="60" bgcolor="#f4f3f3" align="center" style="font-family: Helvetica, arial, sans-serif; font-size: 24px; font-weight: bold; color: #272727;">
		
			<!-- Product Name -->
			App Name</td>
		<td bgcolor="#FFFFFF" style="text-align: center;">
		
			<!-- Buy Button -->
			<a href="#" style="color: #b6b6b6; text-decoration: none; font-weight: bold; font-family: Helvetica, arial, sans-serif;"><img src="http://premiumstuff.net/demo/highdefinition/images/download.jpg" width="209" height="60" border="0" style="display: block;" alt="BUY PRODUCT"></a></td>
		<td width="49" height="60" bgcolor="#FFFFFF"></td>
	</tr>
</tbody></table>

<!-- Price Table Bottom -->
<table width="650" border="0" cellpadding="0" cellspacing="0" align="center" style="border-left: 1px solid #c5c6cb; border-right: 1px solid #c5c6cb;">
<tbody><tr>
	<td height="6" bgcolor="#FFFFFF">
		<img src="http://premiumstuff.net/demo/highdefinition/images/price_bottom.jpg" width="650" height="6" style="display: block;">
	</td>
</tr>

<!-- Shadow Price Table -->
</tbody></table><table width="650" border="0" cellpadding="0" cellspacing="0" align="center" style="border-left: 1px solid #c5c6cb; border-right: 1px solid #c5c6cb;">
<tbody><tr>
	<td height="6" bgcolor="#FFFFFF">
		<img src="http://premiumstuff.net/demo/highdefinition/images/shadow_pricetable.jpg" width="650" height="15" style="display: block;">
	</td>
</tr>
</tbody></table>

<!-- Padding -->
<table width="652" border="0" cellpadding="0" cellspacing="0" align="center" style="border-left: 1px solid #c5c6cb; border-right: 1px solid #c5c6cb;">
<tbody><tr>
	<td width="650" height="15" bgcolor="#FFFFFF">
	</td>
</tr>
</tbody></table>

<!-- Bottom Main-->
<table width="652" border="0" cellpadding="0" cellspacing="0" align="center">
<tbody><tr>
	<td height="7">
		<img src="http://premiumstuff.net/demo/highdefinition/images/bottom.jpg" width="652" height="7" style="display: block;">
	</td>
</tr>
</tbody></table>

<!-- Padding -->
<table width="652" border="0" cellpadding="0" cellspacing="0" align="center">
<tbody><tr>
	<td width="650" height="10" bgcolor="#ebecf2">
	</td>
</tr>
</tbody></table>

<!-- Footer where we put our information and logo -->
<table width="652" border="0" cellpadding="0" cellspacing="0" align="center">
<tbody><tr>
	<td width="650" bgcolor="#ebecf2" align="center">
	
		<!-- logo -->
		<a href="#" style="font-family: Helvetica, arial, sans-serif; font-size: 24px; font-weight: bold; font-style: italic; text-decoration: none; color: #000000"><img src="http://premiumstuff.net/demo/highdefinition/images/logo.jpg" border="0" alt="High Definition"></a><br>
		
		<span style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #585858; font-style: italic; line-height: 16px;">
		
		<!-- Text -->
		This is a one-time email sent to <a href="mailto:support@premiumstuff.net" style="text-decoration: none; color: #1a1a1a; font-weight: bold;">support@premiumstuff.net</a>. If you wish to receive more emails by Company Name, please take a few seconds to <a href="#" style="text-decoration: none; color: #1a1a1a; font-weight: bold;">Subscribe</a></span>
	
	</td>
</tr>
</tbody></table>

<!-- Padding tp space up the footer with the very bottom of the window -->
<table width="652" border="0" cellpadding="0" cellspacing="0" align="center">
<tbody><tr>
	<td width="650" height="20" bgcolor="#ebecf2">
	</td>
</tr>
</tbody></table>

<!-- That's it. In case you need help, don't hesitate to e-mail us.

support@premiumstuff.net

-->


</body></html>