<?php
echo form_open("sales/delete_payment/".urlencode(urlencode($payment_id)),array('id'=>'edit_payment_form'.$payment_id));
//print_r($payment);
?>
<table>
	<tbody>
		<tr>
			<td colspan=2><div style='padding:10px;'>You are about to return a payment to card: </div></td>
		</tr>
		<tr>
			<!--td id="pt_delete"><?php echo anchor("sales/delete_payment/".urlencode(urlencode($payment_id)),'['.lang('common_delete').']');?></td-->
			<td id="pt_type"><?php echo  $payment['payment_type']    ?> </td>
			<td id="pt_amount"><?php echo  to_currency($payment['payment_amount'])  ?>  </td>
		</tr>
	</tbody>
</table>
<div class='clear' style='text-align:center; margin-top:30px;'>
<?php
echo anchor("sales/delete_payment/".$payment_id,'Issue Return',array('name'=>'submit','id'=>'submit','class'=>'submit_button float_right'));
/*echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>'Issue Return',
	'class'=>'submit_button float_right')
);*/
?>
</div>
</form>
<!--script>
	$(document).ready(function(){
		$('#edit_payment_form'.$payment_id).validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
				success:function(response)
				{
					$.colorbox.close();
					//post_person_form_submit(response);
	                submitting = false;
				},
				dataType:'json'
			});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
		},
		messages: 
		{
     	}
	});
	})
</script-->