<div id="popup_box">
<?php
echo form_open('items/add_menu/',array('id'=>'menu_form'));
?>
<ul id="error_message_box"></ul>
<fieldset id="customer_menu">
<legend><?php echo 'Create A New Menu'; ?></legend>

<div class="field_row clearfix">	
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'title',
		'id'=>'title',
		'size'=>20,
		'placeholder'=>'Name of Menu',
		'value'=>'')
	);?>
            <?php echo form_input(array(
		'name'=>'start_time',
		'id'=>'start_time',
		'size'=>30,
		'placeholder'=>'Start Time',
		'value'=>'')
	);?>
            <?php echo form_input(array(
		'name'=>'end_time',
		'id'=>'end_time',
		'size'=>30,
		'placeholder'=>'End Time',
		'value'=>'')
	);?>
	<span class='' id='add_group_button'><?php echo 'Create A Menu';?></span>
	</div>
</div>
<div id='menu_container'>
	<div class="menu_title">	

</div>
<div class='menu_subtitle'>

</div>
	
<?php 
$first_label = true;
foreach($menus as $menu)
{
    
?>
	<div class='form_field'  id='row_<?php echo $menu['menu_id']; ?>'>
		<?	echo '<span id="menu_'.$menu['menu_id'].'"><span  class="group_label">'.$menu['title'].'</span></span>'
			
		//' <span style="color:#ccc;">('.$group['member_count'].' members)</span>';
		?>
		<div class='clear'></div>
	</div>
<? 
$first_label = false;
} ?>
<div id='clear' class='clear'></div>
</div>
</fieldset>
<?php 
echo form_close();
?>
</div>
<script type='text/javascript'>
var menu = {

	add_menu:function() {
		var title = $('#title').val();
                var start_time = $('#start_time').val();
                var end_time = $('#end_time').val();
                
		title = addslashes(title.replace('"', ''));
		if (title != '')
		{
			$.ajax({
	            type: "POST",
	            url: "index.php/items/add_menu/",
	            data: {title:title, start_time:start_time, end_time:end_time},
	            success: function(response){
	            	console.log(response.menu_id);
	           		var html = '<div class="form_field" id="row_'+response.menu_id+'">'+	
							'<span id="gid_'+response.menu_id+'"><span  class="group_label">'+title+'</span></span>';
						
					$('#menu_container #clear').before($(html));
					menu.add_menu_click_event();
	    			$.colorbox.resize();
	            },
	            dataType:'json'
	        });
	    }
	},
	save_group_name:function(group_id) {
		var title = ($('#group_text_'+group_id).val().replace('"', ''));
		$.ajax({
            type: "POST",
            url: "index.php/items/save_menu_name/"+group_id,
            data: "group_label="+title,
            success: function(response){
            	var el = $('#gid_'+group_id);
            	console.log(el.html());
				el.html('<span  class="group_label">'+title+'</span>');
				menu.add_menu_click_event();
            },
            dataType:'json'
        });
	},
        load_menu:function(menu_id) {
            $.ajax({
                type: "POST",
                url: "index.php/items/",
                data: {"menu_id":menu_id},
                success: function(response){
                    
                },
                dataType:'json'
            });
        },
	add_menu_click_event:function() {
		//console.log('adding group event');
		$('.group_label').click(function(e){               
			var el = $(e.target).parent();
			//var text = $(e.target).html();
			var id = el.attr('id');
			var menu_id = id.replace('menu_','');
                        console.log('menu id: ' + menu_id);
			//console.log('id '+id);
                        //menu.load_menu(menu_id);
			//el.html('<span class="delete_item" onclick="menu.delete_group(\''+menu+'\', \''+addslashes(text.replace('"', ''))+'\')">Delete</span><span class="save_item" onclick="menu.save_group_name(\'\')">Save</span><input id="group_text_'+menu+'" value="'+text+'"/>');
                        
                        $.colorbox({href:'index.php/items/manage_menu_items/'+menu_id });
		});
		//console.log('added');
	},
	delete_group:function(menu_id, title) {
		var answer = confirm('Are you sure you want to delete group: '+title+'?');
		if (answer) {
			$.ajax({
	            type: "POST",
	            url: "index.php/items/delete_menu/"+menu_id,
	            data: "",
	            success: function(response){
					$('#row_'+menu_id).remove();
					$.colorbox.resize();
	            },
	            dataType:'json'
	        });
	    }
	}
};

//validation and submit handling
$(document).ready(function()
{
	var submitting = false;
	$('#add_group_button').click(function(){
		menu.add_menu();
	});
	$('#title').keypress(function(e){
		if(e.which == 13) {
			e.preventDefault();
			menu.add_menu();
		}
	});
    $('#menu_form').validate({
		submitHandler:function(form)
		{
			$.colorbox.close();
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li"
	});
	menu.add_menu_click_event();
});
</script>
