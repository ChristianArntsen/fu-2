var ItemModifiersView = Backbone.View.extend({
	tagName: "div",
	className: "modifiers",
	template: _.template( $('#template_item_modifiers').html()),

	initialize: function(attributes, options){
		this.options = {};
		if(attributes.category_id){
			this.options.category_id = attributes.category_id;
		}
	},

	events: {
		"click a.option_button": "changeModifierOption"
	},

	render: function() {
		data = {};
		data.modifiers = this.model.get('modifiers');
		data.category_id = this.options.category_id;

		this.$el.html(this.template(data));
		return this;
	},

	changeModifierOption: function(event){
		var btn = $(event.currentTarget);
		var selectedOption = btn.data('value');
		var selectedPrice = btn.data('price');
		var modifierId = btn.data('modifier-id');

		btn.addClass('selected');
		btn.siblings().removeClass('selected');
		btn.parents('span').siblings('span.price').find('span.value').text(selectedPrice);

		if(selectedOption == 'no'){
			selectedPrice = 0.00;
		}

		this.model.get('modifiers').get(modifierId).set({'selected_option': selectedOption, 'selected_price': selectedPrice});
		return false;
	}
});