var CartListItemView = Backbone.View.extend({
	tagName: "tr",
	className: "reg-item-top",
	template: _.template( $('#template_cart_item').html() ),

	events: {
		"click a.edit_item": "openEditWindow",
		"click": "selectItem"
	},

	initialize: function() {
		this.listenTo(this.model, "change", this.render);
		this.listenTo(this.model.get('modifiers'), "change", this.render);
		this.listenTo(this.model.get('sides'), "add remove change", this.render);
		this.listenTo(this.model.get('soups'), "add remove change", this.render);
		this.listenTo(this.model.get('salads'), "add remove change", this.render);
	},

	render: function() {
		this.$el.html(this.template(this.model.attributes));
		if(this.model.get('selected')){
			this.$el.addClass('selected');
		}else{
			this.$el.removeClass('selected');
		}

		if(this.model.get('is_ordered')){
			this.$el.addClass('ordered');
		}else{
			this.$el.removeClass('ordered');
		}

		if(this.model.get('is_paid')){
			this.$el.addClass('paid');
		}else{
			this.$el.removeClass('paid');
		}
		return this;
	},

	openEditWindow: function(){
		var item = this.model;
		var editItemWindow = new EditItemView({model:item});

		$.colorbox({
			html: editItemWindow.render().el,
			width: 960
		});
		$('#cboxLoadedContent').css({'margin-top':'0px'});
		$('#cboxTitle').css({'display':'none'});
		return false;
	},

	selectItem: function(event){
		if(this.model.get('selected')){
			this.model.set({"selected":false});
			this.$el.removeClass('selected');
		}else{
			this.model.set({"selected":true});
			this.$el.addClass('selected');
		}
		return false
	},

	close: function() {
		this.$el.unbind();
		this.$el.empty();
	}
});
