<script type="text/html" id="template_cart_item">
	<td class="reg_item_seat"><%=seat%></td>
	<td class="reg_item_name">
		<div>
			<%=name%>
			<% if(is_paid == 1){ %>
			<span style="float: right;">PAID</span>
			<% } %>
		</div>
		<% if(modifiers.length > 0){ %>
		<ul class="modifiers">
		<% _.each(modifiers.models, function(modifier){ %>
			<li>
				<span><%-modifier.get('name') %>: <%-_.capitalize(modifier.get('selected_option')) %></span>
				<span style="float: right;"><%-accounting.formatMoney(modifier.get('selected_price')) %></span>
			</li>
		<% }); %>
		</ul>
		<% } %>

		<!-- SOUPS -->
		<% if(soups.length > 0){ %>
		<strong style="font-size: 12px; display: block; line-height: 12px; margin-top: 5px;">Soups</strong>
		<ul class="modifiers">
		<% _.each(soups.models, function(side){ %>
			<li>
				<span><%-side.get('name') %></span>
				<span style="float: right;"><%-accounting.formatMoney(side.get('price')) %></span>
			</li>
		<% }); %>
		</ul>
		<% } %>

		<!-- SALADS -->
		<% if(salads.length > 0){ %>
		<strong style="font-size: 12px; display: block; line-height: 12px; margin-top: 5px;">Salads</strong>
		<ul class="modifiers">
		<% _.each(salads.models, function(side){ %>
			<li>
				<span><%-side.get('name') %></span>
				<span style="float: right;"><%-accounting.formatMoney(side.get('price')) %></span>
			</li>
		<% }); %>
		</ul>
		<% } %>

		<!-- GENERAL SIDES -->
		<% if(sides.length > 0){ %>
		<strong style="font-size: 12px; display: block; line-height: 12px; margin-top: 5px;">Sides</strong>
		<ul class="modifiers">
		<% _.each(sides.models, function(side){ %>
			<li>
				<span><%-side.get('name') %></span>
				<span style="float: right;"><%-accounting.formatMoney(side.get('price')) %></span>
			</li>
		<% }); %>
		</ul>
		<% } %>
	</td>
	<td class="reg_item_price editable">
		<div><%=accounting.formatMoney(price)%></div>
	</td>
	<td class="reg_item_discount editable">
		<div><%=accounting.toFixed(discount, 0)%></div>
	</td>
	<td class="reg_item_total">
		<a class="edit_item fnb_button">Edit</a>
	</td>
</script>
