<script type="text/html" id="template_item_sides">
<%
console.debug(number_sides); //JBDEBUG
if(sides.length > number_sides){
	number_sides = sides.length;
}

for(var position = 1; position < number_sides + 1; position++){

var extraText = '';
if(position > number_sides){
	extraText = '(Extra)';
} %>
<div class="side" style="float: none; width: auto;">
	<h1>Side #<%-position %> <%-extraText%></h1>
	<ul style="overflow: hidden; display: block;">
	<% _.each(App.sides.models, function(side){
	if(side.get('category') == 'Soups' || side.get('category') == 'Salads'){ return true; }

	var selected = '';
	if(sides.length > 0 && sides.get(position) && sides.get(position).get('item_id') == side.get('item_id')){
		selected = ' selected';
	}
	%>
		<li>
			<a class="fnb_button side <%-selected%>" href="#" data-type="side" data-position="<%-position%>" data-side-id="<%-side.get('item_id')%>">
				<%-_.capitalize(side.get('name'))%>
			</a>
		</li>
	<% }); %>
	</ul>
</div>
<% } %>
</script>