var Receipt = Backbone.Model.extend({
	idAttribute: "receipt_id",
	defaults: {
		"receipt_id": null,
		"items": [],
		"date_paid": null,
		"status": "pending"
	},

	set: function(attributes, options){
		if(!attributes.receipt_id && !this.get('receipt_id')){
			attributes.receipt_id = App.receipts.getNextNumber();
			attributes.id = attributes.receipt_id;
		}
		Backbone.Model.prototype.set.call(this, attributes, options);
	},

	initialize: function(){
		var receipt_id = this.get('receipt_id');

		// Initialize item collection
		var itemCollection = new ReceiptCart(this.get('items'));
		itemCollection.url = App.api_table + 'receipts/' + receipt_id + '/items/';

		// Initialize payment collection
		var paymentCollection = new PaymentCollection(this.get('payments'));
		paymentCollection.url = App.api_table + 'receipts/' + receipt_id + '/payments/';

		this.set({
			"items": itemCollection,
			"payments": paymentCollection
		});

		this.listenTo(this.get('payments'), 'add remove reset', this.markPaid, this);
	},

	getTotal: function(){
		var total = 0.00;
		var splitItems = this.collection.getSplitItems();
		var items = this.get('items').models;

		_.each(items, function(item){
			var line = item.get('line');
			var divisor = splitItems[line];

			var splitTotal = item.getSplitTotal(divisor);
			total += splitTotal;
		});

		return parseFloat(accounting.toFixed(total, 2));
	},

	getTotalDue: function(){
		var totalDue = 0.00;
		var total = this.getTotal();
		var totalPaid = this.get('payments').getTotal();

		totalDue = total - totalPaid;
		return parseFloat(accounting.toFixed(totalDue, 2));
	},

	isPaid: function(){
		if(this.getTotalDue() > 0){
			return false;
		}
		return true;
	},

	markPaid: function(){

		var isPaid = 1;
		if(this.get('payments').getTotal() == 0){
			var isPaid = 0;
		}

		// Mark each item in cart as paid or unpaid
		_.each(this.get('items').models, function(item){
			var line = item.get('line');
			App.cart.get(line).set({'is_paid': isPaid});
		});

		// Mark receipt as paid (if entire receipt has been paid)
		if(this.isPaid()){
			this.set({'status':'complete'});
		}
	}
});