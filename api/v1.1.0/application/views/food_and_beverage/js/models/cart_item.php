var CartItem = Backbone.Model.extend({
	idAttribute: "line",
	defaults: {
		"line": null,
		"name": "",
		"seat": 1,
		"item_id": "",
		"category": "",
		"sub_category": "",
		"department": "",
		"description": "",
		"discount":0,
		"price": 0.00,
		"subtotal": 0.00,
		"total": 0.00,
		"tax": 0.00,
		"quantity": 1,
		"is_ordered": 0,
		"is_paid": 0,
		"splits": 0,
		"paid_splits": 0,
		"modifiers": [],
		"sides": [],
		"soups": [],
		"salads": [],
		"number_sides": 0,
		"number_soups": 0,
		"number_salads": 0
	},

	initialize: function(){
		var line = this.get('line');
		var modifierCollection = new ModifierCollection(this.get('modifiers'));
		var sideCollection = new ItemSideCollection(this.get('sides'));
		var soupCollection = new ItemSideCollection(this.get('soups'));
		var saladCollection = new ItemSideCollection(this.get('salads'));

		// Keep all sides sorted by position
		sideCollection.comparator = 'position';
		soupCollection.comparator = 'position';
		saladCollection.comparator = 'position';

		// Create url to save sides to database
		sideCollection.url = App.api_table + "cart/" + this.get('line') + "/sides";
		soupCollection.url =  App.api_table + "cart/" + this.get('line') + "/soups";
		saladCollection.url =  App.api_table + "cart/" + this.get('line') + "/salads";

		this.set({
			modifiers: modifierCollection,
			sides: sideCollection,
			soups: soupCollection,
			salads: saladCollection
		});

		this.calculatePrice();

		// If any changes made to item, re-calcuate price (total, tax, etc)
		this.listenTo(this, "change", this.calculatePrice);
		this.listenTo(this.get('modifiers'), "change", this.calculatePrice);
		this.listenTo(this.get('sides'), "add remove change", this.calculatePrice);
		this.listenTo(this.get('soups'), "add remove change", this.calculatePrice);
		this.listenTo(this.get('salads'), "add remove change", this.calculatePrice);
	},

	set: function(attributes, options){
		if(!this.attributes.line){
			this.attributes.line = App.cart.getNextLine();
			this.id = this.attributes.line;
		}
		Backbone.Model.prototype.set.call(this, attributes, options);
	},

	calculatePrice: function(){
		var cartItem = this;

		// Loop through all different sides and get their totals
		var sideTypes = ['sides', 'soups', 'salads'];
		var sideTotal = 0;
		var sideTotalTax = 0;
		_.each(sideTypes, function(sideType){
			if(cartItem.get(sideType) && cartItem.get(sideType).length > 0){
				sideTotal += parseFloat(cartItem.get( sideType ).getTotalPrice());
				sideTotalTax += parseFloat(cartItem.get( sideType ).getTotalTax());
			}
		});

		var modifierTotal = parseFloat(this.get('modifiers').getTotalPrice());
		var price = parseFloat(this.get('price'));

		var subtotal = this.getSubtotal(price + modifierTotal, this.get('discount'));
		var tax = this.getTax(subtotal) + sideTotalTax;

		subtotal += sideTotal;
		var total = this.getTotal(subtotal, tax);

		this.set({"subtotal": subtotal, "tax": tax, "total": total});
	},

	getSplitTotal: function(divisor){
		var total = this.get('total');
		if(!divisor || divisor == 1){
			return parseFloat(accounting.toFixed(total, 2));
		}
		return parseFloat(accounting.toFixed(total / divisor, 2));
	},

	getSubtotal: function(price, discount){
		var price = parseFloat(price);
		var discount = parseFloat(discount);
		var qty = 1;
		var subtotal = (price * (100 - discount) / 100) * qty;
		return parseFloat(accounting.toFixed(subtotal,2));
	},

	getTax: function(subtotal){
		var totalTax = 0.00;
		var subtotal = parseFloat(subtotal);

		if(this.get('taxes')){
			_.each(this.get('taxes'), function(tax){
				var percentage = parseFloat(tax.percent);
				var taxAmount = parseFloat((percentage / 100) * subtotal);
				totalTax += parseFloat(accounting.toFixed(taxAmount, 2));
			});
		}

		return parseFloat(accounting.toFixed(totalTax, 2));
	},

	getTotal: function(subtotal, total){
		return parseFloat(subtotal + total);
	},

	isSoupsComplete: function(){
		if(this.get('soups').length < this.get('number_soups')){
			return false;
		}
		return true;
	},

	isSidesComplete: function(){
		if(this.get('sides').length < this.get('number_sides')){
			return false;
		}
		return true;
	},

	isSaladsComplete: function(){
		if(this.get('salads').length < this.get('number_salads')){
			return false;
		}
		return true;
	},

	isModifiersComplete: function(categoryId){
		if(!this.get('modifiers').isComplete(categoryId)){
			return false;
		}
		return true;
	},

	// Checks if all required fields, sides, and modifiers are set
	isComplete: function(){

		if(!this.isSoupsComplete()){ return false }
		if(!this.isSidesComplete()){ return false }
		if(!this.isSaladsComplete()){ return false }
		if(!this.isModifiersComplete()){ return false }

		return true;
	}
});
