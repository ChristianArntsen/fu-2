var ReceiptItem = CartItem.extend({
	initialize: function(){
		var line = this.get('line');
		var cartItem = App.cart.where({"line":line});

		var modifierCollection = new ModifierCollection(this.get('modifiers'));
		var sideCollection = new ItemSideCollection(this.get('sides'));
		var soupCollection = new ItemSideCollection(this.get('soups'));
		var saladCollection = new ItemSideCollection(this.get('salads'));

		// Keep all sides sorted by position
		sideCollection.comparator = 'position';
		soupCollection.comparator = 'position';
		saladCollection.comparator = 'position';

		// Create url to save sides to database
		sideCollection.url = App.api_table + "cart/" + this.get('line') + "/sides";
		soupCollection.url =  App.api_table + "cart/" + this.get('line') + "/soups";
		saladCollection.url =  App.api_table + "cart/" + this.get('line') + "/salads";

		this.set({
			modifiers: modifierCollection,
			sides: sideCollection,
			soups: soupCollection,
			salads: saladCollection
		});

		this.calculatePrice();

		// If cart item changes, automatically update receipt item
		if(cartItem[0]){
			this.listenTo(cartItem[0], "change", this.update, this);
			this.listenTo(cartItem[0], "remove", this.delete, this);
		}
	},

	update: function(data){
		console.debug('updating receipt item...'); //JBDEBUG
		console.debug(data); //JBDEBUG
		this.set(data.attributes);
	},

	delete: function(data){
		this.collection.remove(this);
	}
});