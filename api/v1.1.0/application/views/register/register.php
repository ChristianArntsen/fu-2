<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/register_page.css?<?php echo APPLICATION_VERSION; ?>" />
<title>ForeUP <?php echo lang('login_login'); ?></title>
<script src="<?php echo base_url();?>js/jquery-1.3.2.min.js?<?php echo APPLICATION_VERSION; ?>" type="text/javascript" language="javascript" charset="UTF-8"></script>
<script type="text/javascript">
$(document).ready(function()
{
	$("#register_form input:first").focus();
});
</script>
</head>
<body>
	<div id="welcome_message" class="top_message">
		<?php echo lang('register_welcome_message'); ?>
	</div>
	<?php if (validation_errors()) {?>
		<div id="welcome_message" class="top_message_error">
			<?php echo validation_errors(); ?>
		</div>
	<?php } ?>
<?php echo form_open('register') ?>
<div id="container">
	<div id="top">
		<?php echo img(array('src' => $this->Appconfig->get_foreup_image()));?>
	</div>
	<table id="login_form">
	
		<tr id="form_field_first_name">	
			<td class="form_field_label required"><?php echo lang('common_first_name'); ?>: </td>
			<td class="form_field">
			<?php echo form_input(array(
			'name'=>'first_name', 
			'value'=>'',
			'size'=>'20')); ?>
			</td>
		</tr>
	
		<tr id="form_field_last_name">	
			<td class="form_field_label required"><?php echo lang('common_last_name'); ?>: </td>
			<td class="form_field">
			<?php echo form_input(array(
			'name'=>'last_name', 
			'value'=>'',
			'size'=>'20')); ?>
			</td>
		</tr>
	
		<tr id="form_field_email">	
			<td class="form_field_label"><?php echo lang('common_email'); ?>: </td>
			<td class="form_field">
			<?php echo form_input(array(
			'name'=>'email', 
			'value'=>'',
			'size'=>'20')); ?>
			</td>
		</tr>
	
		<tr id="form_field_phone_number">	
			<td class="form_field_label"><?php echo lang('common_phone_number'); ?>: </td>
			<td class="form_field">
			<?php echo form_input(array(
			'name'=>'phone_number', 
			'value'=>'',
			'size'=>'20')); ?>
			</td>
		</tr>
	
		<tr id="form_field_username">	
			<td class="form_field_label required"><?php echo lang('login_username'); ?>: </td>
			<td class="form_field">
			<?php echo form_input(array(
			'name'=>'username', 
			'value'=>'',
			'size'=>'20')); ?>
			</td>
		</tr>
	
		<tr id="form_field_password">	
			<td class="form_field_label required"><?php echo lang('login_password'); ?>: </td>
			<td class="form_field">
			<?php echo form_password(array(
			'name'=>'password', 
			'value'=>'',
			'size'=>'20')); ?>
			</td>
		</tr>
		
		<tr id="form_field_password_confirm">	
			<td class="form_field_label required"><?php echo lang('login_password_confirm'); ?>: </td>
			<td class="form_field">
			<?php echo form_password(array(
			'name'=>'password_confirm', 
			'value'=>'',
			'size'=>'20')); ?>
			</td>
		</tr>
		
		<tr id="form_field_submit">	
			<td id="submit_button" colspan="2">
				<?php echo form_submit('login_button',lang('login_register')); ?>
			</td>
		</tr>
	</table>
	<table id="bottom">
		<tr>
			<td id="left">
				<?php echo anchor('login', lang('login_login')); ?> 
			</td>
			<td id="right">
				<?php echo date("Y")?> <?php echo lang('login_version'); ?> <?php echo APPLICATION_VERSION; ?>
			</td>
		</tr>
	</table>
</div>
<?php echo form_close(); ?>
</body>
</html>