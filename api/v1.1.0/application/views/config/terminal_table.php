<table class='form_field' id='terminal_list'>
	<tr>
		<td></td>
		<td></td>
		<td>Terminal Name</td>
		<td>QB Tab</td>
		<?php if ($this->config->item('track_cash')) { ?>
		<td>Track Cash</td>
		<?php } ?>
		<?php if ($this->config->item('webprnt')) { ?>
		<td>WebPRNT IP</td>
		<td>WebPRNT Kitchen IP</td>
		<? } ?>
		
	</tr>
	<?php foreach($terminals as $terminal) { ?>
		<?php $this->load->view('config/terminal_row', array('terminal'=>$terminal));?>
	<?php } ?>
</table>
