<?php
	$times_array = array(
                        '2399'=>'N/A',
                        '0000'=>'12:00am',
                        '0030'=>'12:30am',
                        '0100'=>'1:00am',
                        '0130'=>'1:30am',
                        '0200'=>'2:00am',
                        '0230'=>'2:30am',
                        '0300'=>'3:00am',
                        '0330'=>'3:30am',
                        '0400'=>'4:00am',
                        '0430'=>'4:30am',
                        '0500'=>'5:00am',
                        '0530'=>'5:30am',
                        '0600'=>'6:00am',
                        '0630'=>'6:30am',
                        '0700'=>'7:00am',
                        '0730'=>'7:30am',
                        '0800'=>'8:00am',
                        '0830'=>'8:30am',
                        '0900'=>'9:00am',
                        '0930'=>'9:30am',
                        '1000'=>'10:00am',
                        '1030'=>'10:30am',
                        '1100'=>'11:00am',
						'1130'=>'11:30am',
						'1200'=>'12:00pm',
                        '1230'=>'12:30pm',
                        '1300'=>'1:00pm',
                        '1330'=>'1:30pm',
                        '1400'=>'2:00pm',
                        '1430'=>'2:30pm',
                        '1500'=>'3:00pm',
                        '1530'=>'3:30pm',
                        '1600'=>'4:00pm',
                        '1630'=>'4:30pm',
                        '1700'=>'5:00pm',
                        '1730'=>'5:30pm',
                        '1800'=>'6:00pm',
                        '1830'=>'6:30pm',
                        '1900'=>'7:00pm',
                        '1930'=>'7:30pm',
                        '2000'=>'8:00pm',
                        '2030'=>'8:30pm',
                        '2100'=>'9:00pm',
                        '2130'=>'9:30pm',
                        '2200'=>'10:00pm',
                        '2230'=>'10:30pm',
						'2300'=>'11:00pm',
						'2330'=>'11:30pm',
						'2400'=>'12:00am')
?>
<table id="days_options">
            <tbody>
                <tr class="field_row clearfix">
                    <td class="first_cell"></td>
                    <td class="day_cell">Sun</td>
                    <td class="day_cell">Mon</td>
                    <td class="day_cell">Tue</td>
                    <td class="day_cell">Wed</td>
                    <td class="day_cell">Thu</td>
                    <td class="day_cell">Fri</td>
                    <td class="day_cell">Sat</td>
                </tr>
                <tr class="field_row clearfix">
                    <td class="first_cell"><?php echo form_label(lang('config_days_open').':', 'days_open',array('class'=>'wide')); ?></td>
                    <td class="day_cell"><?php echo form_checkbox('open_sun', '1', $this->config->item('open_sun') ? true : false);  ?></td>
                    <td class="day_cell"><?php echo form_checkbox('open_mon', '1', $this->config->item('open_mon') ? true : false);  ?></td>
                    <td class="day_cell"><?php echo form_checkbox('open_tue', '1', $this->config->item('open_tue') ? true : false);  ?></td>
                    <td class="day_cell"><?php echo form_checkbox('open_wed', '1', $this->config->item('open_wed') ? true : false);  ?></td>
                    <td class="day_cell"><?php echo form_checkbox('open_thu', '1', $this->config->item('open_thu') ? true : false);  ?></td>
                    <td class="day_cell"><?php echo form_checkbox('open_fri', '1', $this->config->item('open_fri') ? true : false);  ?></td>
                    <td class="day_cell"><?php echo form_checkbox('open_sat', '1', $this->config->item('open_sat') ? true : false);  ?></td>
                </tr>
                <tr class="field_row clearfix">
                    <td class="first_cell"><?php echo form_label(lang('config_weekend_days').':', 'weekend_days',array('class'=>'wide')); ?></td>
                    <td class="day_cell"><?php echo form_checkbox('weekend_sun', '1', $this->config->item('weekend_sun') ? true : false);  ?></td>
                    <td class="day_cell"><?php echo form_checkbox(array('name'=>'weekend_mon', 'value'=>'1', 'checked'=>false, 'disabled'=>true));  ?></td>
                    <td class="day_cell"><?php echo form_checkbox(array('name'=>'weekend_tue', 'value'=>'1', 'checked'=>false, 'disabled'=>true));  ?></td>
                    <td class="day_cell"><?php echo form_checkbox(array('name'=>'weekend_wed', 'value'=>'1', 'checked'=>false, 'disabled'=>true));  ?></td>
                    <td class="day_cell"><?php echo form_checkbox(array('name'=>'weekend_thu', 'value'=>'1', 'checked'=>false, 'disabled'=>true));  ?></td>
                    <td class="day_cell"><?php echo form_checkbox('weekend_fri', '1', $this->config->item('weekend_fri') ? true : false);  ?></td>
                    <td class="day_cell"><?php echo form_checkbox('weekend_sat', '1', $this->config->item('weekend_sat') ? true : false);  ?></td>
                </tr>
            </tbody>
        </table>
       <div class="field_row clearfix">	
        <?php echo form_label(lang('config_open_time').':', 'open_time',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_dropdown('open_time', $times_array,
                        $this->config->item('open_time'));
                ?>
                    <span class="settings_note">
                        (First tee time of the day)
                    </span>
                </div>
        </div>
        <div class="field_row clearfix">	
        <?php echo form_label(lang('config_close_time').':', 'close_time',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_dropdown('close_time', $times_array,
                        $this->config->item('close_time'));
                ?>
                    <span class="settings_note">
                        (Last tee time of the day)
                    </span>
                </div>
        </div>
        <div class="field_row clearfix">	
        <?php echo form_label(lang('config_early_bird_hours').':', 'early_bird_hours',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_dropdown('early_bird_hours_begin', $times_array,
                        $this->config->item('early_bird_hours_begin'));
                ?>
                <?php echo form_dropdown('early_bird_hours_end', $times_array,
                        $this->config->item('early_bird_hours_end'));
                ?>
                    <span class="settings_note">
                        (Times for early bird hours)
                    </span>
                </div>
        </div>        
        <div class="field_row clearfix">	
        <?php echo form_label(lang('config_morning_hours').':', 'morning_hours_begin',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_dropdown('morning_hours_begin', $times_array,
                        $this->config->item('morning_hours_begin'));
                ?>
                <?php echo form_dropdown('morning_hours_end', $times_array,
                        $this->config->item('morning_hours_end'));
                ?>
                    <span class="settings_note">
                        (Times for morning hours)
                    </span>
                </div>
        </div>        
        <div class="field_row clearfix">	
        <?php echo form_label(lang('config_afternoon_hours').':', 'afternoon_hours',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_dropdown('afternoon_hours_begin', $times_array,
                        $this->config->item('afternoon_hours_begin'));
                ?>
                <?php echo form_dropdown('afternoon_hours_end', $times_array,
                        $this->config->item('afternoon_hours_end'));
                ?>
                    <span class="settings_note">
                        (Times for afternoon hours)
                    </span>
                </div>
        </div>        
        <div class="field_row clearfix">	
        <?php echo form_label(lang('config_twilight_hour').':', 'twilight_hour',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_dropdown('twilight_hour', $times_array,
                        $this->config->item('twilight_hour'));
                ?>
                    <span class="settings_note">
                        (Time that twilight hours starts)
                    </span>
                </div>
        </div>        
        <div class="field_row clearfix">	
        <?php echo form_label(lang('config_super_twilight_hour').':', 'super_twilight_hour',array('class'=>'wide')); ?>
                <div class='form_field'>
                <?php echo form_dropdown('super_twilight_hour', $times_array,
                        $this->config->item('super_twilight_hour'));
                ?>
                    <span class="settings_note">
                        (Time that super twilight hours start)
                    </span>
                </div>
        </div> 
        <div class="field_row clearfix">	
        <?php echo form_label(lang('config_holidays').':', 'holidays',array('class'=>'wide')); ?>
                    <div class='form_field'>
                <?php echo form_dropdown('holidays', array(
                        '0'=>'Off',
                        '1'=>'On'),
                        $this->config->item('holidays'));
                ?>
                       <a id='holiday_view' href='index.php/config/holidays' title='Manage Holidays'>Select Holidays</a>
                       <script>
                       		$('#holiday_view').colorbox({width:720});
                       </script>
                </div>
        </div>
               
        <div class="field_row clearfix">	
        <?php echo form_label(lang('config_holes').':', 'holes',array('class'=>'wide')); ?>
                    <div class='form_field'>
                <?php echo form_dropdown('holes', array(
                        '9'=>'9',
                        '18'=>'18',
                        '27'=>'27',
                        '36'=>'36'),
                        $this->config->item('holes'));
                ?>
                </div>
        </div>
        <div class="field_row clearfix">	
        <?php echo form_label(lang('config_online_booking').':', 'holes',array('class'=>'wide')); ?>
                    <div class='form_field'>
                <!--?php echo form_dropdown('online_booking', array(
                        '0'=>'Off',
                        '1'=>'On'),
                        $this->config->item('online_booking'));
                ?-->
                       URL for Online Booking: <a target="_blank" href="<?php echo site_url();?>/be/reservations/<?php echo $this->session->userdata('course_id');?>"><?php echo site_url();?>/be/reservations/<?php echo $this->session->userdata('course_id');?></a>
                </div>
        </div>
        <div class="field_row clearfix">	
        <?php echo form_label(lang('config_online_booking_protected').':', 'online_booking_protected',array('class'=>'wide')); ?>
                    <div class='form_field'>
                <?php echo form_dropdown('online_booking_protected', array(
                        '0'=>'Off',
                        '1'=>'On'),
                        $this->config->item('online_booking_protected'));
                ?>
                </div>
        </div>
        <div id='booking_rules_box' style='display:none'>
	        <div class="field_row clearfix">	
	        <?php echo form_label(lang('config_online_booking_rules').':', 'booking_rules',array('class'=>'wide')); ?>
	                <div class='form_field'>
	                	<textarea id='booking_rules' name='booking_rules' rows="10" cols="96"><?php echo $this->config->item('booking_rules'); ?></textarea>
	                </div>
	        </div>
        </div>
		<script type='text/javascript'>
			$('#booking_rules_box').expandable({title:'Booking Rules:'});
			$('#booking_rules').wysiwyg();
			$('#holiday_view').colorbox();
		</script>