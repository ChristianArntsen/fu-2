</div>
</div>
	
	<table id="footer_info">
		<tr>
			<td id="menubar_footer">
             <!--               <?php if ($this->session->userdata('person_id')){?>
			<?php echo lang('common_welcome')." <b> {$this->session->userdata('first_name')} {$this->session->userdata('last_name')}! | </b>"; ?>
			<?php echo anchor("be/logout",lang("common_logout")); ?>
                            <?php }?>-->
                <!--span>Online booking provided by</span>
    			<img src="images/header/header_logo.png" alt="ForeUP"/-->
			</td>
	
			<td id="menubar_date_time" class="menu_date">
				<?php
				if($this->config->item('time_format') == '24_hour')
				{
					echo date('H:i');					
				}
				else
				{
					echo date('h:i');
				}
				?>
			</td>
			<td id="menubar_date_day" class="menu_date mini_date">
				<?php echo date('D') ?>	
				<br />
				<?php
				if($this->config->item('time_format') != '24_hour')
				{
					echo date('a');
				}
				?>
			</td>
			<td id="menubar_date_spacer" class="menu_date">
				|
			</td>
			<td id="menubar_date_date" class="menu_date">
				<?php echo date('d') ?>
			</td>
			<td id="menubar_date_monthyr" class="menu_date mini_date">
				<?php echo date('F') ?>
				<br />
				<?php echo date('Y') ?>
			</td>
		</tr>
	</table>
	
	<div id="footer_spacer"></div>

	<table id="footer">
		<tr>
			<td id="footer_cred">
				<?php echo lang('common_please_visit_my').' '.date("Y"); ?> 
					<a href="http://www.foreup.com" target="_blank">
						<?php echo lang('common_website'); ?>
					</a> 
				<?php echo lang('common_learn_about_project'); ?>
			</td>
			<td id="footer_version">
				<?php echo lang('common_you_are_using_online_booking')?>
			</td>
		</tr>
	</table>
        <div id='contactBox'>
        </div>
        <!-- Feedback Button -->
	    <!--div id="feedback_button">
	        <a href="#" title=""></a>
	    </div-->
	    <!-- Feedback Button End -->
        <script>
            /*$(function() {
                $('#contactBox').contactable({
                    name:'Name',
                    email:'Email',
                    message:'Message',
                    subject : 'Subject',
                    receivedMsg:'',
                    notReceivedMsg: '',
                    hideOnSubmit:true
                });
            })*/
        </script>
</body>
</html>