<?php
require_once ("secure_area.php");
class Config extends Secure_area 
{
	function __construct()
	{
		parent::__construct('config');
		$this->load->model('Teesheet');
		$this->load->model('Schedule');
		$this->load->model('Green_fee');
		$this->load->model('Fee');
		$this->load->model('Terminal');
		$this->load->model('Customer_loyalty');
	}
	
	function index()
	{		
		$data['controller_name']=strtolower(get_class());
		/*
		 * NEW RESERVATION SYSTEM
		 */
		if ($this->session->userdata('reservations'))
		{
	        $data['teetime_prices']=$this->Fee->get_info();//$this->Item->get_teetime_prices();
	        $data['teetime_type']=$this->Fee->get_type_info();
			$data['teesheet_table'] = '';//get_teesheets_manage_table($this->Schedule->get_all($this->config->item('per_page'), $this->uri->segment(3)),$this);
			$data['teesheets'] = $this->Schedule->get_all();
		}
		/*
		 * OLD TEESHEET SYSTEM
		 */
		else 
		{
	        $data['teetime_prices']=$this->Green_fee->get_info();//$this->Item->get_teetime_prices();
	        $data['teetime_type']=$this->Green_fee->get_type_info();
			$data['teesheet_table'] = get_teesheets_manage_table($this->Teesheet->get_all($this->config->item('per_page'), $this->uri->segment(3)),$this);
			$data['teesheets'] = $this->Teesheet->get_all();
		}
		$data['teetime_department']=$this->Item->get_teetime_department();
        $data['teetime_tax_rate']=$this->Item->get_teetime_tax_rate();
		$data['cart_department']=$this->Item->get_cart_department();
        $data['cart_tax_rate']=$this->Item->get_cart_tax_rate();
		$data['course_id']=$this->session->userdata('course_id');
		$departments = $this->Item->get_department_suggestions();
		$data['use_terminals']=($this->session->userdata('use_terminals')?1:0);
		$data['use_loyalty']=($this->config->item('use_loyalty')?1:0);
		$data['terminals']=$this->Terminal->get_all($data['course_id'])->result_object();
		$data['loyalty_rates']=$this->Customer_loyalty->get_rates()->result_object();
		$data['departments']['all'] = 'Department';
		$data['page'] = ($this->input->get('page') ? $this->input->get('page') : 1);
		foreach ($departments as $department) {
			$data['departments'][$department['label']] = $department['label']; 
		}
		$categories = $this->Item->get_category_suggestions();
		$data['categories']['all'] = 'Category';
		foreach ($categories as $category) {
			$data['categories'][$category['label']] = $category['label']; 
		}
		$data['no_weekend'] = (!$this->config->item('weekend_fri') && !$this->config->item('weekend_sat') && !$this->config->item('weekend_sun'));

		// Quickbooks, load user (if one exists)
		$this->load->model('quickbooks');
		$data['quickbooks'] = $this->quickbooks->get_user($this->session->userdata('course_id'));

		// Get list of Quickbooks accounts
		$qbAccounts = $this->quickbooks->get_accounts();

		if(!empty($qbAccounts)){
		
			// Build array for dropdown menu
			$dropdownAccounts = array(0 => '- No account selected -');
			foreach($qbAccounts as $account){
				$dropdownAccounts[$account['quickbooks_id']] = $account['name'] .' - '. $account['type'];

				if(!empty($account['is_sales'])){
					$data['quickbooks']['sales_account'] = $account['quickbooks_id'];
				}
				if(!empty($account['is_redemption'])){
					$data['quickbooks']['redemption_account'] = $account['quickbooks_id'];
				}
				if(!empty($account['is_tips'])){
					$data['quickbooks']['tips_account'] = $account['quickbooks_id'];
				}								
			}
		}else{
			$dropdownAccounts = array(0=>'- No Accounts Available -');
		}
		$data['quickbooks']['accounts'] = $dropdownAccounts;
		
		$this->load->view("config/settings", $data);
	}
	
	function template(){
		$this->load->view('email_templates/template_5');
	}
	function holidays(){
		$data['holidays']=$this->Appconfig->get_holiday_info();
		$this->load->view('holidays/form', $data);
	}
	function add_holiday() 
	{
		$holiday_label = $this->input->post("holiday_label");
		$holiday_date = $this->input->post("holiday_date");
		echo json_encode(array('success'=>$this->Appconfig->add_holiday($holiday_label, $holiday_date)));
	}
	function save_holiday_name($holiday_date)
	{
		$holiday_label = $this->input->post('holiday_label');
		$new_date = $this->input->post('new_date');
		echo json_encode(array('success'=>$this->Appconfig->update_holiday($holiday_date, $holiday_label, $new_date)));
	}
	function delete_holiday($holiday_date) 
	{
		echo json_encode(array('success'=>$this->Appconfig->delete_holiday($holiday_date)));
	}
	
	function view($settings_section)
        {
            $course_id = $this->session->userdata('course_id');
            if ($settings_section === 'teesheet')
               $this->load->view('config/teesheet');
            else if ($settings_section === 'sales')
               $this->load->view('config/sales');
        }	
	function view_terminal_options($terminal_id)
	{
		if ($this->permissions->is_admin())
		{
			$data = array();
			$data['terminal_info'] = $this->Terminal->get_info($terminal_id);
			$this->load->view('config/form_terminals', $data);
		}
		else
			echo 'You do not have permissions to access these settings.';
	}
	function save_terminal_options($terminal_id = -1)
	{
		$terminal_data = array(
			'label'=>$this->input->post('t_label'),
			'quickbutton_tab'=>$this->input->post('t_quickbutton_tab'),
			'mercury_id'=>$this->input->post('t_mercury_id'),
			'mercury_password'=>$this->input->post('t_mercury_password'),
			'ets_key'=>$this->input->post('t_ets_key'),
			'auto_print_receipts'=>$this->input->post('t_auto_print_receipts'),
			'webprnt_ip'=>$this->input->post('t_webprnt_ip'),
			'webprnt_hot_ip'=>$this->input->post('t_webprnt_hot_ip'),
			'webprnt_cold_ip'=>$this->input->post('t_webprnt_cold_ip'),
			'track_cash'=>$this->input->post('t_track_cash'),
			'cash_register'=>$this->input->post('t_cash_register'),
			'print_tip_line'=>$this->input->post('t_print_tip_line'),
			'signature_slip_count'=>$this->input->post('t_signature_slip_count'),
			'credit_card_receipt_count'=>$this->input->post('t_credit_card_receipt_count'),
			'non_credit_card_receipt_count'=>$this->input->post('t_non_credit_card_receipt_count')
		);
		
		$this->Terminal->save($terminal_data, $terminal_id);
	}
	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest_dept_cat_item()
	{
		// function suggest()
		// {
			// $suggestions = $this->Item->get_search_suggestions($this->input->get('term'),100);
			// echo json_encode($suggestions);
		// }
// 		
		// function item_search()
		// {
			// $suggestions = $this->Item->get_item_search_suggestions($this->input->get('term'),100);
			// echo json_encode($suggestions);
		// }
// 		
		// /*
		// Gives search suggestions based on what is being searched for
		// */
		// function suggest_department()
		// {
			// $suggestions = $this->Item->get_department_suggestions($this->input->get('term'));
			// echo json_encode($suggestions);
		// }
// 	
		// /*
		// Gives search suggestions based on what is being searched for
		// */
		// function suggest_category()
		// {
			// $suggestions = $this->Item->get_category_suggestions($this->input->get('term'));
			// echo json_encode($suggestions);
		// }
// 	
		// /*
		// Gives search suggestions based on what is being searched for
		// */
		// function suggest_subcategory()
		// {
			// $suggestions = $this->Item->get_subcategory_suggestions($this->input->get('term'));
			// echo json_encode($suggestions);
		// }
		//	$suggestions = $this->Item_kit->get_search_suggestions($this->input->get('term'),100);
		$suggestions = $this->Item->get_department_suggestions($this->input->get('term'));
		$suggestions = array_merge($suggestions, $this->Item->get_category_suggestions($this->input->get('term')));
		$suggestions = array_merge($suggestions, $this->Item->get_subcategory_suggestions($this->input->get('term')));
		$suggestions = array_merge($suggestions, $this->Item->get_item_search_suggestions($this->input->get('term'),100));
		$suggestions = array_merge($suggestions, $this->Item_kit->get_search_suggestions($this->input->get('term'),100));
		echo json_encode($suggestions);
	}
	function save($settings_section)
	{
		$course_id = $this->session->userdata('course_id');
		$batch_save_data = array();
		if ($settings_section == 'details')
		{
			if(!empty($_FILES["company_logo"]) && $_FILES["company_logo"]["error"] == UPLOAD_ERR_OK && ($_SERVER['HTTP_HOST'] !='demo.phppointofsale.com' && $_SERVER['HTTP_HOST'] !='demo.phppointofsalestaging.com'))
			{
				$allowed_extensions = array('png', 'jpg', 'jpeg', 'gif');
				$extension = strtolower(end(explode('.', $_FILES["company_logo"]["name"])));
				
				if (in_array($extension, $allowed_extensions))
				{
					$config['image_library'] = 'gd2';
					$config['source_image']	= $_FILES["company_logo"]["tmp_name"];
					$config['create_thumb'] = FALSE;
					$config['maintain_ratio'] = TRUE;
					$config['width']	 = 170;
					$config['height']	= 60;
					$this->load->library('image_lib', $config); 
					$this->image_lib->resize();
					$company_logo = $this->Appfile->save($_FILES["company_logo"]["name"], file_get_contents($_FILES["company_logo"]["tmp_name"]), $this->config->item('company_logo'));
				}
			}
			elseif($this->input->post('delete_logo'))
			{
				$this->Appfile->delete($this->config->item('company_logo'));
			}
			$batch_save_data=array(
				'name'=>$this->input->post('name'),
				'address'=>$this->input->post('address'),
				'phone'=>$this->input->post('phone'),
				'email'=>$this->input->post('email'),
				'fax'=>$this->input->post('fax'),
				'city'=>$this->input->post('city'),
				'state'=>$this->input->post('state'),
				'zip'=>$this->input->post('zip'),
			    'website'=>$this->input->post('website')
			 );
			
	        $this->session->set_userdata('zip', $this->input->post('zip'));
	                
			if (isset($company_logo))
			{
				$batch_save_data['company_logo'] = $company_logo;
			}
			elseif($this->input->post('delete_logo'))
			{
				$batch_save_data['company_logo'] = 0;
			}
			
		}
		else if ($settings_section == 'hours')
		{
			//if ($this->config->item('open_time') != $this->input->post('open_time'))
	        //    $this->Teesheet->adjust_teetimes();
	                
	        $batch_save_data = array(
				'open_time'=>$this->input->post('open_time'),
				'close_time'=>$this->input->post('close_time'),
				'early_bird_hours_begin'=>$this->input->post('early_bird_hours_begin'),
		        'early_bird_hours_end'=>$this->input->post('early_bird_hours_end'),
		        'morning_hours_begin'=>$this->input->post('morning_hours_begin'),
		        'morning_hours_end'=>$this->input->post('morning_hours_end'),
		        'afternoon_hours_begin'=>$this->input->post('afternoon_hours_begin'),
		        'afternoon_hours_end'=>$this->input->post('afternoon_hours_end'),
		        'twilight_hour'=>$this->input->post('twilight_hour'),
		        'super_twilight_hour'=>$this->input->post('super_twilight_hour'),
		        'holidays'=>$this->input->post('holidays'),
		        'holes'=>$this->input->post('holes'),
		        'online_booking'=>$this->input->post('online_booking'),
		        'online_booking_protected'=>$this->input->post('online_booking_protected'),
		        'booking_rules'=>htmlentities($this->input->post('booking_rules')),
		        'open_sun'=>($this->input->post('open_sun'))?1:0,    
		        'open_mon'=>($this->input->post('open_mon'))?1:0,    
		        'open_tue'=>($this->input->post('open_tue'))?1:0,    
		        'open_wed'=>($this->input->post('open_wed'))?1:0,    
		        'open_thu'=>($this->input->post('open_thu'))?1:0,    
		        'open_fri'=>($this->input->post('open_fri'))?1:0,    
		        'open_sat'=>($this->input->post('open_sat'))?1:0,    
		        'weekend_fri'=>($this->input->post('weekend_fri'))?1:0,    
		        'weekend_sat'=>($this->input->post('weekend_sat'))?1:0,    
		        'weekend_sun'=>($this->input->post('weekend_sun'))?1:0    
			);
			$this->session->set_userdata('openhour', $this->input->post('open_time'));
	        $this->session->set_userdata('closehour', $this->input->post('close_time'));
		}
		else if ($settings_section == 'green_fees')
		{
			if (!$this->input->post()) {
				
			}
			else {
			// Save green fees
				$price_cartegory_arrays = array();
		        $teesheets = $this->session->userdata('reservations') ? $this->Schedule->get_all() : $this->Teesheet->get_all();
		        foreach($teesheets->result() as $teesheet)
				{
					// Save headers
					$price_category_array= array(
						'course_id'=>$course_id,
						'teesheet_id'=>$teesheet->teesheet_id,
						'price_category_1'=>'Regular',
						'price_category_2'=>'Early Bird',
						'price_category_3'=>'Morning',
						'price_category_4'=>'Afternoon',
						'price_category_5'=>'Twilight',
						'price_category_6'=>'Super Twilight',
						'price_category_7'=>'Holiday',
						'price_category_8'=>$this->input->post("price_category_8_".$teesheet->teesheet_id),
						'price_category_9'=>$this->input->post("price_category_9_".$teesheet->teesheet_id),
						'price_category_10'=>$this->input->post("price_category_10_".$teesheet->teesheet_id),
						'price_category_11'=>$this->input->post("price_category_11_".$teesheet->teesheet_id),
						'price_category_12'=>$this->input->post("price_category_12_".$teesheet->teesheet_id),
						'price_category_13'=>$this->input->post("price_category_13_".$teesheet->teesheet_id),
						'price_category_14'=>$this->input->post("price_category_14_".$teesheet->teesheet_id),
						'price_category_15'=>$this->input->post("price_category_15_".$teesheet->teesheet_id),
						'price_category_16'=>$this->input->post("price_category_16_".$teesheet->teesheet_id),
						'price_category_17'=>$this->input->post("price_category_17_".$teesheet->teesheet_id),
						'price_category_18'=>$this->input->post("price_category_18_".$teesheet->teesheet_id),
						'price_category_19'=>$this->input->post("price_category_19_".$teesheet->teesheet_id),
						'price_category_20'=>$this->input->post("price_category_20_".$teesheet->teesheet_id),
						'price_category_21'=>$this->input->post("price_category_21_".$teesheet->teesheet_id),
						'price_category_22'=>$this->input->post("price_category_22_".$teesheet->teesheet_id),
						'price_category_23'=>$this->input->post("price_category_23_".$teesheet->teesheet_id),
						'price_category_24'=>$this->input->post("price_category_24_".$teesheet->teesheet_id),
						'price_category_25'=>$this->input->post("price_category_25_".$teesheet->teesheet_id),
						'price_category_26'=>$this->input->post("price_category_26_".$teesheet->teesheet_id),
						'price_category_27'=>$this->input->post("price_category_27_".$teesheet->teesheet_id),
						'price_category_28'=>$this->input->post("price_category_28_".$teesheet->teesheet_id),
						'price_category_29'=>$this->input->post("price_category_29_".$teesheet->teesheet_id),
						'price_category_30'=>$this->input->post("price_category_30_".$teesheet->teesheet_id),
						'price_category_31'=>$this->input->post("price_category_31_".$teesheet->teesheet_id),
						'price_category_32'=>$this->input->post("price_category_32_".$teesheet->teesheet_id),
						'price_category_33'=>$this->input->post("price_category_33_".$teesheet->teesheet_id),
						'price_category_34'=>$this->input->post("price_category_34_".$teesheet->teesheet_id),
						'price_category_35'=>$this->input->post("price_category_35_".$teesheet->teesheet_id),
						'price_category_36'=>$this->input->post("price_category_36_".$teesheet->teesheet_id),
						'price_category_37'=>$this->input->post("price_category_37_".$teesheet->teesheet_id),
						'price_category_38'=>$this->input->post("price_category_38_".$teesheet->teesheet_id),
						'price_category_39'=>$this->input->post("price_category_39_".$teesheet->teesheet_id),
						'price_category_40'=>$this->input->post("price_category_40_".$teesheet->teesheet_id),
						'price_category_41'=>$this->input->post("price_category_41_".$teesheet->teesheet_id),
						'price_category_42'=>$this->input->post("price_category_42_".$teesheet->teesheet_id),
						'price_category_43'=>$this->input->post("price_category_43_".$teesheet->teesheet_id),
						'price_category_44'=>$this->input->post("price_category_44_".$teesheet->teesheet_id),
						'price_category_45'=>$this->input->post("price_category_45_".$teesheet->teesheet_id),
						'price_category_46'=>$this->input->post("price_category_46_".$teesheet->teesheet_id),
						'price_category_47'=>$this->input->post("price_category_47_".$teesheet->teesheet_id),
						'price_category_48'=>$this->input->post("price_category_48_".$teesheet->teesheet_id),
						'price_category_49'=>$this->input->post("price_category_49_".$teesheet->teesheet_id),
						'price_category_50'=>$this->input->post("price_category_50_".$teesheet->teesheet_id)
					);
					$this->Green_fee->save_types($price_category_array);
					$default_price_class = $this->input->post('default_price_class_'.$teesheet->teesheet_id);
					$teesheet_data = array('default_price_class'=>$default_price_class);
					if ($this->session->userdata('reservations'))
					{
						$this->Schedule->save($teesheet_data, $teesheet->teesheet_id);	
					}
					else 
					{
						$this->Teesheet->save($teesheet_data, $teesheet->teesheet_id);
					}					
					if ($this->session->userdata('teesheet_id') == $teesheet->teesheet_id)
						$this->session->set_userdata('default_price_class', $default_price_class);
					for ($i = 1; $i <=8; $i++)
					{
						$price_array= array(
							'item_number'=>$course_id.'_'.$i,
							'teesheet_id'=>$teesheet->teesheet_id,
							'price_category_1'=>$this->input->post("{$teesheet->teesheet_id}_1_{$i}"),
							'price_category_2'=>$this->input->post("{$teesheet->teesheet_id}_2_{$i}"),
							'price_category_3'=>$this->input->post("{$teesheet->teesheet_id}_3_{$i}"),
							'price_category_4'=>$this->input->post("{$teesheet->teesheet_id}_4_{$i}"),
							'price_category_5'=>$this->input->post("{$teesheet->teesheet_id}_5_{$i}"),
							'price_category_6'=>$this->input->post("{$teesheet->teesheet_id}_6_{$i}"),
							'price_category_7'=>$this->input->post("{$teesheet->teesheet_id}_7_{$i}"),
							'price_category_8'=>$this->input->post("{$teesheet->teesheet_id}_8_{$i}"),
							'price_category_9'=>$this->input->post("{$teesheet->teesheet_id}_9_{$i}"),
							'price_category_10'=>$this->input->post("{$teesheet->teesheet_id}_10_{$i}"),
							'price_category_11'=>$this->input->post("{$teesheet->teesheet_id}_11_{$i}"),
							'price_category_12'=>$this->input->post("{$teesheet->teesheet_id}_12_{$i}"),
							'price_category_13'=>$this->input->post("{$teesheet->teesheet_id}_13_{$i}"),
							'price_category_14'=>$this->input->post("{$teesheet->teesheet_id}_14_{$i}"),
							'price_category_15'=>$this->input->post("{$teesheet->teesheet_id}_15_{$i}"),
							'price_category_16'=>$this->input->post("{$teesheet->teesheet_id}_16_{$i}"),
							'price_category_17'=>$this->input->post("{$teesheet->teesheet_id}_17_{$i}"),
							'price_category_18'=>$this->input->post("{$teesheet->teesheet_id}_18_{$i}"),
							'price_category_19'=>$this->input->post("{$teesheet->teesheet_id}_19_{$i}"),
							'price_category_20'=>$this->input->post("{$teesheet->teesheet_id}_20_{$i}"),
							'price_category_21'=>$this->input->post("{$teesheet->teesheet_id}_21_{$i}"),
							'price_category_22'=>$this->input->post("{$teesheet->teesheet_id}_22_{$i}"),
							'price_category_23'=>$this->input->post("{$teesheet->teesheet_id}_23_{$i}"),
							'price_category_24'=>$this->input->post("{$teesheet->teesheet_id}_24_{$i}"),
							'price_category_25'=>$this->input->post("{$teesheet->teesheet_id}_25_{$i}"),
							'price_category_26'=>$this->input->post("{$teesheet->teesheet_id}_26_{$i}"),
							'price_category_27'=>$this->input->post("{$teesheet->teesheet_id}_27_{$i}"),
							'price_category_28'=>$this->input->post("{$teesheet->teesheet_id}_28_{$i}"),
							'price_category_29'=>$this->input->post("{$teesheet->teesheet_id}_29_{$i}"),
							'price_category_30'=>$this->input->post("{$teesheet->teesheet_id}_30_{$i}"),
							'price_category_31'=>$this->input->post("{$teesheet->teesheet_id}_31_{$i}"),
							'price_category_32'=>$this->input->post("{$teesheet->teesheet_id}_32_{$i}"),
							'price_category_33'=>$this->input->post("{$teesheet->teesheet_id}_33_{$i}"),
							'price_category_34'=>$this->input->post("{$teesheet->teesheet_id}_34_{$i}"),
							'price_category_35'=>$this->input->post("{$teesheet->teesheet_id}_35_{$i}"),
							'price_category_36'=>$this->input->post("{$teesheet->teesheet_id}_36_{$i}"),
							'price_category_37'=>$this->input->post("{$teesheet->teesheet_id}_37_{$i}"),
							'price_category_38'=>$this->input->post("{$teesheet->teesheet_id}_38_{$i}"),
							'price_category_39'=>$this->input->post("{$teesheet->teesheet_id}_39_{$i}"),
							'price_category_40'=>$this->input->post("{$teesheet->teesheet_id}_40_{$i}"),
							'price_category_41'=>$this->input->post("{$teesheet->teesheet_id}_41_{$i}"),
							'price_category_42'=>$this->input->post("{$teesheet->teesheet_id}_42_{$i}"),
							'price_category_43'=>$this->input->post("{$teesheet->teesheet_id}_43_{$i}"),
							'price_category_44'=>$this->input->post("{$teesheet->teesheet_id}_44_{$i}"),
							'price_category_45'=>$this->input->post("{$teesheet->teesheet_id}_45_{$i}"),
							'price_category_46'=>$this->input->post("{$teesheet->teesheet_id}_46_{$i}"),
							'price_category_47'=>$this->input->post("{$teesheet->teesheet_id}_47_{$i}"),
							'price_category_48'=>$this->input->post("{$teesheet->teesheet_id}_48_{$i}"),
							'price_category_49'=>$this->input->post("{$teesheet->teesheet_id}_49_{$i}"),
							'price_category_50'=>$this->input->post("{$teesheet->teesheet_id}_50_{$i}")
						);
						$this->Green_fee->save($price_array);
					}
				}
				$price_sql = $this->db->last_query();
				
		        // Save taxes and departments
		        // Teetime Department
				$teetime_department = $this->input->post('teetime_department');
		        $teetime_tax_rate = $this->input->post('teetime_tax_rate');
		        $cart_department = $this->input->post('cart_department');
		        $cart_tax_rate = $this->input->post('cart_tax_rate');
		        
		        for ($i = 1; $i <=8; $i++)
				{
					$item_id = $this->Item->get_item_id("{$course_id}_{$i}");
					$department = ($i==1 || $i==2 || $i==5 || $i==6)?$cart_department:$teetime_department;
					$tax_rate = ($i==1 || $i==2 || $i==5 || $i==6)?$cart_tax_rate:$teetime_tax_rate;
					
					$item_data = array('department'=>$department);
					$this->Item->save($item_data, $item_id);
					//echo $this->db->last_query();
		            
					$items_taxes_data = array();
					$items_taxes_data[] = array('course_id'=>$course_id,'name'=>'Sales Tax', 'percent'=>$tax_rate, 'cumulative' => '0' );
					$this->Item_taxes->save($items_taxes_data, $item_id);
				}
			}
			$batch_save_data=array('course_id'=>$course_id);		
		}
		else if ($settings_section == 'fees')
		{
			if (!$this->input->post()) {
				
			}
			else {
			// Save green fees
				$price_cartegory_arrays = array();
		        $schedules = $this->Schedule->get_all();
		        foreach($schedules->result() as $schedule)
				{
					// Save headers
					$price_category_array= array(
						'course_id'=>$course_id,
						'schedule_id'=>$schedule->schedule_id,
						'price_category_1'=>'Regular',
						'price_category_2'=>'Early Bird',
						'price_category_3'=>'Morning',
						'price_category_4'=>'Afternoon',
						'price_category_5'=>'Twilight',
						'price_category_6'=>'Super Twilight',
						'price_category_7'=>'Holiday',
						'price_category_8'=>$this->input->post("price_category_8_".$schedule->schedule_id),
						'price_category_9'=>$this->input->post("price_category_9_".$schedule->schedule_id),
						'price_category_10'=>$this->input->post("price_category_10_".$schedule->schedule_id),
						'price_category_11'=>$this->input->post("price_category_11_".$schedule->schedule_id),
						'price_category_12'=>$this->input->post("price_category_12_".$schedule->schedule_id),
						'price_category_13'=>$this->input->post("price_category_13_".$schedule->schedule_id),
						'price_category_14'=>$this->input->post("price_category_14_".$schedule->schedule_id),
						'price_category_15'=>$this->input->post("price_category_15_".$schedule->schedule_id),
						'price_category_16'=>$this->input->post("price_category_16_".$schedule->schedule_id),
						'price_category_17'=>$this->input->post("price_category_17_".$schedule->schedule_id),
						'price_category_18'=>$this->input->post("price_category_18_".$schedule->schedule_id),
						'price_category_19'=>$this->input->post("price_category_19_".$schedule->schedule_id),
						'price_category_20'=>$this->input->post("price_category_20_".$schedule->schedule_id),
						'price_category_21'=>$this->input->post("price_category_21_".$schedule->schedule_id),
						'price_category_22'=>$this->input->post("price_category_22_".$schedule->schedule_id),
						'price_category_23'=>$this->input->post("price_category_23_".$schedule->schedule_id),
						'price_category_24'=>$this->input->post("price_category_24_".$schedule->schedule_id),
						'price_category_25'=>$this->input->post("price_category_25_".$schedule->schedule_id),
						'price_category_26'=>$this->input->post("price_category_26_".$schedule->schedule_id),
						'price_category_27'=>$this->input->post("price_category_27_".$schedule->schedule_id),
						'price_category_28'=>$this->input->post("price_category_28_".$schedule->schedule_id),
						'price_category_29'=>$this->input->post("price_category_29_".$schedule->schedule_id),
						'price_category_30'=>$this->input->post("price_category_30_".$schedule->schedule_id),
						'price_category_31'=>$this->input->post("price_category_31_".$schedule->schedule_id),
						'price_category_32'=>$this->input->post("price_category_32_".$schedule->schedule_id),
						'price_category_33'=>$this->input->post("price_category_33_".$schedule->schedule_id),
						'price_category_34'=>$this->input->post("price_category_34_".$schedule->schedule_id),
						'price_category_35'=>$this->input->post("price_category_35_".$schedule->schedule_id),
						'price_category_36'=>$this->input->post("price_category_36_".$schedule->schedule_id),
						'price_category_37'=>$this->input->post("price_category_37_".$schedule->schedule_id),
						'price_category_38'=>$this->input->post("price_category_38_".$schedule->schedule_id),
						'price_category_39'=>$this->input->post("price_category_39_".$schedule->schedule_id),
						'price_category_40'=>$this->input->post("price_category_40_".$schedule->schedule_id),
						'price_category_41'=>$this->input->post("price_category_41_".$schedule->schedule_id),
						'price_category_42'=>$this->input->post("price_category_42_".$schedule->schedule_id),
						'price_category_43'=>$this->input->post("price_category_43_".$schedule->schedule_id),
						'price_category_44'=>$this->input->post("price_category_44_".$schedule->schedule_id),
						'price_category_45'=>$this->input->post("price_category_45_".$schedule->schedule_id),
						'price_category_46'=>$this->input->post("price_category_46_".$schedule->schedule_id),
						'price_category_47'=>$this->input->post("price_category_47_".$schedule->schedule_id),
						'price_category_48'=>$this->input->post("price_category_48_".$schedule->schedule_id),
						'price_category_49'=>$this->input->post("price_category_49_".$schedule->schedule_id),
						'price_category_50'=>$this->input->post("price_category_50_".$schedule->schedule_id)
					);
					$this->Fee->save_types($price_category_array);
					$default_price_class = $this->input->post('default_price_class_'.$schedule->schedule_id);
					$schedule_data = array('default_price_class'=>$default_price_class);
					$this->Schedule->save($schedule_data, $schedule->schedule_id);	
					
					if ($this->session->userdata('schedule_id') == $schedule->schedule_id)
						$this->session->set_userdata('default_price_class', $default_price_class);
					for ($i = 1; $i <=8; $i++)
					{
						$price_array= array(
							'item_number'=>$course_id.'_'.$i,
							'schedule_id'=>$schedule->schedule_id,
							'price_category_1'=>$this->input->post("{$schedule->schedule_id}_1_{$i}"),
							'price_category_2'=>$this->input->post("{$schedule->schedule_id}_2_{$i}"),
							'price_category_3'=>$this->input->post("{$schedule->schedule_id}_3_{$i}"),
							'price_category_4'=>$this->input->post("{$schedule->schedule_id}_4_{$i}"),
							'price_category_5'=>$this->input->post("{$schedule->schedule_id}_5_{$i}"),
							'price_category_6'=>$this->input->post("{$schedule->schedule_id}_6_{$i}"),
							'price_category_7'=>$this->input->post("{$schedule->schedule_id}_7_{$i}"),
							'price_category_8'=>$this->input->post("{$schedule->schedule_id}_8_{$i}"),
							'price_category_9'=>$this->input->post("{$schedule->schedule_id}_9_{$i}"),
							'price_category_10'=>$this->input->post("{$schedule->schedule_id}_10_{$i}"),
							'price_category_11'=>$this->input->post("{$schedule->schedule_id}_11_{$i}"),
							'price_category_12'=>$this->input->post("{$schedule->schedule_id}_12_{$i}"),
							'price_category_13'=>$this->input->post("{$schedule->schedule_id}_13_{$i}"),
							'price_category_14'=>$this->input->post("{$schedule->schedule_id}_14_{$i}"),
							'price_category_15'=>$this->input->post("{$schedule->schedule_id}_15_{$i}"),
							'price_category_16'=>$this->input->post("{$schedule->schedule_id}_16_{$i}"),
							'price_category_17'=>$this->input->post("{$schedule->schedule_id}_17_{$i}"),
							'price_category_18'=>$this->input->post("{$schedule->schedule_id}_18_{$i}"),
							'price_category_19'=>$this->input->post("{$schedule->schedule_id}_19_{$i}"),
							'price_category_20'=>$this->input->post("{$schedule->schedule_id}_20_{$i}"),
							'price_category_21'=>$this->input->post("{$schedule->schedule_id}_21_{$i}"),
							'price_category_22'=>$this->input->post("{$schedule->schedule_id}_22_{$i}"),
							'price_category_23'=>$this->input->post("{$schedule->schedule_id}_23_{$i}"),
							'price_category_24'=>$this->input->post("{$schedule->schedule_id}_24_{$i}"),
							'price_category_25'=>$this->input->post("{$schedule->schedule_id}_25_{$i}"),
							'price_category_26'=>$this->input->post("{$schedule->schedule_id}_26_{$i}"),
							'price_category_27'=>$this->input->post("{$schedule->schedule_id}_27_{$i}"),
							'price_category_28'=>$this->input->post("{$schedule->schedule_id}_28_{$i}"),
							'price_category_29'=>$this->input->post("{$schedule->schedule_id}_29_{$i}"),
							'price_category_30'=>$this->input->post("{$schedule->schedule_id}_30_{$i}"),
							'price_category_31'=>$this->input->post("{$schedule->schedule_id}_31_{$i}"),
							'price_category_32'=>$this->input->post("{$schedule->schedule_id}_32_{$i}"),
							'price_category_33'=>$this->input->post("{$schedule->schedule_id}_33_{$i}"),
							'price_category_34'=>$this->input->post("{$schedule->schedule_id}_34_{$i}"),
							'price_category_35'=>$this->input->post("{$schedule->schedule_id}_35_{$i}"),
							'price_category_36'=>$this->input->post("{$schedule->schedule_id}_36_{$i}"),
							'price_category_37'=>$this->input->post("{$schedule->schedule_id}_37_{$i}"),
							'price_category_38'=>$this->input->post("{$schedule->schedule_id}_38_{$i}"),
							'price_category_39'=>$this->input->post("{$schedule->schedule_id}_39_{$i}"),
							'price_category_40'=>$this->input->post("{$schedule->schedule_id}_40_{$i}"),
							'price_category_41'=>$this->input->post("{$schedule->schedule_id}_41_{$i}"),
							'price_category_42'=>$this->input->post("{$schedule->schedule_id}_42_{$i}"),
							'price_category_43'=>$this->input->post("{$schedule->schedule_id}_43_{$i}"),
							'price_category_44'=>$this->input->post("{$schedule->schedule_id}_44_{$i}"),
							'price_category_45'=>$this->input->post("{$schedule->schedule_id}_45_{$i}"),
							'price_category_46'=>$this->input->post("{$schedule->schedule_id}_46_{$i}"),
							'price_category_47'=>$this->input->post("{$schedule->schedule_id}_47_{$i}"),
							'price_category_48'=>$this->input->post("{$schedule->schedule_id}_48_{$i}"),
							'price_category_49'=>$this->input->post("{$schedule->schedule_id}_49_{$i}"),
							'price_category_50'=>$this->input->post("{$schedule->schedule_id}_50_{$i}")
						);
						$this->Fee->save($price_array);
					}
				}
				$price_sql = $this->db->last_query();
				
		        // Save taxes and departments
		        // Teetime Department
				$reservation_department = $this->input->post('teetime_department');
		        $reservation_tax_rate = $this->input->post('teetime_tax_rate');
		        $cart_department = $this->input->post('cart_department');
		        $cart_tax_rate = $this->input->post('cart_tax_rate');
		        
		        for ($i = 1; $i <=8; $i++)
				{
					$item_id = $this->Item->get_item_id("{$course_id}_{$i}");
					$department = ($i==1 || $i==2 || $i==5 || $i==6)?$cart_department:$reservation_department;
					$tax_rate = ($i==1 || $i==2 || $i==5 || $i==6)?$cart_tax_rate:$reservation_tax_rate;
					
					$item_data = array('department'=>$department);
					$this->Item->save($item_data, $item_id);
		            
					$items_taxes_data = array();
					$items_taxes_data[] = array('course_id'=>$course_id,'name'=>'Sales Tax', 'percent'=>$tax_rate, 'cumulative' => '0' );
					$this->Item_taxes->save($items_taxes_data, $item_id);
				}
			}
			$batch_save_data=array('course_id'=>$course_id);		
		}
		else if ($settings_section == 'pos')
		{
			$batch_save_data=array(
				'default_tax_1_rate'=>$this->input->post('default_tax_1_rate'),		
				'default_tax_1_name'=>$this->input->post('default_tax_1_name'),		
				'default_tax_2_rate'=>$this->input->post('default_tax_2_rate'),	
				'default_tax_2_name'=>$this->input->post('default_tax_2_name'),
				'default_tax_2_cumulative' => $this->input->post('default_tax_2_cumulative') ? 1 : 0,	
				'unit_price_includes_tax' => $this->input->post('unit_price_includes_tax') ? 1 : 0,
				'customer_credit_nickname'=>trim($this->input->post('customer_credit_nickname')),
				'member_balance_nickname'=>trim($this->input->post('member_balance_nickname')),
				'credit_department' => $this->input->post('credit_department_category') == 0 ? 1 : 0,	
				'credit_department_name' => $this->input->post('department'),	
				'credit_category' => $this->input->post('credit_department_category') == 1 ? 1 : 0,	
				'credit_category_name' => $this->input->post('category'),	
				'return_policy'=>preg_replace("/\s+/", " ", $this->input->post('return_policy')),
				'print_after_sale'=>$this->input->post('print_after_sale'),
				'webprnt'=>$this->input->post('webprnt'),
				'webprnt_ip'=>$this->input->post('webprnt_ip'),
				'webprnt_hot_ip'=>$this->input->post('webprnt_hot_ip'),
				'webprnt_cold_ip'=>$this->input->post('webprnt_cold_ip'),
				'print_credit_card_receipt'=>$this->input->post('print_credit_card_receipt'),
				'print_sales_receipt'=>$this->input->post('print_sales_receipt'),
				'print_two_receipts'=>$this->input->post('print_two_receipts'),
				'print_two_signature_slips'=>$this->input->post('print_two_signature_slips'),
				'print_two_receipts_other'=>$this->input->post('print_two_receipts_other'),
				'print_tip_line'=>$this->input->post('print_tip_line'),
				'cash_drawer_on_cash'=>$this->input->post('cash_drawer_on_cash'),
				'updated_printing'=>$this->input->post('updated_printing'),
				'after_sale_load'=>$this->input->post('after_sale_load'),
				'teesheet_updates_automatically'=>$this->input->post('teesheet_updates_automatically')?1:0,
				'send_reservation_confirmations'=>$this->input->post('send_reservation_confirmations')?1:0,
				'fnb_login'=>$this->input->post('fnb_login'),
				'receipt_printer'=>$this->input->post('receipt_printer'),
				'track_cash' => $this->input->post('track_cash'),
				'blind_close' => $this->input->post('blind_close'),
				'separate_courses' => $this->input->post('separate_courses')
			);
		}
		else if ($settings_section == 'misc')
		{       
	        $batch_save_data=array(
				'currency_symbol'=>$this->input->post('currency_symbol'),
				'language'=>($this->input->post('language'))?$this->input->post('language'):'english',
				'timezone'=>$this->input->post('timezone'),
				'date_format'=>$this->input->post('date_format'),
				'time_format'=>$this->input->post('time_format'),
				'mailchimp_api_key'=>$this->input->post('mailchimp_api_key'),
				'billing_email'=>$this->input->post('billing_email'),
				'number_of_items_per_page'=>$this->input->post('number_of_items_per_page'),
				'hide_back_nine'=>$this->input->post('hide_back_nine')?1:0
		    );
			$this->session->set_userdata('hide_back_nine', $batch_save_data['hide_back_nine']);
		}
		else if ($settings_section == 'sales')
        {
            $batch_save_data=array(
                'print_after_sale'=>$this->input->post('print_after_sale'),
                'print_two_receipts'=>$this->input->post('print_two_receipts'),
                'print_two_receipts_other'=>$this->input->post('print_two_receipts_other'),
                'after_sale_load'=>$this->input->post('after_sale_load'),
        		'return_policy'=>$this->input->post('return_policy'),
		        //'receipt_printer'=>$this->input->post('receipt_printer')
                'track_cash' => $this->input->post('track_cash'),
            );
        }		
		else if ($settings_section == 'teesheet')
        {
            $batch_save_data=array(
                'teesheet_updates_automatically'=>$this->input->post('teesheet_updates_automatically')?1:0,
                'send_reservation_confirmations'=>$this->input->post('teetime_confirmations')?1:0,
                'teetime_reminders'=>$this->input->post('teetime_reminders')?1:0
            );
        }
		else if ($settings_section == 'terminals')
		{
			$terminal_labels = $this->input->post('terminal_label');
			$terminal_tabs = $this->input->post('terminal_tab');
			$terminal_ids = $this->input->post('terminal_id');
			$delete_terminal_ids = $this->input->post('delete_terminal_id');
			$use_terminals = $this->input->post('use_terminals')?1:0;
			$this->session->set_userdata('use_terminals', $use_terminals);
			
			foreach($terminal_labels as $index => $terminal_label)
			{
				$terminal_data = array('label'=>$terminal_label, 'quickbutton_tab'=>$terminal_tabs[$index], 'course_id'=> $this->session->userdata('course_id'));
				if ($delete_terminal_ids[$index] == '1')
					$this->Terminal->delete($terminal_ids[$index]);
				else 
					$this->Terminal->save($terminal_data, $terminal_ids[$index]);
				//echo $this->db->last_query().'<br/>';
			}
			
			$batch_save_data = array('course_id' => $course_id, 'use_terminals' => $use_terminals);	
		}		
		else if ($settings_section == 'loyalty')
		{
			$loyalty_labels = $this->input->post('loyalty_label');
			$loyalty_rate_ids = $this->input->post('loyalty_rate_id');
			$delete_loyalty_rate_ids = $this->input->post('delete_loyalty_rate_id');
			$loyalty_types = $this->input->post('type');
			$loyalty_value = $this->input->post('value');
			$loyalty_tee_time_indexes = $this->input->post('tee_time_index');
			$loyalty_price_categories = $this->input->post('price_category');
			$points_per_dollar = $this->input->post('points_per_dollar');
			$dollars_per_point = $this->input->post('dollars_per_point');
			$use_loyalty = $this->input->post('use_loyalty')?1:0;
			
			foreach($loyalty_labels as $index => $loyalty_label)
			{
				$loyalty_data = array(
					'label'				=>$loyalty_label, 
					'course_id'			=> $this->session->userdata('course_id'),
					'type'				=>$loyalty_types[$index],
					'value'				=>$loyalty_value[$index],
					'tee_time_index'	=>$loyalty_tee_time_indexes[$index],
					'price_category'	=>$loyalty_price_categories[$index],
					'points_per_dollar'	=>$points_per_dollar[$index],
					'dollars_per_point'	=>$dollars_per_point[$index]
				);
				if ($delete_loyalty_rate_ids[$index] == '1')
					$this->Customer_loyalty->delete($loyalty_rate_ids[$index]);
				else 
					$this->Customer_loyalty->save($loyalty_data, $loyalty_rate_ids[$index]);
				//echo $this->db->last_query().'<br/>';
			}
			
			$batch_save_data = array('course_id' => $course_id, 'use_loyalty' => $use_loyalty);	
		}		
		$open_time = $this->config->item('open_time');
		if($this->Appconfig->save_all($batch_save_data))
		{
			if ($settings_section == 'hours' && $open_time != $this->input->post('open_time'))
				$this->Teesheet->adjust_teetimes();
			echo json_encode(array('success'=>true,
			'message'=>lang('config_saved_successfully') 
			));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('config_saved_unsuccessfully')));
		}
	}
	
	function backup()
	{
		$this->load->dbutil();
		$prefs = array(
			'format'      => 'txt',             // gzip, zip, txt
			'add_drop'    => FALSE,              // Whether to add DROP TABLE statements to backup file
			'add_insert'  => TRUE,              // Whether to add INSERT data to backup file
			'newline'     => "\n"               // Newline character used in backup file
    	);
		$backup =&$this->dbutil->backup($prefs);
		$backup = 'SET FOREIGN_KEY_CHECKS = 0;'."\n".$backup."\n".'SET FOREIGN_KEY_CHECKS = 1;';
		force_download('foreup_database.sql', $backup);
	}
}
?>