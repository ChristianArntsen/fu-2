<?php
require_once("report.php");
class Summary_tee_times extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array(array('data'=>lang('reports_tee_time_stat'), 'align'=> 'left'), array('data'=>lang('reports_total'), 'align'=> 'right'));
	}
	
	public function getData()
	{
		$teesheet_id = $this->params["teesheet_id"];
        $statsArray = array();
        $dayString = date('Ymd0000', strtotime($this->params['start_date']))-1000000;
        $edayString = date('Ymd2399', strtotime($this->params['end_date']))-1000000;
        $now = date('YmdHi')-1000000;
		$this->db->select("holes,
        	count(*) AS teetimes,
        	sum(case WHEN `status` = 'checked in' OR `status` = '' THEN 1 ELSE 0 END) AS tcheckin,
        	sum(case WHEN `status` = 'walked in' THEN 1 ELSE 0 END) AS twalkin,
        	sum(`player_count`) AS players,
        	SUM(CASE WHEN start < $now  AND paid_player_count < player_count THEN player_count - paid_player_count ELSE 0 END) AS no_show_tee_times, 
			SUM(CASE WHEN paid_player_count > player_count THEN player_count ELSE paid_player_count END) AS cplayers,
        	sum(case WHEN (`status` = 'checked in' OR `status` = 'walked in' OR `status` = '') THEN `paid_carts` ELSE 0 END) AS ccarts,
        	sum(`carts`) AS carts");
        $this->db->from('teetime');
        $this->db->where("teesheet_id = '$teesheet_id'
            AND `start` > '$dayString' 
            AND `start` < '$edayString'
            AND `type` = 'teetime'
            AND `status` != 'deleted'
            AND `TTID` NOT LIKE '____________________b'
            GROUP BY holes");
        //$this->db->group_by('holes');
        
        $results = $this->db->get();
		
		return $results->result_array();
	}
	public function getSummaryData()
	{
	}
}
?>