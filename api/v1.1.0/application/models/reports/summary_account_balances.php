<?php
require_once("report.php");
class Summary_account_balances extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		$customer_account_balance = ($this->config->item('customer_credit_nickname') ? $this->config->item('customer_credit_nickname'):lang('customers_account_balance'));
		$member_account_balance = ($this->config->item('member_balance_nickname') ? $this->config->item('member_balance_nickname'):lang('customers_member_account_balance'));
		return array(array('data'=>lang('giftcards_customer_name'), 'align'=>'left'), array('data'=>$customer_account_balance, 'align'=> 'left'), array('data'=>$member_account_balance, 'align'=> 'left'));
	}
	
	public function getData()
	{
		if (!$this->permissions->is_super_admin())
            $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->select('IF(account_balance<0, CONCAT("<span style=\'color:red\'>$", FORMAT(account_balance,2), "</span>"), CONCAT("<span>$", FORMAT(account_balance,2), "</span>")) as account_balance, IF(member_account_balance<0, CONCAT("<span style=\'color:red\'>$", FORMAT(member_account_balance,2), "</span>"), CONCAT("<span>$", FORMAT(member_account_balance,2), "</span>")) as member_balance, CONCAT(last_name, ", ", first_name) as customer_name', false);
		$this->db->from('customers');
		$this->db->where('deleted', 0);
		$this->db->join('people', 'customers.person_id = people.person_id', 'left');
		$this->db->order_by('last_name, first_name');

		return $this->db->get()->result_array();		
	}
	
	public function getSummaryData()
	{
		if (!$this->permissions->is_super_admin())
            $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->select('sum(IF(account_balance > 0, account_balance, 0)) as positive_balance, sum(IF(account_balance < 0, account_balance, 0)) as negative_balance, sum(account_balance) AS net, sum(IF(member_account_balance > 0, member_account_balance, 0)) as positive_member_balance, sum(IF(member_account_balance < 0, member_account_balance, 0)) as negative_member_balance, sum(member_account_balance) AS mnet', false);
		$this->db->from('customers');
		$this->db->where('deleted', 0);
		$this->db->join('people', 'customers.person_id = people.person_id', 'left');
	
		$results = $this->db->get()->result_array();		
		return $results[0];
	}
}
?>