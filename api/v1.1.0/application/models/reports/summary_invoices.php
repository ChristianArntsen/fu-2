<?php
require_once("report.php");
class Summary_invoices extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array(
			array('data'=>lang('invoices_invoice_number'), 'align'=>'left'), 
			array('data'=>lang('invoices_invoice_date_billed'), 'align'=> 'left'), 
			array('data'=>lang('invoices_invoice_customer_name'), 'align'=> 'left'), 
			array('data'=>lang('invoices_invoice_total'), 'align'=> 'right'), 
			array('data'=>lang('invoices_invoice_paid'), 'align'=> 'right'),
			array('data'=>lang('invoices_invoice_amount_due'), 'align'=> 'right')
		);
	}	
	public function getData()
	{
		
		$this->db->select('invoice_id, invoice_number, month_billed as date, CONCAT(first_name, " ",last_name) as customer_name, total, paid, total-paid as due', false);
		$this->db->from('invoices');
		$this->db->where('deleted', 0);
		// $this->db->where("date >= '{$this->params['start_date']}'");
		// $this->db->where("date < '{$this->params['end_date']}'");
		$this->db->where("month_billed >= '{$this->params['start_date']}'");
		$this->db->where("month_billed < '{$this->params['end_date']}'");
		$this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->join('people', 'invoices.person_id = people.person_id', 'left');
		$this->db->order_by('invoice_number');
		$result = $this->db->get()->result_array(); 
		
		return	$result;
	}
	
	public function getSummaryData()
	{
		$this->db->select('SUM(total) as total_invoice_value, SUM(paid) as paid_invoice_value, SUM(total-paid) as due_invoice_value', false);
		$this->db->from('invoices');
		$this->db->where('deleted', 0);
		// $this->db->where("date >= '{$this->params['start_date']}'");
		// $this->db->where("date < '{$this->params['end_date']}'");
		$this->db->where("month_billed >= '{$this->params['start_date']}'");
		$this->db->where("month_billed < '{$this->params['end_date']}'");
		$this->db->where('course_id', $this->session->userdata('course_id'));        

		return $this->db->get()->row_array();		
	}
}
?>