<?php
class Credit_card extends CI_Model
{
	function get_info($credit_card_id)
	{
		$this->db->from('billing_credit_cards');
		$this->db->where('credit_card_id', $credit_card_id);
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('billing_credit_cards');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}	
	}
	function get($course_id)
	{
		$this->db->from('billing_credit_cards');
		$this->db->where('course_id', $course_id);
		$this->db->where('deleted != ', 1);
		$this->db->where('token !=', '');
		return $this->db->get()->result_array();
	}
	function save($credit_card_data, $credit_card_id = -1)
	{
		if ($credit_card_id == -1)
		{
			$this->db->insert('billing_credit_cards', $credit_card_data);
		}
		else {
			$this->db->where('credit_card_id', $credit_card_id);
			$this->db->update('billing_credit_cards', $credit_card_data);
		}
	}
	function record_charges($credit_card_id, $items = '', $totals = '', $billing_id = '')
	{
		//echo "<br/><br/>Recording Charges<br/><br/>";
		$items = ($items != '')?$items:$this->get_charge_items();
		$totals = ($totals != '')?$totals:$this->get_charge_totals();
		//print_r($items);
		//echo "<br/>Totals<br/>";
		//print_r($totals);
		
		$charge_data = array(
			'credit_card_id'=>$credit_card_id,
			'billing_id'=>$billing_id,
			'person_id'=>$this->session->userdata('person_id'),
			'total'=>$totals['total']
		);
		if ($this->db->insert('billing_charges', $charge_data))
			$charge_data['charge_id'] = $this->db->insert_id();
		
		
		$charge_items_data = array();
		foreach ($items as $item)
		{
			$charge_items_data[] = array(
				'charge_id'=>$charge_data['charge_id'],
				'line_number'=>$item['line_number'],
				'description'=>$item['description'],
				'amount'=>$item['amount']
			);
		}
		$this->db->insert_batch('billing_charge_items', $charge_items_data);
		$this->clear_charge_items();
		
		return true;
	}
	function delete($credit_card_id)
	{
		$this->db->where('credit_card_id', $credit_card_id);
		return $this->db->update('billing_credit_cards', array('deleted'=>1));
	}
	function add_charge_item($description, $amount) {
		$items = ($this->session->userdata('course_charge_items'))?$this->session->userdata('course_charge_items'):array();
		$maxkey = 0;
		foreach ($items as $index =>$item)
		{
            //We primed the loop so maxkey is 0 the first time.
            //Also, we have stored the key in the element itself so we can compare.
			if($maxkey <= $item['line_number'])
				$maxkey = $item['line_number'];
		}

		$insertkey=$maxkey+1;
		$items[$insertkey] = array('line_number'=>$insertkey,'description'=>$description, 'amount'=>$amount);
		$this->session->set_userdata('course_charge_items', $items);
		
		return array('line_number'=>$insertkey, 'totals'=>$this->get_charge_totals(), 'items'=>$this->session->userdata('course_charge_items'));
	}
	function delete_charge_item($line_number) {
		$items = $this->session->userdata('course_charge_items');
		unset($items[$line_number]);
		$this->session->set_userdata('course_charge_items', $items);
		return array('totals'=>$this->get_charge_totals());
	}
	function update_tax_rate($tax_rate){
		$this->session->set_userdata('course_charge_tax_rate', $tax_rate);
		return array('totals'=>$this->get_charge_totals());
	}
	function clear_charge_items() {
		$this->session->unset_userdata('course_charge_items');
	}
	function get_charge_items()
	{
		$items = $this->session->userdata('course_charge_items');
		return $items;
	}
	function get_charge_totals()
	{
		$subtotal = 0;
		$items = $this->session->userdata('course_charge_items');
		$tax_rate = $this->session->userdata('course_charge_tax_rate');
		foreach ($items as $item)
		{
			$subtotal += $item['amount'];
		}
		return array('subtotal'=>$subtotal, 'taxes'=>$subtotal*$tax_rate/100, 'total'=>$subtotal*(1+$tax_rate/100));
	}
}
	