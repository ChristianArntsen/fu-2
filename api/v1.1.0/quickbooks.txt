The Quickbooks sync feature will allow your ForeUp account
to sync all inventory items, customers, tax, and sales with your
Quickbooks desktop application automatically.

To begin, you are required to create a brand new company file to store the
ForeUp data. Open Quickbooks, in the top left click File >> New Company...
Set up your new company with the proper information and structure.

Next, you need to create a few extra accounts to store employee tips
and member credit transactions. Towards the top left, select
Lists >> Chart of Accounts. Within the chart of accounts
window select Account >> New toward the bottom left.

The first account we will creat will be the "Giftcards/Member Credit"
account. For account type select "Other Account Types", then in the drop
down choose "Other Current Liability" and click "Continue".

[new account image]

In the next window, for Account Name put "Giftcards/Member Credit", then
click "Save & New" at the bottom.

For the next account, change the dropdown at the top of the window to
"Income". The Account Name will be "Tips", then click
"Save & Close".



Step 1
---------
Download and install the QuickBooks Web Connector program. This is a
separate program which needs to be on the same computer with Quickbooks.
The Web Connector allows your QuickBooks application to
communicate with ForeUp over the internet.

	http://marketplace.intuit.com/webconnector/

Step 2
---------
Open your QuickBooks desktop application and log in to your company file.
First, we need to make sure sales tax is turned on (even if you don't charge sales tax).
In QuickBooks navigate to Edit >> Preferences, on the left choose
"Sales Tax". Then click the "Company Preferences" tab.

For "Do you charge sales tax?" select Yes. Next, click "Add sales
tax item..." For "Type" choose "Sales Tax Item". For "Sales Tax Name"
it MUST be just "Sales Tax". Place whatever you want in the Description.
Next, type a name in the "Tax Agency" field (IRS in the United States).
Then click OK for this new item. If the tax agency you entered is new, a
"Vendor Not Found" window will popup, you can select "Quick Add", or
"Set up" if you want to add more details to the vendor.

(Note: Make SURE you create and select a tax agency for each tax item
or it will cause errors when syncing!)

Next you need to add another tax item. This item will use the same
fields as above, but the name MUST be "Non". And the "Tax Rate" must
be 0.

Finally, in the Sales Tax tab for "Your most common sales tax item", select
"Non". Then click OK.

Step 3
---------
On the ForeUp software, navigate to Settings >> Misc. Click the "Enable
QuickBooks" button at the bottom. You should then see basic QuickBooks
settings appear, such as a username and password.

Step 4
---------
Click on "Download QWC File" and save this file to your computer. Once
the file has downloaded, locate it on your computer and double-click
it. A window titled "Authorize New Web Service" should pop up, click
"OK".

If this is your first time connecting ForeUp, another window will pop up
to allow permission to your company file. (Note: Make sure QuickBooks is
maximized to see the second pop up window.) In
this new window, select "Yes, always; allow access even if QuickBooks
is not running". Then click "Continue...". A second confirmation box
will popup, click "Done".

Step 5
---------
Open your QuickBooks Web Connector program. The ForeUp software should
be listed in the window. Click the text box in the "Password" column
on the right. Fill in your QWC password which is displayed
on the ForeUp Settings page, under Misc >> QuickBooks Integration >>
QWC Password. When done typing, press enter, then "Yes" in the
confirmation box.

Step 6
---------
In the QuickBooks Web Connector, check the box to the left of the ForeUp
entry. Next, click the button "Update Selected" near the top of the
window. This forces your QuickBooks application to synchronize with the
ForeUp software.

The first synchronization will only send your list of accounts to
ForeUp. Once the sync has completed, refresh your ForeUp settings page.
Navigate to the "Misc" tab again. You should now be able to select which
QuickBooks accounts you would like to use as your "Sales Account",
"Redemption Account" and "Tips Account". Once you have mapped each
account, click "Save".

Now that your accounts have been mapped, you can begin synchronizing
your ForeUp sales data with QuickBooks. You can force sync
the web connector manually (by using the method above), or wait for
15 minutes and let it sync automatically.

The initial sync may send alot of data and may take 15 - 30 minutes
to complete.

Step 7 - Optional
--------
If you want QuickBooks to sync without having QuickBooks open, you
need to enter the path to your QuickBooks company file into the
ForeUp settings. In ForeUp Settings >> Misc you will see "Company
File Location". Place the full path to your company file here, such
as "C:\Users\Joe\Documents\MyCompanyFile.QBW" Make sure to include
alls paces in your path. Once the path is entered, click Save.