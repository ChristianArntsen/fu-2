<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Email Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/helpers/email_helper.html
 */

// ------------------------------------------------------------------------

/**
 * Validate email address
 *
 * @access	public
 * @return	bool
 */
if ( ! function_exists('valid_email'))
{
	function valid_email($address)
	{
		return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $address)) ? FALSE : TRUE;
	}
}

// ------------------------------------------------------------------------

/**
 * Send an email
 *
 * @access	public
 * @return	bool
 */
if ( ! function_exists('send_email'))
{
	function send_email($recipient, $subject = 'Test email', $message = 'Hello World')
	{
		echo 'just sent mail';
		return mail($recipient, $subject, $message);
	}
}
if ( ! function_exists('url_encode'))
{
	function url_encode($string) {
        //$string = str_replace(' ', '', $string);
        $replacements= array('%26', '%27', '%22', '%21', '%2A', '%28', '%29', '%3B', '%3A', '%40', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%23', '%5B', '%5D');
        $entities = array('&', "'", '"', '!', '*', '(', ')', ';', ':', '@', '=', '+', '$', ',', '/', '?', '#', '[', ']');
        return str_replace($entities, $replacements, ($string));
    }
}
if ( ! function_exists('send_sendgrid'))
{
	function send_sendgrid($to, $subject, $message, $from = '', $fromName = '', $campaign_id = 0, $course_id = false)
	{
		$success = true;
		$errors = 0;
		$from = ($from == '')?'no-reply@foreup.com':$from;
		$fromName = ($fromName == '')?'ForeUP':$fromName;

		$to_text = '';
		$bcc_text = '';
		if (!is_array($to) && $to == '')
		{
			send_sendgrid_email('to=jhopkins@foreup.com&', "Error - No recipients", url_encode("To: $to<br/><br/>From: $from<br/><br/>Subject: $subject<br/><br/>Message: $message<br/><br/>Errors: "), "", $from, 'ForeUP Error');
			$errors++;
			$success = false;
		}
		else if (is_array($to))
		{
			$chunked_to = array_chunk($to['to'], 800);
			$chunked_sub_customer = array_chunk($to['sub']['%customer_id%'], 800);
			$chunked_sub_campaign = array_chunk($to['sub']['%campaign_id%'], 800);

			// Separate each group by 800
			foreach($chunked_to as $key => $chunk)
			{
				//$to_text = 'jhopkins@foreup.com';
				$to_text = 'joebush08@gmail.com';
				$bcc_text = '';
				$xsmtpapi = json_encode(array('to'=>$chunked_to[$key], 'sub'=>array('%customer_id%'=>$chunked_sub_customer[$key], '%campaign_id%'=>$chunked_sub_campaign[$key])));

				$result = json_decode(send_sendgrid_email($to_text, $subject, $message, $xsmtpapi, $from, $fromName));

				log_message("error", 'email_helper send_sendgrid recipients - '.json_encode($chunk));
				log_message("error", 'email_helper send_sendgrid campaign_id - '.$campaign_id.' response '.json_encode($result));

				if ($result->message == 'error')
				{
					$errors = '';
					foreach($result->errors as $error)
						$errors .= $error;
					send_sendgrid_email('to=jhopkins@foreup.com&', "Error - Sendgrid reported error array", url_encode("To: ".implode(', ', $chunk)."<br/><br/>From: $from<br/><br/>Subject: $subject<br/><br/>Errors: $errors<br/><br/>Message: $message"), "", $from, 'ForeUP Error');
					$errors++;
					$success = false;
				}
			}
		}
		else
		{
			$to_text = "to=$to&";
			$result = json_decode(send_sendgrid_email($to_text, $subject, $message, $bcc_text, $from, $fromName));
			//$result = json_decode('{"message":"error","errors":["error messages"]}');
			if ($result->message == 'error')
			{
				$errors = '';
				foreach($result->errors as $error)
					$errors .= $error;
				send_sendgrid_email('to=jhopkins@foreup.com&', "Error - Sendgrid reported error individual", url_encode("To: $to<br/><br/>From: $from<br/><br/>Subject: $subject<br/><br/>Errors: $errors<br/><br/>Message: $message"), "", $from, 'ForeUP Error');
				$errors++;
				$success = false;
			}
		}
		$CI = &get_instance();
		$CI->load->model('Communication');
		$CI->Communication->record('Email', count($to), $from, $campaign_id, $course_id);

		return array('success'=>$success, 'errors'=>$errors);
	}
}

if ( ! function_exists('send_sendgrid_email'))
{
	function send_sendgrid_email($to_text, $subject, $message, $xsmtpapi, $from, $fromName)
	{
		//echo "api_user=foreup&api_key=GolfCompete#17&{$to_text}{$bcc_text}subject=$subject&html=$message&from=$from&fromname=$fromName<br/>";
		$ch2=curl_init('https://sendgrid.com/api/mail.send.json');
        curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch2, CURLOPT_POST, 1);

		$post_fields = array(
			'api_user' => 'foreup',
			'api_key' => 'GolfCompete#17',
			'to' => $to_text,
			'subject' => $subject,
			'html' => $message,
			'x-smtpapi' => $xsmtpapi,
			'from' => $from,
			'fromname' => $fromName
		);
		curl_setopt($ch2, CURLOPT_POSTFIELDS, $post_fields);
        //curl_setopt($ch2, CURLOPT_POSTFIELDS,"api_user=foreup&api_key=GolfCompete#17&{$to_text}".url_encode($bcc_text)."subject=$subject&html=$message&from=$from&fromname=$fromName");
		curl_setopt($ch2, CURLOPT_HEADER, false);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);

		$data2 = curl_exec($ch2);
		//print_r($data2);
		return $data2;
    }
}

/* End of file email_helper.php */
/* Location: ./system/helpers/email_helper.php */