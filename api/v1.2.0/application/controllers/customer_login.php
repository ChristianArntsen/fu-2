<?php
/*************************************************
 * change   author    date          remarks
 * 0.1      MPogay   08-JUN-2012    add validation for user
 **************************************************/
class Customer_login extends CI_Controller
{
  private $mode;

	function __construct()
	{
		parent::__construct();
		$this->load->library('encrypt');
		$this->load->model('User');
    }

	function index()
	{
		redirect(site_url('be/reservations'));
	}

	function login_check($username)
	{
		$password = $this->input->post("password");
		$valid = $this->Customer->login($username,$password);

		if(!$valid)
		{
			$this->form_validation->set_message('login_check', lang('login_invalid_username_and_password'));
			return false;
		}
		return true;
	}

	function reset_password()
	{
		$this->load->view('customers/login/reset_password');
	}

	function login()
	{
		$this->load->view('customers/login/login');
	}

	function do_reset_password_notify()
	{
		$username = $this->input->post('username');
		$reset_data = array();
		if ($username)
		{
			$customer = $this->User->get_by_username($this->input->post('username'));
			$course_id = $this->input->post('course_id');
			if ($customer)
			{
				$data = array();
				$data['customer'] = $customer;
			    $data['reset_key'] = base64url_encode($this->encrypt->encode($customer->person_id.'|'.(time() + (2 * 24 * 60 * 60))));
				if ($course_id)
					$data['course'] = $this->Course->get_info($course_id);
	//print_r($customer);
				if ($customer->email != '')
				{
					$reset_data['success'] = true;
					send_sendgrid(
						$customer->email,
						lang('login_reset_password'),
						$this->load->view("customers/login/reset_password_email",$data, true),
						'support@foreup.com',
						($data['course'] ? $data['course']->name : 'ForeUP Support')
					);
				}
			}
		}
		else {
			redirect(site_url('customer_login/reset_password'));
		}
		$this->load->view('customers/login/do_reset_password_notify', $reset_data);
	}

	function reset_password_enter_password($key=false)
	{
		if ($key)
		{
			$data = array();
		    list($customer_id, $expire) = explode('|', $this->encrypt->decode(base64url_decode($key)));
			if ($customer_id && $expire && $expire > time())
			{
				//echo 'cid '.$customer_id.' - '.$expire.'<br/>';
				$customer = $this->Customer->get_info($customer_id);
				//print_r($customer);
				$data['username'] = '';//$customer->username;
				$data['key'] = $key;
				$this->load->view('customers/login/reset_password_enter_password', $data);
			}
		}
	}

	function do_reset_password($key=false)
	{
		if ($key)
		{
	    	list($customer_id, $expire) = explode('|', $this->encrypt->decode(base64url_decode($key)));

			if ($customer_id && $expire && $expire > time())
			{
				$password = $this->input->post('password');
				$confirm_password = $this->input->post('confirm_password');

				if (($password == $confirm_password) && strlen($password) >=8)
				{
					if ($this->User->update_password($customer_id, md5($password)))
					{
						$this->load->view('customers/login/do_reset_password');
					}
				}
				else
				{
					$data = array();
					$customer = $this->User->get_info($customer_id);
					$data['username'] = $customer->email;
					$data['key'] = $key;
					$data['error_message'] = lang('login_passwords_must_match_and_be_at_least_8_characters');
					$this->load->view('customers/login/reset_password_enter_password', $data);
				}
			}
		}
	}
}
?>