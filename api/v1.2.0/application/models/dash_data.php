<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dash_data extends CI_Model
{
	public $metrics;
	public $categories;

	function __construct(){
		parent::__construct();

		$this->metrics = array(
		'revenue' => array(
			'profit' 				=> array('name'=>'Profit', 'number_type'=>'money', 'grouped_by' => 'time'),
			'revenue' 				=> array('name'=>'Revenue', 'number_type'=>'money', 'grouped_by' => 'time')
		),
		'tee_time' => array(
			'tee_times' 			=> array('name'=>'Tee Times', 'number_type'=>'number', 'grouped_by' => 'time'),
			'paid_tee_times' 		=> array('name'=>'Paid Tee Times', 'number_type'=>'number', 'grouped_by' => 'time'),
			'players' 				=> array('name'=>'Players', 'number_type'=>'number', 'grouped_by' => 'time'),
			'paid_players' 			=> array('name'=>'Paid Players', 'number_type'=>'number', 'grouped_by' => 'time'),
			'paid_carts' 			=> array('name'=>'Paid Carts', 'number_type'=>'number', 'grouped_by' => 'time')
		),
		'payments_processed' => array(
			'ets' 					=> array('name'=>'ETS', 'number_type'=>'money', 'grouped_by' => 'time'),
			'mercury' 				=> array('name'=>'Mercury', 'number_type'=>'money', 'grouped_by' => 'time')
		),
		'bartered_teetime_sales' => array(
			'amount' 				=> array('name'=>'Bartered Teetime Sales', 'number_type'=>'money', 'grouped_by' => 'time')
		));

		$this->categories = array(
			'revenue' => 'Revenue',
			'tee_time' => 'Tee Time'
		);

		$this->formats = array(
			'hour' => 'ga',
			'day' => 'j',
			'dayname' => 'l',
			'week' => 'j',
			'month' => 'M',
			'year' => 'Y'
		);

		$this->course_id = $this->session->userdata('course_id');
	}

	function get_metrics(){
		return $this->metrics;
	}

	function get_categories(){
		return $this->categories;
	}

	// Returns date string based on relative period passed
	function get_dates($period, $period_num){

		$start = new DateTime();
		$start->setTime(0, 0, 0);

		$end = new DateTime();
		$end->setTime(23, 59, 59);

		if($period == 'day'){
			$start->modify($period_num.' '.$period.'s');
			$end->modify($period_num.' '.$period.'s');

		}else if($period == 'week'){
			$start->setISODate($start->format('Y'), (int) $start->format('W') - abs($period_num), 1);
			$end->setISODate($end->format('Y'), (int) $end->format('W')- abs($period_num), 7);

		}else if($period == 'month'){
			$start->setDate($start->format('Y'), $start->format('m') - abs($period_num), 1);
			$end->setDate($end->format('Y'), $end->format('m') - abs($period_num), $start->format('t'));

		}else if($period == 'year'){
			$start->setDate($start->format('Y') - abs($period_num), 1, 1);
			$end->setDate($end->format('Y') - abs($period_num), 12, 31);
		}

		$startDate = $start->format('Y-m-d');
		$endDate = $end->format('Y-m-d');

		return array('start' => $startDate, 'end' => $endDate);
	}

	function fetch_data($metric_category, $metrics, $period, $period_num, $group_by, $then_by, $categories, $widget_type, $start = false, $end = false, $filters = array(), $filter_by_course = true)
	{
		if($start === false || $end === false){
			$dates = $this->get_dates($period, $period_num);
			$start = $dates['start'];
			$end = $dates['end'];
		}

		switch($metric_category){
			case 'tee_time':
				$rows = $this->tee_time($metrics, $group_by, $start, $end);
			break;
			case 'bartered_teetime_sales':
				$rows = $this->bartered_teetime_sales($metrics, $group_by, $start, $end);
			break;
			case 'revenue':
				if($metrics[0] == 'revenue' || $metrics[0] == 'profit'){
					$rows = $this->revenue($metrics, $group_by, $start, $end, $filters, $filter_by_course);

				}else if($metrics[0] == 'teetime_groups'){
					$rows = $this->revenue_by_teetime_group($metrics, $group_by, $start, $end);

				}else if($metrics[0] == 'top_items'){
					$rows = $this->revenue_by_top_items($metrics, $group_by, $start, $end);
				}
			break;
			case 'payments_processed':
				$rows = $this->payments_processed($metrics, $group_by, $start, $end, $filters, $filter_by_course);
			break;
		}

		if(empty($categories) && !in_array($group_by, array('hour','day','dayname','week','month','year'))){
			$categories = $this->create_categories($rows, $group_by);
		}

		return $this->structure_results($metric_category, $rows, $metrics, $group_by, $then_by, $categories, $widget_type, $period, $period_num);
	}

	function create_time_categories($type, $start, $end)
	{
		$cat_data = array();
		$format = $this->formats[$type];

		if($type == 'year' || empty($start) || empty($end)){
			$query = $this->db->query("SELECT DATE(sale_time) AS date FROM ".$this->db->dbprefix('sales')." ORDER BY sale_time ASC LIMIT 1");
			$row = $query->row_array();

			$start = $row['date'];
			$end = date('Y-m-d');
			$time_unit = '+1 year';
		}
		else if ($type == 'dayname')
		{
			// we just want mon - sun here
			$time_unit = ' +1 day';
			$start = date('Y-m-d', strtotime('last Monday', strtotime($start)));
			$end = date('Y-m-d', strtotime($start." + 7 days"));
		}
		else if ($type == 'month')
		{
			// This accounts for the +1 month error when you happen to start on the 31st of the current month
			$time_unit = ' first day of next month';
		}
		else if ($type == 'hour')
		{
			$time_unit = ' +1 '.$type;
			//$start .= ' 00:00:00';
			$end = date('Y-m-d H:i:s', strtotime($start.'+ 23 hours'));
		}
		else if ($type == 'day')
		{
			$time_unit = ' +1 '.$type;
			$start = date('Y-m-d 00:00:00', strtotime($start));
			$end = date('Y-m-d 00:00:00', strtotime($end));
		}
		else if ($type == 'total')
		{
			return array(0 => 'total');
		}
		else
		{
			//default increment...
			$time_unit = ' +1 '.$type;
		}

		for ($i = $start; $i <= $end; $i = date('Y-m-d H:i:s', strtotime($i.' '.$time_unit)))
		{
			$cat_data[] = date($format, strtotime($i));
		}

		if($type == 'year'){
			$cat_data[] = date($format, strtotime($i + 1));
		}

		return $cat_data;
	}

	function create_categories($rows, $key){
		$categories = array();
		foreach($rows as $row){
			$categories[] = $row[$key];
		}

		return $categories;
	}

	// Structures data retrieved from database to be sent to charts
	function structure_results($metric_category, $rows, $metrics, $group_by, $then_by, $categories, $widget_type, $period = null, $period_num = 0){

		$return_data = array();
		$chart_data = array();
		$categorized_rows = array();
		$group_by_columns = array();

		$name_addition = '';
		$date = new DateTime();
		switch($period){
			case 'year':
				$date->modify($period_num.' years');
				$name_addition = '('.$date->format('Y').')';
			break;
			case 'month':
				$date->modify($period_num.' months');
				$name_addition = '('.$date->format('M') .' '.$date->format('Y').')';
			break;
			case 'day':
				$date->modify($period_num.' days');
				$name_addition = '('.$date->format('M') .' '.$date->format('jS').')';
			break;
			default:
				$name_addition = '';
			break;
		}

		$group_by_time = 'true';

		// Loop through rows of data and re-key the data by date (category)
		foreach($rows as $key => $row){
			$date = new DateTime();

			switch($group_by){
				case 'hour':
					$date->setTime($row['hour'], 0, 0);
					$newKey = $date->format($this->formats[$group_by]);
				break;
				case 'day':
					$date->setDate($row['year'], 1, $row['day']);
					$newKey = $date->format($this->formats[$group_by]);
				break;
				case 'dayname':
					$newKey = $row['dayname'];
				break;
				case 'week':
					$date->setISODate($row['year'], $row['week'], 1);
					$newKey = $date->format($this->formats[$group_by]);
				break;
				case 'month':
					$date->setDate($row['year'], $row['month'], 1);
					$newKey = $date->format($this->formats[$group_by]);
				break;
				case 'year':
					$date->setDate($row['year'], 1, 1);
					$newKey = $date->format($this->formats[$group_by]);
				break;
				case 'total':
					$newKey = 'total';
				break;
				default:
					$newKey = $row[$group_by];
					$group_by_time = false;
				break;
			}

			if(!empty($then_by)){
				$newKey .= ',' . $row[$then_by];
				$group_by_columns[$row[$then_by]] = $row[$then_by];
			}

			if(!isset($categorized_rows[$newKey])){
				$categorized_rows[$newKey] = $row;
			}else{
				foreach($metrics as $metric){
					$categorized_rows[$newKey][$metric] += $row[$metric];
				}
			}
		}

		if(!empty($then_by)){
			$newCategories = array();
			foreach($group_by_columns as $col){
				foreach($categories as $category){
					$newCategories[] = $category.','.$col;
				}
			}
			$categories = $newCategories;
		}

		foreach($categories as $category){
			foreach($metrics as $column){

				if(!empty($then_by)){
					$columns = explode(',', $category);
					$return_data =& $chart_data[$columns[1]];
					$return_data['name'] = $columns[1];
					$return_data['number_type'] = $this->metrics[$metric_category][$column]['number_type'];

				}else if($group_by_time){
					$return_data =& $chart_data[$column];
					$return_data['name'] = $this->metrics[$metric_category][$column]['name'].' '.$name_addition;
					$return_data['number_type'] = $this->metrics[$metric_category][$column]['number_type'];

				}else{
					if($group_by_time){
						$return_data =& $chart_data[$column];
					}else{
						$return_data =& $chart_data[$category];
					}
					$return_data['name'] = $category;
					$return_data['number_type'] = $this->metrics[$metric_category][$column]['number_type'];
				}

				if($widget_type == 'pie'){
					$return_data['y'] = (float) $categorized_rows[$category][$column];

				}else{
					// If there was data for that category (date), use it, otherwise use 0
					if(!empty($categorized_rows[$category])){
						$return_data['data'][] = (float) $categorized_rows[$category][$column];
					}else{
						$return_data['data'][] = 0;
					}
				}
			}
		}

		return array_values($chart_data);
	}

	function revenue($metrics, $group_by, $start, $end, $filters = null, $filter_by_course = true)
	{
		if ($group_by == 'hour' && !empty($start))
		{
			$end .= ' 23:59:59';
			$start .= ' 00:00:00';
		}
		$function = 'sum';
		$function_title = ($function == 'sum') ? 'Totals' : (($function == 'avg') ? 'Averages' : '');

		$this->load->model('reports/Summary_sales');
		$model = $this->Summary_sales;
		$model->setParams(array('start_date'=>$start, 'end_date'=>$end, 'sale_type' => 'sales', 'department' => 'all'));
		if($group_by != 'teetime_type')
		{
			$this->db->query("DROP TEMPORARY TABLE IF EXISTS ".$this->db->dbprefix('sales_items_temp'));
			$this->Sale->create_sales_items_temp_table(array('start_date'=>$start, 'end_date'=>$end, 'sale_type'=>'sales', 'department'=>'all', 'course_id'=>$this->course_id), $filter_by_course);
		}
		//echo $this->db->last_query();
		//echo '----------------';
		//$this->db->from('sales_items_temp');
		//$result = $this->db->get()->result_array();
		//print_r($result);
		//echo '----------------';
		if ($group_by == 'hour')
		{
			$this->db->from('sales_items_temp');
			$this->db->where('deleted', 0);
			$this->db->select("HOUR(sale_date) AS hour, YEAR(sale_date) AS year, $function(total) as revenue,$function(profit) as profit");
			$this->db->group_by('hour');
			$this->db->order_by('hour');
		}
		else if ($group_by == 'day')
		{
			$this->db->from('sales_items_temp');
			$this->db->where('deleted', 0);
			$this->db->select("DAYOFYEAR(sale_date) AS day, YEAR(sale_date) AS year, $function(total) as revenue,$function(profit) as profit");
			$this->db->group_by('year, day');
			$this->db->order_by('year, day');
		}
		else if ($group_by == 'dayname')
		{
			$this->db->from('sales_items_temp');
			$this->db->where('deleted', 0);
			$this->db->select("DAYNAME(sale_date) AS dayname, WEEKDAY(sale_date) AS weekday, YEAR(sale_date) AS year, $function(total) as revenue,$function(profit) as profit");
			$this->db->group_by('dayname');
			$this->db->order_by('weekday');
		}
		else if ($group_by == 'week')
		{
			$this->db->from('sales_items_temp');
			$this->db->where('deleted', 0);
			$this->db->select("WEEK(sale_date) AS week, YEAR(sale_date) AS year, $function(total) as revenue,$function(profit) as profit");
			$this->db->group_by('year, week');
			$this->db->order_by('year, week');
		}
		else if ($group_by == 'month')
		{
			$this->db->from('sales_items_temp');
			$this->db->where('deleted', 0);
			$this->db->select("MONTH(sale_date) AS month, YEAR(sale_date) AS year, $function(total) as revenue,$function(profit) as profit");
			$this->db->group_by('year, month');
			$this->db->order_by('year, month');
		}
		else if ($group_by == 'year')
		{
			$this->db->from('sales_items_temp');
			$this->db->where('deleted', 0);
			$this->db->select("YEAR(sale_date) AS year, $function(total) as revenue,$function(profit) as profit");
			$this->db->group_by('year');
			$this->db->order_by('year');
		}
		else if ($group_by == 'total')
		{
			$this->db->from('sales_items_temp');
			$this->db->where('deleted', 0);
			$this->db->select("YEAR(sale_date) AS year, $function(total) as revenue,$function(profit) as profit");
		}
		else if($group_by == 'teetime_type')
		{
			// $this->db->select("IF(s.subcategory = '', 'No Type', s.subcategory) AS `teetime_type`, ROUND(SUM(s.subtotal), 2) AS revenue,
				// ROUND(SUM(s.profit), 2) AS profit", false);
			// $this->db->from('sales_items_temp AS s');
			// $this->db->where('s.deleted', 0);
			// $this->db->where("s.teetime_id != ''");
			// $this->db->group_by('s.subcategory');
			// $this->db->order_by('revenue DESC');
			// $this->db->limit(5);


			$this->db->select("price_category as `teetime_type`, ROUND(SUM(s.subtotal),2) AS revenue, ROUND(SUM(s.profit),2) AS profit", false);
			$this->db->from('sales_items AS s');
			$this->db->join('sales', 'sales.sale_id = s.sale_id');
			$this->db->where('sales.deleted', 0);
			$this->db->where('s.price_category !=', '');
			$this->db->where('sales.course_id', $this->course_id);
			$this->db->group_by('s.price_category');
			$this->db->order_by('revenue DESC');
			$this->db->limit(5);
		}
		else if($group_by == 'department')
		{
			$this->db->select("IF(department = '', '- No Department -', department) AS department,
				ROUND(SUM(subtotal), 2) AS revenue, ROUND(SUM(profit), 2) AS profit", false);
			$this->db->from('sales_items_temp');
			$this->db->where('deleted', 0);
			$this->db->group_by('department');
			$this->db->order_by('revenue DESC');
			$this->db->limit(5);
		}
		else if($group_by == 'item')
		{
			$this->db->select("i.name AS `item`, ROUND(SUM(s.subtotal), 2) AS revenue, ROUND(SUM(s.profit), 2) AS profit", false);
			$this->db->from('sales_items_temp AS s');
			$this->db->join('items AS i', 'i.item_id = s.item_id', 'inner');
			$this->db->where('s.deleted', 0);
			$this->db->group_by('s.item_id');
			$this->db->order_by('revenue DESC');
			$this->db->limit(10);
		}

		if(!empty($filters)){
			if(in_array('teetimes', $filters)){
				$this->db->where("teetime_id != ''");
			}
			if(in_array('bartered_teetimes', $filters)){
				$this->db->join('teetimes_bartered', 'teetimes_bartered.teetime_id = sales_items_temp.teetime_id', 'inner');
				$this->db->where("sales_items_temp.teetime_id != ''");
			}
		}

		$report_data = $this->db->get();
		//echo ($this->db->last_query());
		$rows = $report_data->result_array();

		if($group_by == 'teetime_type'){


		}
		return $rows;
	}

	function payments_processed($metrics, $group_by, $start, $end, $filters = null, $filter_by_course = true)
	{
		if(!empty($start) && !empty($end)){
			$end .= ' 23:59:59';
			$start .= ' 00:00:00';
		}

		$select = "IF(mercury_id != '', SUM(amount), 0) AS mercury, IF(ets_id != '', SUM(amount), 0) AS ets";

		if ($group_by == 'hour')
		{
			$select = "HOUR(trans_post_time) AS hour, YEAR(trans_post_time) AS year";
			$sql_group_by = 'hour';
			$sql_order_by = 'hour';
		}
		else if ($group_by == 'day')
		{
			$select = "DAYOFYEAR(trans_post_time) AS day, YEAR(trans_post_time) AS year";
			$sql_group_by = 'year, day';
			$sql_order_by = 'year, day';
		}
		else if ($group_by == 'dayname')
		{
			$select = "DAYNAME(trans_post_time) AS dayname, WEEKDAY(trans_post_time) AS weekday, YEAR(trans_post_time) AS year";
			$sql_group_by = 'dayname';
			$sql_order_by = 'weekday';
		}
		else if ($group_by == 'week')
		{
			$select = "WEEK(trans_post_time) AS week, YEAR(trans_post_time) AS year";
			$sql_group_by = 'year, week';
			$sql_order_by = 'year, week';
		}
		else if ($group_by == 'month')
		{
			$select = "MONTH(trans_post_time) AS month, YEAR(trans_post_time) AS year";
			$sql_group_by = 'year, month';
			$sql_order_by = 'year, month';
		}
		else if ($group_by == 'year')
		{
			$select = "YEAR(trans_post_time) AS year";
			$sql_group_by = 'year';
			$sql_order_by = 'year';
		}

		$where = '1 = 1';
		if(!empty($start) || !empty($end)){
			$where = "trans_post_time BETWEEN '".$start."' AND '".$end."'";
		}

		if(!empty($sql_group_by)){
			$sql_group_by = "GROUP BY $sql_group_by";
		}
		if(!empty($sql_order_by)){
			$sql_order_by = "ORDER BY $sql_order_by";
		}

		$report_data = $this->db->query("(SELECT ROUND(SUM(amount),2) AS ets, 0 AS mercury, $select
			FROM ".$this->db->dbprefix('sales_payments_credit_cards')."
			WHERE $where AND trans_post_time != '0000-00-00 00:00:00'
				AND ets_id IS NOT NULL AND ets_id != ''
			$sql_group_by)
			UNION ALL
			(SELECT 0 AS ets, ROUND(SUM(amount),2) AS mercury, $select
			FROM ".$this->db->dbprefix('sales_payments_credit_cards')."
			WHERE $where AND trans_post_time != '0000-00-00 00:00:00'
				AND mercury_id IS NOT NULL AND mercury_id != ''
			$sql_group_by)
			$sql_order_by");

		$rows = $report_data->result_array();

		if($group_by == 'total'){
			$new_row = array('ets'=>0,'mercury'=>0);
			foreach($rows as $key => $row){
				$new_row['ets'] += (float) $row['ets'];
				$new_row['mercury'] += (float) $row['mercury'];
			}
			return array($new_row);
		}

		return $rows;
	}

	function bartered_teetime_sales($metrics, $group_by, $start, $end, $filters = null, $filter_by_course = true)
	{
		if(!empty($start) && !empty($end)){
			$end .= ' 23:59:59';
			$start .= ' 00:00:00';
		}

		$this->db->select("SUM(amount) AS amount, ".$this->db->dbprefix('courses').".name AS course", false);

		if ($group_by == 'hour')
		{
			$select = "HOUR(trans_post_time) AS hour, YEAR(trans_post_time) AS year";
			$sql_group_by = 'hour';
			$sql_order_by = 'hour';
		}
		else if ($group_by == 'day')
		{
			$select = "DAYOFYEAR(trans_post_time) AS day, YEAR(trans_post_time) AS year";
			$sql_group_by = 'year, day';
			$sql_order_by = 'year, day';
		}
		else if ($group_by == 'dayname')
		{
			$select = "DAYNAME(trans_post_time) AS dayname, WEEKDAY(trans_post_time) AS weekday, YEAR(trans_post_time) AS year";
			$sql_group_by = 'dayname';
			$sql_order_by = 'weekday';
		}
		else if ($group_by == 'week')
		{
			$select = "WEEK(trans_post_time) AS week, YEAR(trans_post_time) AS year";
			$sql_group_by = 'year, week';
			$sql_order_by = 'year, week';
		}
		else if ($group_by == 'month')
		{
			$select = "MONTH(trans_post_time) AS month, YEAR(trans_post_time) AS year";
			$sql_group_by = 'year, month';
			$sql_order_by = 'year, month';
		}
		else if ($group_by == 'year')
		{
			$select = "YEAR(trans_post_time) AS year";
			$sql_group_by = 'year';
			$sql_order_by = 'year';
		}

		$this->db->select($select);
		$this->db->from("sales_payments_credit_cards");
		$this->db->join("teetimes_bartered", "teetimes_bartered.invoice_id = sales_payments_credit_cards.invoice", "inner");
		$this->db->join("teesheet", "teesheet.teesheet_id = teetimes_bartered.teesheet_id", "inner");
		$this->db->join("courses", "courses.course_id = teesheet.course_id", "inner");

		if(!empty($start) || !empty($end)){
			$this->db->where("trans_post_time BETWEEN '".$start."' AND '".$end."'");
		}
		$sql_group_by .= ",".$this->db->dbprefix('teetimes_bartered').".teesheet_id";

		if($group_by != 'total'){
			$this->db->group_by($sql_group_by);
			$this->db->order_by($sql_order_by);
		}
		$report_data = $this->db->get();

		$rows = $report_data->result_array();

		return $rows;
	}

	function revenue_by_top_items($metrics, $period, $period_num, $group_by)
	{
		$dates = $this->get_dates($period, $period_num);

		$start = $dates['start'].' 00:00:00';
		$end = $dates['end'].' 23:59:59';

		$this->load->model('reports/Summary_sales');
		$this->Summary_sales->setParams(array('start_date'=>$start, 'end_date'=>$end, 'sale_type' => $sale_type, 'department' => $department));

		$this->db->query("DROP TEMPORARY TABLE IF EXISTS ".$this->db->dbprefix('sales_items_temp'));
		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start, 'end_date'=>$end, 'sale_type' => $sale_type, 'department' => $department));

		$this->db->select("i.name, ROUND(SUM(s.subtotal), 2) AS total", false);
		$this->db->from('sales_items_temp AS s');
		$this->db->join('items AS i', 'i.item_id = s.item_id', 'inner');
		$this->db->where('s.deleted', 0);
		$this->db->group_by('s.item_id');
		$this->db->order_by('total DESC');
		$this->db->limit(10);

		$report_data = $this->db->get();
		$rows = $report_data->result_array();

		return $rows;
	}

	function teetime_sales($metrics, $group_by, $start, $end){

		$start = $dates['start'].' 00:00:00';
		$end = $dates['end'].' 23:59:59';

		$this->load->model('reports/Summary_sales');
		$this->Summary_sales->setParams(array('start_date'=>$start, 'end_date'=>$end, 'sale_type' => '', 'department' => ''));

		$this->db->query("DROP TEMPORARY TABLE IF EXISTS ".$this->db->dbprefix('sales_items_temp'));
		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start, 'end_date'=>$end, 'sale_type' => $sale_type, 'department' => $department));

		$this->db->select("i.name, ROUND(SUM(s.subtotal), 2) AS total", false);
		$this->db->from('sales_items_temp AS s');
		$this->db->join('items AS i', 'i.item_id = s.item_id', 'inner');
		$this->db->where('s.deleted', 0);
		$this->db->group_by('s.item_id');
		$this->db->order_by('total DESC');
		$this->db->limit(10);
	}

	function fetch_customer_data()
	{
		$get = $this->db->query("SELECT COUNT(*) as customers,
				SUM(CASE phone_number WHEN '' THEN 0 WHEN 0 THEN 0  ELSE 1 END) as texts,
				SUM(CASE email WHEN '' THEN 0 ELSE 1 END) as emails
			FROM (`foreup_customers`)
			JOIN `foreup_people` ON `foreup_customers`.`person_id` = `foreup_people`.`person_id`
			WHERE `course_id` = '9999' AND `deleted` = 0");
		$report_data = $get->result_array();

		$customers = $report_data[0]['customers'] ? (int)$report_data[0]['customers'] : 0;
		$emails = $report_data[0]['emails'] ? $report_data[0]['emails'] : 0;
		$texts = $report_data[0]['texts'] ? $report_data[0]['texts'] : 0;
		$percent_emails = (float)$emails / $customers ? number_format(((float)$emails / $customers), 3) : 0;
		$percent_texts = (float)$texts / $customers ? number_format(((float)$texts / $customers), 3) : 0;

		$data = array(
			'Total_Customers'=>$customers,
			'Percent_Emails'=>$percent_emails,
			'Percent_Texts'=>$percent_texts
		);

		return $data;
	}

	function fetch_tee_sheet_data()
	{
		$sale_type = 'sales';
		$department = 'all';
		$function = 'sum';
		$type = 'day';
		$start = $this->input->post('start') ? date('Y-m-d', strtotime($this->input->post('start'))) : date('Y-m-d');
		$end = $this->input->post('end') ? date('Y-m-d 23:59', strtotime($this->input->post('end'))) : date('Y-m-d H:i');
		//$start = date('Y-m-d', strtotime($this->input->post('start')));
		//$end = date('Y-m-dT23:59', strtotime($this->input->post('end')));
		$start_string = (date('Ymd', strtotime($start))-100).'0000';
		$end_string = (date('YmdHi', strtotime($end))-1000000);//only look at todays tee times up until the current time
		$now = date('YmdHi')-1000000;
		$teesheet_id = $this->session->userdata('teesheet_id');
		$get = $this->db->query("SELECT
				DAYOFYEAR(CONCAT(SUBSTRING(start+1000000, 1, 4),'-',SUBSTRING(start+1000000, 5, 2),'-',SUBSTRING(start+1000000, 7, 2))) AS day,
				SUBSTRING(start+1000000, 1, 4) AS year,
				COUNT(start) AS tee_times,
				SUM(CASE WHEN paid_player_count > 0 THEN 1 ELSE 0 END) AS paid_tee_times,
				SUM(CASE WHEN start < $now  AND paid_player_count < player_count THEN player_count - paid_player_count ELSE 0 END) AS no_show_tee_times,
				SUM(player_count) AS players,
				SUM(CASE WHEN paid_player_count > player_count THEN player_count ELSE paid_player_count END) AS paid_players,
				SUM(paid_carts) AS paid_carts
			FROM (`foreup_teetime`)
			LEFT JOIN foreup_teesheet ON foreup_teetime.teesheet_id = foreup_teesheet.teesheet_id
			WHERE `status` != 'deleted'
				AND foreup_teesheet.teesheet_id = $teesheet_id
				AND `start` > $start_string
				AND `start` < $end_string
				AND `type` = 'teetime'
				AND course_id = {$this->course_id}
				AND TTID LIKE '____________________'
			GROUP BY `year`, `day`
			ORDER BY `year`, `day`");
		$report_data = $get->result_array();
		$total_players = $report_data[0]['players'];
		$paid_players = (float)$report_data[0]['paid_players'];
		$no_show_players = $report_data[0]['no_show_tee_times'];//$total_players - $paid_players;
		$average_group_size = (float)$report_data[0]['paid_players'] / $report_data[0]['paid_tee_times'] ? (float)$report_data[0]['paid_players'] / $report_data[0]['paid_tee_times'] : 0;

		$data = array(
			'header'=> array(
				'Bookings'=>($total_players?$total_players:0),
				'Players_Checked_in'=>($paid_players?$paid_players:0),
				'Player_No_Shows'=>($no_show_players?$no_show_players:0)
			)
		);

		return $data;
	}
	function fetch_reservation_data()
	{
		$sale_type = 'sales';
		$department = 'all';
		$function = 'sum';

		$type = 'day';
		$start = $this->input->post('start') ? date('Y-m-d', strtotime($this->input->post('start'))) : date('Y-m-d');
		$end = $this->input->post('end') ? date('Y-m-d 23:59', strtotime($this->input->post('end'))) : date('Y-m-d H:i');
		//$start = date('Y-m-d', strtotime($this->input->post('start')));
		//$end = date('Y-m-dT23:59:59', strtotime($this->input->post('end')));
//echo $start.' - '.$end;
		$start_string = (date('Ymd', strtotime($start))-100).'0000';
		$end_string = (date('YmdHi', strtotime($end))-1000000);//only look at todays tee times up until the current time
		$now = date('YmdHi')-1000000;
		$schedule_id = $this->session->userdata('schedule_id');
		$schedule_type = $this->session->userdata('schedule_type');
//echo $start_string.'  -  '.$end_string;
		$get = $this->db->query("
			SELECT
				DAYOFYEAR(CONCAT(SUBSTRING(start+1000000, 1, 4),'-',SUBSTRING(start+1000000, 5, 2),'-',SUBSTRING(start+1000000, 7, 2))) AS day,
				SUBSTRING(start+1000000, 1, 4) AS year,
				COUNT(start) AS tee_times,
				SUM(CASE WHEN paid_player_count > 0 THEN 1 ELSE 0 END) AS paid_tee_times,
				SUM(CASE WHEN start < $now  AND paid_player_count < player_count THEN player_count - paid_player_count ELSE 0 END) AS no_show_tee_times,
				SUM(player_count) AS players,
				SUM(CASE WHEN paid_player_count > player_count THEN player_count ELSE paid_player_count END) AS paid_players,
				SUM(paid_carts) AS paid_carts
			FROM (`foreup_reservations`)
			LEFT JOIN foreup_tracks ON foreup_tracks.track_id = foreup_reservations.track_id
			WHERE `status` != 'deleted'
				AND foreup_tracks.schedule_id = $schedule_id
				AND `start` > $start_string
				AND `start` < $end_string
				AND (`type` = 'reservation' OR `type` = 'teetime')
				AND reservation_id LIKE '____________________'
			GROUP BY `year`, `day`
			ORDER BY `year`, `day`");
//			echo $this->db->last_query();
		$report_data = $get->result_array();
		if ($schedule_type == 'tee_sheet')
		{
			$total_players = $report_data[0]['players'];
			$paid_players = (float)$report_data[0]['paid_players'];
			$no_show_players = $report_data[0]['no_show_tee_times'];//$total_players - $paid_players;
			$average_group_size = (float)$report_data[0]['paid_players'] / $report_data[0]['paid_tee_times'] ? (float)$report_data[0]['paid_players'] / $report_data[0]['paid_tee_times'] : 0;
	
			$data = array(
				'header'=> array(
					'Bookings'=>($total_players?$total_players:0),
					'Players_Checked_in'=>($paid_players?$paid_players:0),
					'Player_No_Shows'=>($no_show_players?$no_show_players:0)
				)
			);
		}
		else
		{
			$total_players = $report_data[0]['tee_times'];
			$paid_players = (float)$report_data[0]['paid_tee_times'];
			$no_show_players = $report_data[0]['no_show_tee_times'];//$total_players - $paid_players;
			$average_group_size = (float)$report_data[0]['paid_players'] / $report_data[0]['paid_tee_times'] ? (float)$report_data[0]['paid_players'] / $report_data[0]['paid_tee_times'] : 0;
	
			$data = array(
				'header'=> array(
					'Bookings'=>($total_players?$total_players:0),
					'Bookings_Checked_in'=>($paid_players?$paid_players:0),
					'Booking_No_Shows'=>($no_show_players?$no_show_players:0)
				)
			);
		}

		return $data;
	}
	function fetch_marketing_data()
	{
		$limits = $this->Billing->get_monthly_limits($this->course_id);
		$this->load->model('Communication');
		$com_stats = $this->Communication->get_stats($this->course_id);

		$data = array(
			'header' => array(
				'Emails_Sent'=>$com_stats['emails_mk_this_month'],
				'Emails_Remaining'=>$limits['email_limit'] - $com_stats['emails_mk_this_month'],
				'Texts_Sent'=>$com_stats['texts_mk_this_month'],
				'Texts_Remaining'=>$limits['text_limit'] - $com_stats['texts_mk_this_month']
			)
		);

		return $data;
	}

	function fetch_pos_data()
	{
		// $start_date = date('Y-m-d');
		// $end_date = date('Y-m-dTH:i');
		// $sale_type = '';
		// $department = 'all';

		//$this->load->model('reports/Summary_payments');
		//$model = $this->Summary_payments;
		//$model->setParams(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type, 'department' => $department));
		//$this->Sale->create_sales_items_temp_table(array('start_date'=>$start_date, 'end_date'=>$end_date, 'sale_type' => $sale_type, 'department' => $department));

		//$include_average_sales = true;
		//$report_data = $model->getData($include_average_sales);
		$this->db->select('sales_payments.sale_id, sales_payments.payment_type, payment_amount');
		$this->db->from('sales_payments');
		$this->db->join('sales', 'sales.sale_id = sales_payments.sale_id');
		$this->db->where('course_id', $this->course_id);
		$this->db->where('sale_time >=', date('Y-m-d 00:00:00'));
		$this->db->where('sale_time <=', date('Y-m-d 23:59:59'));
		$this->db->where('deleted', 0);

		$payments = $this->db->get()->result_array();
		$total = 0;
		$sale_ids = array();
		$pay_type_totals = array();

		foreach($payments as $payment)
		{
			$total += $payment['payment_amount'];
			$sale_ids[$payment['sale_id']] = 1;
			if (strpos($payment['payment_type'], 'M/C') !== false || strpos($payment['payment_type'], 'VISA') !== false || strpos($payment['payment_type'], 'AMEX') !== false || strpos($payment['payment_type'], 'DCVR') !== false)
			{
				$payment['payment_type'] = 'Credit Card';
			}
			else if (strpos($payment['payment_type'], 'Change') !== false)
			{
				$payment['payment_type'] = 'Cash';
			}
			if (!$pay_type_totals[$payment['payment_type']])
				$pay_type_totals[$payment['payment_type']] = $payment['payment_amount'];
			else
				$pay_type_totals[$payment['payment_type']] += $payment['payment_amount'];
		}

		$data = array(
			'header' => array(
				'Total'=>to_currency($total),
				'Average_Sale'=>to_currency($total/count($sale_ids))
			),
			'more' => array(
				'Cash'=>to_currency($pay_type_totals['Cash']),
				'Credit_Card'=>to_currency($pay_type_totals['Credit Card']),
				'Check'=>to_currency($pay_type_totals['Check'])
			)
		);

		return $data;
	}

	function fetch_group_size_data()
	{
		$sale_type = 'sales';
		$department = 'all';
		$title = '';
		$function = 'sum';
		$type = 'day';
		$start = date('Y-m-d');
		$end = date('Y-m-dTH:i');
		$start_string = (date('Ymd', strtotime($start))-100).'0000';
		$end_string = (date('YmdHi', strtotime($end))-1000000);

		$get = $this->db->query("SELECT DAYOFYEAR(CONCAT(SUBSTRING(start+1000000, 1, 4),'-',SUBSTRING(start+1000000, 5, 2),'-',SUBSTRING(start+1000000, 7, 2))) AS day, SUBSTRING(start+1000000, 1, 4) AS year, COUNT(start) AS tee_times, SUM(CASE WHEN paid_player_count > 0 THEN 1 ELSE 0 END) AS paid_tee_times, SUM(player_count) AS players, SUM(paid_player_count) AS paid_players, SUM(paid_carts) AS paid_carts FROM (`foreup_teetime`) LEFT JOIN foreup_teesheet ON foreup_teetime.teesheet_id = foreup_teesheet.teesheet_id WHERE `status` != 'deleted' AND `start` > $start_string AND `start` < $end_string AND course_id = {$this->course_id} AND TTID LIKE '____________________' GROUP BY `year`, `day` ORDER BY `year`, `day`");
		$report_data = $get->result_array();
		$report_data[0]['average_group_size'] = (float)$report_data[0]['paid_players'] / $report_data[0]['paid_tee_times'];
		return $report_data;
		// echo "<pre>";
		// print_r($report_data);
		// echo "</pre>";

	}

	function fetch_players_data()
	{
		$type = $this->input->post('type') ? $this->input->post('type') : 'month';
		$start = $this->input->post('start') ? date('Y-m-d', strtotime($this->input->post('start'))) : date('Y-m-d', strtotime('-1 year'));
		$end = $this->input->post('end') ? date('Y-m-d', strtotime($this->input->post('end'))) : date('Y-m-d');
		$sale_type = 'sales';
		$department = 'all';
		$title = '';
		$function = 'sum';

		$department = $this->input->post('department') ? $this->input->post('department') : urldecode($department);

		$start_string = (date('Ymd', strtotime($start))-100).'0000';
		$end_string = (date('Ymd', strtotime($end))-100).'2399';


		$is_static_widget = true;//tells if the widget can be changed by the user.
		if ($is_static_widget) {
			//only look at todays tee times up until the current time
			$type = 'day';
			$start = date('Y-m-d');
			$end = date('Y-m-dTH:i');

			$start_string = (date('Ymd', strtotime($start))-100).'0000';
			$end_string = (date('YmdHi', strtotime($end))-1000000);
		}


		if ($type == 'hour')
		{
			$title = 'Daily Players ';
			$get = $this->db->query("SELECT SUBSTRING(start, 9, 2) AS hour, SUBSTRING(start+1000000, 1, 4) AS year, COUNT(start) AS tee_times, SUM(CASE WHEN paid_player_count > 0 THEN 1 ELSE 0 END) AS paid_tee_times, SUM(player_count) AS players, SUM(paid_player_count) AS paid_players, SUM(paid_carts) AS paid_carts FROM (`foreup_teetime`) LEFT JOIN foreup_teesheet ON foreup_teetime.teesheet_id = foreup_teesheet.teesheet_id WHERE `status` != 'deleted' AND `start` > $start_string AND `start` < $end_string AND course_id = {$this->course_id} AND TTID LIKE '____________________' GROUP BY `hour` ORDER BY `hour`");
		}
		else if ($type == 'day')
		{
			$title = 'Daily Missed Tee Times ';
			$get = $this->db->query("SELECT DAYOFYEAR(CONCAT(SUBSTRING(start+1000000, 1, 4),'-',SUBSTRING(start+1000000, 5, 2),'-',SUBSTRING(start+1000000, 7, 2))) AS day, SUBSTRING(start+1000000, 1, 4) AS year, COUNT(start) AS tee_times, SUM(CASE WHEN paid_player_count > 0 THEN 1 ELSE 0 END) AS paid_tee_times, SUM(player_count) AS players, SUM(paid_player_count) AS paid_players, SUM(paid_carts) AS paid_carts FROM (`foreup_teetime`) LEFT JOIN foreup_teesheet ON foreup_teetime.teesheet_id = foreup_teesheet.teesheet_id WHERE `status` != 'deleted' AND `start` > $start_string AND `start` < $end_string AND course_id = {$this->course_id} AND TTID LIKE '____________________' GROUP BY `year`, `day` ORDER BY `year`, `day`");
		}
		else if ($type == 'dayname')
		{
			$title = 'Daily Missed Tee Times ';
			$get = $this->db->query("SELECT DAYNAME(CONCAT(SUBSTRING(start+1000000, 1, 4),'-',SUBSTRING(start+1000000, 5, 2),'-',SUBSTRING(start+1000000, 7, 2))) AS dayname, WEEKDAY(CONCAT(SUBSTRING(start+1000000, 1, 4),'-',SUBSTRING(start+1000000, 5, 2),'-',SUBSTRING(start+1000000, 7, 2))) AS weekday, SUBSTRING(start+1000000, 1, 4) AS year, COUNT(start) AS tee_times, SUM(CASE WHEN paid_player_count > 0 THEN 1 ELSE 0 END) AS paid_tee_times, SUM(player_count) AS players, SUM(paid_player_count) AS paid_players, SUM(paid_carts) AS paid_carts FROM (`foreup_teetime`) LEFT JOIN foreup_teesheet ON foreup_teetime.teesheet_id = foreup_teesheet.teesheet_id WHERE `status` != 'deleted' AND `start` > $start_string AND `start` < $end_string AND course_id = {$this->course_id} AND TTID LIKE '____________________' GROUP BY `dayname` ORDER BY `weekday`");
		}
		else if ($type == 'week')
		{
			$title = 'Weekly Missed Tee Times ';
			$get = $this->db->query("SELECT WEEK(CONCAT(SUBSTRING(start+1000000, 1, 4),'-',SUBSTRING(start+1000000, 5, 2),'-',SUBSTRING(start+1000000, 7, 2))) AS week, SUBSTRING(start+1000000, 1, 4) AS year, COUNT(start) AS tee_times, SUM(CASE WHEN paid_player_count > 0 THEN 1 ELSE 0 END) AS paid_tee_times, SUM(player_count) AS players, SUM(paid_player_count) AS paid_players, SUM(paid_carts) AS paid_carts FROM (`foreup_teetime`) LEFT JOIN foreup_teesheet ON foreup_teetime.teesheet_id = foreup_teesheet.teesheet_id WHERE `status` != 'deleted' AND `start` > $start_string AND `start` < $end_string AND course_id = {$this->course_id} AND TTID LIKE '____________________' GROUP BY `year`, `week` ORDER BY `year`, `week`");
		}
		else if ($type == 'month')
		{
			$title = 'Monthly Missed Tee Times ';
			$get = $this->db->query("SELECT SUBSTRING(start+1000000, 5, 2) AS month, SUBSTRING(start+1000000, 1, 4) AS year, COUNT(start+100000) AS tee_times, SUM(CASE WHEN paid_player_count > 0 THEN 1 ELSE 0 END) AS paid_tee_times, SUM(player_count) AS players, SUM(paid_player_count) AS paid_players, SUM(paid_carts) AS paid_carts FROM (`foreup_teetime`) LEFT JOIN foreup_teesheet ON foreup_teetime.teesheet_id = foreup_teesheet.teesheet_id WHERE `status` != 'deleted' AND `start` > $start_string AND `start` < $end_string AND course_id = {$this->course_id} AND TTID LIKE '____________________' GROUP BY `year`, `month` ORDER BY `year`, `month`");
		}
		else if ($type == 'year')
		{
			$title = 'Yearly Missed Tee Times ';
			$get = $this->db->query("SELECT SUBSTRING(start+1000000, 1, 4) AS year, COUNT(start) AS tee_times, SUM(CASE WHEN paid_player_count > 0 THEN 1 ELSE 0 END) AS paid_tee_times, SUM(player_count) AS players, SUM(paid_player_count) AS paid_players, SUM(paid_carts) AS paid_carts FROM (`foreup_teetime`) LEFT JOIN foreup_teesheet ON foreup_teetime.teesheet_id = foreup_teesheet.teesheet_id WHERE `status` != 'deleted' AND `start` > $start_string AND `start` < $end_string AND course_id = {$this->course_id} AND TTID LIKE '____________________' GROUP BY `year` ORDER BY `year`");
		}
		//echo $this->db->last_query();
		// $report_data = $get->result_array();
		// echo "<pre>";
		// print_r($report_data);
		// echo "</pre>";

		// print_r($report_data);

		$total_players = $report_data[0]['players'];
		$paid_players = $report_data[0]['paid_players'];
		$paid_players_percent = ($paid_players / $total_players) * 100;
		$no_show_players = $total_players - $paid_players;
		$no_show_players_percent = ($no_show_players / $total_players) * 100;

		$categories = array('','');//categories are Missed and Complete but they don't need to be shown

		$return_data = array(
			array('name'=>'Checked-In : '.$paid_players,'y'=>$paid_players_percent,'color'=>"#000000"),
			array('name'=>'No-Shows : '.$no_show_players,'y'=>$no_show_players_percent,'color'=>"#004D99")
		);
		// return $return_data;

		return array('players'=>$paid_players,'categories'=>$categories, 'data'=>$return_data, 'title'=>'Players', 'missed_percent'=>$no_show_players_percent);

	}

	function fetch_missed_tee_time_data()
	{
		$type = $this->input->post('type') ? $this->input->post('type') : 'month';
		$start = $this->input->post('start') ? date('Y-m-d', strtotime($this->input->post('start'))) : date('Y-m-d', strtotime('-1 year'));
		$end = $this->input->post('end') ? date('Y-m-d', strtotime($this->input->post('end'))) : date('Y-m-d');
		$sale_type = 'sales';
		$department = 'all';
		$title = '';
		$function = 'sum';

		$department = $this->input->post('department') ? $this->input->post('department') : urldecode($department);

		$start_string = (date('Ymd', strtotime($start))-100).'0000';
		$end_string = (date('Ymd', strtotime($end))-100).'2399';


		$is_static_widget = true;//tells if the widget can be changed by the user.
		if ($is_static_widget) {
			//only look at todays tee times up until the current time
			$type = 'day';
			$start = date('Y-m-d');
			$end = date('Y-m-dTH:i');

			$start_string = (date('Ymd', strtotime($start))-100).'0000';
			$end_string = (date('YmdHi', strtotime($end))-1000000);
		}


		if ($type == 'hour')
		{
			$title = 'Hourly Missed Tee Times ';
			$get = $this->db->query("SELECT SUBSTRING(start, 9, 2) AS hour, SUBSTRING(start+1000000, 1, 4) AS year, COUNT(start) AS tee_times, SUM(CASE WHEN paid_player_count > 0 THEN 1 ELSE 0 END) AS paid_tee_times, SUM(player_count) AS players, SUM(paid_player_count) AS paid_players, SUM(paid_carts) AS paid_carts FROM (`foreup_teetime`) LEFT JOIN foreup_teesheet ON foreup_teetime.teesheet_id = foreup_teesheet.teesheet_id WHERE `status` != 'deleted' AND `start` > $start_string AND `start` < $end_string AND course_id = {$this->course_id} AND TTID LIKE '____________________' GROUP BY `hour` ORDER BY `hour`");
		}
		else if ($type == 'day')
		{
			$title = 'Daily Missed Tee Times ';
			$get = $this->db->query("SELECT DAYOFYEAR(CONCAT(SUBSTRING(start+1000000, 1, 4),'-',SUBSTRING(start+1000000, 5, 2),'-',SUBSTRING(start+1000000, 7, 2))) AS day, SUBSTRING(start+1000000, 1, 4) AS year, COUNT(start) AS tee_times, SUM(CASE WHEN paid_player_count > 0 THEN 1 ELSE 0 END) AS paid_tee_times, SUM(player_count) AS players, SUM(paid_player_count) AS paid_players, SUM(paid_carts) AS paid_carts FROM (`foreup_teetime`) LEFT JOIN foreup_teesheet ON foreup_teetime.teesheet_id = foreup_teesheet.teesheet_id WHERE `status` != 'deleted' AND `start` > $start_string AND `start` < $end_string AND course_id = {$this->course_id} AND TTID LIKE '____________________' GROUP BY `year`, `day` ORDER BY `year`, `day`");
		}
		else if ($type == 'dayname')
		{
			$title = 'Daily Missed Tee Times ';
			$get = $this->db->query("SELECT DAYNAME(CONCAT(SUBSTRING(start+1000000, 1, 4),'-',SUBSTRING(start+1000000, 5, 2),'-',SUBSTRING(start+1000000, 7, 2))) AS dayname, WEEKDAY(CONCAT(SUBSTRING(start+1000000, 1, 4),'-',SUBSTRING(start+1000000, 5, 2),'-',SUBSTRING(start+1000000, 7, 2))) AS weekday, SUBSTRING(start+1000000, 1, 4) AS year, COUNT(start) AS tee_times, SUM(CASE WHEN paid_player_count > 0 THEN 1 ELSE 0 END) AS paid_tee_times, SUM(player_count) AS players, SUM(paid_player_count) AS paid_players, SUM(paid_carts) AS paid_carts FROM (`foreup_teetime`) LEFT JOIN foreup_teesheet ON foreup_teetime.teesheet_id = foreup_teesheet.teesheet_id WHERE `status` != 'deleted' AND `start` > $start_string AND `start` < $end_string AND course_id = {$this->course_id} AND TTID LIKE '____________________' GROUP BY `dayname` ORDER BY `weekday`");
		}
		else if ($type == 'week')
		{
			$title = 'Weekly Missed Tee Times ';
			$get = $this->db->query("SELECT WEEK(CONCAT(SUBSTRING(start+1000000, 1, 4),'-',SUBSTRING(start+1000000, 5, 2),'-',SUBSTRING(start+1000000, 7, 2))) AS week, SUBSTRING(start+1000000, 1, 4) AS year, COUNT(start) AS tee_times, SUM(CASE WHEN paid_player_count > 0 THEN 1 ELSE 0 END) AS paid_tee_times, SUM(player_count) AS players, SUM(paid_player_count) AS paid_players, SUM(paid_carts) AS paid_carts FROM (`foreup_teetime`) LEFT JOIN foreup_teesheet ON foreup_teetime.teesheet_id = foreup_teesheet.teesheet_id WHERE `status` != 'deleted' AND `start` > $start_string AND `start` < $end_string AND course_id = {$this->course_id} AND TTID LIKE '____________________' GROUP BY `year`, `week` ORDER BY `year`, `week`");
		}
		else if ($type == 'month')
		{
			$title = 'Monthly Missed Tee Times ';
			$get = $this->db->query("SELECT SUBSTRING(start+1000000, 5, 2) AS month, SUBSTRING(start+1000000, 1, 4) AS year, COUNT(start+100000) AS tee_times, SUM(CASE WHEN paid_player_count > 0 THEN 1 ELSE 0 END) AS paid_tee_times, SUM(player_count) AS players, SUM(paid_player_count) AS paid_players, SUM(paid_carts) AS paid_carts FROM (`foreup_teetime`) LEFT JOIN foreup_teesheet ON foreup_teetime.teesheet_id = foreup_teesheet.teesheet_id WHERE `status` != 'deleted' AND `start` > $start_string AND `start` < $end_string AND course_id = {$this->course_id} AND TTID LIKE '____________________' GROUP BY `year`, `month` ORDER BY `year`, `month`");
		}
		else if ($type == 'year')
		{
			$title = 'Yearly Missed Tee Times ';
			$get = $this->db->query("SELECT SUBSTRING(start+1000000, 1, 4) AS year, COUNT(start) AS tee_times, SUM(CASE WHEN paid_player_count > 0 THEN 1 ELSE 0 END) AS paid_tee_times, SUM(player_count) AS players, SUM(paid_player_count) AS paid_players, SUM(paid_carts) AS paid_carts FROM (`foreup_teetime`) LEFT JOIN foreup_teesheet ON foreup_teetime.teesheet_id = foreup_teesheet.teesheet_id WHERE `status` != 'deleted' AND `start` > $start_string AND `start` < $end_string AND course_id = {$this->course_id} AND TTID LIKE '____________________' GROUP BY `year` ORDER BY `year`");
		}
		//echo $this->db->last_query();
		$report_data = $get->result_array();

		// print_r($report_data);

		$total_players = $report_data[0]['players'];
		$paid_players = $report_data[0]['paid_players'];
		$paid_players_percent = ($paid_players / $total_players) * 100;
		$no_show_players = $total_players - $paid_players;
		$no_show_players_percent = ($no_show_players / $total_players) * 100;

		$categories = array('','');//categories are Missed and Complete but they don't need to be shown

		$return_data = array(
			array('name'=>'No-Shows : '.$no_show_players,'y'=>$no_show_players_percent,'color'=>"#004D99"),
			array('name'=>'Complete : '.$paid_players,'y'=>$paid_players_percent,'color'=>"#000000")
		);

		return array('no_shows'=>$no_show_players,'no_show_percent'=>$no_show_players_percent,'categories'=>$categories, 'data'=>$return_data, 'title'=>'Missed Tee Times', 'missed_percent'=>$no_show_players_percent);
	}

	function tee_time($metrics, $group_by, $start, $end, $filters = null, $filter_by_course = true)
	{
		$start_string = (date('Ymd', strtotime($start))-100).'0000';
		$end_string = (date('Ymd', strtotime($end))-100).'2399';

		if ($group_by == 'hour')
		{
			$title = 'Hourly Tee Times ';
			$get = $this->db->query("SELECT SUBSTRING(start, 9, 2) AS hour, SUBSTRING(start+1000000, 1, 4) AS year,
				COUNT(start) AS tee_times,
				SUM(CASE WHEN paid_player_count > 0 THEN 1 ELSE 0 END) AS paid_tee_times,
				SUM(player_count) AS players, SUM(paid_player_count) AS paid_players,
				SUM(paid_carts) AS paid_carts
			FROM (`foreup_teetime`)
			LEFT JOIN foreup_teesheet
				ON foreup_teetime.teesheet_id = foreup_teesheet.teesheet_id
			WHERE `status` != 'deleted'
				AND `start` > $start_string
				AND `start` < $end_string
				AND course_id = {$this->course_id}
				AND TTID LIKE '____________________'
			GROUP BY `hour`
			ORDER BY `hour`");
		}
		else if ($group_by == 'day')
		{
			$title = 'Daily Tee Times ';
			$get = $this->db->query("SELECT DAYOFYEAR(CONCAT(SUBSTRING(start+1000000, 1, 4),'-',SUBSTRING(start+1000000, 5, 2),'-',SUBSTRING(start+1000000, 7, 2))) AS day,
				SUBSTRING(start+1000000, 1, 4) AS year,
				COUNT(start) AS tee_times,
				SUM(CASE WHEN paid_player_count > 0 THEN 1 ELSE 0 END) AS paid_tee_times,
				SUM(player_count) AS players,
				SUM(paid_player_count) AS paid_players,
				SUM(paid_carts) AS paid_carts
			FROM (`foreup_teetime`)
			LEFT JOIN foreup_teesheet
				ON foreup_teetime.teesheet_id = foreup_teesheet.teesheet_id
			WHERE `status` != 'deleted'
				AND `start` > $start_string
				AND `start` < $end_string
				AND course_id = {$this->course_id}
				AND TTID LIKE '____________________'
			GROUP BY `year`, `day`
			ORDER BY `year`, `day`");
		}
		else if ($group_by == 'dayname')
		{
			$title = 'Daily Tee Times ';
			$get = $this->db->query("SELECT DAYNAME(CONCAT(SUBSTRING(start+1000000, 1, 4),'-',SUBSTRING(start+1000000, 5, 2),'-',SUBSTRING(start+1000000, 7, 2))) AS dayname, WEEKDAY(CONCAT(SUBSTRING(start+1000000, 1, 4),'-',SUBSTRING(start+1000000, 5, 2),'-',SUBSTRING(start+1000000, 7, 2))) AS weekday, SUBSTRING(start+1000000, 1, 4) AS year, COUNT(start) AS tee_times, SUM(CASE WHEN paid_player_count > 0 THEN 1 ELSE 0 END) AS paid_tee_times, SUM(player_count) AS players, SUM(paid_player_count) AS paid_players, SUM(paid_carts) AS paid_carts FROM (`foreup_teetime`) LEFT JOIN foreup_teesheet ON foreup_teetime.teesheet_id = foreup_teesheet.teesheet_id WHERE `status` != 'deleted' AND `start` > $start_string AND `start` < $end_string AND course_id = {$this->course_id} AND TTID LIKE '____________________' GROUP BY `dayname` ORDER BY `weekday`");
		}
		else if ($group_by == 'week')
		{
			$title = 'Weekly Tee Times ';
			$get = $this->db->query("SELECT WEEK(CONCAT(SUBSTRING(start+1000000, 1, 4),'-',SUBSTRING(start+1000000, 5, 2),'-',SUBSTRING(start+1000000, 7, 2))) AS week, SUBSTRING(start+1000000, 1, 4) AS year, COUNT(start) AS tee_times, SUM(CASE WHEN paid_player_count > 0 THEN 1 ELSE 0 END) AS paid_tee_times, SUM(player_count) AS players, SUM(paid_player_count) AS paid_players, SUM(paid_carts) AS paid_carts FROM (`foreup_teetime`) LEFT JOIN foreup_teesheet ON foreup_teetime.teesheet_id = foreup_teesheet.teesheet_id WHERE `status` != 'deleted' AND `start` > $start_string AND `start` < $end_string AND course_id = {$this->course_id} AND TTID LIKE '____________________' GROUP BY `year`, `week` ORDER BY `year`, `week`");
		}
		else if ($group_by == 'month')
		{
			$title = 'Monthly Tee Times ';
			$get = $this->db->query("SELECT SUBSTRING(start+1000000, 5, 2) AS month, SUBSTRING(start+1000000, 1, 4) AS year, COUNT(start+100000) AS tee_times, SUM(CASE WHEN paid_player_count > 0 THEN 1 ELSE 0 END) AS paid_tee_times, SUM(player_count) AS players, SUM(paid_player_count) AS paid_players, SUM(paid_carts) AS paid_carts FROM (`foreup_teetime`) LEFT JOIN foreup_teesheet ON foreup_teetime.teesheet_id = foreup_teesheet.teesheet_id WHERE `status` != 'deleted' AND `start` > $start_string AND `start` < $end_string AND course_id = {$this->course_id} AND TTID LIKE '____________________' GROUP BY `year`, `month` ORDER BY `year`, `month`");
		}
		else if ($group_by == 'year')
		{
			$title = 'Yearly Tee Times ';
			$get = $this->db->query("SELECT SUBSTRING(start+1000000, 1, 4) AS year, COUNT(start) AS tee_times, SUM(CASE WHEN paid_player_count > 0 THEN 1 ELSE 0 END) AS paid_tee_times, SUM(player_count) AS players, SUM(paid_player_count) AS paid_players, SUM(paid_carts) AS paid_carts FROM (`foreup_teetime`) LEFT JOIN foreup_teesheet ON foreup_teetime.teesheet_id = foreup_teesheet.teesheet_id WHERE `status` != 'deleted' AND `start` > $start_string AND `start` < $end_string AND course_id = {$this->course_id} AND TTID LIKE '____________________' GROUP BY `year` ORDER BY `year`");
		}
		else if ($group_by == 'total')
		{
			$title = 'Total Tee Times ';
			$get = $this->db->query("SELECT
				COUNT(start) AS tee_times,
				SUM(CASE WHEN paid_player_count > 0 THEN 1 ELSE 0 END) AS paid_tee_times,
				SUM(player_count) AS players,
				SUM(paid_player_count) AS paid_players,
				SUM(paid_carts) AS paid_carts
			FROM (`foreup_teetime`)
			LEFT JOIN foreup_teesheet
				ON foreup_teetime.teesheet_id = foreup_teesheet.teesheet_id
			WHERE `status` != 'deleted'
				AND `start` > $start_string
				AND `start` < $end_string
				AND course_id = {$this->course_id}
				AND TTID LIKE '____________________'");
		}

		$rows = $get->result_array();
		return $rows;
	}

	function fetch_tee_time_table_data()
	{
		/*$start = $this->input->post('start') ? date('Y-m-d', strtotime($this->input->post('start'))) : date('Y-m-d', strtotime('-1 year'));
		$end = $this->input->post('end') ? date('Y-m-d', strtotime($this->input->post('end'))) : date('Y-m-d');
		$start_string = (date('Ymd', strtotime($start))-100).'0000';
		$end_string = (date('Ymd', strtotime($end))-100).'2399';
		$get = $this->db->query("SELECT SUBSTRING(start, 9, 2) AS hour, COUNT(start) AS tee_times, SUM(player_count) AS players, SUM(paid_player_count) AS paid_players, SUM(paid_carts) AS paid_carts
				FROM (`foreup_teetime`)
				LEFT JOIN foreup_teesheet ON foreup_teetime.teesheet_id = foreup_teesheet.teesheet_id
				JOIN foreup_customers ON foreup_teetime.person_id = foreup_customers.person_id
				JOIN foreup_customers ON foreup_teetime.person_id_2 = foreup_customers.person_id
				JOIN foreup_customers ON foreup_teetime.person_id_3 = foreup_customers.person_id
				JOIN foreup_customers ON foreup_teetime.person_id_4 = foreup_customers.person_id
				JOIN foreup_customers ON foreup_teetime.person_id_5 = foreup_customers.person_id
				WHERE `status` != 'deleted' AND `start` > $start_string AND `start` < $end_string AND course_id = {$this->course_id} AND TTID LIKE '____________________' GROUP BY `hour` ORDER BY `hour`");
		*/
		$type = $this->input->post('type') ? $this->input->post('type') : 'month';
		$start = $this->input->post('start') ? date('Y-m-d', strtotime($this->input->post('start'))) : date('Y-m-d', strtotime('-1 year'));
		$end = $this->input->post('end') ? date('Y-m-d', strtotime($this->input->post('end'))) : date('Y-m-d');
		if ($type == 'hour')
		{
			$end .= ' 23:59:59';
			$start .= ' 00:00:00';
		}
		$sale_type = 'sales';
		$department = 'all';
		$title = '';
		$function = 'sum';
		$function_title = ($function == 'sum') ? 'Totals' : (($function == 'avg') ? 'Averages' : '');
		$department = $this->input->post('department') ? $this->input->post('department') : urldecode($department);
		$this->load->model('reports/Summary_sales');
		$model = $this->Summary_sales;
		$model->setParams(array('start_date'=>$start, 'end_date'=>$end, 'sale_type' => $sale_type, 'department' => $department));

		$this->db->query("DROP TEMPORARY TABLE IF EXISTS ".$this->db->dbprefix('sales_items_temp'));
		$this->Sale->create_sales_items_temp_table(array('start_date'=>$start, 'end_date'=>$end, 'sale_type' => $sale_type, 'department' => $department));
		//echo $this->db->last_query();
		//$this->db->from('sales_items_temp');
		//$this->db->get();
		//echo $this->db->last_query();
		//$tabular_data = array();
		$this->db->select('SUM(quantity_purchased) AS quantity, SUM(quantity_purchased * item_unit_price * (100 - discount_percent) / 100) AS price, category, subcategory, description, item_unit_price');
		$this->db->from('sales_items_temp');
		$this->db->where('deleted', 0);
		$this->db->where("(category = 'Green Fees' OR category ='Carts')");
		$this->db->where('subcategory !=', '');
		$this->db->group_by('category, subcategory');
		$this->db->order_by('category, quantity desc');
		$get = $this->db->get();
		$cart_stats = $gf_stats = array();
		$cart_other = $gf_other = $cart_stat_counter = $gf_stat_counter = $total_rounds = $total_carts = $total_gf_rev = $total_cart_rev = 0;
		foreach($get->result_array() AS $result)
		{
			if ($result['category'] == 'Carts')
			{
				$total_carts += $result['quantity'];
				$total_cart_rev += $result['price'];
				if($cart_counter == 0)
					$cart_stats[] = array('name'=>$result['subcategory'], 'y'=>(int) $result['quantity'], 'sliced'=>true, 'selected'=>true);
				else if($cart_counter < 5)
					$cart_stats[] = array($result['subcategory'], (int) $result['quantity']);
				else
					$cart_other += (int) $result['quantity'];
				$cart_counter++;
			}
			else if ($result['category'] == 'Green Fees')
			{
				$total_rounds += $result['quantity'];
				$total_gf_rev += $result['price'];
				if($gf_counter == 0)
					$gf_stats[] = array('name'=>$result['subcategory'], 'y'=>(int) $result['quantity'], 'sliced'=>true, 'selected'=>true);
				else if($gf_counter < 5)
					$gf_stats[] = array($result['subcategory'], (int) $result['quantity']);
				else
					$gf_other += (int) $result['quantity'];
				$gf_counter++;
			}
//			print_r($result);
	//		echo '<br/><br/>';
		}
		if ($cart_other > 0)
			$cart_stats[5] = array('Other', $cart_other);
		if ($gf_other > 0)
			$gf_stats[5] = array('Other', $gf_other);
		//echo $this->db->last_query();
		//print_r($cart_stats);
		//echo '<br/><br/>';
		//print_r($gf_stats);
		return array('green_fees'=>array('title'=>'Rates', 'data'=>$gf_stats, 'total'=>$total_rounds, 'revenue'=>$total_gf_rev), 'carts'=>array('title'=>'Carts Rates', 'data'=>$cart_stats, 'total'=>$total_carts, 'revenue'=>$total_cart_rev));
	}
	function fetch_tee_time_data_2($type, $start, $end)
	{
		$start = '201200000000';
		$end = '201212312399';
		if ($dow)
		{
			$get = $this->db->query("SELECT WEEKDAY(CONCAT(SUBSTRING(start+1000000, 1, 4),'-',SUBSTRING(start+1000000, 5, 2),'-',SUBSTRING(start+1000000, 7, 2))) AS dow, COUNT(start) AS tee_times, SUM(CASE WHEN paid_player_count > 0 THEN 1 ELSE 0 END) AS paid_tee_times, SUM(player_count) AS players, SUM(paid_player_count) AS paid_players, SUM(paid_carts) AS paid_carts FROM (`foreup_teetime`) LEFT JOIN foreup_teesheet ON foreup_teetime.teesheet_id = foreup_teesheet.teesheet_id WHERE `status` != 'deleted' AND `start` > $start AND `start` < $end AND course_id = {$this->course_id} AND TTID LIKE '____________________' GROUP BY `dow` ORDER BY `dow`");
			$report_data = $get->result_array();
			$return_data = array(
							array('name'=>'Booked Tee Times','data'=>array(0,0,0,0,0,0,0)),
							array('name'=>'Paid Tee Times','data'=>array(0,0,0,0,0,0,0)),
							array('name'=>'Booked Players','data'=>array(0,0,0,0,0,0,0)),
							array('name'=>'Paid Players','data'=>array(0,0,0,0,0,0,0)),
							array('name'=>'Paid Carts','data'=>array(0,0,0,0,0,0,0)));

			foreach($report_data as $data)
			{
				$return_data[0]['data'][$data['dow']] = (float) $data['tee_times'];
				$return_data[1]['data'][$data['dow']] = (float) $data['paid_tee_times'];
				$return_data[2]['data'][$data['dow']] = (float) $data['players'];
				$return_data[3]['data'][$data['dow']] = (float) $data['paid_players'];
				$return_data[4]['data'][$data['dow']] = (float) $data['paid_carts'];
			}
		}
		else
		{
			$get = $this->db->query("SELECT SUBSTRING(start+1000000, 5, 2) AS month, COUNT(start+100000) AS tee_times, SUM(CASE WHEN paid_player_count > 0 THEN 1 ELSE 0 END) AS paid_tee_times, SUM(player_count) AS players, SUM(paid_player_count) AS paid_players, SUM(paid_carts) AS paid_carts FROM (`foreup_teetime`) LEFT JOIN foreup_teesheet ON foreup_teetime.teesheet_id = foreup_teesheet.teesheet_id WHERE `status` != 'deleted' AND `start` > $start AND `start` < $end AND course_id = {$this->course_id} AND TTID LIKE '____________________' GROUP BY `month` ORDER BY `month`");
			$report_data = $get->result_array();
			$return_data = array(
							array('name'=>'Booked Tee Times','data'=>array(0,0,0,0,0,0,0,0,0,0,0,0)),
							array('name'=>'Paid Tee Times','data'=>array(0,0,0,0,0,0,0,0,0,0,0,0)),
							array('name'=>'Booked Players','data'=>array(0,0,0,0,0,0,0,0,0,0,0,0)),
							array('name'=>'Paid Players','data'=>array(0,0,0,0,0,0,0,0,0,0,0,0)),
							array('name'=>'Paid Carts','data'=>array(0,0,0,0,0,0,0,0,0,0,0,0)));

			foreach($report_data as $data)
			{
				$return_data[0]['data'][$data['month']-1] = (float) $data['tee_times'];
				$return_data[1]['data'][$data['month']-1] = (float) $data['paid_tee_times'];
				$return_data[2]['data'][$data['month']-1] = (float) $data['players'];
				$return_data[3]['data'][$data['month']-1] = (float) $data['paid_players'];
				$return_data[4]['data'][$data['month']-1] = (float) $data['paid_carts'];
			}
		}

		return $return_data;
	}

	function fetch_mercury_data()
	{
		$this->load->model('Communication');

		$report_data = $this->db->query("SELECT  MONTH(sale_time) AS month, SUM(payment_amount) AS total FROM foreup_sales_payments JOIN foreup_sales ON foreup_sales.sale_id = foreup_sales_payments.sale_id WHERE foreup_sales_payments.payment_type = 'Credit Card' OR foreup_sales_payments.payment_type LIKE 'VISA%' OR foreup_sales_payments.payment_type LIKE 'M/C%' OR foreup_sales_payments.payment_type LIKE 'DCVR%' OR foreup_sales_payments.payment_type LIKE 'AMEX%'")->result_array();

		$return_data = array(
						array('name'=>'Mercury Charges','data'=>array(0,0,0,0,0,0,0,0,0,0,0,0)));

		foreach($report_data as $data)
		{
			$return_data[0]['data'][$data['month']-1] = (float) $data['total'];
		}

		return $return_data;

		// Add more data later
		$foreup_percent = 0.4;
		$results = array();
		$start_date = date('Y-m-01');
		$end_date = date('Y-m-'.$this->Communication->days_in_month(date('m')));
		$pm_start_date = date('Y-m-01', strtotime($start_date.' -1 month'));
		$pm_end_date = date('Y-m-'.$this->Communication->days_in_month(date('m', strtotime($start_date.' -1 month'))), strtotime($start_date.' -1 month'));
		$q_results = $this->db->query("SELECT memo, SUM(auth_amount) AS total, COUNT(DISTINCT mercury_id) AS courses,
			sum(IF (trans_post_time >= '$start_date' AND trans_post_time <= '$end_date', auth_amount, 0 )) AS this_month,
			sum(IF (trans_post_time >= '$pm_start_date' AND trans_post_time <= '$pm_end_date', auth_amount, 0 )) AS last_month
			FROM foreup_sales_payments_credit_cards WHERE status = 'Approved' AND mercury_id != 494691720 AND mercury_id != 88430119384 AND mercury_id != '' GROUP BY memo");
		while($result = $q_results->fetch_assoc())
			$results[$result['memo']] = $result;

		$data = array(
			'cc_sales'=>to_currency($cc_sales[0]['total']),
			'courses'=> empty($results['ForeUP v.1.0']) ? '' : $results['ForeUP v.1.0']['courses'],
			'mercury_sales'=> empty($results['ForeUP v.1.0']) ? '' : to_currency($results['ForeUP v.1.0']['total']),
			'last_month'=> empty($results['ForeUP v.1.0']) ? '' : to_currency($results['ForeUP v.1.0']['last_month'] * $foreup_percent/100),
			'this_month'=> empty($results['ForeUP v.1.0']) ? '' : to_currency($results['ForeUP v.1.0']['this_month'] * $foreup_percent/100),
			'total_revenue'=> empty($results['ForeUP v.1.0']) ? '' : to_currency($results['ForeUP v.1.0']['total'] * $foreup_percent/100)
		);
		return $data;
	}

	function fetch_text_data()
	{
		$final_array = array('Email'=>'','Text'=>'');
		$start_date = date('Y-m-01');
		$end_date = date('Y-m-'.$this->days_in_month(date('m')));
		$pm_start_date = date('Y-m-01', strtotime($start_date.' -1 month'));
		$pm_end_date = date('Y-m-'.$this->days_in_month(date('m', strtotime($start_date.' -1 month'))), strtotime($start_date.' -1 month'));
		$course_sql = '';
		if ($course_id != '')
			$course_sql = "WHERE course_id = $course_id";
		$results = $this->db->query("SELECT type, sum(IF (date >= '$start_date' AND date <= '$end_date', recipient_count, 0 )) AS this_month,
			sum(IF (date >= '$pm_start_date' AND date <= '$pm_end_date', recipient_count, 0 )) AS last_month,
			sum(IF (date >= '$start_date' AND date <= '$end_date' AND campaign_id != 0, recipient_count, 0 )) AS mk_this_month,
			sum(IF (date >= '$pm_start_date' AND date <= '$pm_end_date' AND campaign_id != 0, recipient_count, 0 )) AS mk_last_month,
			sum(recipient_count) AS total_sent
			FROM foreup_communications $course_sql GROUP BY type");
		foreach ($results->result_array() as $result)
			$final_array[$result['type']] = $result;

		$data = array(
			'emails_this_month'=>isset($final_array['Email']['this_month'])?$final_array['Email']['this_month']:0,
			'emails_last_month'=>isset($final_array['Email']['last_month'])?$final_array['Email']['last_month']:0,
			'emails_mk_this_month'=>isset($final_array['Email']['mk_this_month'])?$final_array['Email']['mk_this_month']:0,
			'emails_mk_last_month'=>isset($final_array['Email']['mk_last_month'])?$final_array['Email']['mk_last_month']:0,
			'emails_total_sent'=>isset($final_array['Email']['total_sent'])?$final_array['Email']['total_sent']:0,
			'texts_mk_this_month'=>isset($final_array['Text']['mk_this_month'])?$final_array['Text']['mk_this_month']:0,
			'texts_mk_last_month'=>isset($final_array['Text']['mk_last_month'])?$final_array['Text']['mk_last_month']:0,
			'texts_total_sent'=>isset($final_array['Text']['total_sent'])?$final_array['Text']['total_sent']:0
		);

		return $data;
	}
}