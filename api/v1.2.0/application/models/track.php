<?php
class Track extends CI_Model 
{
	function exists($track_id)
	{
		$this->db->from('tracks');
		$this->db->where('track_id',$track_id);
		$this->db->where('deleted',0);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	function get_info($track_id = false) {
		$this->db->from('tracks');	
		$this->db->where('deleted', 0);
		$this->db->where('track_id', $track_id);
		$query = $this->db->get();
		
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get all the fields from tracks table
			$fields = $this->db->list_fields('tracks');
			
			//append those fields to base parent object, we we have a complete empty object
			foreach ($fields as $field)
			{
				$person_obj->$field='';
			}
			
			return $person_obj;
		}
	}
	function get_all($schedule_id = false) {
		$this->db->from('tracks');
		$this->db->where('deleted', 0);
		$this->db->where('schedule_id', ($schedule_id?$schedule_id:$this->session->userdata('schedule_id')));
		$this->db->order_by("track_id", 'asc');
		
		$tracks = $this->db->get();
		return $tracks;
	}
	
	function get_track_array($schedule_id = false)
	{
		$track_array = array();
		$tracks = $this->get_all($schedule_id)->result_array();
		foreach($tracks as $track)
		{
			$track_array[$track['track_id']] = $track['title'];
		}
		return $track_array;
	}
	function row_html($track = false, $tracks = false, $schedule = false)
	{
		$track_array = array(0=>'Select Reround Track');
		if ($tracks)
			foreach($tracks->result_object() as $reround_track)
				$track_array[$reround_track->track_id] = $reround_track->title;
			
		$employee_name = '';
		if ($track->employee_id)
		{
			$employee_info = $this->Employee->get_info($track->employee_id);		
			$employee_name = $employee_info->last_name.', '.$employee_info->first_name;
		}
		$reround_class = $employee_class = '';
		if ($schedule->type == 'tee_sheet')
		{
			$employee_class = 'hidden';
		}
		else if ($schedule->type == 'lessons')
		{
			$reround_class = 'hidden';
		}
		$row_html = 
			"<div class='field_row clearfix'>".
				form_label((''), 'name',array('class'=>'wide')).
				"<div class='form_field'>".
				form_hidden('track_id[]', $track->track_id).
				form_input(array(
					'name'=>'track_name[]',
					'size'=>'20',
					'id'=>'name',
					'placeholder'=>lang('schedules_name'),
					'value'=>$track->title)
				).
				form_input(array(
					'name'=>'track_employee[]',
					'size'=>'20',
					'id'=>'name',
					'placeholder'=>lang('schedules_employee'),
					'value'=>$employee_name,
					'class'=>$employee_class
				)).
				form_hidden('track_employee_id[]', $track->employee_id).
				form_dropdown(
					'reround_track_id[]',
					$track_array,
					$track->reround_track_id,
					"class='$reround_class'"
				).
				form_dropdown(
					'track_online_booking[]',
					array(0=>'Off', 1=>'On'),
					$track->online_booking
				).
				"</div>".
			"</div>";
		return $row_html;
	}
	function save(&$track_data,$track_id=false)
	{
		if (!$track_id or !$this->exists($track_id))
		{
			if($this->db->insert('tracks',$track_data))
			{
				$track_data['track_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}

		$this->db->where('track_id', $track_id);
		return $this->db->update('tracks',$track_data);
	}
	
}
	
