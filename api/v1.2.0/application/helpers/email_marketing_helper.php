<?php
// Function checks if an image in email newsletter was replaced, if so
// it returns the replaced image url instead of the default
function email_marketing_image_url($image_url, $replacement_images = null){
	if(empty($replacement_images)){
		$CI =& get_instance();
		$replacement_images = $CI->session->userdata('marketing_email_images');
	}

	if(isset($replacement_images[$image_url])){
		$image_url = $replacement_images[$image_url]['url'];
	}

	return $image_url;
}

// Returns the image ID of the replaced image (if there is one)
function email_marketing_image_id($image_url, $replacement_images = null){
	if(empty($replacement_images)){
		$CI =& get_instance();
		$replacement_images = $CI->session->userdata('marketing_email_images');
	}

	if(isset($replacement_images[$image_url])){
		return $replacement_images[$image_url]['image_id'];
	}

	return 0;
}
?>