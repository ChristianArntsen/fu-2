<?php $this->load->view("partial/header"); ?>
<?php //print_r($course_payments)?>
<style>
	#table_holder {
		width:auto;
	}
</style>
<div id="content_area_wrapper">
<div id="content_area">
<table id="title_bar">
	<tbody><tr>
		<td id="title_icon">
			<img src="/images/menubar/billings.png" alt="title icon">
		</td>
		<td id="title">
			<?= $course_info->name ?>
		</td>
		<!--td id="title_search">
			<form action="http://localhost:8888/index.php/billings/search" method="post" accept-charset="utf-8" id="search_form">				<input type="text" name="search" id="search" class="ui-autocomplete-input" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true">
				<img src="http://localhost:8888/images/spinner_small.gif" alt="spinner" id="spinner">
			</form>
		</td-->
	</tr>
</tbody></table>
<table id="contents">
	<tbody><tr>
		<!--td id="commands">
			<!--div id="new_button">
				<a href="http://localhost:8888/index.php/billings/view/-1" class="colbox none new cboxElement" title="New Billing">New Billing</a><a href="http://localhost:8888/index.php/credit_cards/view/-1" class="colbox none new cboxElement" title="Manage Credit Cards">Manage Credit Cards</a><a href="http://localhost:8888/index.php/billings/delete" id="delete" class="delete_inactive">Delete</a>
			</div-->
		<!--/td>
		<td style="width:10px;"></td-->
		<td id="item_table">
			<div id="table_holder">
			<table class='tablesorter course_table'>
				<thead>
					<tr>
						<th class='leftmost'>Date</th>
						<th class='header'>Description</th>
						<th class='header'>Amount</th>
						<th class='header'>Credit Card</th>
						<th class='rightmost header'>Charged By</th>
					</tr>
				</thead>
				<tbody>
				<?php 
					$last_date = '';
					foreach($course_payments as $course_payment) {
					$date = ($last_date != $course_payment['date'])?$course_payment['date']:'';
					?>
					<tr>
						<td><?=$date?></td>
						<td><?=$course_payment['description']?></td>
						<td style='text-align:right;'><?=to_currency($course_payment['amount'])?></td>
						<td style='text-align:right;'><?=$course_payment['masked_account']?></td>
						<td class='rightmost'><?=$course_payment['person_id']?></td>
					</tr>	
				<?php 
					$last_date = $course_payment['date'];
					} ?>
				</tbody>
			</table>
			<div id="pagination"></div>
		</td>
	</tr>
</tbody></table>
<div id="feedback_bar"></div>
</div>
</div>

<?php $this->load->view("partial/footer"); ?>