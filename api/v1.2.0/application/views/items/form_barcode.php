<?php
echo form_open('items/generate_barcodes',array('id'=>'barcodes_form', 'target'=>'_blank'));
?>
<fieldset id="barcode_basic_info">
<legend><?php echo lang("items_barcode_general_information"); ?></legend>
<div class="field_row clearfix">
<?php echo form_label('Avery Label Size', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_radio('label_size','5267',TRUE);?> <img src='../images/pieces/label_5267.png'/>
	<?php echo form_radio('label_size','5160',TRUE);?>  <img src='../images/pieces/label_5160.png'/>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label("Start printing at label".':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'start_index',
		'id'=>'start_index',
		'value'=>1)
	);?>
	</div>
</div>
</fieldset>
<fieldset id="barcode_basic_info">
<legend><?php echo lang("items_barcode_quantity_information"); ?></legend>
<?php foreach ($items as $item) {?>
<div class="field_row clearfix">
<?php echo form_label($item['name'].':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'barcode_quantity',
		'id'=>$item['id'],
		'value'=>1)
	);?>
	</div>
</div>
<?php } ?>
<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_submit'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	var submitting = false;
    $('#barcodes_form').submit(function(){
		var count = 0;
		var item_ids = new Array();
		$('input[name=barcode_quantity]').each(function(index){
			count = $(this).val();
			while (count > 0)
			{
				item_ids.push($(this).attr('id'));
				count--;
			}
		});
		var label_size = $('input:[name=label_size]:checked').val();
		var start_index = $('#start_index').val();
		$('#barcodes_form').attr('action','<?php echo site_url("items/generate_barcodes");?>/'+item_ids.join('~')+'/'+label_size+'/'+start_index);
		return true;
	});
});
</script>