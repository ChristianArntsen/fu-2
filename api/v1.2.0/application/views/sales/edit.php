<?php 
if ($type != 'popup') {
       $this->load->view("partial/header"); ?>
       <table id="title_bar">
               <tr>
                       <td id="title_icon">
                               <img src='<?php echo base_url()?>images/menubar/sales.png' alt='title icon' />
                       </td>
                       <td id="title"><?php echo lang('sales_register')." - ".lang('sales_edit_sale'); ?> POS <?php echo $sale_info['sale_id']; ?></td>
               </tr>
       </table>
       <br />
<?php } else { ?>
<style>
       #edit_sale_wrapper {
               width:100%;
       }
</style>
<?php } ?>
<div id="edit_sale_wrapper">
	<?php echo form_hidden('edit_sale_id', $sale_info['sale_id']);?>
	<fieldset>
	<?php echo form_open("sales/save/".$sale_info['sale_id'],array('id'=>'sales_edit_form')); ?>
	<ul id="error_message_box"></ul>
	
	<div class="field_row clearfix">
	<?php echo form_label(lang('sales_receipt').':', 'sales_receipt'); ?>
		<div class='form_field'>
			<?php echo anchor('sales/receipt/'.$sale_info['sale_id'], 'POS '.$sale_info['sale_id'], array('target' => '_blank'));?>
			<?php if ($this->config->item('print_after_sale') || $this->config->item('webprnt')) { ?>
			<span style='font-size:12px; cursor:pointer; color:blue;' onclick='print_sales_receipt("<?=$sale_info['sale_id']?>")'>Print</span>
			<?php } ?>
		</div>
	</div>
	
	<div class="field_row clearfix">
	<?php echo form_label(lang('sales_date').':', 'date'); ?>
		<div class='form_field'>
			<?php echo form_input(array('name'=>'date','value'=>date(get_date_format().' '.get_time_format(), strtotime($sale_info['sale_time'])), 'id'=>'date'));?>
		</div>
	</div>
	
	<div class="field_row clearfix">
	<?php echo form_label(lang('sales_customer').':', 'customer'); ?>
		<div class='form_field'>
			<?php //echo form_dropdown('customer_id', $customers, $sale_info['customer_id'], 'id="customer_id"');?>
			<?php echo form_input(array('name'=>'customer_name','id'=>'customer_name','value'=>$customer_name)); ?>
			<?php echo form_hidden('person_id', $sale_info['customer_id']); ?>
			<?php if ($sale_info['customer_id']) { ?>
				<?php echo anchor('sales/email_receipt/'.$sale_info['sale_id'], lang('sales_email_receipt'), array('id' => 'email_receipt'));?>
			<?php }?>
		</div>
	</div>
	
	<div class="field_row clearfix">
	<?php echo form_label(lang('sales_employee').':', 'employee'); ?>
		<div class='form_field'>
			<?php echo form_dropdown('sale_employee_id', $employees, $sale_info['employee_id'], 'id="sale_employee_id"');?>
		</div>
	</div>
	
	<div class="field_row clearfix">
	<?php echo form_label(lang('sales_comment').':', 'comment'); ?>
		<div class='form_field'>
			<?php echo form_textarea(array('name'=>'comment','value'=>$sale_info['comment'],'rows'=>'4','cols'=>'23', 'id'=>'comment'));?>
		</div>
	</div>
	<?php if ($invoice_id) { ?>
	<div class='field_row clearfix'>
	<?php 
		//print_r($credit_card_payments);
		echo form_label('', 'comment');
		echo "<a href='index.php/sales/tip_window/{$sale_info['sale_id']}/{$invoice_id}/width~500' title='Add Tip - POS {$sale_info['sale_id']}'}' class='colbox2'>Add Tip</a>";
		/*echo form_label('Credit Card Payment:', 'comment');
		foreach($credit_card_payments as $cc_payment)
		{
			$cardholder_name = '';
			echo " <div class='form_field'>".$cc_payment['payment_type'].' - $'.$cc_payment['amount'].$cardholder_name." (<a title='Refund to CC' class='colbox' href='index.php/sales/confirm_refund/{$cc_payment['invoice']}'>Refund to CC</a>)</div>";
		} */
	?>
	</div>
	<?php } ?>
	<div class='field_row clearfix'>
	<?php 
		//print_r($credit_card_payments);
		echo form_label('', 'comment');
		echo "<a href='javascript:void(0)' onclick='sales.load_return(\"{$sale_info['sale_id']}\", \"{$type}\", \"{$sales_page}\")'>Issue Return</a>";
		/*echo form_label('Credit Card Payment:', 'comment');
		foreach($credit_card_payments as $cc_payment)
		{
			$cardholder_name = '';
			echo " <div class='form_field'>".$cc_payment['payment_type'].' - $'.$cc_payment['amount'].$cardholder_name." (<a title='Refund to CC' class='colbox' href='index.php/sales/confirm_refund/{$cc_payment['invoice']}'>Refund to CC</a>)</div>";
		} */
	?>
	</div>
	<?php
	echo form_submit(array(
		'name'=>'submit',
		'id'=>'submit',
		'value'=>lang('common_save'),
		'class'=>'submit_button float_left')
	);
	?>
	</form>

	<?php if ($sale_info['deleted'])
	{
	?>
	<?php echo form_open("sales/undelete/".$sale_info['sale_id'],array('id'=>'sales_undelete_form')); ?>
		<?php
		echo form_submit(array(
			'name'=>'submit',
			'id'=>'submit',
			'value'=>lang('sales_undelete_entire_sale'),
			'class'=>'submit_button float_right')
		);
		?>
	</form>
	<?php
	}
	else
	{
	?>
	<?php echo form_open("sales/delete/".$sale_info['sale_id'].'/'.$type,array('id'=>'sales_delete_form')); ?>
		<?php
		echo form_submit(array(
			'name'=>'submit',
			'id'=>'submit',
			'value'=>lang('sales_delete_entire_sale'),
			'class'=>'delete_button float_right')
		);
		?>
	</form>
	<?php
	}
	?>
</fieldset>

</div>
<script>
	var has_cc_payment = 0;

	function print_sales_receipt(sale_id) {
		<?php if ($this->config->item('print_after_sale')) { ?>
		//return;
		$.ajax({
           type: "POST",
           url: "index.php/sales/receipt/"+sale_id+"/1",
           data: "",
           success: function(response){
		   	//Delete suspended sale button
		   	    var receipt_data = '';
                // Items
				var data = response;
                <?php if ($this->config->item('webprnt')) { ?>
                    var receipt_data = build_webprnt_receipt(data);
	                console.log(receipt_data);
	                var header_data = {
                        course_name:'<?php echo $this->config->item('name')?>',
                        address:'<?php echo str_replace(array("\r", "\r\n", "\n"), ' ', addslashes($this->config->item('address')))?>',
                        address_2:'<?php echo $this->config->item('city').', '.$this->config->item('state').' '.$this->config->item('zip')?>',
                        phone:'<?php echo $this->config->item('phone')?>',
                        employee_name:'<?php echo $user_info->last_name.', '.$user_info->first_name; ?>',
                        customer:''// Need to load this in from data
                    };
                    receipt_data = webprnt.add_receipt_header(receipt_data, header_data)
                    receipt_data = webprnt.add_paper_cut(receipt_data);

                    webprnt.print(receipt_data, "http://<?=$this->config->item('webprnt_ip')?>/StarWebPrint/SendMessage");
	                //print_webprnt_receipt(receipt_data, false, data.sale_id, data);
	            <?php } else { ?>
	                var receipt_data = build_receipt(data);
	                console.log(receipt_data);
	                print_receipt(receipt_data, false, data.sale_id, data);
			    <?php } ?>        
		   	console.dir(response);
		   	//print_receipt(response);
		   },
           dataType:'json'
        });
        <?php } else { ?>
        	window.location = '<?php echo site_url(''); ?>/sales/receipt/'+sale_id;
        <?php } ?>	
   };
   function add_white_space(str_one, str_two)
	{
	       var width = 42;
	       var strlen_one = str_one.length;
	       var strlen_two = str_two.length;
	       var white_space = '';
	       var white_space_length = 0;
	       if (strlen_one + strlen_two >= width)
	               return (str_one.substr(0, width - strlen_two - 4)+'... '+str_two); //truncated if text is longer than available space
	       else
	               white_space_length = width - (strlen_one + strlen_two);
	
	       for (var i = 0; i < white_space_length; i++)
	               white_space += ' ';
	       return str_one+white_space+str_two;
	}

   function build_webprnt_receipt(data){
		// Old Receipt
		console.dir(data);
		var builder = new StarWebPrintBuilder();
		var receipt_data = '';
		var cart = data.cart;
	    //Itemized
	    for (var line in cart)
	    {
	        receipt_data += builder.createTextElement({data:add_white_space(cart[line].name+' ',cart[line].quantity+' @ '+parseFloat(cart[line].price).toFixed(2)+'    $'+(cart[line].price*cart[line].quantity).toFixed(2))+'\n'});
	    	if (parseInt(cart[line].discount) > 0)
	    		receipt_data += builder.createTextElement({data:add_white_space('     '+parseFloat(cart[line].discount).toFixed(2)+'% discount', '-$'+(cart[line].discount/100*cart[line].price*cart[line].quantity).toFixed(2))+'\n'});
	    }
	    // Totals
	    receipt_data += builder.createTextElement({data:'\n\n'+add_white_space('Subtotal: ','$'+parseFloat(data.subtotal).toFixed(2))+'\n'});
	    var taxes = data.taxes;
	    for (var tax in taxes)
	           receipt_data += builder.createTextElement({data:add_white_space(tax+': ','$'+parseFloat(taxes[tax]).toFixed(2))+'\n'});
	
	       receipt_data += builder.createTextElement({data:add_white_space('Total: ','$'+parseFloat(data.total).toFixed(2))+'\n'});
	
	    // Payment Types
	    receipt_data += builder.createTextElement({data:'\nPayments:\n'});
	    var payments = data.payments;
	    for (var payment in payments)
	    {
		    has_cc_payment += (payment+'').indexOf('xxxx') === -1 ? 0 : 1;
			receipt_data += builder.createTextElement({data:add_white_space(payment+': ','$'+parseFloat(payments[payment].payment_amount).toFixed(2))+'\n'});
	    }
	       // Change due
	       receipt_data += builder.createTextElement({data:'\n'+add_white_space('Change Due: ',(data.amount_change))+'\n'});
	    <?php if ($this->config->item('print_tip_line')) { ?>
	    receipt_data += builder.createTextElement({data:'\n\n'+add_white_space('Tip: ','$________.____')});
	    receipt_data += builder.createTextElement({data:'\n\n'+add_white_space('TOTAL CHARGE: ','$________.____')});
	    receipt_data += builder.createTextElement({data:'\n\n\nX_____________________________________________\n'});
		<?php } ?>
	    //receipt_data += chr(27)+chr(97)+chr(49);
	    receipt_data += builder.createAlignmentElement({position:'center'});
	    <?php if(trim($this->config->item('return_policy')) != '') { ?>
	    receipt_data += builder.createTextElement({data:"\n\n"});// Some spacing at the bottom
	    receipt_data += builder.createTextElement({data:"<?=addcslashes($this->config->item('return_policy'), implode('',array('"',"'")))?>"});
	    <?php } ?>
	    // NEED TO FIGURE OUT WHAT THE EQUIVALENT OF THIS IS FOR STAR WEBPRNT
		//receipt_data += "\x1Dh" +chr(80);  // ← Set height
	    receipt_data += builder.createTextElement({data:"\n\n"});// Some spacing at the bottom
	    var sale_num = data.sale_id.replace('POS ', '');
	    var len = sale_num.length;
	    // BARCODE STUFF // ADD EQUIVALENT OF THIS FOR STAR WEBPRNT
		// if (len == 3)
	        // receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0A\x7B\x41\x50\x4F\x53\x20"+sale_num;
		// else if (len == 4)
	        // receipt_data += "\x1D\x88\x01\x1D\x6B\x49\x0B\x7B\x41\x50\x4F\x53\x20"+sale_num;
	    // else if (len == 5)
	        // receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0C\x7B\x41\x50\x4F\x53\x20"+sale_num;
	    // else if (len == 6)
	        // receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0D\x7B\x41\x50\x4F\x53\x20"+sale_num;
	    // else if (len == 7)
	        // receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0E\x7B\x41\x50\x4F\x53\x20"+sale_num;
	    // else if (len == 8)
	        // receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0F\x7B\x41\x50\x4F\x53\x20"+sale_num;
	    // else if (len == 9)
	        // receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x10\x7B\x41\x50\x4F\x53\x20"+sale_num;
	    // else if (len == 10)
	        // receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x11\x7B\x41\x50\x4F\x53\x20"+sale_num;
	    // Sale id and barcode
	    receipt_data += builder.createBarcodeElement({symbology:'Code128', width:'width2', height:40, hri:false, data:data.sale_id});
	    receipt_data += builder.createTextElement({data:'\n\nSale ID: '+data.sale_id+'\n'});
	
	    return receipt_data;
	}
	function chr(i) {
      return String.fromCharCode(i);
	}

	function build_receipt(data){
		console.dir(data);
		var receipt_data = '';
		var cart = data.cart;
	    //Itemized
	    for (var line in cart)
	    {
	        receipt_data += add_white_space(cart[line].name+' ',cart[line].quantity+' @ '+parseFloat(cart[line].price).toFixed(2)+'    $'+(cart[line].price*cart[line].quantity).toFixed(2))+'\n';
	    	if (parseInt(cart[line].discount) > 0)
	    		receipt_data += add_white_space('     '+parseFloat(cart[line].discount).toFixed(2)+'% discount', '-$'+(cart[line].discount/100*cart[line].price*cart[line].quantity).toFixed(2))+'\n';
	    }
	    // Totals
	    receipt_data += '\n\n'+add_white_space('Subtotal: ','$'+parseFloat(data.subtotal).toFixed(2))+'\n';
	    var taxes = data.taxes;
	    for (var tax in taxes)
	           receipt_data += add_white_space(tax+': ','$'+parseFloat(taxes[tax]).toFixed(2))+'\n';
	
	       receipt_data += add_white_space('Total: ','$'+parseFloat(data.total).toFixed(2))+'\n';
	
	    // Payment Types
	    receipt_data += '\nPayments:\n';
	    var payments = data.payments;
	    for (var payment in payments)
	    {
		    has_cc_payment += (payment+'').indexOf('xxxx') === -1 ? 0 : 1;
		    receipt_data += add_white_space(payment+': ','$'+parseFloat(payments[payment].payment_amount).toFixed(2))+'\n';
	    }
	       // Change due
	       receipt_data += '\n'+add_white_space('Change Due: ',(data.amount_change))+'\n';
	    <?php if ($this->config->item('print_tip_line')) { ?>
	    receipt_data += '\n\n'+add_white_space('Tip: ','$________.____');
	    receipt_data += '\n\n'+add_white_space('TOTAL CHARGE: ','$________.____');
	    receipt_data += '\n\n\nX_____________________________________________\n';
		<?php } ?>
	    receipt_data += chr(27)+chr(97)+chr(49);
	    <?php if(trim($this->config->item('return_policy')) != '') { ?>
	    receipt_data += "\n\n";// Some spacing at the bottom
	    receipt_data += "<?=addcslashes($this->config->item('return_policy'), implode('',array('"',"'")))?>";
	    <?php } ?>
		receipt_data += "\x1Dh" +chr(80);  // ← Set height
	    receipt_data += "\n\n";// Some spacing at the bottom
	    var sale_num = data.sale_id.replace('POS ', '');
	    var len = sale_num.length;
		if (len == 3)
	        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0A\x7B\x41\x50\x4F\x53\x20"+sale_num;
		else if (len == 4)
	        receipt_data += "\x1D\x88\x01\x1D\x6B\x49\x0B\x7B\x41\x50\x4F\x53\x20"+sale_num;
	    else if (len == 5)
	        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0C\x7B\x41\x50\x4F\x53\x20"+sale_num;
	    else if (len == 6)
	        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0D\x7B\x41\x50\x4F\x53\x20"+sale_num;
	    else if (len == 7)
	        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0E\x7B\x41\x50\x4F\x53\x20"+sale_num;
	    else if (len == 8)
	        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x0F\x7B\x41\x50\x4F\x53\x20"+sale_num;
	    else if (len == 9)
	        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x10\x7B\x41\x50\x4F\x53\x20"+sale_num;
	    else if (len == 10)
	        receipt_data += "\x1D\x77\x02\x1D\x6B\x49\x11\x7B\x41\x50\x4F\x53\x20"+sale_num;
	    // Sale id and barcode
	    receipt_data += '\n\nSale ID: '+data.sale_id+'\n';
	
	    return receipt_data;
	}
	function print_receipt(receipt, add_delay, sale_id, data) {
		try
		{
			add_delay = add_delay == undefined ? false : add_delay;
			var applet = document.getElementById('qz');
			if (applet != null)
			{
		        //if ($.isFunction(applet.findPrinter))
		    	{
		    		applet.findPrinter("foreup");
		    		//var t = new Date();
		    		//var ct = 0;
		   			//while (!applet.isDoneFinding() && ct < t.getTime() + 4000) {
			           // Wait
			        //   console.log('not done finding yet');
			        //   c = new Date();
			        //   ct = c.getTime();
			       // }
			       if (add_delay) {
				       	var t = new Date();
			    		var ct = 0;
			   			while (!applet.isDoneFinding() && ct < t.getTime() + 4000) {
				           // Wait
				        //   console.log('not done finding yet');
				           //c = new Date();
				           //ct = c.getTime();
				        }
				        setTimeout(function(){
	
					        // Send characters/raw commands to applet using "append"
					        // Hint:  Carriage Return = \r, New Line = \n, Escape Double Quotes= \"
					        applet.append(chr(27)+chr(64));//Resets the printer
					        applet.append(chr(29) + chr(33) + chr(16)); //Font-size x2
					        applet.append(chr(27) + "\x61" + "\x31"); // center justify
	
					        applet.append("<?php echo $this->config->item('name')?>\n");
					        applet.append(chr(27)+chr(64));//Resets the printer
					        applet.append(chr(27) + "\x61" + "\x31"); // center justify
					        applet.append(chr(27) + chr(77) + chr(48)); //Font - Normal font
					        applet.append("<?php echo str_replace(array("\r", "\r\n", "\n"), ' ', addslashes($this->config->item('address')))?>\n");
					        applet.append("<?php echo $this->config->item('city').', '.$this->config->item('state').' '.$this->config->item('zip')?>\n");
					        applet.append("<?php echo $this->config->item('phone')?>\n\n");
					        applet.append(chr(27) + chr(97) + chr(48));// Left justify
					        applet.append("Employee: <?php echo $user_info->last_name.', '.$user_info->first_name; ?>\n");
					        var d = new Date();
					        var h = (d.getHours() > 12)?d.getHours()-12:d.getHours();
					        var m = (d.getMinutes() < 10)?('0'+d.getMinutes().toString()):d.getMinutes();
					        var ap = (d.getHours() < 12)?'am':'pm';
					        applet.append("Date: "+(d.getMonth()+1)+'/'+d.getDate()+'/'+d.getFullYear()+' '+h+':'+m+ap+"\n\n");
	
					        applet.append(receipt);
					        //applet.appendImage('/images/test/test.png', "ESCP");
					        //applet.append("\r\n");
					        //applet.appendImage('http://10.150.13.82:8888/images/test/test.png', "ESCP");
					        //applet.append("\r\n");
					        //applet.appendImage('../../images/test/test.png', "ESCP");
					        //applet.append("\r\n");
					        applet.append("\n\n\n\n\n\n");// Some spacing at the bottom
					        applet.append(chr(27) + chr(105)); //Cuts the receipt
	
					        // Send characters/raw commands to printer
					        //applet.forceAccept();
					        applet.print();
					        open_cash_drawer();
					    }, 1);
				    }
				    else {
				    	console.log('not timing out');
						// Send characters/raw commands to applet using "append"
				        // Hint:  Carriage Return = \r, New Line = \n, Escape Double Quotes= \"
				        applet.append(chr(27)+chr(64));//Resets the printer
				        //applet.append(chr(29) + chr(33) + chr(16)); //Font-size x2
				        applet.append(chr(27) + "\x61" + "\x31"); // center justify
	
				        applet.append("<?php echo $this->config->item('name')?>\n");
				        applet.append(chr(27)+chr(64));//Resets the printer
				        applet.append(chr(27) + "\x61" + "\x31"); // center justify
				        applet.append(chr(27) + chr(77) + chr(48)); //Font - Normal font
				        applet.append("<?php echo str_replace(array("\r", "\r\n", "\n"), ' ', addslashes($this->config->item('address')))?>\n");
				        applet.append("<?php echo $this->config->item('city').', '.$this->config->item('state').' '.$this->config->item('zip')?>\n");
				        applet.append("<?php echo $this->config->item('phone')?>\n\n");
				        applet.append(chr(27) + chr(97) + chr(48));// Left justify
				        applet.append("Employee: <?php echo $user_info->last_name.', '.$user_info->first_name; ?>\n");
				        <?php //if ($customer != '') { ?>
				        if (data != undefined && data.customer != undefined)
					        applet.append("Customer: <?php //echo $customer; ?>"+data.customer+"\n");
				        <?php //} ?>
				        var d = new Date();
				        var h = (d.getHours() > 12)?d.getHours()-12:d.getHours();
				        var m = (d.getMinutes() < 10)?('0'+d.getMinutes().toString()):d.getMinutes();
				        var ap = (d.getHours() < 12)?'am':'pm';
				        applet.append("Date: "+(d.getMonth()+1)+'/'+d.getDate()+'/'+d.getFullYear()+' '+h+':'+m+ap+"\n\n");
				        //applet.append("<?php echo $cab_name.': '.$customer_account_balance; ?>\n");
				        //applet.append("<?php echo $cmb_name.': '.$customer_member_balance; ?>\n");
				        <?php if ($giftcard) { ?>
				        //applet.append("Giftcard Balance: <?php echo $giftcard_balance; ?>\n");
				        <?php } ?>
	
				        applet.append(receipt);
				        //applet.appendImage('/images/test/test.png', "ESCP");
				        //applet.append("\r\n");
				        //applet.appendImage('http://10.150.13.82:8888/images/test/test.png', "ESCP");
				        //applet.append("\r\n");
				        //applet.appendImage('../../images/test/test.png', "ESCP");
				        //applet.append("\r\n");
				        <?php if ($this->config->item('online_booking')) { ?>
			        	applet.append("\nAnd remember, you can book your\nnext tee time online at\n<?=$this->config->item('website')?>");
			        	<?php } ?>
	
				        applet.append("\n\n\n\n\n\n");// Some spacing at the bottom
				        applet.append(chr(27) + chr(105)); //Cuts the receipt
	
				        // Send characters/raw commands to printer
				        //applet.forceAccept();
				        applet.print();
				        open_cash_drawer();
			        }
				}
			}
		}
		catch(err)
		{
			set_feedback('Unable to print receipt at this time. Please print manually','error_message',false);
		}
	}
	console.log('setting up validate');
	$(document).ready(function(){
		
		$('#customer_name').focus().select();
		$('#customer_name' ).autocomplete({
			source: '<?php echo site_url('customers/customer_search/last_name'); ?>',
			delay: 10,
			autoFocus: false,
			minLength: 0,
			select: function(event, ui) {
				event.preventDefault();
				$('#customer_name').val(ui.item.label);
				$('#person_id').val(ui.item.value);
			},
			focus: function(event, ui) {
				event.preventDefault();
				//$('#teetime_title').val(ui.item.label);
			}
		});
		$('.colbox').colorbox({width:550});
		$('.colbox2').colorbox2();
		$('#feedback_bar').click(function(){$('#feedback_bar').slideUp(250,function(){$('#feedback_bar').removeClass()});});
		$("#email_receipt").click(function()
		{
			$.get($(this).attr('href'), function()
			{
				alert("<?php echo lang('sales_receipt_sent'); ?>")
			});
			
			return false;
		});
		//$('#date').datePicker({startDate: '<?php echo get_js_start_of_time_date(); ?>'});
	    $('#date').datetimepicker();        
	       
	    $("#sales_undelete_form").submit(function()
		{
			if (!confirm('<?php echo lang("sales_undelete_confirmation"); ?>'))
			{
				return false;
			}
		});
	    $('#sales_delete_form').validate({
            submitHandler:function(form)
            {
                if (confirm("<?php echo lang("sales_delete_confirmation"); ?>"))
                {
                    $(form).ajaxSubmit({
                        success:function(response)
                        {
                        	if(response.success)
                            {
                                set_feedback('Sale succesfully deleted.','success_message',false);
                                var sale_id = $('#edit_sale_id').val();
                                if (typeof delete_recent_transaction == 'function')
	                                delete_recent_transaction(sale_id);
		                        if (typeof reports != 'undefined')
	                            	reports.generate();
						        $.colorbox.close();
                            }
                            else
                            {
                                set_feedback('Unable to delete sale.','error_message',true);    
                            }
                        },
                        dataType:'json'
                    });
                }
            },
            errorLabelContainer: "#error_message_box",
            wrapper: "li",
            rules: 
            {
            },
            messages: 
            {
            }
        });
	   $('#sales_edit_form').validate({
			submitHandler:function(form)
			{
				$(form).ajaxSubmit({
				success:function(response)
				{
					if(response.success)
					{
						set_feedback(response.message,'success_message',false);
						$.colorbox.close();
	                    reports.generate();
					}
					else
					{
						set_feedback(response.message,'error_message',true);	
						
					}
				},
				dataType:'json'
			});
	
			},
			errorLabelContainer: "#error_message_box",
	 		wrapper: "li",
			rules: 
			{
	   		},
			messages: 
			{
			}
		});
		console.log('inside doc ready');
	});
</script>
<?php 
if ($type != 'popup')
{
?>
 <div id="feedback_bar"></div>
<?php  $this->load->view("partial/footer"); 
}
?>
