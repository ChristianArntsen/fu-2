<?php 
//print_r($suspended_sales);
	$suspended_sales_html = '';
	$selected_table = '';
	foreach($suspended_sales->result_array() as $ss)
	{
		$selected_class = '';
		if ("{$ss['sale_id']}" == "$selected_suspended_sale_id")
		{
		//	print_r($ss);
			$selected_class = 'selected';
			$selected_table = $ss['table_id'];
		}
		$total = $this->Sale_suspended->get_sale_total($ss['sale_id']);
		$ss_customer = $this->Sale_suspended->get_customer($ss['sale_id']);
		//echo $this->db->last_query();
		$suspended_sales_html .= "<a class='table_number $selected_class' id='ss_{$ss['sale_id']}' href='javascript:void(0)' onclick='sales.unsuspend_sale({$ss['sale_id']}, {$ss['table_id']})'><div><div class='table_number_label'>Table {$ss['table_id']}</div><div class='price_box'>".to_currency($total)."</div><div class='clear'></div><div class='ss_customer_name'>".($ss_customer->last_name?$ss_customer->last_name.', '.$ss_customer->first_name:'')."</div></div></a>";
	}
	$suspended_sales_html = ($suspended_sales_html != '' ? $suspended_sales_html : '<div class="no_suspended_sales">No Suspended Sales</div>'); 
?>
<?php echo $suspended_sales_html;?>
<div class='clear'></div>
<div class='contextMenu' id='myMenu' style='display:none'>
	<ul>
	    <li id="print_suspended_sale">Print Receipt</li>            
	    <li id="delete_suspended_sale">Delete</li>            
	</ul>
</div>
<script type="text/javascript">
var bindings = {
	'delete_suspended_sale': function(t) {
		var sale_id = $(t).attr('id').replace('ss_','');
		//console.log(sale_id);
		//return;
		$.ajax({
           type: "POST",
           url: "index.php/sales/delete_suspended_sale/"+sale_id,
           data: "",
           success: function(response){
		   	//Delete suspended sale button
		   	$(t).remove();
		   },
           dataType:'json'
        });
   	},
	'print_suspended_sale': function(t) {
		var sale_id = $(t).attr('id').replace('ss_','');
		//console.log(sale_id);
		<?php if ($this->config->item('print_after_sale')) { ?>
		//return;
		$.ajax({
           type: "POST",
           url: "index.php/sales/print_suspended_sale/"+sale_id+'/true',
           data: "",
           success: function(response){
		   	//Delete suspended sale button
		   	    var receipt_data = '';
                // Items
				var data = response;
                <?php if ($this->config->item('webprnt')) { ?>
                    var receipt_data = build_webprnt_receipt(data, true);
	                console.log(receipt_data);
	                var header_data = {
                        course_name:'<?php echo $this->config->item('name')?>',
                        address:'<?php echo str_replace(array("\r", "\r\n", "\n"), ' ', addslashes($this->config->item('address')))?>',
                        address_2:'<?php echo $this->config->item('city').', '.$this->config->item('state').' '.$this->config->item('zip')?>',
                        phone:'<?php echo $this->config->item('phone')?>',
                        employee_name:'<?php echo $user_info->last_name.', '.$user_info->first_name; ?>',
                        customer:''// Need to load this in from data
                    };
                    receipt_data = webprnt.add_receipt_header(receipt_data, header_data)
                    receipt_data = webprnt.add_paper_cut(receipt_data);

                    webprnt.print(receipt_data, "http://<?=$this->config->item('webprnt_ip')?>/StarWebPrint/SendMessage");
	                //print_webprnt_receipt(receipt_data, false, data.sale_id, data);
	            <?php } else { ?>
	                var receipt_data = build_receipt(data);
	                console.log(receipt_data);
	                print_receipt(receipt_data, false, data.sale_id, data);
			    <?php } ?>        
		   	console.dir(response);
		   	//print_receipt(response);
		   },
           dataType:'json'
        });
        <?php } else { ?>
        	window.location = '<?php echo site_url(''); ?>/sales/receipt/'+sale_id;
        <?php } ?>	
    }
};
$('.table_number').contextMenu('myMenu',{
	bindings:bindings,
	onShowMenu: function(e, menu) {
        return menu;
      }
});
</script>