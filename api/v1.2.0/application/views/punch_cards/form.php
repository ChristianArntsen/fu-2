<ul id="error_message_box"></ul>
<?php
if ($cart_line)
	echo form_open('sales/save_punch_card_details/'.$cart_line,array('id'=>'punch_card_form'));
else
	echo form_open('giftcards/save_punch_card/'.$punch_card_info->punch_card_id,array('id'=>'punch_card_form'));
?>
<fieldset id="punch_card_basic_info">

<div class="field_row clearfix">
<?php echo form_label(lang('giftcards_punch_card_number').':<span class="required">*</span>', 'punch_card_number',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo $view_only ? $punch_card_info->punch_card_number :
	form_input(array(
		'name'=>'punch_card_number',
		'size'=>'24',
		'maxlength'=>'16',
		'id'=>'punch_card_number',
		'value'=>$punch_card_info->punch_card_number)
	);?>
	</div>
</div>

<!-- <div class="field_row clearfix">
<?php echo form_label(lang('giftcards_card_value').':<span class="required">*</span>', 'name',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo $view_only ? $punch_card_info->value :
	form_input(array(
		'name'=>'value',
		'size'=>'8',
		'id'=>'value',
		'value'=>$punch_card_info->value)
	);?>
	</div>
</div> -->
<div class="field_row clearfix">
<?php echo form_label(lang('giftcards_customer_name').':', 'customer',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php echo $view_only ? $customer : form_input(array('name'=>'customer','id'=>'customer','value'=>($punch_card_info->customer_name != '' ? $punch_card_info->customer_name : ''), 'placeholder'=>'No Customer'/*lang('sales_start_typing_customer_name'),  'accesskey' => 'c'*/));?>
		<?php echo form_hidden('customer_id', $punch_card_info->customer_id);?>
		<?php //echo form_dropdown('customer_id', $customers, $giftcard_info->customer_id, 'id="customer_id"');?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('giftcards_expiration_date').':', 'expiration_date',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php
		$expiration_date = ($punch_card_info->expiration_date != '0000-00-00')?$punch_card_info->expiration_date:'No expiration'; 
		echo $view_only ? $expiration_date : form_input(array('name'=>'expiration_date','id'=>'expiration_date','value'=>$expiration_date));?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('giftcards_details').':', 'details',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php 
			echo form_hidden('item_kit_id', $punch_card_info->item_kit_id);
		?>
		<?php
			$items = $punch_card_info->value;
			//print_r($items);
			foreach($items as $item)
			{
				$qty = floor($item->quantity);
				echo "{$item->name} (Qty $qty)";
			}
		?>
	</div>
</div>

<!-- <div class="field_row clearfix">
<?php echo form_label(lang('giftcards_department').':', 'department',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php
		echo $view_only ? '' :
			form_radio(array(
                    'name'=>'dept_cat',
                    'id'=>'dept_cat_1',
                    'value'=>'department',
                    'checked'=>$punch_card_info->category == ''));	
		echo $view_only ? $punch_card_info->department : form_input(array('name'=>'department','id'=>'department','value'=>$punch_card_info->department));?>
	</div>
</div>
<div class="field_row clearfix">
<?php echo form_label(lang('giftcards_category').':', 'category',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php
		echo $view_only ? '' :
			form_radio(array(
                    'name'=>'dept_cat',
                    'id'=>'dept_cat_2',
                    'value'=>'category',
                    'checked'=>$punch_card_info->category != ''));	
		echo $view_only ? $punch_card_info->category : form_input(array('name'=>'category','id'=>'category','value'=>$punch_card_info->category, 'readonly'=>'readonly'));?>
	</div>
</div> -->
<div class="field_row clearfix">
<?php echo form_label(lang('giftcards_notes').':', 'details',array('class'=>'wide')); ?>
	<div class='form_field'>
		<?php
		echo $view_only ? $punch_card_info->details : form_textarea(array('name'=>'details','id'=>'details','value'=>$punch_card_info->details,'rows'=>2));?>
	</div>
</div>

<?php
echo $view_only ? '' :
form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
if (!$view_only) {
?>
<style>
	#details {
		width:470px;
	}
</style>
<script type='text/javascript'>

//validation and submit handling
$(document).ready(function()
{
	$('#expiration_date').datepicker({dateFormat:'yy-mm-dd'});
	$('#customer').click(function()
    {
    	$(this).attr('value','');
       	$('input[name=customer_id]').val('');
    });
    $('#punch_card_number').bind('keypress', function(e) {
    	var code = (e.keyCode ? e.keyCode : e.which);
		 if(code == 13) { //Enter keycode
		   //Do something
		   e.preventDefault();
		 }
    });
    $( "#customer" ).autocomplete({
		source: '<?php echo site_url("sales/customer_search/last_name"); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)
		{
			event.preventDefault();
			$("#customer").val(ui.item.label);
			$("input[name=customer_id]").val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$("#teetime_title").val(ui.item.label);
		}
	});
	var submitting = false;
    $('#punch_card_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$(form).mask("<?php echo lang('common_wait'); ?>");
			<?php if ($punch_card_info->punch_card_id) { ?>
				$(form).ajaxSubmit({
					success:function(response)
					{
						$.colorbox.close();
		                submitting = false;
		                <?php if ($cart_line) {?>
		                	console.dir(response);
						    //result = eval('('+response+')');
			                sales.update_cart_info(<?php echo ($cart_line) ?>, response.item_info);
			                sales.update_basket_totals(<?php echo ($cart_line) ?>, response.basket_info);
		           	
						<?php } else { ?>
							post_giftcard_form_submit(response);
						<?php } ?>
					},
					dataType:'json'
				});
			<?php } else {?>
				$.ajax({
		           type: "POST",
		           url: <?php if ($cart_line) {?>"index.php/sales/punch_card_exists/"+$('#punch_card_number').val()<?php } else { ?>"index.php/giftcards/punch_card_exists/"+$('#punch_card_number').val()<?php } ?>,
		           data: "",
		           success: function(response){
				   	   if (!response.exists)
				   	   {
	   	   					$(form).ajaxSubmit({
								success:function(response)
								{
									$.colorbox.close();
					                submitting = false;
					                <?php if ($cart_line) {?>
					                	console.dir(response);
									    //result = eval('('+response+')');
						                sales.update_cart_info(<?php echo ($cart_line) ?>, response.item_info);
						                sales.update_basket_totals(<?php echo ($cart_line) ?>, response.basket_info);
					           	
									<?php } else { ?>
										post_giftcard_form_submit(response);
									<?php } ?>
								},
								dataType:'json'
							});
	
				   	   }
				   	   else
				   	   {
				   	   		submitting = false;
				   	   		$(form).unmask();
							alert('That punch card number is already in use, please select a different one.');
				   	   }
				   },
		           dataType:'json'
		        });
		    <?php } ?>
		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			punch_card_number:
			{
				required:true
			}
   		},
		messages:
		{
			punch_card_number:
			{
				required:"<?php echo lang('punch_card_number_required'); ?>",
				number:"<?php echo lang('punch_card_number'); ?>"
			}
		}
	});
});
</script>
<script>
	$(document).ready(function(){
		var gcn = $('#punch_card_number');
		gcn.keydown(function(event){
			// Allow: backspace, delete, tab, escape, and enter
	        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
	             // Allow: Ctrl+A
	            (event.keyCode == 65 && event.ctrlKey === true) || 
	             // Allow: home, end, left, right
	            (event.keyCode >= 35 && event.keyCode <= 39)) {
	                 // let it happen, don't do anything
	                 return;
	        }
	        else {
	            // Ensure that it is a number and stop the keypress
	            //if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
	            	console.log('kc '+event.keyCode);
	            if (event.keyCode == 186 || event.keyCode == 187 || event.keyCode == 191 || (event.shiftKey && event.keyCode == 53)) {
	                event.preventDefault(); 
	            }   
	        }
		});
		//gcn.focus();
	})
</script>
<?php } ?>