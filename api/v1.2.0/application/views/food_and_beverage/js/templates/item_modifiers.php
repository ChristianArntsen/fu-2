<script type="text/html" id="template_item_modifiers">
<% if(title){ %>
<h1><%=title %></h1>
<% } %>
<ul id="sales_item_modifiers">
<% if(modifiers){ %>
<% _.each(modifiers.models, function(modifier){
if(category_id != null && modifier.get('category_id') != category_id){
	return true;
}
var options = modifier.get('options');
var selected_option = modifier.get('selected_option');
var selected_price = modifier.get('selected_price');
%>
	<li>
		<span class="name"><%= modifier.get('name') %></span>
		<span class="price">
			$<span class="value"><%-accounting.formatMoney(selected_price,'')%></span>
		</span>
		<span class="options">
		<% _.each(modifier.get('options'), function(option){ %>
		<% if(selected_option && selected_option.toLowerCase() == option.label.toLowerCase()) {
			var btnClass = ' selected';
		}else{
			var btnClass = '';
		} %>
			<a data-modifier-id="<%= modifier.get('modifier_id') %>" data-price="<%-accounting.toFixed(option.price, 2) %>" class="option_button<%= btnClass %>" data-value="<%= option.label %>" href="#">
				<%-_.capitalize(option.label)%>
			</a>
		<% }); %>
		</span>
	</li>
<% }); %>
<% }else{ %>
<li>No modifiers available</li>
<% } %>
</ul>
<% if(title){ %>
<a class="fnb_button back" style="font-size: 14px;">&lt; Back</a>
<% } %>
</script>