var RecentTransactionView = Backbone.View.extend({
	tagName: "li",
	template: _.template( $('#template_recent_transaction').html() ),

	events: {
		"click a.add_tip": "addTip"
	},

	initialize: function() {
		this.listenTo(this.model, "change", this.render);
		this.listenTo(this.model.get('tips'), "add remove change", this.render);
	},

	render: function() {
		this.$el.html(this.template(this.model.attributes));
		return this;
	},

	addTip: function(event){
		var button = $(event.currentTarget);
		var data = {};
		var type = button.data('type');
		var amount = button.data('amount');
		var invoice_id = parseInt(button.data('invoice-id'));
		if(isNaN(invoice_id)){
			invoice_id = 0;
		}
		var tips = this.model.get('tips');

		if(tips && invoice_id != 0){
			var tipModel = tips.findWhere({'invoice_id': invoice_id});
			if(tipModel){
				amount = tipModel.get('amount');
			}

		}else if(tips && tips.get(type)){
			var tipModel = tips.get(type);

			if(tipModel){
				amount = tipModel.get('amount');
			}
		}

		data.invoice_id = invoice_id;
		data.type = type;
		data.amount = amount;

		var addTipWindow = new AddTipView({model: data, collection: tips});

		$.colorbox2({
			title: 'Add Tip',
			html: addTipWindow.render().el,
			width: 600,
			onClosed: function(){
				addTipWindow.remove();
			}
		});

		return false;
	}
});