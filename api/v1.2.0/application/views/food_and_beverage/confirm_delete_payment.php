<?php
echo form_open("food_and_beverage/delete_payment/".urlencode(urlencode($payment_id)),array('id'=>'edit_payment_form'.$payment_id));
//print_r($payment);
?>
<table>
	<tbody>
		<tr>
			<td colspan=2><div style='padding:10px;'>You are about to return a payment to card: </div></td>
		</tr>
		<tr>
			<!--td id="pt_delete"><?php echo anchor("food_and_beverage/delete_payment/".urlencode(urlencode($payment_id)),'['.lang('common_delete').']');?></td-->
			<td id="pt_type"><?php echo  $payment['payment_type']    ?> </td>
			<td id="pt_amount"><?php echo  to_currency($payment['payment_amount'])  ?>  </td>
		</tr>
	</tbody>
</table>
<div class='clear' style='text-align:center; margin-top:30px;'>
<?php
echo anchor("food_and_beverage/delete_payment/".$payment_id,'Issue Return',array('name'=>'submit','id'=>'submit','class'=>'submit_button float_right'));
?>
</div>
</form>
