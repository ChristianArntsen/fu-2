	<?php
		$bgc = '#c8c8c8';
		$bc = '#606060';
		$fc = '#606060';
		$hfs = '10px';
		$hfw = 'bold';
		$cp = '2px 5px';
	?>
<div id="receipt_wrapper">
	<table cellspacing="0" cellpadding="3" style='width:600px; font-family:arial,sans-serif; font-size:12px; border-collapse:collapse'>
		<thead>
			<tr>
				<th>
					<!-- Logo -->
					<img src='http://rc.foreup.com/images/header/header_logo2.png'/>
				</th>
				<th style='text-align:left; font-size:12px; font-weight: normal'>
					<div id="company_name">ForeUP</div>
					<div id="company_address">815 West 1250 South</div>
					<div id="company_address">Orem, UT 84058</div>
					<div id="company_phone">p: 801.215.9487</div>
					<div id="company_phone">e: billing@foreup.com</div>
				</th>
				<th colspan=2 style='font-size:24px; text-align: right;'>
					Invoices
				</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' >GOLF COURSE</td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' >CREDIT CARD</td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' >ITEMS</td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' >TOTAL</td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' >SUCCESS</td>				
			</tr>
			<?php foreach ($summary_data as $data) { ?>
				<tr>
					<td style='border:1px solid <?=$bc?>;'><?php echo $data['customer']; ?></td>
					<td style='border:1px solid <?=$bc?>;'><?php echo $data['payment_data']['card_type'].' '.$data['payment_data']['masked_account']; ?></td>
					<td style='border:1px solid <?=$bc?>;'>
						<table style='font-family:arial,sans-serif; font-size:12px; border-collapse:collapse'>
							<tbody>
							<?php foreach ($data['items'] as $line=>$item) { ?>
								<tr>
									<td style='border:1px solid <?=$bc?>;'><?php echo $item['description']; ?></td>
									<td style='padding:<?=$cp?>; border:1px solid <?=$bc?>; text-align:right;'><?php echo to_currency($item['amount']); ?></td>
								</tr>
							<?php } ?>
							</tbody>
						</table>
					</td>
					<td style='border:1px solid <?=$bc?>;'><?php echo $data['totals']['total']; ?></td>
					<td style='border:1px solid <?=$bc?>; color:<?php echo $data['payment_data']['status'] == 'Approved' ? 'green' : 'red'?>;'><?php echo $data['payment_data']['status']; ?></td>
				</tr>
			<?php } ?>
			<tr>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' ></td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' ></td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' ></td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' >TOTAL</td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' >(<?=$failed_count + $billed_count?>) <?=$total_failed + $total_billed?></td>				
			</tr>
			<tr>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' ></td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' ></td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' ></td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' >TOTAL FAILED</td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' >(<?=$failed_count?>) <?=$total_failed?></td>				
			</tr>
			<tr>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' ></td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' ></td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' ></td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' >TOTAL BILLED</td>
				<td style='font-size:<?=$hfs?>; font-weight:<?=$hfw?>; padding:<?=$cp?>; border:1px solid <?=$bc?>; background:<?=$bgc?>; color:<?=$fc?>' >(<?=$billed_count?>) <?=$total_billed?></td>				
			</tr>
			
		</tbody>
	</table>
</div>