<html>
  <head>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

    <script type="text/javascript">
	$(function () {
		var chart;
		$(document).ready(function() {
			$.ajax({
			   type: "POST",
			   url: "<?php echo site_url('dashboard/fetch_mercury_data'); ?>",
			   data: '',
			   success: function(response){
				console.dir(response);
					chart = new Highcharts.Chart({
						chart: {
							renderTo: 'chart_div',
							type: 'line',
							marginRight: 130,
							marginBottom: 25
						},
						title: {
							text: 'Mercury Charges',
							x: -20 //center
						},
						subtitle: {
							text: '',
							x: -20
						},
						xAxis: {
							categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
								'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
						},
						yAxis: {
							title: {
								text: 'Dollars ($)'
							},
							plotLines: [{
								value: 0,
								width: 1,
								color: '#808080'
							}]
						},
						tooltip: {
							formatter: function() {
									return '<b>'+ this.series.name +'</b><br/>'+
									this.x +': $'+ (this.y).toFixed(2);
							}
						},
						legend: {
							layout: 'vertical',
							align: 'right',
							verticalAlign: 'top',
							x: -10,
							y: 100,
							borderWidth: 0
						},
						series: response
					});
				},
				dataType:'json'
			});
		});
	});
	</script>

  </head>
  <body>
  	<script src="<?php echo base_url('js/highcharts.js'); ?>"></script>
	<script src="<?php echo base_url('js/exporting.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('js/gray.js'); ?>"></script>
	<div id="chart_div" style="width: 900px; height: 300px;"></div>
  </body>
</html>