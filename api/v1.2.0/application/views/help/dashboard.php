<?= $superadmin ?>
<div class="wrapper">
<div id="desk-external-variables-page_index" class="desk-external-variables">
  <div id="system-snippets-just_moderated"></div>
</div>
<?= $search_form; ?>
<div id='support-main'>

	<div class='support-body'>
		<div class='content dashboard'>
			<h3>Browse by Topic</h3>
      <table width='100%' cellspacing='0'>
        <tr class="row1">
          <td class="col1">
            <ul class="modules">
              <?php foreach($modules['col1'] as $mc1): ?>
              <li>
                <div class="module-item">
                  <img src="<?= $mc1['img'] ?>" alt=""/>
                  <a href="<?= $mc1['link'] ?>"><?= $mc1['module'] ?></a>
                </div>
              </li>
              <?php endforeach; ?>
            </ul>
          </td>
          <td class="col2">
            <ul class="modules">
              <?php foreach($modules['col2'] as $mc2): ?>
              <li>
                <div class="module-item">
                  <img src="<?= $mc2['img'] ?>" alt=""/>
                  <a href="<?= $mc2['link'] ?>"><?= $mc2['module'] ?></a>
                </div>
              </li>
              <?php endforeach; ?>
            </ul>
          </td><td class="col3">
            <ul class="modules">
              <?php foreach($modules['col3'] as $mc3): ?>
              <li>
                <div class="module-item">
                  <img src="<?= $mc3['img'] ?>" alt=""/>
                  <a href="<?= $mc3['link'] ?>"><?= $mc3['module'] ?></a>
                </div>
              </li>
              <?php endforeach; ?>
            </ul>
          </td>
        </tr>
      </table>
      <br/>
			<table width='100%' cellspacing='0'>
				<tr class="row1">
          <td class="col1" style="width:357px;">
						<div class='topic'>
							<h4>Common Questions</h4>

								<h5 class='articles'><?= $common_questions_count; ?> Articles <a href='<?= site_url('support/get_all_common_questions') ?>'>View All</a>
								</h5>
								<ul>
                    <?php foreach($common_questions as $cq): ?>
										<li>
											<a href='<?= $cq['link']; ?>'><?= $cq['title']; ?></a>
										</li>
                    <?php endforeach; ?>		
								</ul>
						</div>
					</td>
          <td class="col2"  style="width:357px;">
						<div class='topic'>
							<h4>Tutorials</h4>
								<h5 class='articles'><?= $tutorials_count; ?> Articles <a href='<?= site_url('support/get_all_tutorials') ?>'>View All</a></h5>
								<ul>
										<?php foreach($tutorials as $tut): ?>
										<li>
											<a href='<?= $tut['link']; ?>'><?= $tut['title']; ?></a>
										</li>
                    <?php endforeach; ?>		
								</ul>
						</div>
					</td>
          </tr>
				</table>

		</div>
	</div>
</div>
  <?= $side_bar ?>
</div>

<script type="text/javascript">

$(document).ready(function(){
    $('.onclick-go-back').click(function() {
        history.back();
        return false;
    });

    $('#articleSubmitButton').removeClass('hidden');
    

  $('#topic_content').wysiwyg({
    initialContent: '',
    maxHeight:25,
    height:25,
    width: 239,
    maxWidth:239,
    controls: {
      html: {visible : true }
    },
    iFrameClass: 'wysiwyg-title-iframe'
  });
      
  $('#articleSubmitButton').click(function(e){
    e.preventDefault();
    var content = $('#topic_content').val();
    content = $.trim(content);
    var title = $('#topic_title').val();
    title = $.trim(title);
    if(content.length < 1 || title.length < 1)
    {
       alert('Error: Title or Content must not be empty.');
    }
    else
    {
      var ra = '{"list":[';
      for(j=0;j<5;j++)
      {
        var input_id = '#topic_related_articles-' + j;
        var mask_id = '#ra-mask-' + j;
        var id = $(mask_id).html();
        if(id.length > 0) ra = ra + obj_to_string({id:id,title:$(input_id).val()}) + ',';
      }

      ra = ra.substring(0, ra.length-1);
      ra = ra + ']}';
      
      $.ajax({
          url: "<?= site_url('support/save'); ?>",
          type: "POST",
          data: {
              id: $('#topic_id').val(),
              type: $('#topic_type').val(),
              title: title,
              content: content,
              topic_id: $('#topic_module').val(),
              keywords: $('#topic_keywords').val(),
              related_articles: ra
          },
          success: function( res ) {
            alert(res);
            window.location.reload();
          },
          dataType:'text'
        });
      }
  });
  
	});
  </script>