<?= $superadmin ?>

<div class="wrapper <?php if(empty($superadmin)) echo 'single-topic'; ?>">
  <?= $search_form; ?>
  <div id="breadcrumbs">
    <a href="<?= site_url('support'); ?>">Home</a> 
    › <?= $from ?>
    › <?= $title ?>
  </div>  
  <div id="support-main">    
      <div class="support-body">
        <div class="content article">
          <div class="title">
            <h3><span id="post-title"><?= $title; ?></span></h3>
            <div class="meta">Last Updated: <?= $date_modified; ?></div>
          </div>
          <div class="article-content" id="post-content"><?= $contents ?></div>
          <div id="rate-response"><?= $options; ?></div>
          <span id="post-type" class="hidden"><?= $type ?></span>
          <span id="post-topic" class="hidden"><?= $topic_id ?></span>
          <span id="post-keywords" class="hidden"><?= $keywords ?></span>
          <div class="related-articles">
            <?php if(count($related_articles) > 0): ?>
            <p>Other users also find these articles helpful:</p>
            <ol>
            <?php foreach($related_articles as $ra): ?>
              <li><a href="<?= $ra['link']; ?>"><?= $ra['title']; ?></a></li>
            <?php endforeach; ?>
            </ol>
            <?php endif; ?>
          </div>
        </div>
    </div>
  </div>
  <?= $side_bar ?>
</div>


<div id="dialog" title="Confirmation Required" class="hidden">
  Are you sure you want to delete the selected article?
</div>

<div id="dialog-form" title="Thank you for your help" class="hidden">
	<form>
	<fieldset>
		<label for="name">"We are always wanting to improve. What about this article could be improved?"</label><br/>
		<textarea rows="5" cols="30" name="imporvement-details" id="imporvement-details" style="width:250px;"></textarea>
	</fieldset>
	</form>
</div>

<script type="text/javascript">
$(document).ready(function()
{
  // reset all values
var reset = function(){
    $('#topic_module').attr('disabled', '');
    $('#articleSubmitButton').removeClass('hidden');
    $('#topic_type').val('');
    $('#topic_module').val('');
    $('#topic_title').val('');
    $('#topic_content').val('');
    $('#topic_id').val('');
    $('#topic_keywords').val('');
    $('#topic_module').val('<?= $topic_id; ?>');
}

reset();

$('#topic_content').wysiwyg({
        initialContent: '',
        maxHeight:25,
        height:25,
        width: 239,
        maxWidth:239,
        controls: {
          html: {visible : true }
        },
        iFrameClass: 'wysiwyg-title-iframe'
      });
      
  $('.article-edit').click(function(){
      reset();
      var id = '<?= $post_id ?>';

      var type = $('#post-type').html();
      var topic = $('#post-topic').html();
      var title = $('#post-title').html();
      var content = $('#post-content').html();
      var keywords = $('#post-keywords').html();

      $('#topic_type').val(type);
      $('#topic_title').val(title);
      $('#topic_content').val(content);
      $('#topic_id').val(id);
      $('#topic_keywords').val(keywords);
      
      $('#topic_content-wysiwyg-iframe').contents().find('body').html(content);
      $('#topic_title').trigger('blur');
  });

  $('#articleSubmitButton').click(function(e){
    e.preventDefault();

    var content = $('#topic_content').val();
    content = $.trim(content);
    var title = $('#topic_title').val();
    title = $.trim(title);
    if(content.length < 1 || title.length < 1)
    {
       alert('Error: Title or Content must not be empty.');
    }
    else
    {
      var ra = '{"list":[';
      for(j=0;j<5;j++)
      {
        var input_id = '#topic_related_articles-' + j;
        var mask_id = '#ra-mask-' + j;
        var id = $(mask_id).html();
        if(id.length > 0) ra = ra + obj_to_string({id:id,title:$(input_id).val()}) + ',';
      }

      ra = ra.substring(0, ra.length-1);
      ra = ra + ']}';
      
      $.ajax({
          url: "<?= site_url('support/save'); ?>",
          type: "POST",
          data: {
              id: $('#topic_id').val(),
              type: $('#topic_type').val(),
              title: title,
              content: content,
              topic_id: $('#topic_module').val(),
              keywords: $('#topic_keywords').val(),
              related_articles: ra
          },
          success: function( res ) {
            alert(res);
            window.location.reload();
          },
          dataType:'text'
        });
      }
  });

  $('.article-delete').click(function(){
      $("#dialog").dialog({
      buttons : {
        "Confirm" : function() {
          //Some ajax here
          $.ajax({
            url: "<?= site_url('support/delete_post/'); ?>",
            type: "POST",
            data: {
                id: '<?= $post_id ?>'
            },
            success: function( res ) {
              alert(res);
              window.location.href = '<?= site_url("support/get_topics/{$topic_id}"); ?>';
            },
            dataType:'text'
          });
          $(this).dialog("close");
        },
        "Cancel" : function() {
          $(this).dialog("close");
        }
      }
    });

    $("#dialog").dialog("open");
   });

    $('#rate-yes').click(function(e){
      e.preventDefault();
      $.ajax({
        url: $(this).attr('href'),
        type: "GET",
        success: function( res ) {
          $('#rate-response').html(res);
        },
        dataType:'text'
      });
    });

    $('#rate-no').click(function(e){
      e.preventDefault();
         $.ajax({
          url: $(this).attr('href'),
          type: "GET",
          success: function( res ) {
            $('#rate-response').html(res);
            /* improvement dialog */
            $( "#dialog-form" ).dialog({
                modal: true,
                buttons: {
                  "Send": function() {
                    $.ajax({
                      url: "<?= site_url('support/improvement_details/'); ?>",
                      type: "POST",
                      data: {
                          topic: $('#post-title').html(),
                          details: $('#imporvement-details').val()
                      },
                      success: function( res ) {
                        alert(res);
                      },
                      dataType:'text'
                    });
                    $( this ).dialog( "close" );
                  },
                  "Cancel": function() {
                    $( this ).dialog( "close" );
                  }
                }
              });
              $( "#dialog-form" ).removeClass('hidden');
              $( "#dialog-form" ).dialog("open");
              /* eo of improvement dialog */
        },
        dataType:'text'
     });
    });
});
</script>