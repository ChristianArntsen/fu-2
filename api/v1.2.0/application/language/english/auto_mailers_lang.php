<?php
$lang['auto_mailers_new'] = "New Auto Mailer";
$lang['auto_mailers_no_auto_mailers_to_display'] = 'There are no auto mailers to display';
$lang['auto_mailers_components'] = 'Components';
$lang['auto_mailers_marketing_campaigns'] = 'Marketing Campaigns';
$lang['auto_mailers_marketing_campaign'] = 'Campaign Name';
$lang['auto_mailers_event'] = 'Event';
$lang['auto_mailers_days_after_event'] = 'Days after event';
$lang['auto_mailers_basic_information'] = 'Auto Mailer Info';
$lang['auto_mailers_name_required'] = 'Auto mailer name required';
$lang['auto_mailers_saving_success'] = 'Auto mailer successfully saved';
$lang['auto_mailers_saving_error'] = 'Auto mailer failed to save';
$lang['auto_mailers_recipients'] = 'Recipients';
$lang['auto_mailers_recipient'] = 'Recipient';
$lang['auto_mailers_add_recipients'] = 'Add Recipients';
$lang['auto_mailers_start_date'] = 'Start Date';
