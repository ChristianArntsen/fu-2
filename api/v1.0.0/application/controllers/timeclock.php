<?php
// Client Booking Engine
class Timeclock extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();	
	    $this->load->model('timeclock_entry');
    }
	function index()
	{
		$data['last_entries'] = $this->timeclock_entry->get_all(10)->result_array();
		$data['clocked_in'] = $this->timeclock_entry->is_clocked_in();
		$this->load->view('timeclock/manage', $data);		
	}
	function clock_in(){
		$this->timeclock_entry->clock_in();
		$data = $this->timeclock_entry->get_all(10)->result_array();
		echo json_encode($data);
	}
	function clock_out(){
		$this->timeclock_entry->clock_out();
		$data = $this->timeclock_entry->get_all(10)->result_array();
		echo json_encode($data);
	}
	function save($employee_id, $start_time)
	{
		if ($this->input->post('delete_entry'))
			$this->timeclock_entry->delete($employee_id, $start_time);
		else 
			$this->timeclock_entry->save($employee_id, $start_time);
		
		echo json_encode(array());
	}
	function view($employee_id, $start_time) {
		$data = array(
			'employee_info'=>$this->Employee->get_info($employee_id),
			'entry'=>$this->timeclock_entry->get_info($employee_id, $start_time)
		);
		$this->load->view('timeclock/form', $data);
	}
	
}
?>