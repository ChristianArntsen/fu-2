<?php
require_once ("secure_area.php");
class Food_and_beverage extends Secure_area
{
	function __construct()
	{
		parent::__construct('food_and_beverage');
		$this->load->library('sale_lib');
		$this->load->model('schedule');
		$this->load->model('teesheet');
		$this->load->model('green_fee');
		$this->load->model('fee');
		$this->load->model('Customer_loyalty');
		$this->load->model('table_receipt');
	}

	function db2_test()
	{
		$this->db2->from('deals');
		$results = $this->db2->get()->result_array();
		print_r($results);
		echo '<br/><br/>';
		$this->db->from('teetime');
		$this->db->limit('30');
		$s_results = $this->db->get()->result_array();
		print_r($s_results);
	}

	function index()
	{
		$this->_reload();
	}
	function view_login()
	{
		$this->load->view('food_and_beverage/login');
	}
	function login()
	{
		$pin_or_card = $this->input->post('pin_or_card');
		$employee_info = $this->Employee->get_info_by_pin_or_card($pin_or_card);
		if ($employee_info->person_id != '')
		{
			$has_food_and_beverage_permission = false;
			$allowed_modules = $this->Module->get_allowed_modules($employee_info->person_id);
			foreach ($allowed_modules->result_array() as $allowed_module)
			{
				$has_food_and_beverage_permission = ($has_food_and_beverage_permission ? $has_food_and_beverage_permission : $allowed_module['module_id'] == 'food_and_beverage');
			}
//			 print_r($allowed_modules->result_array);
			// echo $has_food_and_beverage_permission;
			if (!$has_food_and_beverage_permission)
			{
				echo json_encode(array('success'=>false, 'message'=>lang('food_and_beverage_no_permission')));
				return;
			}
			else if ($this->Employee->login($employee_info->username, $employee_info->password, true))
			{
				$this->session->set_userdata('fnb_logged_in', true);
				echo json_encode(
					array(
						'success'=>true,
						'software_menu_contents'=>$this->load->view('partial/software_menu', array(), true),
						'user_menu_contents'=>$this->load->view('partial/user_menu', array(), true),
						'employee_name'=>$employee_info->first_name.' '.$employee_info->last_name
					)
				);

			}
			else
				echo json_encode(array('success'=>false, 'message'=>lang('food_and_beverage_error_logging_in')));

			return;
		}

		echo json_encode(array('success'=>false, 'message'=>lang('food_and_beverage_invalid_pin_or_card')));
	}
	function logout()
	{
		$this->session->unset_userdata('fnb_logged_in');
		//$this->session->set_userdata('person_id', '-1');
		$this->session->unset_userdata('user_level');
		$this->session->unset_userdata('auto_mailers');
        $this->session->unset_userdata('config');
        $this->session->unset_userdata('courses');
        $this->session->unset_userdata('customers');
        $this->session->unset_userdata('dashboard');
        $this->session->unset_userdata('employees');
        $this->session->unset_userdata('events');
        $this->session->unset_userdata('giftcards');
        $this->session->unset_userdata('item_kits');
        $this->session->unset_userdata('items');
        $this->session->unset_userdata('invoices');
        $this->session->unset_userdata('marketing_campaigns');
        $this->session->unset_userdata('promotions');
        $this->session->unset_userdata('receivings');
        $this->session->unset_userdata('reports');
        $this->session->unset_userdata('reservations');
        $this->session->unset_userdata('sales');
        $this->session->unset_userdata('schedules');
        $this->session->unset_userdata('suppliers');
        $this->session->unset_userdata('teesheets');
		$this->session->unset_userdata('tournaments');

		$this->session->unset_userdata('table_id');
		$this->session->unset_userdata('table_number');
		$this->cancel_sale();
		//echo json_encode(array('success'=>true));
	}

	function view_override() {
		$this->load->model('employee');
		$data = array (
			'admins_managers'=>$this->employee->get_all(10000,0,array(2,3))->result_array()
		);
		$this->load->view('food_and_beverage/override', $data);
	}
	function override(){
		$this->load->model('employee');
		$person_id = $this->input->post('employee_id');
		$password = $this->input->post('password');
		if ($this->employee->can_override($person_id, $password))
		{
			$this->session->set_userdata('purchase_override', $person_id);
			$this->session->set_userdata('purchase_override_time', date('Y-m-d h:i:sa'));
			echo json_encode(array('success'=>true));
		}
		else
			echo json_encode(array('success'=>false));
	}

	function item_search()
	{
		$suggestions = $this->Item->get_item_search_suggestions($this->input->get('term'),100);
		$suggestions = array_merge($suggestions, $this->Item_kit->get_item_kit_search_suggestions($this->input->get('term'),100));
		$suggestions = array_merge($suggestions, $this->Tournament->get_search_suggestions($this->input->get('term'),100));
		echo json_encode($suggestions);
	}
	function employee_search()
	{
		$suggestions = $this->Employee->get_search_suggestions($this->input->get('term'),100, true);
		echo json_encode($suggestions);
	}
	function customer_search($type='')
	{
		$suggestions = $this->Customer->get_customer_search_suggestions($this->input->get('term'),100,$type);
		echo json_encode($suggestions);
	}

	function select_customer($customer_id = -1)
	{
		$message = false;
		if ($customer_id == -1)
			$customer_id = $this->input->post("customer");
		// Get customer id
		$cid = $this->Customer->get_id_from_account_number($customer_id);
		$customer_id = ($cid) ? $cid : $customer_id;
		$data = array();
		if ($this->Customer->exists($customer_id))
		{
			$this->sale_lib->set_customer($customer_id);
		}
		else
		{
			//$data['error']=lang('sales_unable_to_add_customer');
			$message = array('text'=>lang('sales_unable_to_add_customer'),'type'=>'error_message','persist'=>false);
		}
		$this->_reload($data, 'select_customer', $message);
	}
	function remove_customer($customer_id = -1)
	{
		$this->sale_lib->delete_customer();
		$this->sale_lib->delete_customer_quickbutton($customer_id);
		$this->_reload(null, 'remove_customer');
	}
	function change_taxable()
	{
		$taxable = $this->input->post("taxable");
		$this->sale_lib->set_taxable($taxable);

        echo json_encode($this->sale_lib->get_basket_info());
	}
	function change_mode($mode = 'sale')
	{
		if ($mode == 'sale') {
			$mode = 'return';
		} else {
			$mode = 'sale';
		}
		$this->sale_lib->set_mode($mode);
		$this->_reload($data, "change_mode");
	}

	function set_comment()
	{
 	  $this->sale_lib->set_comment($this->input->post('comment'));
	}

	function set_email_receipt()
	{
 	  $this->sale_lib->set_email_receipt($this->input->post('email_receipt'));
	}

	//Alain Multiple Payments
	function add_payment()
	{
		$message = false;
		$valid_amount = false;
		$ajax = 'add_payment';
		$data=array();
		$data['receipt_id'] = $this->input->post('payment_receipt_id');
		$this->form_validation->set_rules('amount_tendered', 'lang:sales_amount_tendered', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			if ( $this->input->post('payment_type') == lang('sales_gift_card') )
				//$data['error']=lang('sales_must_enter_numeric_giftcard');
				$message = array('text'=>lang('sales_must_enter_numberic_giftcard'),'type'=>'error_message','persist'=>false);
			else
				//$data['error']=lang('sales_must_enter_numeric');
				$message = array('text'=>lang('sales_must_enter_numeric'),'type'=>'error_message','persist'=>false);
 			$this->_reload($data, $ajax, $message);
 			return;
		}

		$payment_type=$this->input->post('payment_type');
		if ( $payment_type == lang('sales_giftcard') )
		{
			$giftcard_number = $this->input->post('giftcard_number');
			$amount_tendered = $this->input->post('amount_tendered');
			$giftcard_id = $this->Giftcard->get_giftcard_id($giftcard_number);
			if(!$this->Giftcard->exists($giftcard_id)  || $this->Giftcard->is_expired($giftcard_id))
			{
				$message = array('text'=>lang('sales_giftcard_does_not_exist'),'type'=>'error_message','persist'=>false);
 				//$data['error']=lang('sales_giftcard_does_not_exist');
				$this->_reload($data, $ajax, $message);
				return;
			}
			$giftcard_info = $this->Giftcard->get_info($giftcard_id);
			$valid_amount = $this->sale_lib->get_valid_amount($giftcard_info->department, $giftcard_info->category);
			$payments = $this->sale_lib->get_payments();
			$payment_type=$this->input->post('payment_type').':'.$giftcard_number;
			$current_payments_with_giftcard = isset($payments[$payment_type]) ? $payments[$payment_type]['payment_amount'] : 0;
			$giftcard_value = $this->Giftcard->get_giftcard_value( $giftcard_number );
			$cur_giftcard_value =  $giftcard_value - $current_payments_with_giftcard;
			if ( $cur_giftcard_value <= 0 && $amount_tendered > 0)
			{
				//$data['error']=lang('sales_giftcard_balance_is').' '.to_currency( $giftcard_value ).' !';
				$message = array('text'=>lang('sales_giftcard_balance_is').' '.to_currency($giftcard_value),'type'=>'error_message','persist'=>false);
 				$this->_reload($data, $ajax, $message);
				return;
			}
			elseif ( ( $cur_giftcard_value - $amount_tendered ) > 0 )
			{
				//$data['warning']=lang('sales_giftcard_balance_is').' '.to_currency( $cur_giftcard_value - $amount_tendered ).' !';
				$message = array('text'=>lang('sales_giftcard_balance_is').' '.to_currency($cur_giftcard_value),'type'=>'warning_message','persist'=>false);
 			}
			$payment_amount=min(min( $amount_tendered, $giftcard_value ), $valid_amount['valid_amount']);
		}
		else if ( $payment_type == lang('sales_punch_card') )
		{
			$punch_card_number = $this->input->post('punch_card_number');
			//$amount_tendered = $this->input->post('amount_tendered');
			$punch_card_id = $this->Punch_card->get_punch_card_id($punch_card_number);
			if($this->Punch_card->is_expired($punch_card_id))
			{
				$message = array('text'=>lang('sales_punch_card_does_not_exist'),'type'=>'error_message','persist'=>false);
 				//$data['error']=lang('sales_giftcard_does_not_exist');
				$this->_reload($data, $ajax, $message);
				return;
			}
			$punch_card_info = $this->Punch_card->get_info($punch_card_id);

			//$valid_amount = $this->sale_lib->get_valid_amount();
			$payments = $this->sale_lib->get_payments();
			$payment_type=$this->input->post('payment_type').':'.$punch_card_number;
			$current_payments_with_punch_card = isset($payments[$payment_type]) ? $payments[$payment_type]['payment_amount'] : 0;
			$punch_card_value = $this->Punch_card->get_punch_card_value($punch_card_id);
			if(!$punch_card_value['success'])
			{
				$message = array('text'=>$punch_card_value['message'],'type'=>'error_message','persist'=>false);
 				//$data['error']=lang('sales_giftcard_does_not_exist');
				$this->_reload($data, $ajax, $message);
				return;
			}
			$payment_amount = $punch_card_value['value'];
		}
		else
		{
			$payment_amount=$this->input->post('amount_tendered');
		}

		if( !$this->sale_lib->add_payment( $payment_type, $payment_amount, $valid_amount) )
		{
			//$data['error']=lang('sales_unable_to_add_payment');
			$message = array('text'=>lang('sales_unable_to_add_payment'),'type'=>'error_message','persist'=>false);
 		}

		$this->_reload($data, $ajax, $message);
	}

	function load_return ($sale_id)
	{
		$this->sale_lib->set_mode('return');
		redirect("food_and_beverage/add/POS {$sale_id}", 'location');
	}
	function confirm_delete_payment($payment_id)
	{
		$payment_id = urldecode(urldecode($payment_id));
		$payments = $this->sale_lib->get_payments();
		$data = array(
			'payment_id'=>$payment_id,
			'payment'=>$payments[$payment_id]
		);

		$this->load->view('food_and_beverage/confirm_delete_payment', $data);
	}
    //Alain Multiple Payments
	function delete_payment($payment_id)
	{
		$payment_id = ($this->input->post('payment_id')) ? $this->input->post('payment_id') : $payment_id;
		//echo urldecode($payment_id);
		//return;
		$this->sale_lib->delete_payment((($payment_id)));
		$this->_reload(array(), 'delete_payment');
	}
	function test_ets()
	{
		$this->load->library('Hosted_payments');
		$payment = new Hosted_payments();
		$payment->initialize("39603DE6-3A38-4D1C-BAC5-C6A241996252");

		$session = $payment->set('action', 'session')
				   		   ->set('amount', 5)
				   		   ->set('successRedirectUrl', 'example_success.php')
						   ->send();

		$data = array(
			'session'=>$session
		);
		$this->load->view('food_and_beverage/ets', $data);
	}
    function process_payment(){
    	$this->load->library('Mercury');
		$mercury = new Mercury();
		$merchant_id = 'get the merchant id';
		$operator_id = 'Jimmy Johns';//'get the operator id';
		$encrypted_data = $this->input->post('encrypted_data');
		$encrypted_key = $this->input->post('encrypted_key');
		$purchase = $this->input->post('purchase');
		$invoice_no = 1;//'get the invoice no';

		$mercury->process_payment($merchant_id, $operator_id, $encrypted_data, $encrypted_key, $purchase, $invoice_no, 'test');
    }
	function open_payment_window($window = 'POS', $tran_type = 'Sale', $frequency = 'OneTime', $total_amount='1.00',$tax_amount='0.00', $previous_card_declined = 'false') {
		// USING ETS FOR PAYMENT PROCESSING
		if ($this->config->item('ets_key'))
		{
			$this->load->library('Hosted_payments');
			$payment = new Hosted_payments();
			$payment->initialize($this->config->item('ets_key'));

			$session = $payment->set('action', 'session')
			  		   ->set('amount', $total_amount)
			   		   ->set('successRedirectUrl', 'index.php/food_and_beverage/ets_payment_made')
					   ->set('errorRedirectUrl', 'index.php/food_and_beverage/ets_process_cancelled')
					   ->send();

		//	print_r($session);
			if ($session->id)
			{
				$user_message = $previous_card_declined!='false'?'Card declined, please try another.':'';
				$return_code = '';
				$this->session->set_userdata('ets_session_id', (string)$session->id);
				//$url = $HC->get_iframe_url('POS', (string)$initialize_results->PaymentID);
				$data = array('user_message'=>$user_message, 'return_code'=>$return_code, 'session'=>$session);
				$this->load->view('food_and_beverage/ets', $data);
			}
			else
			{
				$data = array('processor' => 'ETS');
				$this->load->view('food_and_beverage/cant_load', $data);
			}
			//$data = array('session'=>$session);
			//$this->load->view('food_and_beverage/ets', $data);
		}
		// USING MERCURY FOR PAYMENT PROCESSING
		else if ($this->config->item('mercury_id'))
		{
			$this->load->library('Hosted_checkout_2');
			$HC = new Hosted_checkout_2();

			$HC->set_merchant_credentials($this->config->item('mercury_id'),$this->config->item('mercury_password'));//ForeUP's Credentials
			$HC->set_response_urls('food_and_beverage/payment_made', 'food_and_beverage/process_cancelled');//Response URLs
			$initialize_results = $HC->initialize_payment($total_amount, $tax_amount, $tran_type, $window, $frequency);

			if ((int)$initialize_results->ResponseCode == 0)
			{
				//Set invoice number to save in the database
				//$invoice = $this->sale->add_credit_card_payment(array('tran_type'=>'Sale','frequency'=>$frequency));
				//$this->session->set_userdata('invoice', $invoice);

				$user_message = $previous_card_declined!='false'?'Card declined, please try another.':'';
				$return_code = (int)$initialize_results->ResponseCode;
				$this->session->set_userdata('payment_id', (string)$initialize_results->PaymentID);
				$url = $HC->get_iframe_url('POS', (string)$initialize_results->PaymentID);
				$data = array('user_message'=>$user_message, 'return_code'=>$return_code, 'url'=>$url);
				$this->load->view('food_and_beverage/hc_pos_iframe.php', $data);
			}
		}

	}
	function open_giftcard_window($amount) {
		$data = array('amount'=> $amount);
		$this->load->view('food_and_beverage/giftcard_window.php', $data);
	}
	function complete_sale_window() {
		$data['payments'] = $this->sale_lib->get_payments();
		$data['cart'] = $this->sale_lib->get_basket();
		$data['taxes']=$this->sale_lib->get_basket_taxes();
		$data['total']=$this->sale_lib->get_basket_total();
		$data['amount_change']=to_currency($this->sale_lib->get_basket_amount_due() * -1);

		$this->load->view('food_and_beverage/complete_sale.php', $data);
	}
	function payment_made() {
		$this->load->library('Hosted_checkout_2');
		$HC = new Hosted_checkout_2();
		$HC->set_merchant_credentials($this->config->item('mercury_id'),$this->config->item('mercury_password'));//ForeUP's Credentials
		$payment_id = $this->session->userdata('payment_id');
		$this->session->unset_userdata('payment_id');
		$HC->set_payment_id($payment_id);
		$verify_results = $HC->verify_payment();
		$HC->complete_payment();
		$invoice = $this->session->userdata('invoice_id');

		$credit_card_data = array(
			'course_id'=>$this->session->userdata('selected_course'),
			'token'=>(string)$verify_results->Token,
			'token_expiration'=>date('Y-m-d', strtotime('+2 years')),
			'card_type'=>(string)$verify_results->CardType,
			'masked_account'=>(string)$verify_results->MaskedAccount,
			'cardholder_name'=>(string)$verify_results->CardholderName
		);

		//Update credit card payment data
		$payment_data = array (
			'course_id'=>$this->session->userdata('course_id'),
			'mercury_id'=>$this->config->item('mercury_id'),
			'tran_type'=>(string)$verify_results->TranType,
			'amount'=>(string)$verify_results->Amount,
			'auth_amount'=>(string)$verify_results->AuthAmount,
			'card_type'=>(string)$verify_results->CardType,
			'frequency'=>'OneTime',
			'masked_account'=>(string)$verify_results->MaskedAccount,
			'cardholder_name'=>(string)$verify_results->CardholderName,
			'ref_no'=>(string)$verify_results->RefNo,
			'operator_id'=>(string)$verify_results->OperatorID,
			'terminal_name'=>(string)$verify_results->TerminalName,
			'trans_post_time'=>(string)$verify_results->TransPostTime,
			'auth_code'=>(string)$verify_results->AuthCode,
			'voice_auth_code'=>(string)$verify_results->VoiceAuthCode,
			'payment_id'=>$payment_id,
			'acq_ref_data'=>(string)$verify_results->AcqRefData,
			'process_data'=>(string)$verify_results->ProcessData,
			'token'=>(string)$verify_results->Token,
			'response_code'=>(int)$verify_results->ResponseCode,
			'status'=>(string)$verify_results->Status,
			'status_message'=>(string)$verify_results->StatusMessage,
			'display_message'=>(string)$verify_results->DisplayMessage,
			'avs_result'=>(string)$verify_results->AvsResult,
			'cvv_result'=>(string)$verify_results->CvvResult,
			'tax_amount'=>(string)$verify_results->TaxAmount,
			'avs_address'=>(string)$verify_results->AVSAddress,
			'avs_zip'=>(string)$verify_results->AVSZip,
			'payment_id_expired'=>(string)$verify_results->PaymendIDExpired,
			'customer_code'=>(string)$verify_results->CustomerCode,
			'memo'=>(string)$verify_results->Memo
		);
		$this->sale->update_credit_card_payment($invoice, $payment_data);
		$payment_data['payment_type'] = $payment_data['card_type'].' '.$payment_data['masked_account'];

		//Save receipt data for when page reloads
		$receipt_data .= "Card Type: {$payment_data['card_type']}\\n";
        $receipt_data .= "Card No.: {$payment_data['masked_account']}\\n";
        $receipt_data .= "Auth: {$payment_data['auth_code']}\\n\\n";
        $receipt_data .= "Total: $".number_format($payment_data['auth_amount'], 2)." \\n\\n\\n";
        $receipt_data .= 'I agree to pay the above amount according to the card issuer agreement.\n\n\n';
        $receipt_data .= 'X_____________________________________________\n';
        $receipt_data .= $payment_data['cardholder_name'];
		$this->session->set_userdata('receipt_data', $receipt_data);

		if ($payment_data['response_code'] === 0 && $payment_data['status'] == "Approved")
			$this->load->view('food_and_beverage/payment_made.php', $payment_data);
		else
			$this->load->view('food_and_beverage/payment_made.php', array('status'=>'declined'));
	}
	function ets_payment_made() {
		$response = $this->input->post('response');
		$response = json_decode($response);
		print_r($response);
		return;
		if ($response->status == 'success')
		{
			$credit_card_data = array(
				'course_id'=>$this->session->userdata('selected_course'),
				'token'=>(string)$verify_results->Token,
				'token_expiration'=>date('Y-m-d', strtotime('+2 years')),
				'card_type'=>(string)$verify_results->CardType,
				'masked_account'=>(string)$something,
				'cardholder_name'=>(string)$verify_results->CardholderName
			);
		}

		//Update credit card payment data
		$payment_data = array (
			'course_id'=>$this->session->userdata('course_id'),
			'mercury_id'=>$this->config->item('mercury_id'),
			'tran_type'=>(string)$verify_results->TranType,
			'amount'=>(string)$verify_results->Amount,
			'auth_amount'=>(string)$verify_results->AuthAmount,
			'card_type'=>(string)$verify_results->CardType,
			'frequency'=>'OneTime',
			'masked_account'=>(string)$verify_results->MaskedAccount,
			'cardholder_name'=>(string)$verify_results->CardholderName,
			'ref_no'=>(string)$verify_results->RefNo,
			'operator_id'=>(string)$verify_results->OperatorID,
			'terminal_name'=>(string)$verify_results->TerminalName,
			'trans_post_time'=>(string)$verify_results->TransPostTime,
			'auth_code'=>(string)$verify_results->AuthCode,
			'voice_auth_code'=>(string)$verify_results->VoiceAuthCode,
			'payment_id'=>$payment_id,
			'acq_ref_data'=>(string)$verify_results->AcqRefData,
			'process_data'=>(string)$verify_results->ProcessData,
			'token'=>(string)$verify_results->Token,
			'response_code'=>(int)$verify_results->ResponseCode,
			'status'=>(string)$verify_results->Status,
			'status_message'=>(string)$verify_results->StatusMessage,
			'display_message'=>(string)$verify_results->DisplayMessage,
			'avs_result'=>(string)$verify_results->AvsResult,
			'cvv_result'=>(string)$verify_results->CvvResult,
			'tax_amount'=>(string)$verify_results->TaxAmount,
			'avs_address'=>(string)$verify_results->AVSAddress,
			'avs_zip'=>(string)$verify_results->AVSZip,
			'payment_id_expired'=>(string)$verify_results->PaymendIDExpired,
			'customer_code'=>(string)$verify_results->CustomerCode,
			'memo'=>(string)$verify_results->Memo
		);
		$this->sale->update_credit_card_payment($invoice, $payment_data);
		$payment_data['payment_type'] = $payment_data['card_type'].' '.$payment_data['masked_account'];

		//Save receipt data for when page reloads
		$receipt_data .= "Card Type: {$payment_data['card_type']}\\n";
        $receipt_data .= "Card No.: {$payment_data['masked_account']}\\n";
        $receipt_data .= "Auth: {$payment_data['auth_code']}\\n\\n";
        $receipt_data .= "Total: $".number_format($payment_data['auth_amount'], 2)." \\n\\n\\n";
        $receipt_data .= 'I agree to pay the above amount according to the card issuer agreement.\n\n\n';
        $receipt_data .= 'X_____________________________________________\n';
        $receipt_data .= $payment_data['cardholder_name'];
		$this->session->set_userdata('receipt_data', $receipt_data);

		if ($payment_data['response_code'] === 0 && $payment_data['status'] == "Approved")
			$this->load->view('food_and_beverage/payment_made.php', $payment_data);
		else
			$this->load->view('food_and_beverage/payment_made.php', array('status'=>'declined'));
	}
	function process_cancelled() {
		$this->load->view('food_and_beverage/payment_made.php', array('status'=>'cancelled'));
	}

	function token_transaction($type, $invoice, $purchase_amount = '0.00', $gratuity_amount = '0.00', $tax_amount = '0.00') {
		$this->load->library('Hosted_checkout');
		$HC = new Hosted_checkout();
		$HC->token_transaction($type, $invoice, $purchase_amount, $gratuity_amount, $tax_amount);
	}

	function add($item_number = '', $mode = '')
	{
		$message = false;
		$data=array();
		if ($mode != '')
			$this->sale_lib->set_mode($mode);
		if ($item_number != 'account_balance' && $item_number != 'member_balance')
			$item_number_array = explode('_', $item_number);
		if (count($item_number_array) > 1)
		{
			$item_number = $item_number_array[0];
			$price_index = $item_number_array[1];
			$teetime_type = $item_number_array[2];
		}
		else
		{
			$price_index = $this->input->post('price_index');
			$teetime_type = $this->input->post('teetime_type');
		}
		$mode = $this->sale_lib->get_mode();
		$invoice_info = array();

		if ($item_number != '')
			$item_id_or_number_or_item_kit_or_receipt = urldecode($item_number);
		else
			$item_id_or_number_or_item_kit_or_receipt = $this->input->post("item");
		$quantity = $mode=="sale" ? 1:-1;

		if($item_id_or_number_or_item_kit_or_receipt == 'account_balance' || $item_id_or_number_or_item_kit_or_receipt == 'member_balance')
		{
			$iinikr = $item_id_or_number_or_item_kit_or_receipt;
			// GET CUSTOMER BALANCE
			$customer_id = $this->sale_lib->get_customer();
			$customer_info = $this->Customer->get_info($customer_id);
			// PULL  UP (ADD) "ACCOUNT BALANCE" ITEM OR CREATE IT
			$item_id = $this->Sale->get_balance_item($iinikr);

			$iinikr = $iinikr == 'member_balance' ? 'member_account_balance' : $iinikr;
			$balance = ($customer_info->$iinikr < 0 ? -$customer_info->$iinikr : 0);
			// ADD TO CART/BASKET
			$this->sale_lib->add_item($item_id,1,0,$balance);
			$this->sale_lib->add_item_to_basket($item_id,1,0,$balance);
		}
		else if($this->sale_lib->is_valid_receipt($item_id_or_number_or_item_kit_or_receipt))
		{
			$this->sale_lib->set_mode('return');
			$this->sale_lib->return_entire_sale($item_id_or_number_or_item_kit_or_receipt);
		}
		elseif ($this->sale_lib->is_valid_invoice($item_id_or_number_or_item_kit_or_receipt, $invoice_info))
		{
			if ($this->sale_lib->invoice_in_cart($item_id_or_number_or_item_kit_or_receipt, $invoice_info)){
				//$data['error'] = 'Invoice already added to cart';
				$message = array('text'=>'Invoice already added to cart','type'=>'error_message','persist'=>false);
 			}else {
				$this->sale_lib->add_invoice($invoice_info, $quantity);
				$this->sale_lib->set_customer($invoice_info['person_id']);
			}
		}
		elseif($mode == 'sale' && $this->sale_lib->is_valid_raincheck($item_id_or_number_or_item_kit_or_receipt))
		{
			if ($this->sale_lib->raincheck_is_used_or_expired($item_id_or_number_or_item_kit_or_receipt))
				//$data['error'] = 'Raincheck expired or used';
				$message = array('text'=>'Raincheck expired or used','type'=>'error_message','persist'=>false);
 			else
				$this->sale_lib->apply_raincheck($item_id_or_number_or_item_kit_or_receipt);
		}
		elseif($this->sale_lib->is_valid_tournament($item_id_or_number_or_item_kit_or_receipt))
		{
			if ($this->sale_lib->tournament_without_customer())
			{
				//$data['error'] = 'A customer must be selected before adding a tournament';
				$message = array('text'=>'A customer must be selected before adding a tournament','type'=>'error_message','persist'=>false);
 			}
			else
			{
				$this->sale_lib->add_tournament($item_id_or_number_or_item_kit_or_receipt, $quantity);
				$this->sale_lib->add_tournament_to_basket($item_id_or_number_or_item_kit_or_receipt,$quantity);
			}
		}
		elseif($this->sale_lib->is_valid_item_kit($item_id_or_number_or_item_kit_or_receipt))
		{
			$this->sale_lib->add_item_kit($item_id_or_number_or_item_kit_or_receipt, $quantity);
			$this->sale_lib->add_item_kit_to_basket($item_id_or_number_or_item_kit_or_receipt,$quantity);
		}
		elseif(!$this->sale_lib->add_item($item_id_or_number_or_item_kit_or_receipt,$quantity,0,null,null,null,$price_index,$teetime_type) ||
				!$this->sale_lib->add_item_to_basket($item_id_or_number_or_item_kit_or_receipt,$quantity,0,null,null,null,$price_index,$teetime_type))
		{
			//$data['error']=lang('sales_unable_to_add_item');
			$message = array('text'=>lang('sales_unable_to_add_item'),'type'=>'error_message','persist'=>false);
 		}

		if($this->sale_lib->is_unlimited($item_id_or_number_or_item_kit_or_receipt) != 1 && $this->sale_lib->out_of_stock($item_id_or_number_or_item_kit_or_receipt))
		{
			//$data['warning'] = lang('sales_quantity_less_than_zero');
			$message = array('text'=>lang('sales_quantity_less_than_zero'),'type'=>'error_message','persist'=>false);
 		}
		$this->_reload($data, 'add_item', $message);
	}

	// Adds an item to a receipt
	function add_receipt_item($receipt_id = 0){
		$item_data['line'] = $this->input->post('line');
		$item_data['item_id'] = $this->input->post('item_id');
		$sale_id = $this->input->post('sale_id');

		if($receipt_id == 0){
			$receipt_id = $this->table_receipt->save($sale_id, null, array($item_data));
		}else{
			$success = $this->table_receipt->add_item($item_data['item_id'], $item_data['line'], $sale_id, $receipt_id);
		}

		echo json_encode(array('success'=>true, 'receipt_id'=>$receipt_id, 'sale_id'=>$sale_id));
	}

	// Removes an item from a receipt
	function remove_receipt_item($sale_id, $receipt_id){
		$item_data['line'] = $this->input->post('line');
		$item_data['item_id'] = $this->input->post('item_id');

		if($receipt_id == 0){
			echo json_encode(array('success'=>false));
			return false;
		}

		$success = $this->table_receipt->delete_item($item_data['item_id'], $item_data['line'], $sale_id, $receipt_id);
		echo json_encode(array('success'=>(bool) $success));
	}

	function delete_receipt($sale_id, $receipt_id){
		$success = $this->table_receipt->delete($sale_id, $receipt_id);
		echo json_encode(array('success'=>(bool) $success));
	}

	function receipt_paid($sale_id, $receipt_id){
		$success = $this->table_receipt->mark_paid($sale_id, $receipt_id);
		echo json_encode(array('success'=>(bool) $success));
	}

	function edit_item($line)
	{
		$message = false;
		$data= array();

		$this->form_validation->set_rules('price', 'lang:items_price', 'required|numeric');
		$this->form_validation->set_rules('quantity', 'lang:items_quantity', 'required|numeric');

        $description = $this->input->post("description");
        $serialnumber = $this->input->post("serialnumber");
		$price = $this->input->post("price");
                $item_id = $this->input->post('item_id');
		$quantity = $this->input->post("quantity");
		$discount = $this->input->post("discount");


		if ($this->form_validation->run() != FALSE)
		{
			$this->sale_lib->edit_item($line,$description,$serialnumber,$quantity,$discount,$price, $item_id);
		}
		else
		{
			//$data['error']=lang('sales_error_editing_item');
			$message = array('text'=>lang('sales_error_editing_item'),'type'=>'error_message','persist'=>false);
 		}

		if($this->sale_lib->is_unlimited($item_id_or_number_or_item_kit_or_receipt) != 1 && $this->sale_lib->out_of_stock($this->sale_lib->get_item_id($line)))
		{
			//$data['warning'] = lang('sales_quantity_less_than_zero');
			$message = array('text'=>lang('sales_quantity_less_than_zero'),'type'=>'error_message','persist'=>false);
 		}


		$this->_reload($data, false, $message);
	}

	function delete_item($line)
	{
		$sale_id = $this->sale_lib->get_table_id();
		$item_id = $this->sale_lib->get_item_id($line);
		$this->sale_lib->delete_item($line);
		$this->sale_lib->delete_item_from_basket($line);
		$this->table_receipt->delete_item($item_id, $line, $sale_id);
		$this->_reload(null, "remove_item");
	}

	function delete_customer()
	{
		$this->sale_lib->set_taxable('true');
		$this->sale_lib->delete_customer();
		$this->_reload();
	}

	function complete()
	{
		if (($this->config->item('fnb_login') && !$this->session->userdata('fnb_logged_in')))
		{
			echo json_encode(array('success'=>false, 'message'=>lang('food_and_beverage_not_logged_in')));
			return;
		}

		$message = false;
		$mode = $this->sale_lib->get_mode();
		$no_receipt = $this->input->post('no_receipt');
		$data['cart']=$this->sale_lib->get_basket();
		$data['subtotal']=$this->sale_lib->get_basket_subtotal();
		$data['taxes']=$this->sale_lib->get_basket_taxes();
		$data['total']=$this->sale_lib->get_basket_total();
		$data['receipt_title']=lang('sales_receipt');
		$data['transaction_time']= date(get_date_format().' '.get_time_format());
		$data['has_online_booking'] = $this->permissions->course_has_module('reservations') ? $this->schedule->has_online_booking($this->session->userdata('course_id')) : $this->teesheet->has_online_booking($this->session->userdata('course_id'));
		$data['website'] = $this->config->item('website');
		$customer_id=$this->sale_lib->get_customer();
		$teetime_id=$this->sale_lib->get_teetime();
		$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
		$comment = $this->sale_lib->get_comment();
		$emp_info=$this->Employee->get_info($employee_id);
		$amount_change = $this->sale_lib->get_basket_amount_due();
		$data['amount_change']=to_currency($amount_change * -1);
		if ($amount_change != 0)
			$this->sale_lib->add_payment( 'Change issued', $amount_change);
		$data['payments']=$this->sale_lib->get_payments();
		$data['employee']=$emp_info->first_name.' '.$emp_info->last_name;
		if($customer_id!=-1)
		{
			$cust_info=$this->Customer->get_info($customer_id);
			$data['customer']=$cust_info->last_name.', '.$cust_info->first_name;
		}
		// $tournament_without_customers = $this->sale_lib->tournament_without_customer();
		$missing_or_repeat = $this->sale_lib->missing_or_repeat_giftcard_number();
		$missing_or_repeat_punch_card = $this->sale_lib->missing_or_repeat_punch_card_number();
		$location = 'food_and_beverage';//($this->config->item('after_sale_load') == 1)?'sales':($this->permissions->course_has_module('reservations') ? 'reservations' : 'teesheets');
		if ($missing_or_repeat)
		{
			//$data['error_message'] = lang('sales_missing_or_repeat_giftcard_number');
			$message = array('text'=>lang('sales_missing_or_repeat_giftcard_number'),'type'=>'error_message','persist'=>false);
			$data['giftcard_error_line'] = $missing_or_repeat;
		}
		else if ($missing_or_repeat_punch_card)
		{
			//$data['error_message'] = lang('sales_missing_or_repeat_giftcard_number');
			$message = array('text'=>lang('sales_missing_or_repeat_punch_card_number'),'type'=>'error_message','persist'=>false);
			$data['giftcard_error_line'] = $missing_or_repeat_punch_card;
		}
		else
		{
			//Record purchased teetimes
			if($teetime_id!=-1)
				$teetime_id = $this->permissions->course_has_module('reservations') ? $this->schedule->record_teetime_purchases($data['cart']) : $this->teesheet->record_teetime_purchases($data['cart']);

			//Record raincheck used
			if ($this->session->userdata('raincheck_id'))
				if ($this->Sale->raincheck_redeemed())
					$this->session->unset_userdata('raincheck_id');
			//SAVE sale to database
			$sale_id_number = $this->Sale->save($data['cart'], $customer_id,$employee_id,$comment,$data['payments'],false,$teetime_id);
			$this->sale_lib->set_taxable('true');
			$data['sale_id']='POS '.$sale_id_number;
			if ($data['sale_id'] == 'POS -1')
			{
				//$data['error_message'] = lang('sales_transaction_failed');
				$message = array('text'=>lang('sales_transaction_failed'),'type'=>'error_message','persist'=>false);
			}
			else
			{
				if ($this->sale_lib->get_email_receipt() && !empty($cust_info->email))
				{
					send_sendgrid($cust_info->email, lang('sales_receipt'), $this->load->view("food_and_beverage/receipt_email",$data, true), $this->config->item('email'), $this->config->item('name'));
				}
			}

			if (!$this->config->item('print_after_sale'))
			{
				$sale_id = $this->sale_lib->get_table_id();
				$this->Table->delete($sale_id);
		        $this->sale_lib->delete_customer_quickbuttons();
				$this->sale_lib->delete_table_id();
				$this->sale_lib->delete_table_number();

				if (!$no_receipt)
		            redirect('food_and_beverage/receipt/'.$sale_id_number);

		        $this->sale_lib->clear_all_minus_cart();

				if (count($this->sale_lib->get_cart())==0)
		        {
		            $this->sale_lib->set_teetime(-1);
					$this->session->unset_userdata('purchase_override');
		        	$this->session->unset_userdata('purchase_override_time');
		        }

		        if ($no_receipt){
		        	$this->_reload(array('receipt_data' => $data, 'sale_id' => $data['sale_id']), "completed_sale", $message);
		        }
				return;
			}

		}

		$sale_id = $this->sale_lib->get_table_id();
		$this->Table->delete($sale_id);
		$this->sale_lib->clear_all_minus_cart();
        $this->sale_lib->delete_customer_quickbuttons();
		$this->sale_lib->delete_table_id();
		$this->sale_lib->delete_table_number();
		if (count($this->sale_lib->get_cart())==0)
		{
			$this->sale_lib->set_teetime(-1);
			$this->session->unset_userdata('purchase_override');
	        $this->session->unset_userdata('purchase_override_time');
		}
		if ($mode == 'return')
			$this->sale_lib->clear_all();

		if (count($this->sale_lib->get_cart())>0)
			$this->_reload(array('receipt_data' => $data, 'sale_id' => $data['sale_id']), "completed_sale", $message);
        else {
            $this->_reload(array('receipt_data' => $data, 'sale_id' => $data['sale_id']), "completed_sale", $message);
        }
	}
	function set_quickbutton_tab($tab)
	{
		$this->session->set_userdata('quickbutton_tab', $tab);
	}
	function email_receipt($sale_id, $email = false)
	{
		$sent = false;
		$sale_info = $this->Sale->get_info($sale_id)->row_array();
		$this->sale_lib->copy_entire_sale($sale_id);
		$data['cart']=$this->sale_lib->get_cart();
		$data['payments']=$this->sale_lib->get_payments();
		$data['subtotal']=$this->sale_lib->get_subtotal();
		$data['taxes']=$this->sale_lib->get_taxes($sale_id);
		$data['total']=$this->sale_lib->get_total($sale_id);
		$data['receipt_title']=lang('sales_receipt');
		$data['transaction_time']= date(get_date_format().' '.get_time_format(), strtotime($sale_info['sale_time']));
		$customer_id=$this->sale_lib->get_customer();
		$emp_info=$this->Employee->get_info($sale_info['employee_id']);
		$data['payment_type']=$sale_info['payment_type'];
		$data['amount_change']=to_currency($this->sale_lib->get_amount_due($sale_id) * -1);
		$data['employee']=$emp_info->first_name.' '.$emp_info->last_name;

		if($customer_id!=-1)
		{
			$cust_info=$this->Customer->get_info($customer_id);
			$data['customer']=$cust_info->first_name.' '.$cust_info->last_name.($cust_info->company_name==''  ? '' :' ('.$cust_info->company_name.')');
		}
		$data['sale_id']='POS '.$sale_id;
		if (!empty($cust_info->email) || $email)
		{
			/*$this->load->library('email');
			//$config['mailtype'] = 'html';
			//$this->email->initialize($config);
			$this->email->from($this->config->item('email'), $this->config->item('name'));
			$this->email->to($cust_info->email);

			$this->email->subject(lang('sales_receipt'));
			$this->email->message($this->load->view("food_and_beverage/receipt_email",$data, true));
			$this->email->send();*/
			$email = $email ? $email : $cust_info->email;
			send_sendgrid($email, lang('sales_receipt'), $this->load->view("food_and_beverage/receipt_email",$data, true), $this->config->item('email'), $this->config->item('name'));
			$sent = true;
		}
		$this->sale_lib->clear_all();
		echo json_encode(array('success'=>$sent));
	}

	function receipt($sale_id, $json=false)
	{
		$this->sale_lib->clear_all();
		$sale_info = $this->Sale->get_info($sale_id)->row_array();
		$this->sale_lib->copy_entire_sale($sale_id);
		$data['cart']=$this->sale_lib->get_cart();
		$data['payments']=$this->sale_lib->get_payments();
		$data['subtotal']=$this->sale_lib->get_subtotal();
		$data['taxes']=$this->sale_lib->get_taxes($sale_id);
		$data['total']=$this->sale_lib->get_total($sale_id);
		$data['receipt_title']=lang('sales_receipt');
		$data['transaction_time']= date(get_date_format().' '.get_time_format(), strtotime($sale_info['sale_time']));
		$customer_id=$this->sale_lib->get_customer();
		$emp_info=$this->Employee->get_info($sale_info['employee_id']);
		$data['payment_type']=$sale_info['payment_type'];
		$data['amount_change']=to_currency($this->sale_lib->get_amount_due($sale_id) * -1);
		$data['employee']=$emp_info->first_name.' '.$emp_info->last_name;

		if($customer_id!=-1)
		{
			$cust_info=$this->Customer->get_info($customer_id);
			$data['customer']=$cust_info->first_name.' '.$cust_info->last_name.($cust_info->company_name==''  ? '' :' ('.$cust_info->company_name.')');
		}
		$data['sale_id']='POS '.$sale_id;
		if ($json){
			echo json_encode($data);
		}
		else{
			$this->load->view("food_and_beverage/receipt",$data);
		}
		$this->sale_lib->clear_all();
		if (count($this->sale_lib->get_cart())==0)
        {
            $this->sale_lib->set_teetime(-1);
			$this->session->unset_userdata('purchase_override');
        	$this->session->unset_userdata('purchase_override_time');
        }
	}
	function raincheck($raincheck_id)
	{
		$data = $this->Sale->raincheck_info($raincheck_id)->row_array();
//		echo $this->db->last_query();
		$data['receipt_title']='Raincheck';//lang('sales_raincheck');
		$data['transaction_time']= date(get_date_format().' '.get_time_format(), strtotime($data['date_issued']));
		$emp_info=$this->Employee->get_info($data['employee_id']);
		$data['employee']=$emp_info->first_name.' '.$emp_info->last_name;
		$cust_info=$this->Customer->get_info($data['customer_id']);
		$data['customer']=$cust_info->first_name.' '.$cust_info->last_name;

//print_r($data);

		$this->load->view("food_and_beverage/raincheck",$data);
	}

	function edit($sale_id, $type = '',  $sales_page = false)
	{
		$data = array();

		$data['customers'] = array('' => 'No Customer');
		$data['employees'] = array();
		foreach ($this->Employee->get_all()->result() as $employee)
		{
			$data['employees'][$employee->person_id] = $employee->first_name . ' '. $employee->last_name;
		}

		$data['sale_info'] = $this->Sale->get_info($sale_id)->row_array();
		//$data['credit_card_payments'] = $this->Sale->get_credit_card_payments($sale_id);
		//foreach ($this->Customer->get_all()->result() as $customer)
		$payments = $this->Sale->get_sale_payments($sale_id);
		$data['invoice_id'] = false;
		foreach ($payments->result_array() as $payment)
			if ($payment['invoice_id'])
				$data['invoice_id'] = $payment['invoice_id'];
		$customer = $this->Customer->get_info($data['sale_info']['customer_id']);
		$data['customer_name'] = $customer->first_name . ' '. $customer->last_name;
		$data['type'] = $type;
		$data['sales_page'] = $sales_page;
		$this->load->view('food_and_beverage/edit', $data);
	}
	function confirm_refund($invoice_id, $amount)
	{
		$amount = ($amount < 0)?-$amount:$amount;
		$cc_payment = $this->Sale->get_credit_card_payment($invoice_id);
		$data['cc_payment'] = $cc_payment[0];
		$data['cc_payment']['amount'] = ($cc_payment[0]['amount'] < $amount)?$cc_payment[0]['amount']:$amount;
		$this->load->view('food_and_beverage/confirm_refund', $data);
	}
	function issue_refund($invoice)
	{
		//echo 'issue_refund<br/>';
		$amount = $this->input->post('amount');
		$cc_payment = $this->Sale->get_credit_card_payment($invoice);
		$cc_payment = $cc_payment[0];
		$this->load->library('Hosted_checkout_2');
		$HC = new Hosted_checkout_2();
		//echo 'hc->issue_refund<br/>';
		$HC->set_merchant_credentials($this->config->item('mercury_id'),$this->config->item('mercury_password'));//Golf Course's Credentials
		$HC->set_invoice($invoice);
		$HC->set_cardholder_name($cc_payment['cardholder_name']);
		$HC->set_token($cc_payment['token']);
		$response = $HC->issue_refund($invoice, $amount);
		//echo 'done<br/>';
		//$response;
		if ((int)$response->ResponseCode === 0 && $response->Status == 'Approved')
		{
			$sql = $this->Sale->add_refund_payment($invoice, $amount, $response);
			//$add_psql = $this->db->last_query();
			$this->sale_lib->add_payment('CC Refund', -$amount);
			echo json_encode(array('success'=>true));
		}
		else {
			echo json_encode(array('success'=>false));
		}
	}
	function delete($sale_id, $type = '')
	{
		$data = array();

		if ($this->Sale->delete($sale_id))
		{
			$data['success'] = true;
           // No longer storing recent transactions in the session
           $recent_transactions = $this->session->userdata('recent_transactions');
           foreach($recent_transactions as $index => $rt)
           {
                   if ($rt['sale_id'] == $sale_id)
                   {
                           unset($recent_transactions[$index]);
                   }
           }
           $this->session->set_userdata('recent_transactions', $recent_transactions);
		}
		else
		{
			$data['success'] = false;
		}

		if ($type == 'popup')
            echo json_encode($data);
        else
            $this->load->view('food_and_beverage/delete', $data);
	}
	function view_raincheck($teetime_id = '')
	{

		//holes:{},
		//green_fees:{},
		//cart_fees:{},
		//taxes:{},
		$data = $this->Sale->get_raincheck_info();
		//$data['teetime_info'] = $this->Teetime->get_info($teetime_id);
		$data['taxes'] = array('gf'=>$this->Item->get_teetime_tax_rate(), 'c'=>$this->Item->get_cart_tax_rate());

		$this->load->view('teetimes/raincheck', $data);
	}
	function save_raincheck()
	{
		$customer_id=$this->sale_lib->get_customer();
		$raincheck_data = array(
			'teesheet_id'=>$this->input->post('teesheet_id'),
			'date_issued'=>date('Y-m-d H:i:s'),
			'players'=>$this->input->post('players'),
			'holes_completed'=>$this->input->post('holes'),
			'employee_id'=>$this->session->userdata('person_id'),
			'customer_id'=>$customer_id,
			'green_fee'=>$this->input->post('green_fee'),
			'cart_fee'=>$this->input->post('cart_fee'),
			'tax'=>$this->input->post('tax'),
			'total'=>$this->input->post('total_credit'),
			'green_fee_price_category'=>$this->input->post('green_fee_dropdown'),
			'cart_price_category'=>$this->input->post('cart_fee_dropdown')
		);

		$raincheck_id = $this->Sale->save_raincheck($raincheck_data);
		$raincheck_data['id'] = $raincheck_id;
		$customer = $this->Customer->get_info($customer_id);
		$raincheck_data['customer'] = $customer->first_name.' '.$customer->last_name;
		echo json_encode(array('success'=>$raincheck_id?true:false, 'data'=>$raincheck_data));
	}
	function remove_raincheck()
	{
		$this->sale_lib->remove_raincheck();
		$this->_reload();
	}
	function get_raincheck_info($teesheet_id = ''){
		echo json_encode($this->Sale->get_raincheck_info($teesheet_id));
	}

	function undelete($sale_id)
	{
		$data = array();

		if ($this->Sale->undelete($sale_id))
		{
			$data['success'] = true;
		}
		else
		{
			$data['success'] = false;
		}

		$this->load->view('food_and_beverage/undelete', $data);

	}

	function save($sale_id)
	{
		$sale_data = array(
			'sale_time' => date('Y-m-d h:i:s', strtotime($this->input->post('date'))),
			'customer_id' => $this->input->post('person_id') ? $this->input->post('person_id') : null,
			'employee_id' => $this->input->post('sale_employee_id'),
			'comment' => $this->input->post('comment')
		);

		if ($this->Sale->update($sale_data, $sale_id))
		{
			echo json_encode(array('success'=>true,'message'=>lang('sales_successfully_updated')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('sales_unsuccessfully_updated')));
		}
	}
	function save_invoice_details($cart_line)
	{
		$invoice_info = $this->sale_lib->get_invoice_details($cart_line);

		$new_price;
		foreach($this->input->post('invoice_item') as $index => $payment_amount)
		{
			$invoice_info['invoice_items'][$index]['payment_amount'] = str_replace(',','' , $payment_amount);
			$new_price += str_replace(',','' , $payment_amount);
		}

		$invoice_info['price'] =  $new_price;

		$this->sale_lib->update_invoice_details($invoice_info, $cart_line);
		echo json_encode(array('cart_line'=>$cart_line,'invoice_info'=>$this->sale_lib->get_invoice_details($cart_line),'basket_info'=>$this->sale_lib->get_basket_info(), 'items'=>$this->sale_lib->get_basket()));
	}
	function save_giftcard_details($cart_line)
	{
		echo json_encode(array("item_info"=>$this->sale_lib->save_giftcard_details($cart_line), 'basket_info'=>$this->sale_lib->get_basket_info()));
		//echo json_encode($this->sale_lib->save_giftcard_details($cart_line));
	}
	function save_punch_card_details($cart_line)
	{
		echo json_encode(array("item_info"=>$this->sale_lib->save_punch_card_details($cart_line), 'basket_info'=>$this->sale_lib->get_basket_info()));
		//echo json_encode($this->sale_lib->save_giftcard_details($cart_line));
	}
	function save_item_modifiers($cart_line)
	{
		$item = $this->input->post('item');
		$item_id = $item['item_id'];

		if($item['price'] != ''){
			$this->sale_lib->edit_item_attribute($cart_line, 'price', $item['price'], $item_id);
			$this->sale_lib->edit_basket_item_attribute($cart_line, 'price', $item['price'], $item_id);
		}
		if($item['discount'] != ''){
			$this->sale_lib->edit_item_attribute($cart_line, 'discount', $item['discount'], $item_id);
			$this->sale_lib->edit_basket_item_attribute($cart_line, 'discount', $item['discount'], $item_id);
		}
		if($item['seat'] != ''){
			$this->sale_lib->edit_item_attribute($cart_line, 'seat', $item['seat'], $item_id);
			$this->sale_lib->edit_basket_item_attribute($cart_line, 'seat', $item['seat'], $item_id);
		}
        $item_info = $this->sale_lib->save_item_modifiers($cart_line);
        $item_info['tax'] = $this->sale_lib->get_item_tax_amount($cart_line);

		echo json_encode(array("item_info"=>$item_info, 'basket_info'=>$this->sale_lib->get_basket_info()));
	}
	function tip_window($sale_id, $invoice)
	{
		$data['sale_id'] = $sale_id;
		$data['payment_info'] = $this->Sale->get_sale_payment($sale_id, $invoice)->row_array();
		$data['tip_info'] = $this->Sale->get_sale_tips($sale_id);
		//echo $this->db->last_query();
		$credit_card_info = $this->Sale->get_credit_card_payment($invoice);
		$data['employee'] = $this->Employee->get_info($data['payment_info']['tip_recipient'] ? $data['payment_info']['tip_recipient'] : $this->session->userdata('person_id'));
		$data['credit_card_info'] = $credit_card_info[0];
		//echo $this->db->last_query();
		$this->load->view('food_and_beverage/add_tip', $data);
	}
	function add_tip($sale_id, $invoice = false)
	{
		$this->load->model('payment');
		$gratuity = $this->input->post('tip_amount');
		$new_amount = $this->input->post('new_total');
		$tip_recipient = $this->input->post('tip_recipient');
		//$course_info = $this->Course->get_info($this->session->userdata('course_id'));
		if ($invoice)
		{
			$payment_info = $this->Sale->get_sale_payment($sale_id, $invoice)->row_array();
			//echo $this->db->last_query();
			$credit_card_info = $this->Sale->get_credit_card_payment($invoice);
			$credit_card_info = $credit_card_info[0];
			$this->load->library('Hosted_checkout_2');
			$HC = new Hosted_checkout_2();
			$HC->set_merchant_credentials($this->config->item('mercury_id'),$this->config->item('mercury_password'));//ForeUP's Credentials
			$HC->set_invoice($credit_card_info['invoice']);
			$HC->set_token($credit_card_info['token']);
			$HC->set_ref_no($credit_card_info['ref_no']);
			$HC->set_frequency($credit_card_info['frequency']);
			$HC->set_memo($credit_card_info['memo']);
			$HC->set_auth_code($credit_card_info['auth_code']);
		   	$HC->set_cardholder_name($credit_card_info['cardholder_name']);
			$HC->set_customer_code($credit_card_info['customer_code']);

		    $response = $HC->token_transaction('Adjust', $credit_card_info['amount'], $gratuity, $credit_card_info['tax_amount']);

			if ((string)$response->Status == 'Approved')
			{
				//Update credit card payment info
				$payment_data = array (
					'auth_amount'=>(string)$response->AuthorizeAmount,
					'gratuity_amount'=>(string)$response->GratuityAmount,
					'process_data'=>(string)$response->ProcessData,
					'token'=>(string)$response->Token
				);
				$this->sale->update_credit_card_payment($invoice, $payment_data);

				//Update sale payment info
				$sales_payments_data = array
				(
					'sale_id'=>$sale_id,
					'payment_type'=>"{$credit_card_info['card_type']} ".substr($credit_card_info['masked_account'], -8)." Tip",
					'payment_amount'=>$payment_data['gratuity_amount'],
					'invoice_id'=>$invoice,
					'tip_recipient'=>$tip_recipient
				);
				$this->payment->add($sales_payments_data);
				echo json_encode(array('success'=>true, 'message'=>"Tip of \${$payment_data['gratuity_amount']} added successfully. ", 'response'=>$respons));
				return;
			}
		}
		else
		{
			$tip_type = $this->input->post('tip_type');
			//Update sale payment info
			$sales_payments_data = array
			(
				'sale_id'=>$sale_id,
				'payment_type'=>"$tip_type ".lang('sales_tip'),
				'payment_amount'=>$gratuity,
				'invoice_id'=>0,
				'tip_recipient'=>$tip_recipient
			);
			if ($this->payment->add($sales_payments_data))
			{
				echo json_encode(array('success'=>true, 'message'=>"Tip of \${$gratuity} added successfully. "));
				return;
			}
		}
		echo json_encode(array('success'=>false, 'message'=>'We were unable to add a tip to this sale at this time. Please try again later. Note: Tips must be added before end of day.', 'response'=>$response));
		return;
	}
	function _payments_cover_total()
	{
		$total_payments = 0;

		foreach($this->sale_lib->get_payments() as $payment)
		{
			$total_payments += $payment['payment_amount'];
		}

		/* Changed the conditional to account for floating point rounding (Note: now ignores anything less than half a cent)*/
		if ( ( $this->sale_lib->get_mode() == 'sale' ) && ( ( to_currency_no_money( $this->sale_lib->get_basket_total() ) - $total_payments ) > .0049 ) )
		{
		//echo ( to_currency_no_money( $this->sale_lib->get_basket_total() ) - $total_payments );
			return false;
		}
		return true;
	}

	function _reload($data=array(), $ajax = false, $message = false)
	{
		$sale_id = $this->suspend($this->sale_lib->get_table_number());
		$this->sale_lib->set_table_id($sale_id);

		//if ($data['error_message'])
		//	$message = array('text'=>$data['error_message'],'type'=>'error_message','persist'=>false);
		// TEMPLATE FOR MESSAGE array('text'=>'i forgot that the message lasts longer depending on how long the message is','type'=>'error_message','persist'=>false);
		$data['raincheck_info'] = ($this->session->userdata('raincheck_id'))?$this->sale_lib->get_raincheck():false;
		$data['table_number'] = $this->sale_lib->get_table_number();
		$data['mode']=$this->sale_lib->get_mode();
        $data['recent_transactions']=($this->session->userdata('recent_transactions'))?$this->session->userdata('recent_transactions'):array();//$this->Sale->get_recent_transactions();
		$data['taxes']=$this->sale_lib->get_taxes();
		$data['basket_taxes']=$this->sale_lib->get_basket_taxes();
		$data['amount_due']=$this->sale_lib->get_amount_due();
		$data['basket_amount_due']=$this->sale_lib->get_basket_amount_due();
		$data['total']=$this->sale_lib->get_total();
		$data['basket_total']=$this->sale_lib->get_basket_total();
		$data['payments']=$this->sale_lib->get_payments();
		$data['items_in_cart'] = $this->sale_lib->get_items_in_cart();
		$data['items_in_basket'] = $this->sale_lib->get_items_in_basket();
		$data['subtotal']=$this->sale_lib->get_subtotal();
		$data['basket_subtotal']=$this->sale_lib->get_basket_subtotal();
		$data['taxable_checked']=($this->session->userdata('taxable') == 'false')?false:true;
		$data['taxable_disabled']= '';
        $person_info = $this->Employee->get_logged_in_employee_info();
		$data['cart']=$this->sale_lib->get_cart();
		$data['basket']=$this->sale_lib->get_basket();
		$data['modes']=array('sale'=>lang('sales_sale'),'return'=>lang('sales_return'));
		$data['items_module_allowed'] = $this->Employee->has_permission('items', $person_info->person_id);
		$data['comment'] = $this->sale_lib->get_comment();
		$data['email_receipt'] = $this->sale_lib->get_email_receipt();
		$data['payments_total']=$this->sale_lib->get_payments_total();
		$data['payment_options']=array(
			lang('sales_credit') => lang('sales_credit'),
			lang('sales_cash') => lang('sales_cash'),
			lang('sales_giftcard') => lang('sales_giftcard'),
			lang('sales_check') => lang('sales_check')
		);
		$this->load->helper('sale');
		$data['quickbutton_info'] = $this->Quickbutton->get_all();
		$data['quickbutton_tab'] = $this->session->userdata('quickbutton_tab');
		$data['customer_quickbuttons'] = $this->sale_lib->get_customer_quickbuttons();
		$data['suspended_sales'] = $this->Table->get_all(true);
		$data['selected_suspended_sale_id'] = $this->sale_lib->get_table_id();
		$data['controller_name']=strtolower(get_class());
		$data['items'] = $this->Item->get_all(10000, 0, true, false, 1)->result_array();
		$data['receipts'] = $this->table_receipt->get_all($sale_id);
		$data['sale_id'] = $sale_id;

		if ($data['mode'] == 'return')
		{
			//$this->sale_lib->delete_customer();
			if ($this->session->userdata('return_sale_id'))
			{
				$sale_id = $this->session->userdata('return_sale_id');
				$data['sale_id'] = "POS ".$sale_id;
				$data['credit_card_payments'] = $this->Sale->get_credit_card_payments($sale_id);
				$data['sale_payments'] = $this->Sale->get_sale_payments($sale_id)->result_array();
				//print_r($data['credit_card_payments']);
				//print_r($data['sale_payemnts']);

			}
		}
		$customer_id=$this->sale_lib->get_customer();
		if($customer_id!=-1)
		{
			$info=$this->Customer->get_info($customer_id);
			$data['customer']=$info->first_name.' '.$info->last_name;
			$data['customer_email']=$info->email;
			$data['customer_phone']=$info->phone_number;
			$data['is_member']=$info->member;
			$data['customer_id']=$customer_id;
			$data['customer_account_number']=$info->account_number;
			$data['customer_account_balance']=$info->account_balance;
			$data['customer_member_balance']=$info->member_account_balance;
			$data['cab_allow_negative']=$info->account_balance_allow_negative;
			$data['cmb_allow_negative']=$info->member_account_balance_allow_negative;
			$data['loyalty_points']=$info->loyalty_points;
			$data['taxable_checked']=(!$info->taxable || $this->session->userdata('taxable')=='false')?false:true;
			$data['taxable_disabled']=(!$info->taxable)?'disabled':'';
			$data['customer_groups']=$this->Customer->get_customer_groups($customer_id);
			$data['customer_passes']=$this->Customer->get_customer_passes($customer_id);
		}
		$data['cab_name']=($this->config->item('customer_credit_nickname') == '') ? lang('customers_account_balance') : $this->config->item('customer_credit_nickname');
		$data['cmb_name']=($this->config->item('member_balance_nickname') == '') ? lang('customers_member_account_balance') : $this->config->item('member_balance_nickname');
		$data['payments_cover_total'] = (($data['total'] != 0 || ($data['items_in_basket'] > 0 && count($data['payments']) > 0)) && $this->_payments_cover_total());
		$data['cart_data'] = $this->sale_lib->get_cart();

		if ($ajax == false) // Reload Whole Page
		{
			if ($this->session->userdata('mobile'))
				echo json_encode($data);
			else
				$this->load->view("food_and_beverage/register",$data);
			return;
		}

		$register_box_info = 	 array("cart"						=> $data['cart'],
									   //"giftcard_error_line"		=> $data['giftcard_error_line'],
									   "items_module_allowed"		=> $data['items_module_allowed'],
									   "basket"						=> $data['basket']);
			$customer_info = 	 array("customer_id"				=> $data['customer_id'],
									   "customer"					=> $data['customer'],
									   "customer_quickbuttons"		=> $data['customer_quickbuttons'],
									   "customer_account_number"	=> $data['customer_account_number'],
									   "customer_email"				=> $data['customer_email'],
									   "customer_phone"				=> $data['customer_phone'],
									   "cab_name"					=> $data['cab_name'],
									   "customer_account_balance"	=> $data['customer_account_balance'],
									   "cab_allow_negative"			=> $data['cab_allow_negative'],
									   "is_member"					=> $data['is_member'],
									   "cmb_name"					=> $data['cmb_name'],
									   "customer_member_balance"	=> $data['customer_member_balance'],
									   "cmb_allow_negative"			=> $data['cmb_allow_negative'],
									   "mode"						=> $data['mode'],
									   'loyalty_points'				=> $data['loyalty_points'],
									   "customer_groups"			=> $data['customer_groups'],
									   "customer_passes"			=> $data['customer_passes']);
									   //"giftcard"					=> $giftcard,
									   //"giftcard_balance"			=> $giftcard_balance);
		$suspended_sales_info =  array("suspended_sales" 			=> $data['suspended_sales'],
									   "selected_suspended_sale_id" => $data['selected_suspended_sale_id']);

		if ($ajax == 'add_payment' || $ajax == 'delete_payment')
		{
			if ($this->session->userdata('mobile'))
			{
				echo json_encode(array(
				   'payments'				=>$data['payments'],
				   'raincheck_info'			=>$data['raincheck_info'],
				   'mode'					=>$data['mode'],
				   'sale_payments'			=>$data['sale_payments'],
				   'amount_due'				=>$data['basket_amount_due'],
				   'payments_cover_total'	=>$data['payments_cover_total'],
				   'message'				=>$message));
			}
			else
			{
				echo json_encode(
					array(
						'payments'=>$this->load->view("food_and_beverage/payments_list", $data, true),
						'amount_due'=>$data['basket_amount_due'],
						'payments_cover_total'=>$data['payments_cover_total'],
						'message'=>$message
					)
				);
			}
		}
		else if ($ajax == 'remove_item' || $ajax == 'add_item' || $ajax == 'change_mode')
		{
			// echo "<pre>";
			// print_r($register_box_info);
			// echo "</pre>";
			if ($this->session->userdata('mobile'))
			{
				echo json_encode(
					array(
						'register_box_info'	=>	$register_box_info,
				   		'basket_info'		=>	$this->sale_lib->get_basket_info(),
				   		'payments'			=>  $data['payments'],
						'raincheck_info'	=> 	$data['raincheck_info'],
						'sale_payments'		=>  $data['sale_payments'],
						'amount_due'		=>  $data['basket_amount_due'],
						'mode'				=>  $data['mode'],
						'customer_info'		=>	$customer_info,
						'message'			=>	$message
				   )
			   );
			}
			else
			{
				echo json_encode(
					array(
						'register_box'	=>	$this->load->view("food_and_beverage/register_box", $register_box_info, true),
				   		'basket_info'	=>	$this->sale_lib->get_basket_info(),
				   		'table_top'		=>	$this->load->view("food_and_beverage/table_top", array("mode" => $data['mode']), true),
				   		'payments'				=>  $this->load->view("food_and_beverage/payments_list", $data, true),
				   		'split_payments' 		=>	$this->load->view("food_and_beverage/split_payments", $data, true),
						'mode'					=>  $data['mode'],
						'customer_info_filled'	=>	$this->load->view("food_and_beverage/customer_info_filled", $customer_info, true),
				   		'payment_window'		=>	$this->load->view("food_and_beverage/payment_window", $customer_info, true),
						'message'=>$message
				   )
			   );
			}
		}
		else if ($ajax == 'select_customer' || $ajax == 'remove_customer')
		{

			echo json_encode(array('customer_info_filled'	=>	$this->load->view("food_and_beverage/customer_info_filled", $customer_info, true),
								   'payment_window'			=>	$this->load->view("food_and_beverage/payment_window", $customer_info, true),
								   'message'=>$message
								   ));
		}
		else if ($ajax == 'suspend_sale' || $ajax == 'unsuspend' || $ajax == 'completed_sale' || $ajax == 'cancel_sale')
		{
			//set the return_sale_id blank so when they user selects the 'return' tab it won't pull in the return_sale_id from the session and get the connected payments
			if ($ajax == 'cancel_sale') $this->session->set_userdata('return_sale_id', '');

			if ($this->session->userdata('mobile'))
			{
				$json_array = array('register_box_info'		=>	$register_box_info,
									'basket_info'			=>	$this->sale_lib->get_basket_info(),
									'mode'					=>  $data['mode'],
									'customer_info'			=>	$customer_info,
									'suspended_sales'		=> 	$suspended_sales_info,
									'payments'				=>  $data['payments'],
								    'amount_due'			=>  $data['basket_amount_due'],
								    'payments_cover_total'	=>  $data['payments_cover_total'],
								    'message'				=>	$message,
								    'table_number'			=> 	$data['table_number'],
								    'receipts' 				=> 	$data['receipts'],
								    'sale_id'				=> 	$data['sale_id']
								);
				if ($ajax == 'completed_sale') {
					$json_array['location'] = 'sales';
					$json_array['receipt_data'] = $data['receipt_data']; // need to trim this down
					$json_array['is_cart_empty'] = count($data['cart'])==0?'true':'false';
					$json_array['recent_transactions'] = $data['recent_transactions'];
				}
			}
			else
			{
				$json_array = array('register_box'			=>	$this->load->view("food_and_beverage/register_box", $register_box_info, true),
									'basket_info'			=>	$this->sale_lib->get_basket_info(),
									'table_top'				=>	$this->load->view("food_and_beverage/table_top", array("mode" => $data['mode']), true),
									'mode'					=>  $data['mode'],
									'customer_info_filled'	=>	$this->load->view("food_and_beverage/customer_info_filled", $customer_info, true),
									'payment_window'		=>	$this->load->view("food_and_beverage/payment_window", $customer_info, true),
									'suspended_sales'		=> 	$this->load->view("food_and_beverage/suspended_sales", $suspended_sales_info, true),
									'suspend_button_title'	=>  lang('sales_suspend_sale'),
									'payments'				=>  $this->load->view("food_and_beverage/payments_list", $data, true),
								    'amount_due'			=>  $data['basket_amount_due'],
								    'payments_cover_total'	=>  $data['payments_cover_total'],
								    'customer_info_filled'	=>	$this->load->view("food_and_beverage/customer_info_filled", $customer_info, true),
									'message'				=>	$message,
								    'table_number'			=> 	$data['table_number'],
								    'receipts' 				=> 	$data['receipts'],
								    'sale_id'				=>  $data['sale_id'],
								    'split_payments' 		=>	$this->load->view("food_and_beverage/split_payments", $data, true),
								);
				if ($ajax == 'completed_sale') {
					$json_array['location'] = 'sales';
					$json_array['receipt_data'] = $data['receipt_data']; // need to trim this down
					$json_array['is_cart_empty'] = count($data['cart'])==0?'true':'false';

					$recent_transactions_info = array("recent_transactions" =>	$data['recent_transactions']);
					$json_array['recent_transactions'] = $this->load->view("food_and_beverage/recent_transactions", $recent_transactions_info, true);
					$json_array['message']=$message;
				}
			}
			echo json_encode($json_array);
		}
	}

	function get_recent_transactions($limit = 5)
	{
		if ($this->session->userdata('mobile'))
		{
			echo json_encode($this->Sale->get_recent_transactions($limit));
		}
		else
		{
			echo $this->load->view("food_and_beverage/recent_transactions", array('recent_transactions'=>$this->Sale->get_recent_transactions($limit)), true);
		}
	}

	function invoice_details($invoice_id = -1, $cart_line = 0)
	{
		$data['invoice_info'] = $this->sale_lib->get_invoice_details($cart_line);
		$data['cart_line'] = $cart_line;
		$this->load->view('customers/invoice_form', $data);
	}

    function cancel_sale()
    {
//        echo 'cancelling sale';
       	$this->session->unset_userdata('purchase_override');
      	$this->session->unset_userdata('purchase_override_time');
    	$this->sale_lib->clear_all();
    	$this->_reload(null, "cancel_sale");

    }
	function suspend_menu(){
		$data = array();
		//$data['cart']=$this->sale_lib->get_cart();
		//$data['']
		$fss = array();
		$suspended_sales = $this->Table->get_all(true)->result_array();
		//print_r($suspended_sales);
		foreach($suspended_sales as $ss)
			$fss[$ss['table_id']] = $ss['sale_id'];
		$data['suspended_sales'] = $fss;
		$this->load->view('food_and_beverage/suspend_menu', $data);
	}

	function suspend($table_number = false)
	{
		$message = false;
		if ($table_number)
		{
			$data['cart']=$this->sale_lib->get_cart();
			$data['basket']=$this->sale_lib->get_basket();
			$data['subtotal']=$this->sale_lib->get_subtotal();
			$data['taxes']=$this->sale_lib->get_taxes();
			$data['total']=$this->sale_lib->get_total();
			$data['receipt_title']=lang('sales_receipt');
			$data['transaction_time']= date(get_date_format().' '.get_time_format());
			$customer_id=$this->sale_lib->get_customer();
			$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
			$comment = $this->sale_lib->get_comment();
			$emp_info=$this->Employee->get_info($employee_id);
			//Alain Multiple payments
			$data['payments']=$this->sale_lib->get_payments();
			$data['amount_change']=to_currency($this->sale_lib->get_amount_due() * -1);
			$data['employee']=$emp_info->first_name.' '.$emp_info->last_name;

			if($customer_id!=-1)
			{
				$cust_info=$this->Customer->get_info($customer_id);
		//		$data['customer']=$cust_info->first_name.' '.$cust_info->last_name.($cust_info->company_name==''  ? '' :' ('.$cust_info->company_name.')');
			}

			$total_payments = 0;

			foreach($data['payments'] as $payment)
			{
				$total_payments += $payment['payment_amount'];
			}

			$sale_id = $this->sale_lib->get_table_id();
			//SAVE sale to database
			$sale_id = $this->Table->save($data['cart'], $customer_id,$employee_id,$comment,$data['payments'], $sale_id, $table_number);
			$data['sale_id']='POS '.$sale_id;

			if ($data['sale_id'] == 'POS -1')
			{
				//$data['error'] = lang('sales_transaction_failed');
				$message = array('text'=>lang('sales_transaction_failed'),'type'=>'error_message','persist'=>false);
			}
			else {
				//$data['success'] = lang('sales_successfully_suspended_sale');
				$message = array('text'=>lang('sales_successfully_suspended_sale'),'type'=>'success_message','persist'=>false);
			}
			return $sale_id;
		}
	}
	function giftcard_exists($giftcard_number = '')
	{
		echo json_encode(array('exists'=>$this->Giftcard->get_giftcard_id($giftcard_number)));
	}

	function punch_card_exists($punch_card_number = '')
	{
		echo json_encode(array('exists'=>$this->Punch_card->get_punch_card_id($punch_card_number)));
	}

	function view_giftcard($giftcard_id=-1, $cart_line = 0, $view_only = false)
	{
		$this->load->library('Sale_lib');
		$data = array();
		$data['customers'] = array('' => 'No Customer');
		/*foreach ($this->Customer->get_all()->result() as $customer)
		{
			$data['customers'][$customer->person_id] = $customer->first_name . ' '. $customer->last_name;
		}*/
		$data['view_only'] = false;
		$data['cart_line'] = $cart_line;
		$data['giftcard_info']=$this->Giftcard->get_info($giftcard_id);
		if ($cart_line)
		{
			$giftcard_data = $this->sale_lib->get_giftcard_details($cart_line);
			$data['giftcard_info']->customer_id = $giftcard_data['giftcard_data']['customer_id'];
			$data['giftcard_info']->giftcard_number = $giftcard_data['giftcard_data']['giftcard_number'];
			$data['giftcard_info']->customer_name = $giftcard_data['giftcard_data']['customer_name'];
			$data['giftcard_info']->value = $giftcard_data['value'];
			$data['giftcard_info']->details = $giftcard_data['giftcard_data']['details'];
			$data['giftcard_info']->expiration_date = $giftcard_data['giftcard_data']['expiration_date'];
		}
		$customer_info = $this->Customer->get_info($data['giftcard_info']->customer_id);
		if ($customer_info->last_name.$customer_info->first_name != '')
			$data['customer'] = $customer_info->last_name.', '.$customer_info->first_name;
		else
			$data['customer'] = 'No Customer';

		$this->load->view("giftcards/form",$data);
	}
	function view_punch_card($punch_card_id=-1, $cart_line = 0, $view_only = false)
	{
		$this->load->library('Sale_lib');
		$data = array();
		$data['customers'] = array('' => 'No Customer');
		/*foreach ($this->Customer->get_all()->result() as $customer)
		{
			$data['customers'][$customer->person_id] = $customer->first_name . ' '. $customer->last_name;
		}*/
		$data['view_only'] = false;
		$data['cart_line'] = $cart_line;
		$data['punch_card_info']=$this->Punch_card->get_info($punch_card_id);
		if ($cart_line)
		{
			$punch_card_data = $this->sale_lib->get_punch_card_details($cart_line);
			$data['punch_card_info']->customer_id = $punch_card_data['punch_card_data']['customer_id'];
			$data['punch_card_info']->punch_card_number = $punch_card_data['punch_card_data']['punch_card_number'];
			$data['punch_card_info']->customer_name = $punch_card_data['punch_card_data']['customer_name'];
			$data['punch_card_info']->item_kit_id = $punch_card_data['punch_card_data']['item_kit_id'];
			$data['punch_card_info']->value = $this->Item_kit_items->get_info($punch_card_data['punch_card_data']['item_kit_id'], true);
			$data['punch_card_info']->details = $punch_card_data['punch_card_data']['details'];
			$data['punch_card_info']->expiration_date = $punch_card_data['punch_card_data']['expiration_date'];
		}
		$customer_info = $this->Customer->get_info($data['punch_card_data']->customer_id);
		$data['customer'] = '';
		if ($customer_info->last_name.$customer_info->first_name != '')
			$data['customer'] = $customer_info->last_name.', '.$customer_info->first_name;

		$this->load->view("punch_cards/form",$data);
	}
	function view_modifiers($item_id=-1, $cart_line = 0, $view_only = false)
	{
		$this->load->library('Sale_lib');
		$data = array();
		$data['view_only'] = false;
		$data['cart_line'] = $cart_line;
		$data['item_id'] = $item_id;

		if ($cart_line)
		{
			$items = $this->sale_lib->get_cart();
			$data['item'] = $items[$cart_line];
			$data['modifiers'] = $this->sale_lib->get_modifiers($cart_line);
		}

		$this->load->view("modifiers/food_item_modifiers", $data);
	}
	function suspended()
	{
		$data = array();
		$data['suspended_sales'] = $this->Table->get_all()->result_array();
		$this->load->view('food_and_beverage/suspended', $data);
	}
	function delete_suspended_sale($sale_id)
	{
		$this->Table->delete($sale_id);

		echo json_encode(array());
	}
	function unsuspend($sale_id = false, $table_number = false)
	{
		if(empty($sale_id)){
			$sale_id = false;$sale_id = $this->suspend($table_number);
		}

		$this->sale_lib->clear_all();
		$this->sale_lib->copy_entire_table($sale_id);
		$this->sale_lib->set_table_id($sale_id);
		$this->sale_lib->set_table_number($table_number);

		//$sale_id = $this->Table->get_id_by_table_number($table_number);
		//$this->session->set_userdata('fnb_sale_id', $sale_id);

    	$this->_reload($data, "unsuspend");
	}
	function update_cart() {
        $item_id = '';
        $attribute = '';
        $value = '';
        $course_id = $this->session->userdata('course_id');
        $teesheet_id = $this->session->userdata('teesheet_id');
        $item_number = '';

        $line = $this->input->post('line');
        $checked = $this->input->post('checked');
        if ($this->input->post('price') !== false) {
            $attribute = 'price';
            $value = $this->input->post('price');
        }
        else if ($this->input->post('quantity') !== false) {
            $attribute = 'quantity';
            $value = $this->input->post('quantity');
        }
        else if ($this->input->post('discount') !== false) {
            $attribute = 'discount';
            $value = $this->input->post('discount');
        }
        else if ($this->input->post('item_id')) {
            $item_id = $this->input->post('item_id');
        }
        else if ($this->input->post('item_number')){
            $attribute = 'item_number';
            $item_number = $course_id.'_'.substr($this->input->post('item_number'), strpos($this->input->post('item_number'), '_')+1);
            $item_id = $this->Item->get_item_id($item_number);
        }
		$basket_info = array();
        $this->sale_lib->edit_basket_item_attribute($line, $attribute, $value, $item_id);
        $item_info = $this->sale_lib->edit_item_attribute($line, $attribute, $value, $item_id);
        $item_info['tax'] = $this->sale_lib->get_item_tax_amount($line);

        echo json_encode(array("item_info"=>$item_info, 'basket_info'=>$this->sale_lib->get_basket_info()));
    }
    function update_basket(){
        $line = $this->input->post('line');
        $checked = $this->input->post('checked');
        if ($line == 'all') {
            if ($checked == 'true')
                $this->sale_lib->put_cart_into_basket();
            else
                $this->sale_lib->empty_basket();
        }
        else {
            if ($checked == 'true')
                $this->sale_lib->copy_item_into_basket($line);
            else
                $this->sale_lib->delete_item_from_basket($line);
        }

        echo json_encode($this->sale_lib->get_basket_info());
    }
    function delete_cart_items() {
        $line_array = $this->input->post('line_array');
        $cart_data = $this->sale_lib->get_cart();
        $basket_data = $this->sale_lib->get_basket();
        //print_r($line_array);
        foreach($line_array as $line) {
            $index = str_replace('select_', '', $line);
            unset($cart_data[$index]);
            unset($basket_data[$index]);
        }
        $this->sale_lib->set_cart($cart_data);
        $this->sale_lib->set_basket($basket_data);
    }
	function generate_stats()
	{
		$results = $this->Dash_data->fetch_pos_data();
		echo json_encode($results);
	}

}
?>