<?php
class Communication extends CI_Model
{	
	function record($type, $recipient_count, $from, $campaign_id, $course_id = false)
	{
		$data = array(
			'type'=>$type,
			'recipient_count'=>$recipient_count,
			'from'=>$from,
			'course_id'=>$course_id ? $course_id : $this->session->userdata('course_id'),
			'campaign_id'=>$campaign_id
		);
		$this->db->insert('communications', $data);
		
	}
	
	function days_in_month($month = 0, $year = 0)
	{
		if ($month < 1 OR $month > 12)
		{
			$month = date('m');
		}

		if ( ! is_numeric($year) OR strlen($year) != 4)
		{
			$year = date('Y');
		}
		
		return date('d', strtotime("{$year}-{$month} +2 months -1 day"));
	}
	
	function get_stats($course_id = '')
	{
		$final_array = array('Email'=>'','Text'=>'');
		$start_date = date('Y-m-01');
		$end_date = date('Y-m-'.$this->days_in_month(date('m')));
		$pm_start_date = date('Y-m-01', strtotime($start_date.' -1 month'));
		$pm_end_date = date('Y-m-'.$this->days_in_month(date('m', strtotime($start_date.' -1 month'))), strtotime($start_date.' -1 month'));
		$course_sql = '';
		if ($course_id != '')
			$course_sql = "WHERE course_id = $course_id";
		$results = $this->db->query("SELECT type, sum(IF (date >= '$start_date' AND date <= '$end_date', recipient_count, 0 )) AS this_month, 
			sum(IF (date >= '$pm_start_date' AND date <= '$pm_end_date', recipient_count, 0 )) AS last_month,
			sum(IF (date >= '$start_date' AND date <= '$end_date' AND campaign_id != 0, recipient_count, 0 )) AS mk_this_month, 
			sum(IF (date >= '$pm_start_date' AND date <= '$pm_end_date' AND campaign_id != 0, recipient_count, 0 )) AS mk_last_month, 
			sum(recipient_count) AS total_sent 
			FROM foreup_communications $course_sql GROUP BY type");
		foreach ($results->result_array() as $result)
			$final_array[$result['type']] = $result;
		
		$data = array(
			'emails_this_month'=>isset($final_array['Email']['this_month'])?$final_array['Email']['this_month']:0,
			'emails_last_month'=>isset($final_array['Email']['last_month'])?$final_array['Email']['last_month']:0,
			'emails_mk_this_month'=>isset($final_array['Email']['mk_this_month'])?$final_array['Email']['mk_this_month']:0,
			'emails_mk_last_month'=>isset($final_array['Email']['mk_last_month'])?$final_array['Email']['mk_last_month']:0,
			'emails_total_sent'=>isset($final_array['Email']['total_sent'])?$final_array['Email']['total_sent']:0,
			'texts_mk_this_month'=>isset($final_array['Text']['mk_this_month'])?$final_array['Text']['mk_this_month']:0,
			'texts_mk_last_month'=>isset($final_array['Text']['mk_last_month'])?$final_array['Text']['mk_last_month']:0,
			'texts_total_sent'=>isset($final_array['Text']['total_sent'])?$final_array['Text']['total_sent']:0
		);
		
		return $data;
	}
	
	
}
	
