<?php
require_once("report.php");
class Detailed_account_transactions extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		$customer_account_balance = ($this->config->item('customer_credit_nickname') ? $this->config->item('customer_credit_nickname'):lang('customers_account_balance'));
		$member_account_balance = ($this->config->item('member_balance_nickname') ? $this->config->item('member_balance_nickname'):lang('customers_member_account_balance'));
		return array(array('data'=>lang('giftcards_customer_name'), 'align'=>'left'), array('data'=>'Amount', 'align'=> 'left'), array('data'=>'Date', 'align'=> 'left'), array('data'=>'Details', 'align'=> 'left'));
		
	}
	
	public function getData()
	{
		$account_transactions = $this->Account_transactions->get_account_transaction_data_for_customer(false, false, $this->params['date']);
		$member_account_transactions = $this->Member_account_transactions->get_member_account_transaction_data_for_customer(false, false, $this->params['date']);

		return array('account_transactions'=>$account_transactions, 'member_account_transactions'=>$member_account_transactions);	
	}
	
	public function getSummaryData()
	{
		if (!$this->permissions->is_super_admin())
            $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->select('sum(IF(account_balance > 0, account_balance, 0)) as positive_balance, sum(IF(account_balance < 0, account_balance, 0)) as negative_balance, sum(account_balance) AS net, sum(IF(member_account_balance > 0, member_account_balance, 0)) as positive_member_balance, sum(IF(member_account_balance < 0, member_account_balance, 0)) as negative_member_balance, sum(member_account_balance) AS mnet', false);
		$this->db->from('customers');
		$this->db->where('deleted', 0);
		$this->db->join('people', 'customers.person_id = people.person_id', 'left');
	
		$results = $this->db->get()->result_array();		
		return $results[0];
	}
}
?>