<?php
class Table_Receipt extends CI_Model
{
	function __construct(){
		parent::__construct();
		$this->load->library('sale_lib');
	}

	// Check if current user has permission to access a sale
	function has_sale_access($sale_id)
	{
		$this->db->select('table_id');
		$result = $this->db->get_where('foreup_tables', array('sale_id'=>(int) $sale_id, 'course_id'=>$this->session->userdata('course_id')));

		if($result->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	function receipt_exists($sale_id, $receipt_id){
		$this->db->select('receipt_id');
		$query = $this->db->get_where("table_receipts", array('receipt_id'=>$receipt_id, 'sale_id'=>$sale_id));

		if($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	function mark_paid($sale_id, $receipt_id){
		$query = $this->db->query("UPDATE foreup_table_receipts
			SET date_paid = NOW()
			WHERE sale_id = ".(int) $sale_id."
				AND receipt_id = ". (int) $receipt_id);
		return true;
	}

	function mark_unpaid($sale_id, $receipt_id){
		$query = $this->db->query("UPDATE foreup_table_receipts
			SET date_paid = NULL
			WHERE sale_id = ".(int) $sale_id."
				AND receipt_id = ". (int) $receipt_id);
		return true;
	}

	// Saves a new receipt
	function save($sale_id, $receipt_id, $items = null){

		if(!$this->has_sale_access($sale_id)){
			return false;
		}

		$this->db->insert('table_receipts', array('sale_id'=>$sale_id, 'receipt_id'=>$receipt_id));
		$receipt_id = $this->db->insert_id();

		// If items were passed as well, insert those into database
		if(!empty($items)){
			$item_batch = array();

			// Conform the item array for DB insertion
			foreach($items as $item){
				$item_batch[] = array('receipt_id'=>$receipt_id, 'sale_id'=>$sale_id, 'item_id'=>$item['item_id'], 'line'=>$item['line']);
			}

			$this->db->insert_batch('table_receipt_items', $item_batch);
		}

		return $receipt_id;
	}

	// Retrieve all receipts for a table including items connected to each
	function get_all($sale_id)
	{
		if(!$this->has_sale_access($sale_id)){
			return false;
		}
		$cart_items = $this->sale_lib->get_cart();

		$this->db->select('r.receipt_id, r.date_created AS receipt_date, r.date_paid,
			r.sale_id, ri.line AS item_line,i.item_id, i.name AS item_name', false);
		$this->db->from('table_receipts AS r');
		$this->db->join('table_receipt_items AS ri', 'ri.receipt_id = r.receipt_id AND ri.sale_id = r.sale_id', 'left');
		$this->db->join('items AS i', 'i.item_id = ri.item_id', 'left');
		$this->db->where('r.sale_id', $sale_id);
		$this->db->order_by('ri.receipt_id ASC, ri.line ASC');
		$query = $this->db->get();

		$rows = $query->result_array();
		$receipts = array();
		$item_counts = array();

		// Loop through receipt items, create multi-dimension array by receipt
		foreach($rows as $item){
			$receipt_id = $item['receipt_id'];
			$receipts[$receipt_id]['date_created'] = $item['receipt_date'];
			$receipts[$receipt_id]['sale_id'] = $item['sale_id'];
			$receipts[$receipt_id]['receipt_id'] = $receipt_id;
			$receipts[$receipt_id]['date_paid'] = $item['date_paid'];

			if(!isset($receipts[$receipt_id]['total'])){
				$receipts[$receipt_id]['total'] = 0;
			}

			if(!empty($item['item_id'])){
				unset($item['receipt_id'], $item['receipt_date'],$item['sale_id']);
				$cart_item = $cart_items[$item['item_line']];
				$item['price'] = $cart_item['price'] + $cart_item['modifier_total'];
				$item['seat'] = $cart_item['seat'];
				$item['price'] = round($item['price'] - ($item['price'] * ($cart_item['discount'] / 100)), 2);

				$item['tax'] = $this->sale_lib->get_item_tax_amount($item['item_line']);
				$receipts[$receipt_id]['items'][] = $item;

				if(!isset($item_counts[$item['item_id']])){
					$item_counts[$item['item_id']] = 1;
				}else{
					$item_counts[$item['item_id']] += 1;
				}
			}
		}

		// Loop through each receipt and it's items, calculate actual prices
		// based on how receipt is split up
		foreach($receipts as $receipt_key => $receipt){
			foreach($receipt['items'] as $item_key => $item){
				$total_price = $item['price'] + $item['tax'];
				$divisor = $item_counts[$item['item_id']];
				$split_price = round($total_price / $divisor, 2);

				$receipts[$receipt_key]['items'][$item_key]['total_price'] = $total_price;
				$receipts[$receipt_key]['items'][$item_key]['split_price'] = $split_price;
				$receipts[$receipt_key]['total'] += $split_price;
			}
		}

		return array_values($receipts);
	}

	// Adds a new item to a receipt
	function add_item($item_id, $line, $sale_id, $receipt_id){
		if(!$this->receipt_exists($sale_id, $receipt_id)){
			$this->save($sale_id, $receipt_id);
		}

		$this->db->insert('table_receipt_items', array('sale_id'=>$sale_id, 'receipt_id'=>$receipt_id, 'item_id'=>$item_id, 'line'=>$line));
		return $this->db->insert_id();
	}

	// Removes an item from a receipt
	function delete_item($item_id, $line, $sale_id, $receipt_id = null){

		if(!$this->has_sale_access($sale_id)){
			return false;
		}
		$where['line'] = $line;
		$where['item_id'] = $item_id;
		$where['sale_id'] = $sale_id;

		if(!empty($receipt_id)){
			$where['receipt_id'] = $receipt_id;
		}
		$results = $this->db->delete('table_receipt_items', $where);
		return $results;
	}

	// Delete entire receipt including receipt items associated with it
	function delete($sale_id, $receipt_id){

		if(!$this->has_sale_access($sale_id)){
			return false;
		}
		if(empty($sale_id) || empty($receipt_id)){
			return false;
		}

		$result = $this->db->query("DELETE r.*, ri.*
			FROM foreup_table_receipts AS r
			LEFT JOIN foreup_table_receipt_items AS ri
				ON ri.sale_id = r.sale_id
				AND ri.receipt_id = r.receipt_id
			WHERE r.sale_id = ".(int) $sale_id."
				AND r.receipt_id = ".(int) $receipt_id);

		return $result;
	}
}
?>