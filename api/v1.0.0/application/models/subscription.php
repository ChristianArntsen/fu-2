<?php
class Subscription extends CI_Model
{
	function unsubscribe_email($unsubscribe_data, &$customer_info, $course_id)
	{
		$completed_transactions = FALSE;				
		$customers= $this->Customer->get_info_by_email($unsubscribe_data['email'], $course_id);	
		foreach($customers as $customer_info){											
			$customer_data = array(
				'opt_out_email'=>1,
				'course_news_announcements'=>$unsubscribe_data['course_news_announcements']					
			);
							
			$person_data = array(
				'address_2'=>''
			);
			$giftcards=array();
			$groups=array();
			$passes=array();
						
			if ($this->Customer->save($person_data, $customer_data, $customer_info['person_id'], $giftcards, $groups, $passes))
			{
				
				$this->db->trans_start();	
				
				$this->save($unsubscribe_data);		
						
				$this->db->trans_complete();
				
				if ($this->db->trans_status() === TRUE) $completed_transactions = TRUE;			
			}
		}
			

		return $completed_transactions;
	}
	
	function save($unsubscribe_data){
		if ($this->email_prefernces_exist($unsubscribe_data['email'])) {
			$this->db->where('email', $unsubscribe_data['email']);
			$success = $this->db->update('unsubscribes',$unsubscribe_data);
		} else {
			$this->db->insert('unsubscribes',$unsubscribe_data);
		}		
		
		return;
	}

	function email_prefernces_exist($email)
	{
		$this->db->from('unsubscribes');
		$this->db->where('email',$email);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}
	
	function count_all(){}

	function get_all($limit=10000, $offset=0){}
	
	function get_search_suggestions($search,$limit=25){}

	function search($search, $limit=20){}

	function get_info($tournament_id){}	

	function exists($tournament_id){}	
	
	function delete_list($tournament_ids){}
}
?>
