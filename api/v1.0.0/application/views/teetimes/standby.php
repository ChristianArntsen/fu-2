<div class='background'>
<?php

//log_message('error', 'is the teetimes array in here? ' . $this->input->post('teetimes'));
if ($this->input->get('id') != null && $this->input->get('id') !== '')
{
    echo form_open('/teesheets/teetime_edit_standby/', array('id'=>'standby_form', 'name'=>'standby_form'));
}
else 
{
    echo form_open('/teesheets/teetime_save_standby/'.$standby_info->standyby_id, array('id'=>'standby_form', 'name'=>'standby_form'));
}
?>



<div id='teetime_table'>
	<div id='teetime_people_row_1'>
		<div class='player_info'>
			<input tabindex=1 class='' id='teetime_title' name='teetime_title' placeholder='Last, First name' value='<?php echo $standby_name ?>' style='width:160px, margin-right:5px;'/>
                        <input type='hidden' name='id' value='<?php echo $standby_id ?>'/>
			<input type='hidden' id='person_id' name='person_id' value=''/>
	   		<input tabindex=2 type=text value='<?php echo $standby_email;?>' id='email' name='email' placeholder='Email' class='ttemail'/>
	   		<input  tabindex=3 type=text value='<?php echo $standby_phone; ?>' id='phone' name='phone' placeholder='888-888-8888' class='ttphone'/>
	 	</div>
	 	<span>
                    <?php 
                            
                            if ($standby_holes == '')
                            {
                                $standby_holes = 18;
                            }
                               ?>
		 	<span class='flag_icon'></span>
		 	<span class='holes_buttonset'>
		        <input type='radio' id='teetime_holes_9' name='teetime_holes' value='9' <?php echo ($standby_holes == 9?'checked':'');?>/>
		        <label for='teetime_holes_9' id='teetime_holes_9_label'>9</label>
		        <input type='radio' id='teetime_holes_18' name='teetime_holes' value='18' <?php echo ($standby_holes == 18?'checked':'');?>/>
		        <label for='teetime_holes_18' id='teetime_holes_18_label'>18</label>
		    </span>
	    </span>
	    <input type='checkbox' id='save_customer_1' name='save_customer_1' style='display:none'/>
		<!--label for='save_customer_1' title='Save to Customers'>
			<span class='checkbox_image' id='save_customer_1_image'></span>
    	</label>
   		<a class='expand_players down' href='#' title='Expand'>Expand</a-->
   		<div class='clear'></div>
   	</div>
   	
    <div id='teetimes_row_2' >
        <div id='players' class='player_buttons'>
        	<span class='players_icon'></span>
	 		<span class='players_buttonset'>
                            <?php 
                            log_message('error', 'standby_players_to_start ' . $standby_players);
                            if ($standby_players == '')
                            {
                                $standby_players = 1;
                            }
                               ?>
    			<input type='radio' id='players_1' name='players' value='1' <?php echo ($standby_players == 1?'checked':'');?>/>
    			<label for='players_1' id='players_1_label'>1</label>
    			<input type='radio' id='players_2' name='players' value='2'<?php echo ($standby_players == 2?'checked':'');?>/>
    			<label for='players_2' id='players_2_label'>2</label>
    			<input type='radio' id='players_3' name='players' value='3'<?php echo ($standby_players == 3?'checked':'');?>/>
    			<label for='players_3' id='players_3_label'>3</label>
    			<input type='radio' id='players_4' name='players' value='4'<?php echo ($standby_players == 4?'checked':'');?>/>
    			<label for='players_4' id='players_4_label'>4</label>
    			<input type='radio' id='players_5' name='players' value='5'<?php echo ($standby_players == 5?'checked':'');?>/>
    			<label for='players_5' id='players_5_label'>5</label>
                       
                        </span>
		</div>
	    
		<?php 
                
                //log_message('error', 'teetimes ' . $teetimes);
                echo form_dropdown('standby_time', $teetimes, date('Hi', strtotime($my_standby_time)));?>
		
	</div>
	
    <div id='teetimes_row_3'>
    	<div id='teetime_people_table_holder'>
    		<table id='teetime_people_table' style='display:none'>
    			<tbody>
    				<tr  id='teetime_people_row_label'>
    					<td colspan='4'>Additional players</td>
    				</tr>
    				<?php for ($i = 2; $i <= 5; $i++) { ?>
    				<tr id='teetime_people_row_<? echo $i?>' >
    					<td>
    						<span class='clear_data' onclick='clear_player_data(<? echo $i?>)'>x</span>
    					</td>
			        	<td>
			        		<input tabindex=<? echo (($i-1)*3)+1?> class='' placeholder='Last, First name' id='teetime_title_<? echo $i?>' name='teetime_title_<? echo $i?>' value='<?php $person_name = 'person_name_'.$i; echo $standby_name; ?>' style='width:160px, margin-right:5px;'/>
			        		<input type='hidden' id='person_id_<? echo $i?>' name='person_id_<? echo $i?>' value='<?php $person_id = 'person_id_'.$i; echo $reservation_info->$person_id; ?>'/>
			        	</td>
			            <td>
			        		<input tabindex=<? echo (($i-1)*3)+2?> type=text placeholder='Email' value='<?php echo $customer_info[$reservation_info->$person_id]->email; ?>' id='email_<? echo $i?>' name='email_<? echo $i?>' class='ttemail'/>
			        	</td>
			        	<td>
			        		<input  tabindex=<? echo (($i-1)*3)+3?> type=text placeholder='888-888-8888' value='<?php echo $customer_info[$reservation_info->$person_id]->phone_number; ?>' id='phone_<? echo $i?>' name='phone_<? echo $i?>' class='ttphone'/>
			        		<input type='checkbox' id='save_customer_<? echo $i?>' name='save_customer_<? echo $i?>' style='display:none'/>
			        	</td>
			        	<!--td>
			        		<label for='save_customer_<? echo $i?>'>
			        			<span class='checkbox_image' id='save_customer_<? echo $i?>_image'></span>
				        		<input type='checkbox' id='save_customer_<? echo $i?>' name='save_customer_<? echo $i?>' style='display:none'/>
			        		</label>
			        	</td-->
			        </tr>
			        <?php } ?>
    			</tbody>
    		</table>
   		</div>
	</div>
	
    <div>
    	 <textarea tabindex=20 class='' placeholder='Details'   id='details' name='standby_details' style='width:94%;height:50px;'><?php echo $standby_details; ?></textarea>
    </div>
   <div class='event_buttons_holder'>
       <?php if (!$no_delete)
       {
   		echo "<button id='teetime_delete' class='deletebutton' style='float:left;'>Delete</button>";
       }?>
                        <span class='saveButtons'>
        <?php if ($this->config->item('sales')) {?>
        	
	 		<!--button id='teetime_purchase' disabled>Purchase</button-->
        	
        	<!--button id='purchase_1'>1</button><button id='purchase_2'>2</button><button id='purchase_3'>3</button><button id='purchase_4'>4</button><button id='purchase_5'>5</button-->
        <?php } else { ?>
        	<button id='teetime_checkin'><? echo $checkin_text; ?></button>
        	<input type='hidden' id='checkin' name='checkin' value='0'/>
        <?php } ?>
        </span>
        <button id='teetime_save' style='float:right;'>Save</button>
    	
        <div class='clear'></div>
    </div>

<?php form_close(); ?>
</div>
<script type='text/javascript'>
    
function clear_player_data(row) {
	if (row == 1)
	{
		$('#teetime_title').val('');
		$('#person_id').val('');
		$('#email').val('');
		$('#phone').val('');
	}
	else
	{
		$('#teetime_title_'+row).val('');
		$('#person_id_'+row).val('');
		$('#email_'+row).val('');
		$('#phone_'+row).val('');
	}
}
function focus_on_title(title) {
	<?php if ($reservation_info->type == 'teetime' || $reservation_info->type == '')
				$focus = 'teetime_title';
		  else if ($reservation_info->type == 'closed')
		  		$focus = 'closed_title';
		  else
				$focus = 'event_title';
		  ?>
		  title = (title == undefined)?'<?php echo $focus;?>':title;
		  //console.log('trying to focus on title: '+title);
	
    $('#'+title).focus().select();
}
$(document).ready(function(){
        
	$("#phone").mask2("(999) 999-9999");
	$("#phone_2").mask2("(999) 999-9999");
	$("#phone_3").mask2("(999) 999-9999");
	$("#phone_4").mask2("(999) 999-9999");
	$("#phone_5").mask2("(999) 999-9999");
		
	var submitting = false;
	$('#standby_form').bind('keypress', function(e) {
            
        //console.log('e is: ' + e.keyCode + '+++++++++++++++++++++++++++++++');
    	var code = (e.keyCode ? e.keyCode : e.which);
		 if(code == 13) { //Enter keycode
		   //Do something
		   e.preventDefault();
		   $('#teetime_save').click();
		 }
                 
    });
    $('#standby_form').validate({
		submitHandler:function(form)
		{
			
			var proceed_with_save = true;
                       
			if ($('#teetime_delete').val() == 1)
			{        
                            
                                                            
				var proceed_with_delete = confirm('Are you sure you want to delete this standby entry?');
                                proceed_with_save = false;
                                if (proceed_with_delete)
                                {
                                    console.log('------delete standby------');
                                    
                                    var input = $("<input>").attr("type", "hidden").attr("name", "delete").val("true");
                                    $(form).append(input);
                                    $(form).mask('<?php echo lang('common_wait'); ?>');
                                    //console.log( 'about to submit form');
                                    $(form).ajaxSubmit({
					success:function(response)
					{		
                                            var standby_html = '';
						console.dir(response);
						//Calendar_actions.update_teesheet(response.teesheets);		                
                                                $.each(response, function(data){
                                                    standby_html += "<div class='standby_entry' id='";
                                    standby_html += response[data].standby_id + 
                                            "'><div class='teetime_result' id='" + 
                                            response[data].standby_id + "'><div>" +
                                            response[data].name + ' </br>' + 
                                            response[data].time + '</br> ' + 
                                            response[data].holes + ' Holes ' +
                                            
                                            response[data].players + ' Players' + "</div> </div>";
                                    
                                         standby_html += '</div>';
                                                });
		            	        //currently_editing = '';
                                                
                                                $('#teetime_populate_standby_list').html(standby_html);
                                                $('.standby_entry').draggable({zIndex:1000, containment:'#content_area_wrapper', 
                                    helper:'clone', appendTo:'body'
                              });
                                                $.colorbox.close();
                                        },
					dataType:'json'
                                    });
                                }
			}
			
			if (proceed_with_save)
			{
                            
                            //console.log("I am proceeding with save");
				if (submitting) return;
				submitting = true;
                                var input = $("<input>").attr("type", "hidden").attr("name", "delete").val("false");
                                    $(form).append(input);
				$(form).mask('<?php echo lang('common_wait'); ?>');
				//console.log( 'about to submit form');
				$(form).ajaxSubmit({
					success:function(response)
					{
						
						console.dir(response);
						//Calendar_actions.update_teesheet(response.teesheets);
		                
                                                var standby_html = '';
						console.dir(response);
						//Calendar_actions.update_teesheet(response.teesheets);		                
                                                $.each(response, function(data){
                                                    standby_html += "<div class='standby_entry' id='";
                                    standby_html += response[data].standby_id + 
                                            "'><div class='teetime_result' id='" + 
                                            response[data].standby_id + "'><div>" +
                                            response[data].name + ' </br>' + 
                                            response[data].time + '</br> ' + 
                                            response[data].holes + ' Holes ' +
                                            
                                            response[data].players + ' Players' + "</div> </div>";
                                    
                                         standby_html += '</div>';
                                                });
                                                $('#teetime_populate_standby_list').html(standby_html);
                                                
                                                $('.standby_entry').draggable({zIndex:1000, containment:'#content_area_wrapper', 
                                                    helper:'clone', appendTo:'body'
                                                });
		        		$.colorbox.close();
		            },
					dataType:'json'
				});
			}
			
		},
		errorLabelContainer: '#error_message_box',
 		wrapper: 'li'
	});
	
	$('#teetime_name').focus().select();
	$('#expand_players').click(function(e){
    	var checked = $(e.target).attr('checked');
    	if (checked)
    	{
	    	$('#teetime_people_table').show();
	    }
    	else
    	{
	    	$('#teetime_people_table').hide();
	    }
    	//var x = $('#teetime_form').height();
	    //var y = $('#teetime_form').width();
	
	    $.colorbox.resize();
    }); 
    for (var q = 1; q <= 5; q++) {
		// Save customer event listeners
	    $('#save_customer_'+q).click(function() {
	    	if ($(this).attr('checked'))
	        	$(this).prev().addClass('checked');
	        else
	   	    	$(this).prev().removeClass('checked');
	    });      
	}
	$('#purchase_1').click(function () {$('#purchase_quantity').val(1);});
	$('#purchase_2').click(function () {$('#purchase_quantity').val(2);});
	$('#purchase_3').click(function () {$('#purchase_quantity').val(3);});
	$('#purchase_4').click(function () {$('#purchase_quantity').val(4);});
	$('#purchase_5').click(function () {$('#purchase_quantity').val(5);});
	$('#teetime_checkin').click(function() {$('#checkin').val(1);});
	$('#teetime_delete').click(function(){$('#teetime_delete').val(1);});
	$('#event_delete').click(function(){$('#delete_teetime').val(1);});
	$('#closed_delete').click(function(){$('#delete_teetime').val(1);});
	// Button icons and styles
	$('#paid_carts').button();
	$('#paid_players').button();
	$('#teetime_save').button();
    $('#event_save').button();
    $('#closed_save').button();
    $('#teetime_delete').button();
    $('#event_delete').button();
    $('#closed_delete').button();
    $('.purchase_button').parent().buttonset();
    $('#teetime_checkin').button();
    $('#teetime_holes_9').button();
    $('#event_holes_9').button();
    $('#teetime_confirm').button();
    $('#teetime_cancel').button();
    $('.teebuttons').buttonset();
    $('.holes_buttonset').buttonset();
    $('#players_0').button();
    $('#carts_0').button();
    $('.players_buttonset').buttonset();
    $('.carts_buttonset').buttonset();
    $('#players_0_label').removeClass('ui-button-disabled').removeClass('ui-state-disabled');
    $('#teetime_purchase').removeClass('ui-button-disabled').removeClass('ui-state-disabled');
    $('#players_0_label').children().css('color','#e9f4fe');

    // Teetime name settings
	$( '#teetime_title' ).autocomplete({
		source: '<?php echo site_url('teesheets/customer_search/last_name'); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui) {
			event.preventDefault();
			$('#teetime_title').val(ui.item.label);
			$('#email').val(ui.item.email).removeClass('teetime_email');
			$('#phone').val(ui.item.phone_number).removeClass('teetime_phone');
                        
			$('#person_id').val(ui.item.value);
                        console.log('data: ----' + $('#person_id').val());
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#teetime_title').val(ui.item.label);
		}
	});
	
	
	// Phone number events
	/*$('#phone').mask('999-999-9999');
    $('#phone_2').mask('999-999-9999');
    $('#phone_3').mask('999-999-9999');
    $('#phone_4').mask('999-999-9999');
    $('#phone_5').mask('999-999-9999');*/
    $('#phone').keypress(function (e) {
    	$('#save_customer_1').attr('checked', true);
    	$('#save_customer_1').prev().addClass('checked');
    });
    $( '#phone' ).autocomplete({
		source: '<?php echo site_url('teesheets/customer_search/phone_number'); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)	{
			event.preventDefault();
			$('#teetime_title').val(ui.item.name).removeClass('teetime_name');
			$('#email').val(ui.item.email).removeClass('teetime_email');
			$('#phone').val(ui.item.label);
			$('#person_id').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#phone').val(ui.item.label);
		}
		
	});
    
	// Email
	$('#email').keypress(function (e) {
    	$('#save_customer_1').attr('checked', true);
    	$('#save_customer_1').prev().addClass('checked');
    });
    $( '#email' ).autocomplete({
		source: '<?php echo site_url('teesheets/customer_search/email'); ?>',
		delay: 10,
		autoFocus: false,
		minLength: 0,
		select: function(event, ui)	{
			event.preventDefault();
			$('#teetime_title').val(ui.item.name).removeClass('teetime_name');
			$('#phone').val(ui.item.phone_number).removeClass('teetime_phone');
			$('#email').val(ui.item.label);
			$('#person_id').val(ui.item.value);
		},
		focus: function(event, ui) {
			event.preventDefault();
			//$('#email').val(ui.item.label);
		}
		
	});
  

    //Calendar_actions.event.make_styled_buttons();
    
    
	// CODE BELOW REQUIRES REVIEW
	
    $('#type_teetime').click(function(){
    	$('#teetime_table').show();
    	$('#closed_table').hide();
    	$('#event_table').hide();
    	$.colorbox.resize();
    	focus_on_title('teetime_title');
    });
    $('#type_tournament').click(function(){
    	$('#teetime_table').hide();
    	$('#closed_table').hide();
    	$('#event_table').show();
    	$.colorbox.resize();
    	focus_on_title('event_title');
    });
    $('#type_league').click(function(){
    	$('#teetime_table').hide();
    	$('#closed_table').hide();
    	$('#event_table').show();
    	$.colorbox.resize();
    	focus_on_title('event_title');
    });
    $('#type_event').click(function(){
    	$('#teetime_table').hide();
    	$('#closed_table').hide();
    	$('#event_table').show();
    	$.colorbox.resize();
    	focus_on_title('event_title');
    });
    $('#type_closed').click(function(){
    	$('#teetime_table').hide();
    	$('#closed_table').show();
    	$('#event_table').hide();
    	$.colorbox.resize();
    	focus_on_title('closed_title');
    });

	$('.teetime_name').each(function(index){
		if ($(this).val() != '')
	        $(this).removeClass('teetime_name');
	});
    $('.teetime_phone').each(function(index){
		if ($(this).val() != '')
	        $(this).removeClass('teetime_phone');
	});
	$('.teetime_email').each(function(index){
		if ($(this).val() != '')
	        $(this).removeClass('teetime_email');
	});
    $('.teetime_details').each(function(index){
		if ($(this).val() != '')
	        $(this).removeClass('teetime_details');
	});
    
      
	
});
</script>