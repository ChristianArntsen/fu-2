<div id='table_number_page'>
	<div id='table_menu_controls'>
		<a class='small_button new_quickbutton fnb_button' id='fnb_logout2' href='javascript:void(0)'> 
			<span id='payment_log_out' class=' payment_button_medium'>Logout</span>
		</a>
	</div>
	<div id='table_number_holder'>
		<?php 
			$i = 1;
		 	while ($i <= 50) {
		 		if (isset($suspended_sales[$i]))
				{
					?>
					
					<a class='table_number already_used' href='javascript:void(0)' onClick='fnb.unsuspend_sale(<?=$suspended_sales[$i]?>, <?=$i?>);'><?=$i?></a>
					<?php
				}	
				else
				{
					?>
					<a class='table_number' href='javascript:void(0)' onClick='fnb.unsuspend_sale(-1, <?=$i?>);'><?=$i?></a>
					<?php	
				}
				 
				$i++;
			} 
		?>
		<div class='clear'></div>
	</div>
	<div class='clear'></div>
</div>
