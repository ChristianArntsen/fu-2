<span class="small_note">*Note - Twilight and super-twilight hours may be set in the hours/booking tab.</span>
        <div>
        	<span class="small_note">Department: </span>
        	<? echo form_input(array(
                'name'=>"teetime_department",
                'id'=>"teetime_department",
                'value'=>(isset($teetime_department[0]) && isset($teetime_department[0]['department']))?$teetime_department[0]['department']:''));
			?> 
       		<span class="small_note">Cart Department: </span>
        	<? echo form_input(array(
                'name'=>"cart_department",
                'id'=>"cart_department",
                'value'=>(isset($cart_department[0]) && isset($cart_department[0]['department']))?$cart_department[0]['department']:''));
			?> 
       		<span class="small_note"> Tax Rate: </span>
        	<? echo form_input(array(
                'name'=>"teetime_tax_rate",
                'id'=>"teetime_tax_rate",
                'value'=>(isset($teetime_tax_rate[0]) && isset($teetime_tax_rate[0]['percent']))?$teetime_tax_rate[0]['percent']:''));
			?> 
       		<span class="small_note"> Cart Tax Rate: </span>
        	<? echo form_input(array(
                'name'=>"cart_tax_rate",
                'id'=>"cart_tax_rate",
                'value'=>(isset($cart_tax_rate[0]) && isset($cart_tax_rate[0]['percent']))?$cart_tax_rate[0]['percent']:''));
			?> 
        </div>
        <select id='price_grid_select'>
        <?php
    	foreach($teesheets->result() as $teesheet)
		{
			echo "<option value='$teesheet->schedule_id'>$teesheet->title</option>";
		}
        ?>
        </select>
        <div id='green_fee_prices'>
        	<?php
	    	foreach($teesheets->result() as $teesheet)
			{
			?>
			<table class='price_grid' id='price_grid_<?php echo $teesheet->schedule_id; ?>'>
	        	<thead>
	        		<tr class="field_row ">
	                    <td></td>
	                    <td>Regular</td>
	                    <td class="price_cell early_bird_col">Early Bird</td>
	                    <td class="price_cell morning_col">Morning</td>
	                    <td class="price_cell afternoon_col">Afternoon</td>
	                    <td class="price_cell twilight_col">Twilight</td>
	                    <td class="price_cell super_twilight_col">Super Twilight</td>
	                    <td class="price_cell holiday_col">Holiday</td>
	                    <?php for($j = 8; $j <= 50; $j++) {?>
		                    <td class="price_cell">
		                       <? 
		                       $price_category = 'price_category_'.$j;
		                       echo form_input(array(
		                           'tabindex'=>$j,
								   'name'=>'price_category_'.$j.'_'.$teesheet->schedule_id,
		                           'id'=>'price_category_'.$j.'_'.$teesheet->schedule_id,
		                           'class'=>'price_label price_class price_header_'.$j,
		                           'value'=>$teetime_type[$teesheet->schedule_id]->$price_category)
		                       );?>
		                    </td>
	                 	<?php }?>
	                </tr>
	            </thead>
	            <tbody>
	                <?php for ($i = 1; $i <=9;$i++) {
	                		if ($i == 5)
								continue;
	                          echo "<tr class='field_row'>";
	                          if (false) {
	                              //echo "<td>18 Holes</td><td></td>";
	                          }
	                          else {
	                              $index = ($i > 5)? $i-1:$i;
	                              for($j = 1; $j <= 51; $j++) {
	                                  $jindex = $j-1;
	                                  if ($j == 1){
	                                      if ($i == 1)
	                                          $label = number_format($teesheet->increment/60,2).' Hours';
										  else if ($i == 2)
										  	  $label = number_format($teesheet->increment*2/60,2).' Hour';
	                                      else if ($i == 3)
	                                          $label = number_format($teesheet->increment*3/60,2).' Hours';
	                                      else if ($i == 4)
	                                          $label = number_format($teesheet->increment*4/60,2).' Hours';
										  else if ($i == 6)
	                                          $label = number_format($teesheet->increment*5/60,2).' Hours';
										  else if ($i == 7)
										  	  $label = number_format($teesheet->increment*6/60,2).' Hour';
	                                      else if ($i == 8)
	                                          $label = number_format($teesheet->increment*7/60,2).' Hours';
	                                      else
	                                          $label = number_format($teesheet->increment*8/60,2).' Hours';
	
	                                      echo "<td class='tee_time_label'>$label</td>";
	                                  }
	                                  else {
	                                      if ($jindex == 2)
	                                          $extra_class = 'early_bird_col';
										  else if ($jindex == 3)
	                                          $extra_class = 'morning_col';
	                                      else if ($jindex == 4)
	                                          $extra_class = 'afternoon_col';
										  else if ($jindex == 5)
	                                          $extra_class = 'twilight_col';
	                                      else if ($jindex == 6)
	                                          $extra_class = 'super_twilight_col';
	                                      else if ($jindex == 7)
	                                          $extra_class = 'holiday';
	                                      else
	                                          $extra_class = '';
	                                      echo "<td class='price_cell $extra_class'>";
										  $price_category = 'price_category_'.$jindex;
	                                      echo form_input(array(
	                                      	'tabindex'=>$jindex,
	                                        'name'=>"{$teesheet->schedule_id}_{$jindex}_{$index}",
	                                        'id'=>"{$teesheet->schedule_id}_{$jindex}_{$index}",
	                                        'class'=>"price_label price_box price_col_{$jindex} price_category_{$jindex}_{$teesheet->schedule_id}",
	                                        'value'=>$teetime_prices["{$teesheet->schedule_id}"]["{$this->session->userdata('course_id')}_{$index}"]->$price_category
										  )); 
	                                      echo "</td>";   
	                                  }
	                              }
	                          }
	                          echo "</tr>";
	                      }
	                ?>
	            </tbody>
	        </table>
	        <?php } ?>
	    </div>
	<script type='text/javascript'>
		$(document).ready(function(){
			console.log('adding callbacks');
			$('.price_grid').hide();
			$('#price_grid_'+$('#price_grid_select').val()).show();
			$('#price_grid_select').change(function(e){
				console.log('selecting');
				$('.price_grid').hide();
				$('#price_grid_'+$('#price_grid_select').val()).show();
			})
		})
	</script>
