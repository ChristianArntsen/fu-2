<!DOCTYPE html>
<html>
<head>
	<title><?php echo lang('login_reset_password'); ?></title>
</head>
<body>
<?php 
if ($success)
	echo lang('login_password_reset_has_been_sent'); 
else 
	echo lang('login_password_reset_no_email');
	?>
</body>
</html>