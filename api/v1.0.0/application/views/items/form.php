<script>
$('#select-image').colorbox2({'maxHeight':750,'width':1100});
$(function(){
	$(document).unbind('changeImage').bind('changeImage', function(event){
		$.post('<?php echo site_url('items/save_image'); ?>/<?php echo $item_info->item_id; ?>', {image_id: event.image_id}, function(response){
			$('#image-info img').attr('src', response.thumb_url);
			$('#select-image').attr('href','<?php echo site_url('upload'); ?>/index/items?crop_ratio=1&image_id=' + event.image_id);
			$.colorbox2.close();
		},'json');
	});

	$('#remove-image').click(function(event){
		var url = $(this).attr('href');

		if(confirm('Remove this image?')){

			$.post(url, {image_id:0}, function(response){
				$('#image-info img').attr('src', response.thumb_url);
				$('#select-image').attr('href','<?php echo site_url('upload'); ?>/index/items?crop_ratio=1&image_id=' + response.image_id);
			},'json');
		}
		return false;
	});
});
</script>
<ul id="error_message_box"></ul>
<?php
echo form_open('items/save/'.$item_info->item_id, array('id'=>'item_form'));
?>
<fieldset id="item_basic_info">
<legend><?php echo lang("items_basic_information"); ?></legend>
<div class='left_side'>
<!-- <div class="field_row clearfix">
	<label>Image</label>
	<div id="image-info">
		<img src="<?php echo $image_thumb_url; ?>" />
		<a id="select-image" title="Modify this image" href="<?php echo site_url('upload'); ?>/index/items?crop_ratio=1&image_id=<?php echo $item_info->image_id; ?>">Edit Image</a>
		<a id="remove-image" title="Disconnect image from item" href="<?php echo site_url('items'); ?>/save_image/<?php echo $item_info->item_id; ?>">Reset Image</a>
	</div>
</div> -->
<div class="field_row clearfix">
<?php echo form_label(lang('items_item_number').':', 'name',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'item_number',
		'id'=>'item_number',
		'value'=>$item_info->item_number)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_name').':<span class="required">*</span>', 'name',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'name',
		'id'=>'name',
		'value'=>$item_info->name)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_department').':<span class="required">*</span>', 'department',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php //echo form_dropdown('category', $this->Item->get_categories(), $item_info->category);?>
	<?php echo form_input(array(
		'name'=>'department',
		'id'=>'department',
		'value'=>$item_info->department)
	);?>

	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_category').':', 'category',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php //echo form_dropdown('category', $this->Item->get_categories(), $item_info->category);?>
	<?php echo form_input(array(
		'name'=>'category',
		'id'=>'category',
		'value'=>$item_info->category)
	);?>

	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_subcategory').':', 'subcategory',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php //echo form_dropdown('category', $this->Item->get_categories(), $item_info->category);?>
	<?php echo form_input(array(
		'name'=>'subcategory',
		'id'=>'subcategory',
		'value'=>$item_info->subcategory)
	);?>

	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_supplier').':<span class="required">*</span>', 'supplier',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_dropdown('supplier_id', $suppliers, $selected_supplier);?>
	</div>
</div>


<!--div class="field_row clearfix">
<?php echo form_label(lang('items_location').':', 'location',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'location',
		'id'=>'location',
		'value'=>$item_info->location)
	);?>
	</div>
</div-->

<!--div class="field_row clearfix">
<?php echo form_label(lang('items_allow_alt_desciption').':', 'allow_alt_description',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_checkbox(array(
		'name'=>'allow_alt_description',
		'id'=>'allow_alt_description',
		'value'=>1,
		'checked'=>($item_info->allow_alt_description)? 1  :0)
	);?>
	</div>
</div-->

<!--div class="field_row clearfix">
<?php echo form_label(lang('items_is_serialized').':', 'is_serialized',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_checkbox(array(
		'name'=>'is_serialized',
		'id'=>'is_serialized',
		'value'=>1,
		'checked'=>($item_info->is_serialized)? 1 : 0)
	);?>
	</div>
</div-->
<div class="field_row clearfix">
<?php echo form_label(lang('items_is_giftcard').':', 'is_giftcard',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_checkbox(array(
		'name'=>'is_giftcard',
		'id'=>'is_giftcard',
		'value'=>1,
		'checked'=>($item_info->is_giftcard)? 1 : 0)
	);?>
	</div>
</div>

</div>
<div class='right_side'>
<div class="field_row clearfix">
<?php echo form_label(lang('items_cost_price').':<span class="required">*</span>', 'cost_price',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'cost_price',
		'size'=>'8',
		'id'=>'cost_price',
		'value'=>$item_info->cost_price)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_unit_price').':<span class="required">*</span>', 'unit_price',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'unit_price',
		'size'=>'8',
		'id'=>'unit_price',
		'value'=>$item_info->unit_price)
	);?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_max_discount').':', 'max_discount',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'max_discount',
		'size'=>'8',
		'id'=>'max_discount',
		'value'=>$item_info->max_discount?$item_info->max_discount:100)
	);?>
	%
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_tax_1').':', 'tax_percent_1',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'tax_names[]',
		'id'=>'tax_name_1',
		'size'=>'8',
		'value'=> isset($item_tax_info[0]['name']) ? $item_tax_info[0]['name'] : $this->config->item('default_tax_1_name'))
	);?>
	<?php echo form_input(array(
		'name'=>'tax_percents[]',
		'id'=>'tax_percent_name_1',
		'size'=>'3',
		'value'=> isset($item_tax_info[0]['percent']) ? $item_tax_info[0]['percent'] : $default_tax_1_rate)
	);?>
	%
	<?php echo form_hidden('tax_cumulatives[]', '0'); ?>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_tax_2').':', 'tax_percent_2',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'tax_names[]',
		'id'=>'tax_name_2',
		'size'=>'8',
		'value'=> isset($item_tax_info[1]['name']) ? $item_tax_info[1]['name'] : $this->config->item('default_tax_2_name'))
	);?>
	<?php echo form_input(array(
		'name'=>'tax_percents[]',
		'id'=>'tax_percent_name_2',
		'size'=>'3',
		'value'=> isset($item_tax_info[1]['percent']) ? $item_tax_info[1]['percent'] : $default_tax_2_rate)
	);?>
	%
	<?php echo form_checkbox('tax_cumulatives[]', '1', isset($item_tax_info[1]['cumulative']) && $item_tax_info[1]['cumulative'] ? (boolean)$item_tax_info[1]['cumulative'] : (boolean)$default_tax_2_cumulative); ?>
    <span class="cumulative_label">
	<?php echo lang('common_cumulative'); ?>
    </span>
	</div>
</div>
<div id='additional_taxes' style='display:none; overflow: hidden; display: block; width: 450px;'>	
	<div class="field_row clearfix">
	<?php echo form_label(lang('items_tax_3').':', 'tax_percent_3',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'tax_names[]',
			'id'=>'tax_name_3',
			'size'=>'8',
			'value'=> isset($item_tax_info[2]['name']) ? $item_tax_info[2]['name'] : $this->config->item('default_tax_3_name'))
		);?>
		<?php echo form_input(array(
			'name'=>'tax_percents[]',
			'id'=>'tax_percent_name_3',
			'size'=>'3',
			'value'=> isset($item_tax_info[2]['percent']) ? $item_tax_info[2]['percent'] : $default_tax_3_rate)
		);?>
		%
		<?php echo form_hidden('tax_cumulatives[]', '0'); ?>
		</div>
	</div>
	<div class="field_row clearfix">
	<?php echo form_label(lang('items_tax_4').':', 'tax_percent_4',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'tax_names[]',
			'id'=>'tax_name_4',
			'size'=>'8',
			'value'=> isset($item_tax_info[3]['name']) ? $item_tax_info[3]['name'] : $this->config->item('default_tax_4_name'))
		);?>
		<?php echo form_input(array(
			'name'=>'tax_percents[]',
			'id'=>'tax_percent_name_4',
			'size'=>'3',
			'value'=> isset($item_tax_info[3]['percent']) ? $item_tax_info[3]['percent'] : $default_tax_4_rate)
		);?>
		%
		<?php echo form_hidden('tax_cumulatives[]', '0'); ?>
		</div>
	</div>
	
	<div class="field_row clearfix">
	<?php echo form_label(lang('items_tax_5').':', 'tax_percent_5',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'tax_names[]',
			'id'=>'tax_name_5',
			'size'=>'8',
			'value'=> isset($item_tax_info[4]['name']) ? $item_tax_info[4]['name'] : $this->config->item('default_tax_5_name'))
		);?>
		<?php echo form_input(array(
			'name'=>'tax_percents[]',
			'id'=>'tax_percent_name_5',
			'size'=>'3',
			'value'=> isset($item_tax_info[4]['percent']) ? $item_tax_info[4]['percent'] : $default_tax_5_rate)
		);?>
		%
		<?php echo form_hidden('tax_cumulatives[]', '0'); ?>
		</div>
	</div>
	
	<div class="field_row clearfix">
	<?php echo form_label(lang('items_tax_6').':', 'tax_percent_6',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array(
			'name'=>'tax_names[]',
			'id'=>'tax_name_6',
			'size'=>'8',
			'value'=> isset($item_tax_info[5]['name']) ? $item_tax_info[5]['name'] : $this->config->item('default_tax_6_name'))
		);?>
		<?php echo form_input(array(
			'name'=>'tax_percents[]',
			'id'=>'tax_percent_name_6',
			'size'=>'3',
			'value'=> isset($item_tax_info[5]['percent']) ? $item_tax_info[5]['percent'] : $default_tax_6_rate)
		);?>
		%
		<?php echo form_hidden('tax_cumulatives[]', '0'); ?>
		</div>
	</div>
</div>
<script>
	$('#additional_taxes').expandable({
		title : 'Additional Taxes'
	});	
</script>

<div class="field_row clearfix">
<?php echo form_label(lang('items_quantity').':<span class="required">*</span>', 'quantity',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php
		$disabled = ($item_info->is_unlimited)?'disabled':'';
	echo form_input(array(
		'name'=>'quantity',
		'id'=>'quantity',
		'size'=>'8',
		'value'=>($item_info->is_unlimited)?0:$item_info->quantity,
		$disabled=>$disabled
		)
	);?>
	<?php echo form_checkbox(array(
		'name'=>'is_unlimited',
		'id'=>'is_unlimited',
		'value'=>1,
		'checked'=>($item_info->is_unlimited)? 1 : 0)
	);?>
	<span class="cumulative_label">
	<?php echo lang('items_is_unlimited');?>
	</span>
	</div>
</div>

<div class="field_row clearfix">
<?php echo form_label(lang('items_reorder_level').':<span class="required">*</span>', 'reorder_level',array('class'=>' wide')); ?>
	<div class='form_field'>
	<?php echo form_input(array(
		'name'=>'reorder_level',
		'id'=>'reorder_level',
		'size'=>'8',
		'value'=>($item_info->is_unlimited)?0:$item_info->reorder_level,
		$disabled=>$disabled
		)
	);?>
	</div>
</div>

</div>
<div class='clear'></div>
<div class="field_row clearfix">
<?php echo form_label(lang('items_description').':', 'description',array('class'=>'wide')); ?>
	<div class='form_field'>
	<?php echo form_textarea(array(
		'name'=>'description',
		'id'=>'description',
		'value'=>$item_info->description,
		'rows'=>'3',
		'cols'=>'17')
	);
        ?>
        <input type="hidden" name="food_and_beverage" id='food_and_beverage' value='<?php echo $food_and_beverage; ?>'/>
	</div>
</div>
<style>
#modifiers_container .ui-widget-content {
	padding: 10px;
}

#modifiers_container th {
	text-align: left;
	padding: 5px 0px;
}

#modifiers_container td {
	padding: 2px 0px 2px 0px;
}

#modifiers_container .submit_button {
	margin: 5px 5px 0px 0px;
	height: 20px;
	line-height: 20px;
	color: white;
	font-size: 12px;
}

input.modifier_auto_select {
	display: block;
	margin: 0 auto;
}
#image-info img{
	border-radius:300px;
}
</style>
<div id='modifiers_container' style='display:none; overflow: hidden; display: block; width: 900px;'>
	<label>Add New </label>
	<?php echo form_input(array('name' => "modifier_search", 'value' => '', 'style' => 'width: 345px;', 'class'=>'modifier_search', 'data-placeholder'=>'Search modifiers...')); ?>
	<table id="modifiers">
		<thead>
			<tr>
				<th>Name</th>
				<th>Options</th>
				<th>Price</th>
				<th>Auto Select</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			<?php if(!empty($modifiers)){ ?>
			<?php foreach($modifiers as $key => $modifier){ ?>
			<tr>
				<td>
					<input type="hidden" name="modifiers[<?php echo $modifier['modifier_id']; ?>][modifier_id]" value="<?php echo $modifier['modifier_id']; ?>" class="modifier_id" />
					<div class='form_field'>
						<?php echo form_input(array('name' => "modifiers[{$modifier['modifier_id']}][name]", 'value' => $modifier['name'], 'style' => 'width: 300px;', 'class'=>'modifier_name', 'disabled'=>false));?>
					</div>
				</td>
				<td>
					<div class='form_field'>
						<?php echo form_input(array('name' => "modifiers[{$modifier['modifier_id']}][options]", 'value' => implode(',', $modifier['options']), 'style' => 'width: 380px;', 'class'=>'modifier_options', 'disabled'=>false));?>
					</div>
				</td>
				<td>
					<div class='form_field'>
						<?php echo form_input(array('name' => "modifiers[{$modifier['modifier_id']}][override_price]", 'value' => $modifier['override_price'], 'style' => 'width: 75px;', 'class'=>'modifier_default_price'));?>
					</div>
				</td>
				<td>
					<div class='form_field'>
						<?php
						if($modifier['auto_select'] == 1){
							$checked = true;
						}else{
							$checked = false;
						}
						echo form_checkbox(array('name' => "modifiers[{$modifier['modifier_id']}][auto_select]", 'value' => 1, 'class'=>'modifier_auto_select', 'checked'=>$checked)); ?>
					</div>
				</td>
				<td>
					<a href="#" class="delete_modifier" style="color: white;">X</a>
				</td>
			</tr>
			<?php } }else{ ?>

			<?php } ?>
		</tbody>
	</table>
</div>
<?php
echo form_submit(array(
	'name'=>'submit',
	'id'=>'submit',
	'value'=>lang('common_save'),
	'class'=>'submit_button float_right')
);
?>
</fieldset>
<?php
echo form_close();
?>
<script type='text/javascript'>
function add_modifier(modifierId, modifierName){
	var rowCount = $('#modifiers tr').length - 1;
	var newRow = $('#modifier-template').clone();

	newRow.find('input.modifier_id').attr('name', 'modifiers['+rowCount+'][modifier_id]').val('');
	newRow.find('input.modifier_name').attr('name', 'modifiers['+rowCount+'][name]').val(modifierName);
	newRow.find('input.modifier_default_price').attr('name', 'modifiers['+rowCount+'][default_price]').val('');
	newRow.find('input.modifier_options').attr('name', 'modifiers['+rowCount+'][options]').val('');
	newRow.show();
	newRow.attr('id', modifierId);

	$('#modifiers').append(newRow);
}
<?php if(!empty($item_info->item_id )){ ?>
var itemId = <?php echo $item_info->item_id; ?>;
<?php }else{ ?>
var itemId = 0;
<?php } ?>
//validation and submit handling
$(document).ready(function()
{
	$('#modifiers_container').expandable({
		title : 'Modifiers'
	});

	$('a.delete_modifier').live('click', function(e){
		var row = $(this).parents('tr');
		var modifierId = $(this).parents('tr').find('input.modifier_id').val();
		var url = '<?php echo site_url('modifiers/delete_item'); ?>' + '/' + itemId + '/' + modifierId;

		$.post(url, null, function(response){
			if(response.success){
				row.remove();
			}
		},'json');

		return false;
	});

	$("input.modifier_search").autocomplete({
 		source: '<?php echo site_url('modifiers/search'); ?>',
		delay: 10,
 		autoFocus: false,
 		minLength: 0,
 		select: function( event, ui )
 		{
			$.post('<?php echo site_url('modifiers/save_item'); ?>/' + itemId + '/' + ui.item.value, null, function(response){
				if(response){
					$('#modifiers').append(response);
				}
			});
			$("input.modifier_search").val('');
			return false;
 		}
	});

	$('#is_unlimited').bind('change',function(){
		if($('#is_unlimited').attr('checked')) {
			$('#quantity').val('0');
			$('#quantity').attr('disabled',true);
			$('#reorder_level').val('0');
			$('#reorder_level').attr('disabled',true);
		}
		else {
			$('#quantity').val('<?php echo $item_info->quantity?>');
			$('#quantity').attr('disabled',false);
			$('#reorder_level').val('<?php echo $item_info->reorder_level?>');
			$('#reorder_level').attr('disabled',false);

		}
	});
	var dept = $( "#department" );
	dept.autocomplete({
		source: "<?php echo site_url('items/suggest_department');?>",
		delay: 10,
		autoFocus: false,
		minLength: 0
	});
	dept.click(function(){dept.autocomplete('search','')});

	var cat = $( "#category" );
	cat.autocomplete({
		source: "<?php echo site_url('items/suggest_category');?>",
		delay: 10,
		autoFocus: false,
		minLength: 0
	});
	cat.click(function(){cat.autocomplete('search','')});

	var subcat = $( "#subcategory" );
	subcat.autocomplete({
		source: "<?php echo site_url('items/suggest_subcategory');?>",
		delay: 10,
		autoFocus: false,
		minLength: 0
	});
	subcat.click(function(){subcat.autocomplete('search','')});

	var submitting = false;
    $('#item_form').validate({
		submitHandler:function(form)
		{
			if (submitting) return;
			submitting = true;
			$('#food_and_beverage').val($('#restaurant').attr('checked') ? 1 : 0);// IF $('#restautant') is checked, then we need to mark this item as food_and_beverage
			$(form).mask("<?php echo lang('common_wait'); ?>");
			$(form).ajaxSubmit({
			success:function(response)
			{
				post_item_form_submit(response);
                submitting = false;
				if (response.success)
					$.colorbox.close();
				else
					$(form).unmask();
			},
			dataType:'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules:
		{
			name:"required",
			department:"required",
			cost_price:
			{
				required:true,
				isFloat:true
			},

			unit_price:
			{
				required:true,
				isFloat:true
			},
			tax_percent:
			{
				required:true,
				number:true
			},
			quantity:
			{
				required:true,
				number:true
			},
			reorder_level:
			{
				required:true,
				number:true
			}
   		},
		messages:
		{
			name:"<?php echo lang('items_name_required'); ?>",
			department:"<?php echo lang('items_department_required'); ?>",
			cost_price:
			{
				required:"<?php echo lang('items_cost_price_required'); ?>",
				number:"<?php echo lang('items_cost_price_number'); ?>"
			},
			unit_price:
			{
				required:"<?php echo lang('items_unit_price_required'); ?>",
				number:"<?php echo lang('items_unit_price_number'); ?>"
			},
			tax_percent:
			{
				required:"<?php echo lang('items_tax_percent_required'); ?>",
				number:"<?php echo lang('items_tax_percent_number'); ?>"
			},
			quantity:
			{
				required:"<?php echo lang('items_quantity_required'); ?>",
				number:"<?php echo lang('items_quantity_number'); ?>"
			},
			reorder_level:
			{
				required:"<?php echo lang('items_reorder_level_required'); ?>",
				number:"<?php echo lang('items_reorder_level_number'); ?>"
			}

		}
	});
});
</script>