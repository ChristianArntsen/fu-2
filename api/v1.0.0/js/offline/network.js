$(document).ready(function () {//debugger;
    $(document.body).bind("online", network.check_status);
    $(document.body).bind("offline", newtork.check_status);
    network.check_status();
});

var network = {
	online : true,
	check_status : function () {
		if (navigator.onLine) {
            // Just because the browser says we're online doesn't mean we're online. The browser lies.
            // Check to see if we are really online by making a call for a static JSON resource on
            // the originating Web site. If we can get to it, we're online. If not, assume we're
            // offline.
            $.ajaxSetup({
                async: true,
                cache: false,
                context: $("#status"),
                dataType: "json",
                error: function (req, status, ex) {
                    console.log("Error: " + ex);
                    // We might not be technically "offline" if the error is not a timeout, but
                    // otherwise we're getting some sort of error when we shouldn't, so we're
                    // going to treat it as if we're offline.
                    // Note: This might not be totally correct if the error is because the
                    // manifest is ill-formed.
                    network.show_status(false);
                },
                success: function (data, status, req) {
                    network.show_status(true);
                },
                timeout: 5000,
                type: "GET",
                url: "/js/ping.js"
            });
            $.ajax();
        }
        else {
            newtork.show_status(false);
        }
	},
	show_status : function (online) {
		if (online) {
            $("#online_status").html("Online");
        }
        else {
            $("#online_status").html("Offline");
        }
    
        console.log("Online status: " + online);
	}
};   
