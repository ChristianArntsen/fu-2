<?php
	// Date 12/14/11
	// SQL
	ALTER TABLE  `foreup_courses` ADD  `mercury_id` VARCHAR( 255 ) NOT NULL AFTER  `at_test` ,
		ADD  `mercury_password` VARCHAR( 255 ) NOT NULL AFTER  `mercury_id`

	// Date 12/15/11
	ALTER TABLE  `foreup_customers` ADD  `account_balance` DECIMAL( 15, 2 ) NOT NULL AFTER  `account_number`

	// Date 12/16/11
	ALTER TABLE  `foreup_sales_payments` ADD  `payment_id` VARCHAR( 255 ) NOT NULL

	// Date 12/27/11
	ALTER TABLE  `foreup_receivings` ADD  `shipping_cost` DECIMAL( 15, 2 ) NOT NULL AFTER  `payment_type` ,
		ADD  `invoice_number` VARCHAR( 255 ) NOT NULL AFTER  `shipping_cost`

	// Date 12/29/11
	UPDATE foreup_items SET category = 'Green Fees' WHERE category = 'Tee Times'

	// Date 12/29/11
	CREATE TABLE `foreup_sales_payments_credit_cards` (
	  `tran_type` varchar(255) NOT NULL,
	  `amount` float(15,2) NOT NULL,
	  `auth_amount` float(15,2) NOT NULL,
	  `card_type` varchar(255) NOT NULL,
	  `masked_account` varchar(255) NOT NULL,
	  `cardholder_name` varchar(255) NOT NULL,
	  `ref_no` varchar(255) NOT NULL,
	  `invoice` int(11) NOT NULL AUTO_INCREMENT,
	  `operator_id` varchar(255) NOT NULL,
	  `terminal_name` varchar(255) NOT NULL,
	  `trans_post_time` datetime NOT NULL,
	  `auth_code` varchar(255) NOT NULL,
	  `voice_auth_code` varchar(255) NOT NULL,
	  `payment_id` varchar(255) NOT NULL,
	  `acq_ref_data` varchar(255) NOT NULL,
	  `process_data` varchar(255) NOT NULL,
	  `token` varchar(255) NOT NULL,
	  `frequency` varchar(9) NOT NULL,
	  `response_code` smallint(6) NOT NULL,
	  `status` varchar(30) NOT NULL,
	  `status_message` varchar(30) NOT NULL,
	  `display_message` varchar(255) NOT NULL,
	  `avs_result` varchar(20) NOT NULL,
	  `cvv_result` varchar(20) NOT NULL,
	  `tax_amount` float(15,2) NOT NULL,
	  `avs_address` varchar(255) NOT NULL,
	  `avs_zip` varchar(20) NOT NULL,
	  `payment_id_expired` varchar(10) NOT NULL,
	  `customer_code` varchar(255) NOT NULL,
	  `memo` varchar(255) NOT NULL,
	  `batch_no` varchar(6) NOT NULL,
	  `gratuity_amount` float(15,2) NOT NULL,
	  `voided` int(1) NOT NULL,
	  PRIMARY KEY (`invoice`)
	) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=191 ;

	// Date 12/30/11
	ALTER TABLE `foreup_courses`
		ADD `price_category_9` VARCHAR(255) NOT NULL AFTER `price_category_8`,
		ADD `price_category_10` VARCHAR(255) NOT NULL AFTER `price_category_9`,
		ADD `price_category_11` VARCHAR(255) NOT NULL AFTER `price_category_10`,
		ADD `price_category_12` VARCHAR(255) NOT NULL AFTER `price_category_11`,
		ADD `price_category_13` VARCHAR(255) NOT NULL AFTER `price_category_12`,
		ADD `price_category_14` VARCHAR(255) NOT NULL AFTER `price_category_13`,
		ADD `price_category_15` VARCHAR(255) NOT NULL AFTER `price_category_14`,
		ADD `price_category_16` VARCHAR(255) NOT NULL AFTER `price_category_15`,
		ADD `price_category_17` VARCHAR(255) NOT NULL AFTER `price_category_16`,
		ADD `price_category_18` VARCHAR(255) NOT NULL AFTER `price_category_17`,
		ADD `price_category_19` VARCHAR(255) NOT NULL AFTER `price_category_18`,
		ADD `price_category_20` VARCHAR(255) NOT NULL AFTER `price_category_19`,
		ADD `price_category_21` VARCHAR(255) NOT NULL AFTER `price_category_20`,
		ADD `price_category_22` VARCHAR(255) NOT NULL AFTER `price_category_21`,
		ADD `price_category_23` VARCHAR(255) NOT NULL AFTER `price_category_22`,
		ADD `price_category_24` VARCHAR(255) NOT NULL AFTER `price_category_23`,
		ADD `price_category_25` VARCHAR(255) NOT NULL AFTER `price_category_24`,
		ADD `price_category_26` VARCHAR(255) NOT NULL AFTER `price_category_25`,
		ADD `price_category_27` VARCHAR(255) NOT NULL AFTER `price_category_26`,
		ADD `price_category_28` VARCHAR(255) NOT NULL AFTER `price_category_27`,
		ADD `price_category_29` VARCHAR(255) NOT NULL AFTER `price_category_28`,
		ADD `price_category_30` VARCHAR(255) NOT NULL AFTER `price_category_29`

	// Date 1/3/12
	ALTER TABLE  `foreup_courses` ADD  `receipt_printer` VARCHAR( 255 ) NOT NULL AFTER  `print_after_sale`;
	ALTER TABLE  `foreup_sales_payments` CHANGE  `payment_id`  `invoice_id` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

	// Date 1/4/12
	CREATE TABLE `foreup_account_transactions` (
	  `CID` int(11) NOT NULL,
	  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
	  `trans_customer` int(11) NOT NULL DEFAULT '0',
	  `trans_user` int(11) NOT NULL DEFAULT '0',
	  `trans_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  `trans_comment` text COLLATE utf8_unicode_ci NOT NULL,
	  `trans_description` text COLLATE utf8_unicode_ci NOT NULL,
	  `trans_amount` decimal(15,2) NOT NULL DEFAULT '0.00',
	  PRIMARY KEY (`trans_id`),
	  KEY `foreup_account_transactions_ibfk_1` (`trans_customer`),
	  KEY `foreup_account_transactions_ibfk_2` (`trans_user`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=357 ;

	// Date 1/10/12 revised 1/25/12 - i thought we'd have teesheets and teetimes, but they need to be combined, and teesheets makes more sense,
	// 		so disregard the following SQL changes
/*	INSERT INTO  `ejacketa_pos`.`foreup_modules` (
		`name_lang_key` ,
		`desc_lang_key` ,
		`sort` ,
		`module_id`
		)
		VALUES (
		'module_teetimes',  'module_teetimes_desc',  '0',  'teetimes'
		);

	UPDATE foreup_permissions SET module_id =  'teetimes' WHERE module_id =  'teesheets'

	ALTER TABLE  `foreup_courses` CHANGE  `teesheets`  `teetimes` INT( 1 ) NOT NULL DEFAULT  '1'
*/
	// Date 1/12/12
	ALTER TABLE  `foreup_teetime` ADD  `person_name` VARCHAR( 255 ) NOT NULL AFTER  `person_id` ,
		ADD  `person_id_2` INT NOT NULL AFTER  `person_name` ,
		ADD  `person_name_2` VARCHAR( 255 ) NOT NULL AFTER  `person_id_2` ,
		ADD  `person_id_3` INT NOT NULL AFTER  `person_name_2` ,
		ADD  `person_name_3` VARCHAR( 255 ) NOT NULL AFTER  `person_id_3` ,
		ADD  `person_id_4` INT NOT NULL AFTER  `person_name_3` ,
		ADD  `person_name_4` VARCHAR( 255 ) NOT NULL AFTER  `person_id_4` ,
		ADD  `person_id_5` INT NOT NULL AFTER  `person_name_4` ,
		ADD  `person_name_5` VARCHAR( 255 ) NOT NULL AFTER  `person_id_5`

	// Date 1/13/12
	ALTER TABLE  `foreup_sales_payments_credit_cards` ADD  `CID` INT( 11 ) NOT NULL FIRST ,
		ADD  `mercury_id` VARCHAR( 255 ) NOT NULL AFTER  `CID`

	// Date 1/18/12
	ALTER TABLE  `foreup_customers` ADD  `member` SMALLINT NOT NULL AFTER  `password` ,
		ADD  `price_class` VARCHAR( 255 ) NOT NULL AFTER  `member`

	CREATE TABLE  `ejacketa_pos`.`foreup_groups` (
		`group_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
		`label` VARCHAR( 255 ) NOT NULL ,
		`CID` INT NOT NULL
		) ENGINE = INNODB;

	CREATE TABLE  `ejacketa_pos`.`foreup_group_members` (
		`group_id` INT NOT NULL ,
		`person_id` INT NOT NULL
		) ENGINE = INNODB;

	// Date 1/21/12
	INSERT INTO  `ejacketa_pos`.`foreup_modules` (
		`name_lang_key` ,
		`desc_lang_key` ,
		`sort` ,
		`module_id`
		)
		VALUES (
		'module_marketing_campaigns',  'module_marketing_campaigns_desc',  '120',  'marketing_campaigns'
		);

	ALTER TABLE  `foreup_courses` ADD  `marketing_campaigns` INT( 1 ) NOT NULL AFTER  `items`

	// Date 1/22/12
	CREATE TABLE  `ejacketa_pos`.`foreup_marketing_campaigns` (
		`campaign_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
		`type` VARCHAR( 20 ) NOT NULL ,
		`template` VARCHAR( 255 ) NOT NULL ,
		`content` TEXT NOT NULL ,
		`send_date` DATETIME NOT NULL ,
		`status` VARCHAR( 20 ) NOT NULL ,
		`group` INT NOT NULL ,
		`person_id` INT NOT NULL ,
		`CID` INT NOT NULL ,
		`deleted` TINYINT( 1 ) NOT NULL

	// Date 1/23/12
	ALTER TABLE  `foreup_teesheet` ADD  `title` VARCHAR( 255 ) NOT NULL AFTER  `teesheet_id`

	ALTER TABLE  `foreup_teesheet` ADD  `deleted` TINYINT( 1 ) NOT NULL

	// Date 1/25/12
	ALTER TABLE  `foreup_courses` ADD  `simulator` SMALLINT( 1 ) NOT NULL AFTER  `weekend_sun`

	// Date 1/25/12
	/*
	 * Not implemented yet.
	 * This is a fairly big change that should take place soon.
	 *
	 * Replacing CID, teesheet_id, and TTID with course_id, teesheet_id, and teetime_id (auto-incrementing)
	 *
	 */
	//Courses table
	ALTER TABLE  `foreup_courses` DROP PRIMARY KEY
	ALTER TABLE  `foreup_courses` ADD  `course_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST

	//Teesheet table
	ALTER TABLE  `foreup_teesheet` DROP PRIMARY KEY
	ALTER TABLE  `foreup_teesheet` ADD  `teesheet_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST
	ALTER TABLE  `foreup_teesheet` ADD  `course_id` INT NOT NULL AFTER  `teesheet_id`

	//Teetimes table
	ALTER TABLE  `foreup_teetime` DROP PRIMARY KEY
	ALTER TABLE  `foreup_teetime` ADD  `teetime_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST
	ALTER TABLE  `foreup_teetime` ADD  `teesheet_id` INT NOT NULL AFTER  `teetime_id`

	//Various tables
	//ALTER TABLE  `foreup_account_transactions` ADD  `course_id` INT NOT NULL AFTER  `trans_id`
	ALTER TABLE  `foreup_customers` ADD  `course_id` INT NOT NULL AFTER  `person_id`
	ALTER TABLE  `foreup_employees` ADD  `course_id` INT NOT NULL FIRST ,
	ADD  `teesheet_id` INT NOT NULL AFTER  `course_id`
	ALTER TABLE  `foreup_giftcards` ADD  `course_id` INT NOT NULL AFTER  `giftcard_id`
	ALTER TABLE  `foreup_groups` ADD  `course_id` INT NOT NULL
	ALTER TABLE  `foreup_inventory` ADD  `course_id` INT NOT NULL AFTER  `trans_id`
	ALTER TABLE  `foreup_items` ADD  `course_id` INT NOT NULL FIRST
	ALTER TABLE  `foreup_items_taxes` ADD  `course_id` INT NOT NULL FIRST
	ALTER TABLE  `foreup_item_kits` ADD  `course_id` INT NOT NULL AFTER  `item_kit_id`
	ALTER TABLE  `foreup_item_kits_taxes` ADD  `course_id` INT NOT NULL AFTER  `item_kit_id`
	ALTER TABLE  `foreup_item_kit_items` ADD  `course_id` INT NOT NULL AFTER  `CID`
	ALTER TABLE  `foreup_marketing_campaigns` ADD  `course_id` INT NOT NULL AFTER  `campaign_id`
	ALTER TABLE  `foreup_receivings` ADD  `course_id` INT NOT NULL AFTER  `CID`
	ALTER TABLE  `foreup_sales` ADD  `course_id` INT NOT NULL AFTER  `CID`
	ALTER TABLE  `foreup_sales_payments_credit_cards` ADD  `course_id` INT NOT NULL AFTER  `CID`
	ALTER TABLE  `foreup_suppliers` ADD  `course_id` INT NOT NULL AFTER  `CID`

	//The following should be implemented when code is pushed live
	// For each of the above tables, run these (if applicable)
	UPDATE `foreup_customers` AS t
		LEFT JOIN `foreup_courses` AS e
		ON e.CID = t.CID
		SET t.course_id = e.course_id
	UPDATE `foreup_employees` AS t
		LEFT JOIN `foreup_teesheet` AS e
		ON e.teesheet_id = t.teesheet_id
		SET t.teesheet_id = e.teesheet_id
	UPDATE `foreup_customers` AS t
		LEFT JOIN `foreup_teetime` AS e
		ON e.TTID = t.TTID
		SET t.teetime_id = e.teetime_id

	// Date 1/26/12
	ALTER TABLE  `foreup_teesheet` ADD  `increment` TINYINT NOT NULL AFTER  `holes`

	CREATE TABLE `foreup_course_groups` (
		  `group_id` int(11) NOT NULL AUTO_INCREMENT,
		  `label` varchar(255) NOT NULL,
		  `type` varchar(255) NOT NULL,
		  PRIMARY KEY (`group_id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;
	CREATE TABLE `foreup_course_group_members` (
		  `group_id` int(11) NOT NULL,
		  `course_id` int(11) NOT NULL,
		  PRIMARY KEY (`group_id`,`course_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;

	CREATE TABLE `foreup_customer_groups` (
		  `group_id` int(11) NOT NULL AUTO_INCREMENT,
		  `label` varchar(255) NOT NULL,
		  `CID` int(11) NOT NULL,
		  `course_id` int(11) NOT NULL,
		  PRIMARY KEY (`group_id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

		--
		-- Dumping data for table `foreup_groups` into foreup_customer_groups
		--
	CREATE TABLE `foreup_customer_group_members` (
		  `group_id` int(11) NOT NULL,
		  `person_id` int(11) NOT NULL,
		  PRIMARY KEY (`group_id`,`person_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;

		--
		-- Dumping data for table `foreup_group_members` into foreup_customer_group_members
		--
	DROP TABLE  `foreup_groups` ,
		`foreup_group_members` ;

	ALTER TABLE  `foreup_teesheet` ADD  `default` TINYINT( 1 ) NOT NULL AFTER  `fntime`

	//Date 1/27/12
	CREATE TABLE `foreup_green_fees` (
	  `teesheet_id` int(11) NOT NULL,
	  `item_number` varchar(255) NOT NULL,
	  `price_category_1` decimal(15,2) NOT NULL,
	  `price_category_2` decimal(15,2) NOT NULL,
	  `price_category_3` decimal(15,2) NOT NULL,
	  `price_category_4` decimal(15,2) NOT NULL,
	  `price_category_5` decimal(15,2) NOT NULL,
	  `price_category_6` decimal(15,2) NOT NULL,
	  `price_category_7` decimal(15,2) NOT NULL,
	  `price_category_8` decimal(15,2) NOT NULL,
	  `price_category_9` decimal(15,2) NOT NULL,
	  `price_category_10` decimal(15,2) NOT NULL,
	  `price_category_11` decimal(15,2) NOT NULL,
	  `price_category_12` decimal(15,2) NOT NULL,
	  `price_category_13` decimal(15,2) NOT NULL,
	  `price_category_14` decimal(15,2) NOT NULL,
	  `price_category_15` decimal(15,2) NOT NULL,
	  `price_category_16` decimal(15,2) NOT NULL,
	  `price_category_17` decimal(15,2) NOT NULL,
	  `price_category_18` decimal(15,2) NOT NULL,
	  `price_category_19` decimal(15,2) NOT NULL,
	  `price_category_20` decimal(15,2) NOT NULL,
	  `price_category_21` decimal(15,2) NOT NULL,
	  `price_category_22` decimal(15,2) NOT NULL,
	  `price_category_23` decimal(15,2) NOT NULL,
	  `price_category_24` decimal(15,2) NOT NULL,
	  `price_category_25` decimal(15,2) NOT NULL,
	  `price_category_26` decimal(15,2) NOT NULL,
	  `price_category_27` decimal(15,2) NOT NULL,
	  `price_category_28` decimal(15,2) NOT NULL,
	  `price_category_29` decimal(15,2) NOT NULL,
	  `price_category_30` decimal(15,2) NOT NULL,
	  `price_category_31` decimal(15,2) NOT NULL,
	  `price_category_32` decimal(15,2) NOT NULL,
	  `price_category_33` decimal(15,2) NOT NULL,
	  `price_category_34` decimal(15,2) NOT NULL,
	  `price_category_35` decimal(15,2) NOT NULL,
	  `price_category_36` decimal(15,2) NOT NULL,
	  `price_category_37` decimal(15,2) NOT NULL,
	  `price_category_38` decimal(15,2) NOT NULL,
	  `price_category_39` decimal(15,2) NOT NULL,
	  `price_category_40` decimal(15,2) NOT NULL,
	  `price_category_41` decimal(15,2) NOT NULL,
	  `price_category_42` decimal(15,2) NOT NULL,
	  `price_category_43` decimal(15,2) NOT NULL,
	  `price_category_44` decimal(15,2) NOT NULL,
	  `price_category_45` decimal(15,2) NOT NULL,
	  `price_category_46` decimal(15,2) NOT NULL,
	  `price_category_47` decimal(15,2) NOT NULL,
	  `price_category_48` decimal(15,2) NOT NULL,
	  `price_category_49` decimal(15,2) NOT NULL,
	  `price_category_50` decimal(15,2) NOT NULL
	) ENGINE=InnoDB DEFAULT CHARSET=latin1;

	CREATE TABLE `foreup_green_fee_types` (
	  `course_id` int(11) NOT NULL,
	  `price_category_1` varchar(255) NOT NULL,
	  `price_category_2` varchar(255) NOT NULL,
	  `price_category_3` varchar(255) NOT NULL,
	  `price_category_4` varchar(255) NOT NULL,
	  `price_category_5` varchar(255) NOT NULL,
	  `price_category_6` varchar(255) NOT NULL,
	  `price_category_7` varchar(255) NOT NULL,
	  `price_category_8` varchar(255) NOT NULL,
	  `price_category_9` varchar(255) NOT NULL,
	  `price_category_10` varchar(255) NOT NULL,
	  `price_category_11` varchar(255) NOT NULL,
	  `price_category_12` varchar(255) NOT NULL,
	  `price_category_13` varchar(255) NOT NULL,
	  `price_category_14` varchar(255) NOT NULL,
	  `price_category_15` varchar(255) NOT NULL,
	  `price_category_16` varchar(255) NOT NULL,
	  `price_category_17` varchar(255) NOT NULL,
	  `price_category_18` varchar(255) NOT NULL,
	  `price_category_19` varchar(255) NOT NULL,
	  `price_category_20` varchar(255) NOT NULL,
	  `price_category_21` varchar(255) NOT NULL,
	  `price_category_22` varchar(255) NOT NULL,
	  `price_category_23` varchar(255) NOT NULL,
	  `price_category_24` varchar(255) NOT NULL,
	  `price_category_25` varchar(255) NOT NULL,
	  `price_category_26` varchar(255) NOT NULL,
	  `price_category_27` varchar(255) NOT NULL,
	  `price_category_28` varchar(255) NOT NULL,
	  `price_category_29` varchar(255) NOT NULL,
	  `price_category_30` varchar(255) NOT NULL,
	  `price_category_31` varchar(255) NOT NULL,
	  `price_category_32` varchar(255) NOT NULL,
	  `price_category_33` varchar(255) NOT NULL,
	  `price_category_34` varchar(255) NOT NULL,
	  `price_category_35` varchar(255) NOT NULL,
	  `price_category_36` varchar(255) NOT NULL,
	  `price_category_37` varchar(255) NOT NULL,
	  `price_category_38` varchar(255) NOT NULL,
	  `price_category_39` varchar(255) NOT NULL,
	  `price_category_40` varchar(255) NOT NULL,
	  `price_category_41` varchar(255) NOT NULL,
	  `price_category_42` varchar(255) NOT NULL,
	  `price_category_43` varchar(255) NOT NULL,
	  `price_category_44` varchar(255) NOT NULL,
	  `price_category_45` varchar(255) NOT NULL,
	  `price_category_46` varchar(255) NOT NULL,
	  `price_category_47` varchar(255) NOT NULL,
	  `price_category_48` varchar(255) NOT NULL,
	  `price_category_49` varchar(255) NOT NULL,
	  `price_category_50` varchar(255) NOT NULL
	) ENGINE=InnoDB DEFAULT CHARSET=latin1;

	ALTER TABLE  `foreup_green_fees` ADD PRIMARY KEY (  `item_number` )
	Pull values over from courses with an update join
	Maybe greenfees too
	ALTER TABLE  `foreup_green_fee_types` ADD PRIMARY KEY (  `course_id` )

	// Date 1/28/12
	ALTER TABLE  `foreup_courses` ADD  `early_bird_hours_begin` VARCHAR( 10 ) NOT NULL AFTER  `frontnine` ,
		ADD  `early_bird_hours_end` VARCHAR( 10 ) NOT NULL AFTER  `early_bird_hours_begin` ,
		ADD  `morning_hours_begin` VARCHAR( 10 ) NOT NULL AFTER  `early_bird_hours_end` ,
		ADD  `morning_hours_end` VARCHAR( 10 ) NOT NULL AFTER  `morning_hours_begin` ,
		ADD  `afternoon_hours_begin` VARCHAR( 10 ) NOT NULL AFTER  `morning_hours_end` ,
		ADD  `afternoon_hours_end` VARCHAR( 10 ) NOT NULL AFTER  `afternoon_hours_begin`

	ALTER TABLE  `foreup_teesheet` CHANGE  `increment`  `increment` TINYINT( 4 ) NOT NULL DEFAULT  '8'
	ALTER TABLE  `foreup_teesheet` CHANGE  `frontnine`  `frontnine` SMALLINT( 6 ) NOT NULL DEFAULT  '200'

	// Date 1/30/12
	ALTER TABLE  `foreup_teesheet` ADD  `online_booking` TINYINT( 1 ) NOT NULL AFTER  `default`
	ALTER TABLE  `foreup_courses` ADD  `booking_rules` TEXT NOT NULL AFTER  `online_booking`

	// 2/6/12
	ALTER TABLE  `foreup_sales_items` ADD  `price_category` VARCHAR( 255 ) NOT NULL AFTER  `description`;
	//Unneccessary//ALTER TABLE  `foreup_sales_items` CHANGE  `price_category`  `price_category` TINYINT NOT NULL
	//Unneccessary//ALTER TABLE  `foreup_sales_item_kits` ADD  `price_category` TINYINT NOT NULL AFTER  `description`

	// 2/14/12
	ALTER TABLE  `foreup_items` ADD  `invisible` TINYINT NOT NULL AFTER  `is_serialized`

	// 2/15/12
	ALTER TABLE  `foreup_giftcards` ADD  `expiration_date` DATE NOT NULL AFTER  `customer_id`;
	ALTER TABLE  `foreup_courses` ADD  `holidays` TINYINT NOT NULL AFTER  `super_twilight_hour`
	CREATE TABLE  `ejacketa_pos`.`foreup_holidays` (
		`course_id` INT NOT NULL ,
		`date` DATE NOT NULL ,
		`label` VARCHAR( 255 ) NOT NULL
		) ENGINE = INNODB;
	ALTER TABLE  `ejacketa_pos`.`foreup_holidays` ADD PRIMARY KEY (  `course_id` ,  `date` )
	ALTER TABLE  `foreup_items` ADD  `is_giftcard` TINYINT NOT NULL AFTER  `is_serialized`

	// 2/17/12
	ALTER TABLE  `foreup_green_fee_types` ADD  `teesheet_id` INT NOT NULL AFTER  `course_id`
	// UPDATE indexes on Account_number, giftcard_number, item_number, and item_kit_number... manually
	// TODO: UPDATE indexes on sale_id and receiving_id
	// TODO: UPDATE index on green_fees to be teesheet_id and item_number
	// TODO: Run following SQL to update green fees
	UPDATE foreup_green_fees LEFT JOIN foreup_teesheet
		ON SUBSTRING_INDEX(foreup_green_fees.item_number, '_', 1) = foreup_teesheet.teesheet_id
		SET foreup_green_fees.teesheet_id = SUBSTRING_INDEX( item_number,  '_', 1 ),
			item_number = CONCAT(course_id, CONCAT('_',SUBSTRING_INDEX(item_number, '_', -1)))
	UPDATE foreup_green_fee_types LEFT JOIN foreup_teesheet
		ON foreup_green_fee_types.course_id = foreup_teesheet.course_id
		SET foreup_green_fee_types.teesheet_id = foreup_teesheet.teesheet_id
		WHERE `foreup_teesheet`.`default` = 1

	// 2/22/12
	ALTER TABLE  `foreup_courses` ADD  `teesheet_updates_automatically` TINYINT NOT NULL AFTER  `print_after_sale`;
	ALTER TABLE  `foreup_teesheet` ADD  `online_open_time` VARCHAR( 10 ) NOT NULL AFTER  `holes` ,
		ADD  `online_close_time` VARCHAR( 10 ) NOT NULL AFTER  `online_open_time`


  // 03/21/12
  ALTER TABLE `foreup_marketing_campaigns` add column title varchar(255) not null

 // 03/26/12
 alter table `foreup_marketing_campaigns` add column logo_path varchar(1000)
 alter table `foreup_marketing_campaigns` add column header varchar(1000)

 //03/28/12
 ALTER TABLE  `foreup_teesheet` CHANGE  `online_open_time`  `online_open_time` VARCHAR( 10 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT  '0900';
 ALTER TABLE  `foreup_teesheet` CHANGE  `online_close_time`  `online_close_time` VARCHAR( 10 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT  '1800';
 UPDATE `foreup_teesheet` SET `online_open_time` = '0900' WHERE `online_open_time` = '';
 UPDATE `foreup_teesheet` SET `online_close_time` = '1800' WHERE `online_close_time` = '';

 //03/29/12
 CREATE TABLE `foreup_marketing_campaign_groups`(
  `campaign_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`campaign_id`,`group_id`)
 )ENGINE=InnoDB DEFAULT CHARSET=latin1;

 CREATE TABLE `foreup_billing` (
  `billing_id` int(11) NOT NULL AUTO_INCREMENT,
  `teesheet_id` mediumint(9) NOT NULL,
  `per_day` tinyint(4) NOT NULL,
  `per_week` tinyint(4) NOT NULL,
  `time` mediumint(9) NOT NULL,
  `terms` text NOT NULL,
  `weekday_price` decimal(15,2) NOT NULL,
  `weekend_price` decimal(15,2) NOT NULL,
  `fees` decimal(15,2) NOT NULL,
  `tax_rate` decimal(15,2) NOT NULL,
  PRIMARY KEY (`billing_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

CREATE TABLE `foreup_teetimes_bartered` (
  `teetime_id` int(11) NOT NULL,
  `teesheet_id` int(11) NOT NULL,
  `start` bigint(20) NOT NULL,
  `end` bigint(20) NOT NULL,
  `player_count` tinyint(4) NOT NULL,
  `holes` tinyint(4) NOT NULL,
  `carts` float NOT NULL,
  `date_booked` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `booker_id` varchar(255) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  PRIMARY KEY (`teetime_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

//04/01/12
ALTER TABLE `foreup_marketing_campaigns` add column is_sent int(1) not null default 0;
ALTER TABLE `foreup_marketing_campaigns` change `group` recipients text;

//04/06/12
ALTER TABLE `foreup_teetime` ADD `canceller_id` INT NOT NULL AFTER `booker_id`

//04/08/12
ALTER TABLE  `foreup_teetime` ADD  `date_cancelled` TIMESTAMP NOT NULL AFTER  `date_booked`

//04/13/2012
ALTER TABLE `foreup_customers` add column opt_out_email varchar(1) not null default 'N';
ALTER TABLE `foreup_customers` add column opt_out_text varchar(1) not null default 'N';


CREATE TABLE `foreup_subscription_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `action` varchar(255) NOT NULL,
  `action_date` TIMESTAMP NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

//04/15/2012
ALTER TABLE `foreup_marketing_campaigns` add column `name` varchar(255);

//04/16/2012
CREATE TABLE `foreup_help_topics`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic` varchar(255) NOT NULL,
  `date_modified` timestamp default CURRENT_TIMESTAMP(),
   PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `foreup_help_posts`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `title` varchar(255),
  `contents` text,
  `deleted` int(1) default 0,
   PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

//04/17/12
CREATE TABLE `foreup_quickbuttons` (
  `quickbutton_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`quickbutton_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

CREATE TABLE `foreup_quickbutton_items` (
  `quickbutton_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  PRIMARY KEY (`quickbutton_id`,`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

//04/19/12
ALTER TABLE `foreup_quickbutton_items` ADD `order` TINYINT NOT NULL AFTER `item_id`;

//04/20/12
ALTER TABLE `foreup_help_posts`ADD `date_modified` timestamp default CURRENT_TIMESTAMP();
ALTER TABLE `foreup_help_posts`ADD `views` int(11) default 0;
CREATE TABLE `foreup_help_post_ratings`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` int(1) NOT NULL,
   PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

//04/21/2012
CREATE TABLE `foreup_coupon_definitions`(
  `id` int(11) unsigned not null auto_increment,
  `course_id` int(11) not null,
  `number_available` int(11),
  `title` varchar(255),
  `amount_type` varchar(100),
  `amount` double precision,
  `rules` text,
  `text_text` text,
  `email_text` text,
  `expiration_date` date,
  primary key(`id`)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `foreup_coupons`(
  `id` int(11) unsigned not null auto_increment,
  `coupon_definition_id` int(11) not null,
  `customer_id` int(11) not null,
  `expiration_date` date,
  `redeemed` char(1) default 'N',
  `deleted` int(1) default 0,
  primary key(`id`)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

//04/24/2012
ALTER TABLE `foreup_help_posts`ADD `keywords` varchar(255);

//4/22/12
ALTER TABLE  `foreup_sales_payments_credit_cards` ADD  `token_used` TINYINT NOT NULL AFTER  `token`;

//04/25/2012
DROP TABLE `foreup_coupon_definitions`;
DROP TABLE `foreup_coupons`;

CREATE TABLE `foreup_promotion_definitions`(
  `id` int(11) unsigned not null auto_increment,
  `course_id` int(11) not null,
  `number_available` int(11),
  `name` varchar(255),
  `amount_type` varchar(100),
  `amount` double precision,
  `valid_for` varchar(255)
  `valid_day_start` varchar(255),
  `valid_day_end` varchar(255),
  `valid_between_from` varchar(255),
  `valid_between_to` varchar(255),
  `limit` int(11),
  `buy_quantity` int(11),
  `get_quantity` int(11),
  `min_purchase` double precision,
  `additional_details` text,
  `rules` text,
  `text_text` text,
  `email_text` text,
  `expiration_date` date,
  primary key(`id`)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `foreup_promotions`(
  `id` int(11) unsigned not null auto_increment,
  `coupon_definition_id` int(11) not null,
  `customer_id` int(11) not null,
  `expiration_date` date,
  `redeemed` char(1) default 'N',
  `deleted` int(1) default 0,
  primary key(`id`)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

//04/27/2012
ALTER TABLE `foreup_promotion_definitions` ADD `deleted` int(1) not null default 0;

//04/30/2012
UPDATE `foreup_help_topics`
SET topic = 'Tee Sheet'
WHERE topic = 'Teesheets';

INSERT INTO `foreup_help_topics`(topic) VALUES('General');

ALTER TABLE `foreup_help_posts` ENGINE = MyISAM;
ALTER TABLE `foreup_help_posts` ADD FULLTEXT (title, contents);

//05/07/2012
CREATE TABLE  `ejacketa_pos`.`foreup_rainchecks` (
`raincheck_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`teesheet_id` INT NOT NULL ,
`date_issued` DATETIME NOT NULL ,
`date_redeemed` DATETIME NOT NULL ,
`players` TINYINT NOT NULL ,
`holes_completed` TINYINT NOT NULL ,
`customer_id` INT NOT NULL ,
`employee_id` INT NOT NULL ,
`green_fee` FLOAT( 15, 2 ) NOT NULL ,
`cart_fee` FLOAT( 15, 2 ) NOT NULL ,
`tax` FLOAT( 15, 2 ) NOT NULL ,
`total` FLOAT( 15, 2 ) NOT NULL ,
`green_fee_price_category` VARCHAR( 255 ) NOT NULL ,
`cart_price_category` VARCHAR( 255 ) NOT NULL
) ENGINE = INNODB;

//05/15/2012
ALTER TABLE `foreup_help_posts` ADD FULLTEXT (title, keywords);

//05/16/2012
ALTER TABLE `foreup_help_posts` ADD `related_articles` TEXT;

//5/29/2012
ALTER TABLE  `foreup_courses` ADD  `active` TINYINT NOT NULL AFTER  `course_id`


CREATE TABLE `foreup_users`(
  `id` int(11) unsigned not null auto_increment,
  `username` varchar(255) not null,
  `password` varchar(255) not null,
  `deleted` int(1) default 0,
  primary key(`id`)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `foreup_events`(
  `id` int(11) unsigned not null auto_increment,
  `title` varchar(255) not null,
  `details` varchar(255) not null,
  `start_date` timestamp null,
  `end_date` timestamp null,
  `all_day` int(1) default 0,
  `deleted` int(1) default 0,
  primary key(`id`)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

alter table `foreup_courses` add column `events` int(1) default 0 after employees;

//9/26/12
added the tournaments record to the Modules table.
added the tournaments field to the courses table.

//11/14/2012 James Deibert
CREATE TABLE `foreup_api_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(40) NOT NULL,
  `name` varchar(255) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

CREATE TABLE `foreup_api_key_permissions` (
  `api_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  UNIQUE KEY `course_id` (`api_id`,`course_id`),
  KEY `api_id` (`api_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `foreup_api_limits` (
  `uri` varchar(255) NOT NULL,
  `api_key` varchar(40) NOT NULL,
  `hour_started` int(11) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `foreup_api_logs` (
  `uri` varchar(255) NOT NULL,
  `method` varchar(255) NOT NULL,
  `params` varchar(255) NOT NULL,
  `api_key` varchar(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `authorized` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

alter table `foreup_courses` add column `course_app_data_id` int(11) NOT NULL after CID;
alter table `foreup_courses` add column `tournaments` tinyint(4) NOT NULL DEFAULT '0' after teesheets
alter table `foreup_courses` add column `dashboard` int(1) NOT NULL after tournaments
alter table `foreup_courses` add column `allow_friends_to_invite` tinyint(1) NOT NULL after mercury_password
alter table `foreup_courses` add column `payment_required` tinyint(1) NOT NULL after allow_friends_to_invite

CREATE TABLE `foreup_course_app_data` (
  `course_app_data_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `course_latitude` decimal(10,6) NOT NULL,
  `course_longitude` decimal(10,6) NOT NULL,
  `red` float NOT NULL,
  `green` float NOT NULL,
  `blue` float NOT NULL,
  `alpha` float NOT NULL,
  `home_view` int(11) NOT NULL,
  `gps_view` int(11) NOT NULL,
  `scorecard_view` int(11) NOT NULL,
  `teetime_view` int(11) NOT NULL,
  `food_and_beverage_view` int(11) NOT NULL,
  `last_updated` date NOT NULL,
  PRIMARY KEY (`course_app_data_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

alter table `foreup_customers` add column `api_id` int(11) NOT NULL after course_id

CREATE TABLE `foreup_holes` (
  `hole_id` int(11) NOT NULL AUTO_INCREMENT,
  `link_id` int(11) NOT NULL,
  `par` int(3) NOT NULL,
  `hole_number` int(3) NOT NULL,
  `front_latitude` decimal(10,6) NOT NULL,
  `front_longitude` decimal(10,6) NOT NULL,
  `center_latitude` decimal(10,6) NOT NULL,
  `center_longitude` decimal(10,6) NOT NULL,
  `back_latitude` decimal(10,6) NOT NULL,
  `back_longitude` decimal(10,6) NOT NULL,
  `hole_latitude` decimal(10,6) NOT NULL,
  `hole_longitude` decimal(10,6) NOT NULL,
  `middle_latitude` decimal(10,6) NOT NULL,
  `middle_longitude` decimal(10,6) NOT NULL,
  `tee_1_latitude` decimal(10,6) NOT NULL,
  `tee_1_longitude` decimal(10,6) NOT NULL,
  `tee_2_latitude` decimal(10,6) NOT NULL,
  `tee_2_longitude` decimal(10,6) NOT NULL,
  `tee_3_latitude` decimal(10,6) NOT NULL,
  `tee_3_longitude` decimal(10,6) NOT NULL,
  `tee_4_latitude` decimal(10,6) NOT NULL,
  `tee_4_longitude` decimal(10,6) NOT NULL,
  `tee_5_latitude` decimal(10,6) NOT NULL,
  `tee_5_longitude` decimal(10,6) NOT NULL,
  `tee_6_latitude` decimal(10,6) NOT NULL,
  `tee_6_longitude` decimal(10,6) NOT NULL,
  `tee_7_latitude` decimal(10,6) NOT NULL,
  `tee_7_longitude` decimal(10,6) NOT NULL,
  `tee_8_latitude` decimal(10,6) NOT NULL,
  `tee_8_longitude` decimal(10,6) NOT NULL,
  PRIMARY KEY (`hole_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

CREATE TABLE `foreup_links` (
  `link_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `tee_1_name` varchar(255) NOT NULL,
  `tee_2_name` varchar(255) NOT NULL,
  `tee_3_name` varchar(255) NOT NULL,
  `tee_4_name` varchar(255) NOT NULL,
  `tee_5_name` varchar(255) NOT NULL,
  `tee_6_name` varchar(255) NOT NULL,
  `tee_7_name` varchar(255) NOT NULL,
  `tee_8_name` varchar(255) NOT NULL,
  PRIMARY KEY (`link_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

CREATE TABLE `foreup_sales_tournaments` (
  `sale_id` int(10) NOT NULL,
  `tournament_id` int(10) NOT NULL,
  `teesheet` varchar(255) NOT NULL,
  `price_category` tinyint(4) DEFAULT NULL,
  `line` int(3) NOT NULL,
  `quantity_purchased` decimal(15,2) NOT NULL,
  `tournament_cost_price` decimal(15,2) NOT NULL,
  `tournament_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` int(11) NOT NULL,
  `taxes_paid` decimal(15,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

alter table `foreup_teetime` add column `api_id` int(11) NOT NULL after finish_time

CREATE TABLE `foreup_tournaments` (
  `tournament_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `green_fee` decimal(15,2) NOT NULL,
  `carts_issued` tinyint(4) NOT NULL,
  `cart_fee` decimal(15,2) NOT NULL,
  `customer_credit_fee` decimal(15,2) NOT NULL,
  `member_credit_fee` decimal(15,2) NOT NULL,
  `pot_fee` decimal(15,2) NOT NULL,
  `tax_included` tinyint(4) NOT NULL,
  `inventory_item_taxes` decimal(15,2) NOT NULL,
  `total_cost` decimal(15,2) NOT NULL,
  `accumulated_pot` decimal(15,2) NOT NULL,
  `remaining_pot_balance` decimal(15,2) NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  PRIMARY KEY (`tournament_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

CREATE TABLE `foreup_tournament_inventory_items` (
  `tournament_inventory_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `tournament_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`tournament_inventory_item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=315 ;

CREATE TABLE `foreup_tournament_winners` (
  `tournament_winner_id` int(11) NOT NULL AUTO_INCREMENT,
  `tournament_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `description` varchar(180) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  PRIMARY KEY (`tournament_winner_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=86 ;

12/21/2012
ALTER TABLE  `foreup_courses` ADD  `facebook_page_id` VARCHAR( 255 ) NOT NULL

2/21/2012
ALTER TABLE  `foreup_courses` ADD  `send_email_reminder` TINYINT( 1 ) NOT NULL,
ALTER TABLE  `foreup_courses` ADD  `send_text_reminder` TINYINT( 1 ) NOT NULL




1/2/2013 CHANGES FOR DOING UNSUBSCRIPTIONS BY JAMES.




DROP TABLE `foreup_unsubscribes`

CREATE TABLE `foreup_unsubscribes` (
  `email` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `unsubscribe_all` tinyint(1) NOT NULL,
  `course_news_announcements` tinyint(1) NOT NULL,
  `teetime_reminders` tinyint(1) NOT NULL,
  `foreup_news_announcements` tinyint(1) NOT NULL,
  `comments` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE  `foreup_customers` ADD  `course_news_announcements` TINYINT( 1 ) NOT NULL
ALTER TABLE  `foreup_customers` ADD  `unsubscribe_all` TINYINT( 1 ) NOT NULL AFTER  `opt_out_text`
ALTER TABLE  `foreup_customers` ADD  `teetime_reminders` TINYINT( 1 ) NOT NULL

ALTER TABLE  `foreup_people` ADD  `foreup_news_announcements_unsubscribe` TINYINT( 1 ) NOT NULL



1/3/2013 ADD FIELD TO COURSE TABLE TO STORE THE FACEBOOK EXTENDED ACCESS TOKEN



ALTER TABLE  `foreup_courses` ADD  `facebook_extended_access_token` VARCHAR( 255 ) NOT NULL AFTER  `facebook_page_id`
ALTER TABLE  `foreup_courses` ADD  `facebook_page_name` VARCHAR( 255 ) NOT NULL AFTER  `facebook_page_id`

1/15/2013

CREATE TABLE `foreup_sendhub_accounts` (
  `phone_number` varchar(10) NOT NULL,
  `sendhub_id` varchar(255) NOT NULL,
  `text_reminder_unsubscribe` tinyint(1) NOT NULL,
  UNIQUE KEY `phone_number` (`phone_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

1/25/2013

DROP TABLE `foreup_users`

CREATE TABLE `foreup_users` (
  `person_id` int(10) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `facebook_id` varchar(255) NOT NULL,
  `twitter_id` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

2/5/2013
ALTER TABLE  `foreup_teesheet` ADD  `days_out` INT NOT NULL AFTER  `online_close_time`

ALTER TABLE  `foreup_teesheet` ADD  `days_in_booking_window` INT NOT NULL AFTER  `days_out`

10/24/13
ALTER TABLE  `foreup_teetime` ADD  `person_paid_1` TINYINT NOT NULL AFTER  `person_id`;
ALTER TABLE  `foreup_teetime` ADD  `person_paid_2` TINYINT NOT NULL AFTER  `person_id_2`;
ALTER TABLE  `foreup_teetime` ADD  `person_paid_3` TINYINT NOT NULL AFTER  `person_id_3`;
ALTER TABLE  `foreup_teetime` ADD  `person_paid_4` TINYINT NOT NULL AFTER  `person_id_4`;
ALTER TABLE  `foreup_teetime` ADD  `person_paid_5` TINYINT NOT NULL AFTER  `person_id_5`;
ALTER TABLE  `foreup_teetime` ADD  `cart_paid_1` TINYINT NOT NULL AFTER  `person_paid_1`;
ALTER TABLE  `foreup_teetime` ADD  `cart_paid_2` TINYINT NOT NULL AFTER  `person_paid_2`;
ALTER TABLE  `foreup_teetime` ADD  `cart_paid_3` TINYINT NOT NULL AFTER  `person_paid_3`;
ALTER TABLE  `foreup_teetime` ADD  `cart_paid_4` TINYINT NOT NULL AFTER  `person_paid_4`;
ALTER TABLE  `foreup_teetime` ADD  `cart_paid_5` TINYINT NOT NULL AFTER  `person_paid_5`;
?>

2/20/2013 member Billing
ALTER TABLE  `foreup_customer_billing` ADD  `title` VARCHAR( 255 ) NOT NULL AFTER  `billing_id`

3/6/2013
ALTER TABLE  `foreup_invoices` ADD  `invoice_number` INT NOT NULL AFTER  `invoice_id`

3/7/2013
ALTER TABLE  `foreup_invoices` ADD  `month_billed` DATE NOT NULL AFTER  `invoice_number`
ALTER TABLE  `foreup_invoice_items` CHANGE  `paid_amount`  `paid_amount` FLOAT( 15, 2 ) NOT NULL

3/8/2013
CREATE TABLE  `ejacketa_pos`.`foreup_terminals` (
	`terminal_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
	`label` VARCHAR( 255 ) NOT NULL ,
	`course_id` INT NOT NULL
) ENGINE = INNODB;

ALTER TABLE  `foreup_register_log` ADD  `terminal_id` INT NOT NULL AFTER  `course_id`

ALTER TABLE  `foreup_courses` ADD  `use_terminals` TINYINT NOT NULL AFTER  `separate_courses`

3/12/2013
ALTER TABLE  `foreup_invoice_items` ADD  `pay_account_balance` TINYINT( 4 ) NOT NULL AFTER  `tax`
ALTER TABLE  `foreup_invoice_items` ADD  `pay_member_balance` TINYINT( 4 ) NOT NULL AFTER  `pay_account_balance`

3/15/2013
CREATE TABLE `foreup_sales_invoices` (
  `sale_id` int(10) NOT NULL,
  `invoice_id` int(10) NOT NULL,
  `line` int(3) NOT NULL,
  `price` decimal(15,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE  `foreup_courses` ADD  `tip_line` TINYINT NOT NULL AFTER  `print_two_receipts_other`

3/18/2013
ALTER TABLE  `foreup_courses` ADD  `print_credit_card_receipt` TINYINT NOT NULL DEFAULT  '1' AFTER  `receipt_printer` ,
ADD  `print_tip_line` TINYINT NOT NULL DEFAULT  '1' AFTER  `print_credit_card_receipt`
ALTER TABLE  `foreup_courses` ADD  `print_sales_receipt` TINYINT NOT NULL DEFAULT  '1' AFTER  `print_credit_card_receipt`
ALTER TABLE  `foreup_courses` ADD  `cash_drawer_on_cash` TINYINT NOT NULL AFTER  `print_tip_line`

3/25/2013
ALTER TABLE  `foreup_sales` ADD  `override_authorization_id` INT NOT NULL AFTER  `employee_id`;
ALTER TABLE  `foreup_sales` ADD  `terminal_id` INT NOT NULL AFTER  `sale_id`;

3/26/2013
ALTER TABLE  `foreup_courses` ADD  `foreup_discount_percent` DOUBLE( 15, 2 ) NOT NULL DEFAULT  '25'

4/9/13
ALTER TABLE  `foreup_quickbuttons` ADD  `tab` TINYINT NOT NULL DEFAULT  '1' AFTER  `display_name`
ALTER TABLE  `foreup_terminals` ADD  `quickbutton_tab` TINYINT NOT NULL DEFAULT  '1' AFTER  `label`

4/12/13
CREATE TABLE `foreup_loyalty_transactions` (
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `trans_customer` int(11) NOT NULL DEFAULT '0',
  `trans_user` int(11) DEFAULT NULL,
  `trans_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trans_comment` text COLLATE utf8_unicode_ci NOT NULL,
  `trans_description` text COLLATE utf8_unicode_ci NOT NULL,
  `trans_amount` decimal(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`trans_id`),
  KEY `foreup_loyalty_transactions_ibfk_1` (`trans_customer`),
  KEY `foreup_loyalty_transactions_ibfk_2` (`trans_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

ALTER TABLE  `foreup_customers` ADD  `loyalty_points` INT NOT NULL AFTER  `member_account_balance_allow_negative`

4/15/13
ALTER TABLE  `foreup_courses` ADD  `online_booking_protected` TINYINT NOT NULL AFTER  `online_booking`

4/22/13
CREATE TABLE  `ejacketa_pos`.`foreup_loyalty_rates` (
`loyalty_rate_id` INT NOT NULL ,
`label` VARCHAR( 255 ) NOT NULL ,
`course_id` int(11) NOT NULL,
`type` ENUM(  'department',  'category',  'subcategory',  'item',  'item_kit' ) NOT NULL ,
`value` VARCHAR( 255 ) NOT NULL ,
`points_per_dollar` DOUBLE( 15, 2 ) NOT NULL ,
`dollars_per_point` DOUBLE( 15, 2 ) NOT NULL
) ENGINE = INNODB;

ALTER TABLE  `foreup_loyalty_rates` ADD PRIMARY KEY (  `loyalty_rate_id` );
ALTER TABLE  `foreup_loyalty_rates` CHANGE  `loyalty_rate_id`  `loyalty_rate_id` INT( 11 ) NOT NULL AUTO_INCREMENT

4/25/13
ALTER TABLE  `foreup_courses` ADD  `use_loyalty` TINYINT NOT NULL DEFAULT  '0' AFTER  `use_terminals`

4/27/13
ALTER TABLE  `foreup_billing` ADD  `last_billing_attempt` DATE NOT NULL ,
ADD  `started` TINYINT NOT NULL ,
ADD  `charged` TINYINT NOT NULL ,
ADD  `emailed` TINYINT NOT NULL;

5/1/13
CREATE TABLE `foreup_loyalty_transactions` (
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `trans_customer` int(11) NOT NULL DEFAULT '0',
  `trans_user` int(11) DEFAULT NULL,
  `trans_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trans_comment` text COLLATE utf8_unicode_ci NOT NULL,
  `trans_description` text COLLATE utf8_unicode_ci NOT NULL,
  `trans_amount` decimal(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`trans_id`),
  KEY `foreup_loyalty_transactions_ibfk_1` (`trans_customer`),
  KEY `foreup_loyalty_transactions_ibfk_2` (`trans_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

5/1/13
ALTER TABLE `foreup_courses` ADD `min_required_players` TINYINT NOT NULL DEFAULT '2' AFTER `online_booking_protected` ,
ADD `min_required_carts` TINYINT NOT NULL AFTER `min_required_players` ,
ADD `min_required_holes` TINYINT NOT NULL AFTER `min_required_carts`


5/3/13
INSERT INTO  `ejacketa_pos`.`foreup_modules` (
`name_lang_key` ,
`desc_lang_key` ,
`sort` ,
`module_id`
)
VALUES (
'module_invoices',  'module_invoices_desc',  '110',  'invoices'
);
ALTER TABLE  `foreup_courses` ADD  `invoices` TINYINT NOT NULL AFTER  `giftcards`


// CUSTOMER BILLING UPDATE
ALTER TABLE  `foreup_customer_billing` ADD  `last_billing_attempt` DATE NOT NULL AFTER  `deleted` ,
ADD  `started` TINYINT NOT NULL AFTER  `last_billing_attempt` ,
ADD  `charged` TINYINT NOT NULL AFTER  `started` ,
ADD  `emailed` TINYINT NOT NULL AFTER  `charged`


5/6/13
CREATE TABLE  `ejacketa_pos`.`foreup_queue_reports` (
`queue_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`course_id` INT NOT NULL ,
`url` VARCHAR( 255 ) NOT NULL ,
`run_date` DATE NOT NULL ,
`attempts` TINYINT NOT NULL ,
`completed` TINYINT NOT NULL
) ENGINE = INNODB;


5/7/2013
CREATE TABLE `foreup_queue_invoices` (
  `queue_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `member_balance` tinyint(4) NOT NULL,
  `customer_credit` tinyint(4) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `run_date` date NOT NULL,
  `attempts` tinyint(4) NOT NULL,
  `completed` tinyint(4) NOT NULL,
  PRIMARY KEY (`queue_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;
5/9/2013
ALTER TABLE  `foreup_sales_payments` ADD  `tip_recipient` INT NOT NULL AFTER  `invoice_id`

5/15/2013
ALTER TABLE  `foreup_marketing_campaigns` ADD  `attempts` TINYINT NOT NULL AFTER  `is_sent` ,
ADD  `run_time` DATE NOT NULL AFTER  `attempts`

5/23/2013
ALTER TABLE  `foreup_customer_billing` ADD  `generate_days_before` TINYINT NOT NULL AFTER  `day`
ALTER TABLE  `foreup_customer_billing` ADD  `last_invoice_generation_attempt` DATE NOT NULL AFTER  `generate_days_before`

6/13/13
ALTER TABLE  `foreup_invoices`
ADD  `last_billing_attempt` DATE NOT NULL ,
ADD  `started` TINYINT NOT NULL ,
ADD  `charged` TINYINT NOT NULL ,
ADD  `email` TINYINT NOT NULL ,
ADD  `send_date` DATE NOT NULL ,
ADD  `due_date` DATE NOT NULL

6/24/13
ALTER TABLE  `foreup_teesheet` ADD  `send_thank_you` TINYINT NOT NULL AFTER  `online_booking`;
ALTER TABLE  `foreup_teesheet` ADD  `thank_you_campaign_id` INT NOT NULL AFTER  `send_thank_you`;
ALTER TABLE  `foreup_teesheet` ADD  `thank_you_next_send` DATETIME NOT NULL AFTER  `thank_you_campaign_id`
ALTER TABLE  `foreup_teetime` ADD  `thank_you_emailed` TINYINT NOT NULL AFTER  `confirmation_emailed`

6/27/13
ALTER TABLE  `foreup_customer_billing_items` CHANGE  `quantity`  `quantity` SMALLINT NOT NULL;
ALTER TABLE  `foreup_invoice_items` CHANGE  `quantity`  `quantity` SMALLINT NOT NULL

7/26/13
CREATE TABLE  `foreup_teetime_standbys` (
`standby_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`teesheet_id` INT NOT NULL ,
`person_id` INT NOT NULL ,
`name` VARCHAR( 50 ) NOT NULL ,
`time` DATETIME NOT NULL ,
`players` TINYINT NOT NULL ,
`holes` TINYINT NOT NULL ,
`details` VARCHAR( 255 ) NOT NULL ,
`email` VARCHAR( 100 ) NOT NULL ,
`phone` VARCHAR( 20 ) NOT NULL
) ENGINE = INNODB;

7/31/13
ALTER TABLE `foreup_courses` ADD `ets_key` VARCHAR( 64 ) NOT NULL AFTER `mercury_password`

8/3/2013
DROP TABLE IF EXISTS `foreup_item_modifiers`;
CREATE TABLE IF NOT EXISTS `foreup_item_modifiers` (
  `item_id` int(10) unsigned NOT NULL,
  `modifier_id` int(10) unsigned NOT NULL,
  `override_price` decimal(10,2) DEFAULT NULL,
  `auto_select` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_id`,`modifier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `foreup_modifiers`;
CREATE TABLE IF NOT EXISTS `foreup_modifiers` (
  `modifier_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(10) unsigned NOT NULL,
  `name` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `default_price` decimal(10,2) NOT NULL,
  `options` text COLLATE utf8_unicode_ci,
  `date_created` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`modifier_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `foreup_sales_items_modifiers`;
CREATE TABLE IF NOT EXISTS `foreup_sales_items_modifiers` (
  `sale_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `modifier_id` int(10) unsigned NOT NULL,
  `line` int(10) unsigned NOT NULL,
  `selected_option` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `selected_price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`sale_id`,`item_id`,`modifier_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `foreup_sales_suspended_items_modifiers`;
CREATE TABLE IF NOT EXISTS `foreup_sales_suspended_items_modifiers` (
  `sale_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `modifier_id` int(10) unsigned NOT NULL,
  `line` int(10) unsigned NOT NULL,
  `selected_option` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `selected_price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`sale_id`,`item_id`,`modifier_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 8/19/2013 Marketing email ads
ALTER TABLE `foreup_courses` ADD `marketing_include_ads` BOOLEAN NOT NULL DEFAULT '0' AFTER `marketing_campaigns`;

CREATE TABLE IF NOT EXISTS `foreup_ads` (
  `ad_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alt_text` text NOT NULL,
  `image_url` varchar(255) NOT NULL,
  `destination_url` varchar(1024) NOT NULL,
  `click_limit` int(10) unsigned DEFAULT NULL,
  `click_total` int(10) unsigned NOT NULL DEFAULT '0',
  `view_limit` int(10) unsigned DEFAULT NULL,
  `view_total` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ad_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0;

--
-- DEMO DATA
--
INSERT INTO `foreup_ads` (`ad_id`, `title`, `alt_text`, `image_url`, `destination_url`, `click_limit`, `click_total`, `view_limit`, `view_total`) VALUES
(1, 'Nike Ad 1', 'This is ad #1 - blah blah blah, some more text here.', 'ads/nike/ad1.jpeg', 'http://www.ebay.com', NULL, 0, NULL, 0),
(2, 'Nike Ad 2', 'This is ad #2 - blah blah blah, some more text here.', 'ads/nike/ad2.jpg', 'http://www.google.com', NULL, 0, NULL, 0),
(3, 'Nike Ad 3', 'This is ad #3 - blah blah blah, some more text here.', 'ads/nike/ad3.jpg', 'http://www.newegg.com', NULL, 0, NULL, 0),
(4, 'Nike Ad 4', 'This is ad #4 - blah blah blah, some more text here.', 'ads/nike/ad4.jpg', 'http://www.amazon.com', NULL, 0, NULL, 0);

CREATE TABLE IF NOT EXISTS `foreup_ad_clicks` (
  `ad_id` int(11) NOT NULL,
  `ip_address` varchar(16) NOT NULL,
  `user_agent` varchar(120) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `campaign_id` int(11) NOT NULL,
  `person_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `foreup_ad_views` (
  `ad_id` int(11) NOT NULL,
  `ip_address` varchar(16) NOT NULL,
  `user_agent` varchar(120) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `campaign_id` int(11) NOT NULL,
  `person_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- ************** 8/15/2013 Report speedups *****************
--
ALTER TABLE `foreup_sales` ADD INDEX `course_sale` ( `course_id` , `sale_time` );

ALTER TABLE `foreup_sales_items` ADD `subtotal` DECIMAL( 15, 2 ) NOT NULL AFTER `discount_percent` ,
ADD `tax` DECIMAL( 15, 2 ) NOT NULL AFTER `subtotal` ,
ADD `total` DECIMAL( 15, 2 ) NOT NULL AFTER `tax` ,
ADD `profit` DECIMAL( 15, 2 ) NOT NULL AFTER `total`,
ADD `total_cost` DECIMAL( 15, 2 ) NOT NULL AFTER `profit`,
CHANGE `discount_percent` `discount_percent` DECIMAL( 15, 2 ) NOT NULL DEFAULT '0.00';

ALTER TABLE `foreup_sales_item_kits` ADD `subtotal` DECIMAL( 15, 2 ) NOT NULL AFTER `discount_percent` ,
ADD `tax` DECIMAL( 15, 2 ) NOT NULL AFTER `subtotal` ,
ADD `total` DECIMAL( 15, 2 ) NOT NULL AFTER `tax` ,
ADD `profit` DECIMAL( 15, 2 ) NOT NULL AFTER `total`,
ADD `total_cost` DECIMAL( 15, 2 ) NOT NULL AFTER `profit`,
CHANGE `discount_percent` `discount_percent` DECIMAL( 15, 2 ) NOT NULL DEFAULT '0.00';

ALTER TABLE `foreup_sales_invoices` ADD `subtotal` DECIMAL( 15, 2 ) NOT NULL AFTER `discount_percent` ,
ADD `tax` DECIMAL( 15, 2 ) NOT NULL AFTER `subtotal` ,
ADD `total` DECIMAL( 15, 2 ) NOT NULL AFTER `tax` ,
ADD `profit` DECIMAL( 15, 2 ) NOT NULL AFTER `total`,
ADD `total_cost` DECIMAL( 15, 2 ) NOT NULL AFTER `profit`,
CHANGE `discount_percent` `discount_percent` DECIMAL( 15, 2 ) NOT NULL DEFAULT '0.00';

-- Populate new sales item columns
DROP TEMPORARY TABLE IF EXISTS temp_populate_sales_items;

CREATE TEMPORARY TABLE temp_populate_sales_items
SELECT
	foreup_sales_items.sale_id,
	foreup_sales_items.item_id,
	foreup_sales_items.line,
	quantity_purchased * item_cost_price AS total_cost,
	(item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100) as subtotal,
	ROUND((item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100)+
	ROUND((item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100)*
	(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2) 		+
	((ROUND((item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100)*
	(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2) +
	(item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100)) 		*
	(SUM(CASE WHEN cumulative = 1 THEN percent ELSE 0 END))/100),2) as total,
	ROUND((item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100)*
	(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2) 		+
	((ROUND((item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100)*(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2) +
	(item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100)) 		*
	(SUM(CASE WHEN cumulative = 1 THEN percent ELSE 0 END))/100) as tax, percent as percent,
	(item_unit_price*quantity_purchased-item_unit_price*quantity_purchased*discount_percent/100) -
	(item_cost_price*quantity_purchased) as profit
FROM foreup_sales_items
INNER JOIN foreup_sales
	ON  foreup_sales_items.sale_id = foreup_sales.sale_id
INNER JOIN foreup_items
	ON  foreup_sales_items.item_id = foreup_items.item_id
LEFT OUTER JOIN foreup_sales_items_taxes
	ON  foreup_sales_items.sale_id = foreup_sales_items_taxes.sale_id
  		AND foreup_sales_items.item_id = foreup_sales_items_taxes.item_id
  		AND foreup_sales_items.line = foreup_sales_items_taxes.line
GROUP BY foreup_sales_items.sale_id, foreup_sales_items.item_id, foreup_sales_items.line;

UPDATE foreup_sales_items AS i
INNER JOIN temp_populate_sales_items AS calc
	ON calc.sale_id = i.sale_id
	AND calc.item_id = i.item_id
	AND calc.line = i.line
SET
	i.subtotal = calc.subtotal,
	i.total = calc.total,
	i.tax = calc.tax,
	i.profit = calc.profit,
	i.total_cost = calc.total_cost;

-- Populate new sales item columns
DROP TEMPORARY TABLE IF EXISTS temp_populate_sales_item_kits;

CREATE TEMPORARY TABLE temp_populate_sales_item_kits
SELECT
	foreup_sales_item_kits.sale_id,
	foreup_sales_item_kits.item_kit_id,
	foreup_sales_item_kits.line,
	quantity_purchased * item_kit_cost_price AS total_cost,
	(item_kit_unit_price*quantity_purchased-item_kit_unit_price*quantity_purchased*discount_percent/100) as subtotal,
	ROUND((item_kit_unit_price*quantity_purchased-item_kit_unit_price*quantity_purchased*discount_percent/100)+
	ROUND((item_kit_unit_price*quantity_purchased-item_kit_unit_price*quantity_purchased*discount_percent/100)*
	(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2) 		+
	((ROUND((item_kit_unit_price*quantity_purchased-item_kit_unit_price*quantity_purchased*discount_percent/100)*
	(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2) +
	(item_kit_unit_price*quantity_purchased-item_kit_unit_price*quantity_purchased*discount_percent/100)) 		*
	(SUM(CASE WHEN cumulative = 1 THEN percent ELSE 0 END))/100),2) as total,
	ROUND((item_kit_unit_price*quantity_purchased-item_kit_unit_price*quantity_purchased*discount_percent/100)*
	(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2) 		+
	((ROUND((item_kit_unit_price*quantity_purchased-item_kit_unit_price*quantity_purchased*discount_percent/100)*
	(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2) +
	(item_kit_unit_price*quantity_purchased-item_kit_unit_price*quantity_purchased*discount_percent/100)) 		*
	(SUM(CASE WHEN cumulative = 1 THEN percent ELSE 0 END))/100) as tax, percent as percent,
	(item_kit_unit_price*quantity_purchased-item_kit_unit_price*quantity_purchased*discount_percent/100) -
	(item_kit_cost_price*quantity_purchased) as profit
FROM foreup_sales_item_kits
INNER JOIN foreup_sales
	ON  foreup_sales_item_kits.sale_id = foreup_sales.sale_id
INNER JOIN foreup_item_kits
	ON  foreup_sales_item_kits.item_kit_id = foreup_item_kits.item_kit_id
LEFT OUTER JOIN foreup_sales_item_kits_taxes
	ON  foreup_sales_item_kits.sale_id = foreup_sales_item_kits_taxes.sale_id
  		AND foreup_sales_item_kits.item_kit_id = foreup_sales_item_kits_taxes.item_kit_id
  		AND foreup_sales_item_kits.line = foreup_sales_item_kits_taxes.line
GROUP BY foreup_sales_item_kits.sale_id, foreup_sales_item_kits.item_kit_id, foreup_sales_item_kits.line;

UPDATE foreup_sales_item_kits AS i
INNER JOIN temp_populate_sales_item_kits AS calc
	ON calc.sale_id = i.sale_id
	AND calc.item_kit_id = i.item_kit_id
	AND calc.line = i.line
SET
	i.subtotal = calc.subtotal,
	i.total = calc.total,
	i.tax = calc.tax,
	i.profit = calc.profit,
	i.total_cost = calc.total_cost;

-- Populate new invoice columns
DROP TEMPORARY TABLE IF EXISTS temp_populate_sales_invoices;

CREATE TEMPORARY TABLE temp_populate_sales_invoices
SELECT
	foreup_sales_invoices.sale_id,
	foreup_sales_invoices.invoice_id,
	foreup_sales_invoices.line,
	0.00 AS total_cost,
	(invoice_unit_price*quantity_purchased-invoice_unit_price*quantity_purchased*discount_percent/100) as subtotal,
	ROUND((invoice_unit_price*quantity_purchased-invoice_unit_price*quantity_purchased*discount_percent/100)+
	ROUND((invoice_unit_price*quantity_purchased-invoice_unit_price*quantity_purchased*discount_percent/100)*
	(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2) 		+
	((ROUND((invoice_unit_price*quantity_purchased-invoice_unit_price*quantity_purchased*discount_percent/100)*
	(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2) +
	(invoice_unit_price*quantity_purchased-invoice_unit_price*quantity_purchased*discount_percent/100)) 		*
	(SUM(CASE WHEN cumulative = 1 THEN percent ELSE 0 END))/100),2) as total,
	ROUND((invoice_unit_price*quantity_purchased-invoice_unit_price*quantity_purchased*discount_percent/100)*
	(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2) 		+
	((ROUND((invoice_unit_price*quantity_purchased-invoice_unit_price*quantity_purchased*discount_percent/100)*
	(SUM(CASE WHEN cumulative != 1 THEN percent ELSE 0 END)/100),2) +
	(invoice_unit_price*quantity_purchased-invoice_unit_price*quantity_purchased*discount_percent/100)) 		*
	(SUM(CASE WHEN cumulative = 1 THEN percent ELSE 0 END))/100) as tax, percent as percent,
	(invoice_unit_price*quantity_purchased-invoice_unit_price*quantity_purchased*discount_percent/100) -
	(invoice_cost_price*quantity_purchased) as profit
FROM foreup_sales_invoices
INNER JOIN foreup_sales
	ON foreup_sales_invoices.sale_id = foreup_sales.sale_id
INNER JOIN foreup_invoices
	ON foreup_sales_invoices.invoice_id = foreup_invoices.invoice_id
LEFT OUTER JOIN foreup_sales_invoices_taxes
	ON foreup_sales_invoices.sale_id = foreup_sales_invoices_taxes.sale_id
	AND foreup_sales_invoices.invoice_id = foreup_sales_invoices_taxes.invoice_id
	AND foreup_sales_invoices.line = foreup_sales_invoices_taxes.line
GROUP BY foreup_sales_invoices.sale_id, foreup_sales_invoices.invoice_id, foreup_sales_invoices.line;

UPDATE foreup_sales_invoices AS i
INNER JOIN temp_populate_sales_invoices AS calc
	ON calc.sale_id = i.sale_id
	AND calc.invoice_id = i.invoice_id
	AND calc.line = i.line
SET
	i.subtotal = calc.subtotal,
	i.total = calc.total,
	i.tax = calc.tax,
	i.profit = calc.profit,
	i.total_cost = calc.total_cost;

	// SEPT 2 2013
	// ADDING TEE SHEET LIMITS
	CREATE TABLE  `ejacketa_pos`.`foreup_teesheet_restrictions` (
`teesheet_id` INT NOT NULL ,
`type` VARCHAR( 255 ) NOT NULL ,
`limit` VARCHAR( 255 ) NOT NULL ,
`m` TINYINT NOT NULL ,
`t` TINYINT NOT NULL ,
`w` TINYINT NOT NULL ,
`th` TINYINT NOT NULL ,
`f` TINYINT NOT NULL ,
`sa` TINYINT NOT NULL ,
`su` TINYINT NOT NULL ,
`start_time` SMALLINT NOT NULL
) ENGINE = INNODB;

	ALTER TABLE  `foreup_teesheet_restrictions` CHANGE  `m`  `Mon` TINYINT( 4 ) NOT NULL ,
CHANGE  `t`  `Tue` TINYINT( 4 ) NOT NULL ,
CHANGE  `w`  `Wed` TINYINT( 4 ) NOT NULL ,
CHANGE  `th`  `Thu` TINYINT( 4 ) NOT NULL ,
CHANGE  `f`  `Fri` TINYINT( 4 ) NOT NULL ,
CHANGE  `sa`  `Sat` TINYINT( 4 ) NOT NULL ,
CHANGE  `su`  `Sun` TINYINT( 4 ) NOT NULL
// 8/21/2013 Management Dashboard
CREATE TABLE IF NOT EXISTS `foreup_dashboards` (
  `dashboard_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `course_id` int(10) unsigned NOT NULL,
  `deleted` TINYINT(1) unsigned DEFAULT 0 NOT NULL,
  PRIMARY KEY (`dashboard_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0;

CREATE TABLE IF NOT EXISTS `foreup_dashboard_widgets` (
  `widget_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `period` varchar(24) NOT NULL,
  `data_grouping` varchar(8) NOT NULL,
  `pos_x` int(10) unsigned NOT NULL DEFAULT '0',
  `pos_y` int(10) unsigned NOT NULL DEFAULT '0',
  `width` int(10) unsigned NOT NULL DEFAULT '0',
  `height` int(10) unsigned NOT NULL DEFAULT '0',
  `dashboard_id` int(10) unsigned NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  `deleted` TINYINT(1) unsigned DEFAULT 0 NOT NULL,
  PRIMARY KEY (`widget_id`),
  KEY `metric_id` (`dashboard_id`,`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;

CREATE TABLE IF NOT EXISTS `foreup_dashboard_widget_metrics` (
  `widget_id` int(10) unsigned NOT NULL,
  `metric_category` varchar(255) NOT NULL,
  `metric` varchar(255) NOT NULL,
  `relative_period_num` tinyint(2) NOT NULL,
  `position` int(10) unsigned NOT NULL,
  PRIMARY KEY (`widget_id`,`metric_category`,`metric`,`relative_period_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET foreign_key_checks = 0;
UPDATE foreup_modules SET module_id = 'dashboards' WHERE module_id = 'dashboard';
UPDATE foreup_permissions SET module_id = 'dashboards' WHERE module_id = 'dashboard';
SET foreign_key_checks = 1;

UPDATE  `ejacketa_pos`.`foreup_modules` SET  `name_lang_key` =  'module_dashboards',
`desc_lang_key` =  'module_dashboards_desc' WHERE  `foreup_modules`.`module_id` =  'dashboards';

-- 9/17/2013
-- New table structure for images
DROP TABLE IF EXISTS `foreup_images`;
CREATE TABLE IF NOT EXISTS `foreup_images` (
  `image_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(11) unsigned NOT NULL,
  `module` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `width` int(5) unsigned NOT NULL,
  `height` int(5) unsigned NOT NULL,
  `filesize` decimal(7,2) unsigned NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `saved` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

-- Modify tables to attach images
ALTER TABLE `foreup_items` ADD `image_id` INT( 11 ) UNSIGNED NOT NULL AFTER `description`;
ALTER TABLE `foreup_customers` ADD `image_id` INT( 11 ) UNSIGNED NOT NULL AFTER `price_class`;
ALTER TABLE `foreup_employees` ADD `image_id` INT( 11 ) UNSIGNED NOT NULL AFTER `card`;

-- Add images field to campaign table to store overridden images
ALTER TABLE `foreup_marketing_campaigns` ADD `images` TEXT NOT NULL AFTER `image`;

-- 9/20/2013
-- New table for course management groups
CREATE TABLE IF NOT EXISTS `foreup_course_group_management` (
  `group_id` int(10) unsigned NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 10/3/2013
-- F&B Split Payment tables
CREATE TABLE IF NOT EXISTS `foreup_table_receipts` (
  `receipt_id` int(10) unsigned NOT NULL,
  `sale_id` int(10) unsigned NOT NULL,
  `date_paid` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sale_id`,`receipt_id`),
  KEY `sale_id` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `foreup_table_receipt_items` (
  `receipt_id` int(10) unsigned NOT NULL,
  `sale_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `line` smallint(4) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`receipt_id`,`sale_id`,`item_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `foreup_table_receipt_item_kits` (
  `receipt_id` int(10) unsigned NOT NULL,
  `sale_id` int(10) unsigned NOT NULL,
  `item_kit_id` int(10) unsigned NOT NULL,
  `line` smallint(4) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`receipt_id`,`sale_id`,`item_kit_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

10/7/13
ALTER TABLE  `foreup_rainchecks` ADD  `raincheck_number` INT NOT NULL AFTER  `raincheck_id`;
UPDATE  `foreup_rainchecks` SET raincheck_number = raincheck_id WHERE 1

10/11/2013
ALTER TABLE `foreup_table_items` ADD `seat` SMALLINT( 3 ) UNSIGNED NOT NULL AFTER `line`;

10/17/13
CREATE TABLE  `foreup_notes` (
`course_id` INT NOT NULL ,
`note_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`author` INT NOT NULL ,
`receiver` INT NOT NULL ,
`date` DATETIME NOT NULL ,
`message` TEXT NOT NULL ,
`deleted` TINYINT NOT NULL
) ENGINE = INNODB;

ALTER TABLE  `foreup_notes` CHANGE  `receiver`  `receiver` INT( 11 ) NULL DEFAULT NULL;
ALTER TABLE  `foreup_notes` CHANGE  `receiver`  `recipient` INT( 11 ) NULL DEFAULT NULL;

10/21/13
ALTER TABLE  `foreup_teesheet` ADD  `days_out` TINYINT NOT NULL AFTER  `frontnine` ,
ADD  `days_in_booking_window` TINYINT NOT NULL DEFAULT  '7' AFTER  `days_out` ,
ADD  `minimum_players` TINYINT NOT NULL DEFAULT  '2' AFTER  `days_in_booking_window` ,
ADD  `limit_holes` TINYINT NOT NULL AFTER  `minimum_players`

ALTER TABLE  `foreup_schedules` ADD  `days_out` TINYINT NOT NULL AFTER  `frontnine` ,
ADD  `days_in_booking_window` TINYINT NOT NULL DEFAULT  '7' AFTER  `days_out` ,
ADD  `minimum_players` TINYINT NOT NULL DEFAULT  '2' AFTER  `days_in_booking_window` ,
ADD  `limit_holes` TINYINT NOT NULL AFTER  `minimum_players`

10/22/13
ALTER TABLE  `foreup_courses` ADD  `webprnt` TINYINT NOT NULL AFTER  `print_after_sale`;
ALTER TABLE  `foreup_courses` ADD  `webprnt_ip` VARCHAR( 20 ) NOT NULL AFTER  `webprnt`;

10/30/13
ALTER TABLE  `foreup_courses` ADD  `blind_close` TINYINT NOT NULL AFTER  `track_cash`;

ALTER TABLE  `foreup_teesheet` ADD  `booking_carts` TINYINT NOT NULL AFTER  `limit_holes`;
ALTER TABLE  `foreup_schedules` ADD  `booking_carts` TINYINT NOT NULL AFTER  `limit_holes`;

ALTER TABLE  `foreup_employees` ADD  `zendesk` TINYINT NOT NULL AFTER  `last_login`;

11/7/13
ALTER TABLE  `foreup_invoice_items` ADD  `paid_off` DATETIME NOT NULL AFTER  `paid_amount`;
ALTER TABLE  `foreup_invoice_items` ADD  `person_id` INT NOT NULL AFTER  `paid_off`;

11/10/13
ALTER TABLE  `foreup_sales` ADD  `sale_number` INT NOT NULL AFTER  `course_id`;
ALTER TABLE  `ejacketa_pos`.`foreup_sales` ADD UNIQUE  `sale_number` (  `sale_number` ,  `course_id` );

-- 10/29/13
-- Foreup table tickets to track what F&B items were sent to kitchen
-- ---------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `foreup_table_tickets` (
  `sale_id` int(10) unsigned NOT NULL,
  `ticket_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_number` MEDIUMINT UNSIGNED NOT NULL,
  `employee_id` INT UNSIGNED NOT NULL,
  `printed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `completed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_ordered` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_completed` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ticket_id`),
  KEY `sale_id` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `foreup_table_ticket_items` (
  `ticket_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `line` smallint(3) unsigned NOT NULL,
  PRIMARY KEY (`ticket_id`,`item_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `foreup_table_ticket_item_kits` (
  `ticket_id` int(10) unsigned NOT NULL,
  `item_kit_id` int(10) unsigned NOT NULL,
  `line` smallint(3) unsigned NOT NULL,
  PRIMARY KEY (`ticket_id`,`item_kit_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 10/30/13
-- Now storing table_id (suspended sale ID for f&b) with sales
ALTER TABLE `foreup_sales` ADD `table_id` INT UNSIGNED NOT NULL AFTER `sale_id`;

-- 11/6/2013
-- Stores table numbers and layout info for editing restaurant layouts
CREATE TABLE IF NOT EXISTS `foreup_table_layouts` (
  `layout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `order` tinyint(3) unsigned NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`layout_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `foreup_table_layout_objects` (
  `object_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `layout_id` int(10) unsigned NOT NULL,
  `label` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pos_x` decimal(7,1) unsigned DEFAULT '0.0',
  `pos_y` decimal(7,1) unsigned DEFAULT '0.0',
  `width` smallint(4) unsigned DEFAULT NULL,
  `height` smallint(4) unsigned DEFAULT NULL,
  `rotation` smallint(3) unsigned DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`object_id`),
  UNIQUE KEY `object_label` (`layout_id`,`label`),
  KEY `layout_id` (`layout_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

11/13/13
ALTER TABLE  `foreup_teetime` ADD  `price_class_1` TINYINT NOT NULL AFTER  `person_id`;
ALTER TABLE  `foreup_teetime` ADD  `price_class_2` TINYINT NOT NULL AFTER  `person_id_2`;
ALTER TABLE  `foreup_teetime` ADD  `price_class_3` TINYINT NOT NULL AFTER  `person_id_3`;
ALTER TABLE  `foreup_teetime` ADD  `price_class_4` TINYINT NOT NULL AFTER  `person_id_4`;
ALTER TABLE  `foreup_teetime` ADD  `price_class_5` TINYINT NOT NULL AFTER  `person_id_5`;

11/18/13
ALTER TABLE  `foreup_account_transactions` ADD  `trans_household` INT NOT NULL AFTER  `trans_customer`;
ALTER TABLE  `foreup_member_account_transactions` ADD  `trans_household` INT NOT NULL AFTER  `trans_customer`;
CREATE TABLE `foreup_households` (
  `household_id` int(11) NOT NULL AUTO_INCREMENT,
  `household_head_id` int(11) NOT NULL,
  PRIMARY KEY (`household_id`),
  UNIQUE KEY `household_head_id` (`household_head_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
CREATE TABLE `foreup_household_members` (
  `household_id` int(11) NOT NULL,
  `household_member_id` int(11) NOT NULL,
  UNIQUE KEY `household_member_id` (`household_member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

// ONLINE BOOKING CLASSES
CREATE TABLE `foreup_booking_classes` (
  `booking_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `teesheet_id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price_class` varchar(255) NOT NULL,
  `online_booking_protected` tinyint(4) NOT NULL,
  `online_open_time` varchar(20) NOT NULL,
  `online_close_time` varchar(20) NOT NULL,
  `days_in_booking_window` smallint(6) NOT NULL,
  `minimum_players` tinyint(4) NOT NULL,
  `limit_holes` tinyint(4) NOT NULL,
  `booking_carts` tinyint(4) NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  PRIMARY KEY (`booking_class_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- Food and Beverage Changes 11/29/2013
ALTER TABLE `foreup_table_payments` ADD `receipt_id` INT UNSIGNED NOT NULL AFTER `sale_id`;
ALTER TABLE `foreup_table_payments` DROP PRIMARY KEY ,
ADD PRIMARY KEY ( `sale_id` , `receipt_id` , `payment_type` );

ALTER TABLE `foreup_table_receipts` CHANGE `status` VARCHAR( 16 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pending';

-- 12/4/2013
ALTER TABLE `foreup_tables` CHANGE `table_id` `table_id` MEDIUMINT( 4 ) NOT NULL;

12/6/2013
ALTER TABLE  `foreup_courses` ADD  `webprnt_hot_ip` VARCHAR( 30 ) NOT NULL AFTER  `webprnt_ip` ,
ADD  `webprnt_cold_ip` VARCHAR( 30 ) NOT NULL AFTER  `webprnt_hot_ip`

-- 12/6/2013
ALTER TABLE `foreup_table_items` ADD `is_ordered` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT '0',
ADD `is_paid` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT '0',
ADD `total_splits` SMALLINT( 5 ) UNSIGNED NOT NULL ,
ADD `paid_splits` SMALLINT( 5 ) UNSIGNED NOT NULL;

-- 12/9/2013
ALTER TABLE `foreup_table_payments` ADD `credit_card_invoice_id` INT UNSIGNED NOT NULL AFTER `payment_amount` ,
ADD INDEX ( `credit_card_invoice_id` );
=======
// 	12/9/13
ALTER TABLE  `foreup_items` ADD  `is_side` TINYINT NOT NULL ,
ADD  `add_on_price` FLOAT( 15, 2 ) NOT NULL ,
ADD  `print_priority` TINYINT NOT NULL ,
ADD  `kitchen_printer` TINYINT NOT NULL;

ALTER TABLE  `foreup_items` ADD  `number_of_sides` TINYINT NOT NULL DEFAULT  '0' AFTER  `food_and_beverage`;

CREATE TABLE  `foreup_item_sides` (
`item_id` INT NOT NULL ,
`side_id` INT NOT NULL ,
`not_available` TINYINT NOT NULL DEFAULT  '1',
`default` TINYINT NOT NULL ,
`upgrade_price` FLOAT( 15, 2 ) NOT NULL
) ENGINE = INNODB;

ALTER TABLE  `foreup_item_sides` ADD UNIQUE  `item_side` (  `item_id` ,  `side_id` );

// 12/12/13
CREATE TABLE  `foreup_register_log_counts` (
`register_log_id` INT NOT NULL ,
`change` FLOAT( 15, 2 ) NOT NULL ,
`ones` SMALLINT NOT NULL ,
`fives` SMALLINT NOT NULL ,
`tens` SMALLINT NOT NULL ,
`twenties` SMALLINT NOT NULL ,
`fifties` SMALLINT NOT NULL ,
`hundreds` SMALLINT NOT NULL
) ENGINE = INNODB;

ALTER TABLE  `foreup_customers` ADD  `use_loyalty` TINYINT NOT NULL DEFAULT  '1' AFTER  `member_account_balance_allow_negative`;
ALTER TABLE  `foreup_modifiers` ADD  `category_id` TINYINT NOT NULL DEFAULT  '1' AFTER  `name`;
ALTER TABLE  `foreup_modifiers` ADD  `required` TINYINT NOT NULL AFTER  `category_id`;
ALTER TABLE  `foreup_item_modifiers` ADD  `override_required` TINYINT NOT NULL AFTER  `override_price`;
ALTER TABLE  `foreup_courses` ADD  `hide_back_nine` TINYINT NOT NULL AFTER  `number_of_items_per_page`;

-- 12/14/2013
-- Changes to persist item sides and clean up some DB structure for F&B

-- Table item taxes is redundant and un-needed
DROP TABLE `foreup_table_items_taxes`;

-- Dropped unused fields table_item_modifiers table
-- This table will only track which modifier option has been selected for a cart item
DROP TABLE IF EXISTS `foreup_table_items_modifiers`;
CREATE TABLE IF NOT EXISTS `foreup_table_items_modifiers` (
  `sale_id` int(10) unsigned NOT NULL,
  `modifier_id` int(10) unsigned NOT NULL,
  `line` int(10) unsigned NOT NULL,
  `option` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`sale_id`,`modifier_id`,`line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `foreup_table_item_side_modifiers` (
  `sale_id` int(10) unsigned NOT NULL,
  `modifier_id` int(10) unsigned NOT NULL,
  `line` int(10) unsigned NOT NULL,
  `side_type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `side_position` smallint(5) unsigned NOT NULL,
  `option` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`sale_id`,`line`,`side_type`,`side_position`,`modifier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `foreup_table_item_sides` (
  `sale_id` int(10) unsigned NOT NULL,
  `cart_line` smallint(5) unsigned NOT NULL,
  `position` smallint(5) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`sale_id`,`cart_line`,`position`,`type`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

//12/16/13

CREATE TABLE  `foreup_giftcard_transactions` (
`trans_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`course_id` INT NOT NULL ,
`trans_customer` INT NOT NULL ,
`trans_user` INT NOT NULL ,
`trans_date` TIMESTAMP NOT NULL ,
`trans_comment` TEXT NOT NULL ,
`trans_description` TEXT NOT NULL ,
`trans_amount` DECIMAL( 15, 2 ) NOT NULL
) ENGINE = INNODB;
ALTER TABLE  `foreup_giftcard_transactions` ADD  `trans_giftcard` INT NOT NULL AFTER  `course_id`;
ALTER TABLE  `foreup_giftcards` ADD  `date_issued` DATE NOT NULL AFTER  `details`;

//12/17/13
ALTER TABLE  `foreup_terminals` ADD  `mercury_id` VARCHAR( 255 ) NOT NULL ,
ADD  `mercury_password` VARCHAR( 255 ) NOT NULL ,
ADD  `ets_key` VARCHAR( 255 ) NOT NULL ,
ADD  `auto_print_receipts` TINYINT NULL DEFAULT NULL,
ADD  `receipt_ip` VARCHAR( 20 ) NOT NULL ,
ADD  `hot_webprnt_ip` VARCHAR( 20 ) NOT NULL ,
ADD  `cold_webprnt_ip` VARCHAR( 20 ) NOT NULL ,
ADD  `use_register_log` TINYINT NULL DEFAULT NULL,
ADD  `cash_register` TINYINT NULL DEFAULT NULL,
ADD  `print_tip_line` TINYINT NULL DEFAULT NULL,
ADD  `signature_slip_count` TINYINT NULL DEFAULT NULL,
ADD  `credit_card_receipt_count` TINYINT NULL DEFAULT NULL,
ADD  `non_credit_card_receipt_count` TINYINT NULL DEFAULT NULL

ALTER TABLE  `foreup_items` ADD  `soup_or_salad` ENUM(  'none',  'soup',  'salad',  'either',  'both' ) NOT NULL AFTER  `food_and_beverage`;

-- 12/17/2013
-- Removed modifier auto_select, changing it to default which will store label of default option
ALTER TABLE `foreup_item_modifiers` DROP `auto_select`,
ADD `default` VARCHAR( 128 ) NULL DEFAULT NULL AFTER `override_required`;

-- No need to store selected price, we can get price using selected option
ALTER TABLE `foreup_table_items_modifiers` DROP `price`;
ALTER TABLE `foreup_table_item_side_modifiers` DROP `price`;

-- 12/19/2013
ALTER TABLE  `foreup_table_items` CHANGE  `description`  `comments` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

-- No need to store this data in table_items, we can get it from item table when completing sale
ALTER TABLE `foreup_table_items` DROP `serialnumber`, DROP `item_cost_price`;

// 12/22/13
ALTER TABLE  `foreup_items` ADD  `gl_code` VARCHAR( 20 ) NULL AFTER  `item_id`;

-- 12/27/2013
-- Extra message field for kitchen tickets (Do not make, etc)
ALTER TABLE `foreup_table_tickets` ADD `message` VARCHAR( 255 ) NULL AFTER `table_number`;

-- 1/2/2014
ALTER TABLE `foreup_sales_items` ADD `num_splits` SMALLINT UNSIGNED NOT NULL DEFAULT '1' AFTER `quantity_purchased` ,
ADD `is_side` TINYINT UNSIGNED NOT NULL DEFAULT '0' AFTER `num_splits`;

//1/6/2014
ALTER TABLE  `foreup_teetime` ADD  `raincheck_players_issued` TINYINT NOT NULL AFTER  `api_id`;
ALTER TABLE  `foreup_rainchecks` ADD  `teetime_id` VARCHAR( 40 ) NOT NULL AFTER  `teesheet_id`;


//1/7/2014
ALTER TABLE  `foreup_invoices` ADD  `show_overdue_items` TINYINT NOT NULL DEFAULT '1' AFTER  `include_itemized_sales`;
ALTER TABLE  `foreup_customer_billing` ADD  `show_overdue_items` TINYINT NOT NULL DEFAULT '1' AFTER  `include_itemized_sales`;

-- 1/10/2014
CREATE TABLE IF NOT EXISTS `foreup_event_people` (
  `teetime_id` varchar(21) COLLATE utf8_unicode_ci NOT NULL,
  `person_id` int(10) unsigned NOT NULL,
  `checked_in` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `paid` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `cart` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_checked_in` datetime DEFAULT NULL,
  `date_paid` datetime DEFAULT NULL,
  `player_position` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `hole` tinyint(3) unsigned NOT NULL DEFAULT '0',
  KEY `teetime_id` (`teetime_id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 1/13/2014
ALTER TABLE `foreup_teetime`  ADD `default_cart_fee` SMALLINT UNSIGNED NOT NULL AFTER `person_name_5`,
ADD `default_price_category` SMALLINT UNSIGNED NOT NULL AFTER `default_cart_fee`;

-- 1/20/2014
CREATE TABLE IF NOT EXISTS `foreup_price_colors` (
  `course_id` int(10) unsigned NOT NULL,
  `price_category` smallint(5) unsigned NOT NULL,
  `color` varchar(7) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`course_id`,`price_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

//1/21/2014
ALTER TABLE  `foreup_course_groups` ADD  `shared_tee_sheet` TINYINT NOT NULL DEFAULT  '1' AFTER  `type` ,
ADD  `shared_customers` TINYINT NOT NULL DEFAULT  '1' AFTER  `shared_tee_sheet` ,
ADD  `shared_giftcards` TINYINT NOT NULL DEFAULT  '1' AFTER  `shared_customers`

//1/23/2014
CREATE TABLE `foreup_inventory_audits` (
  `inventory_audit_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`inventory_audit_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE `foreup_inventory_audit_items` (
  `inventory_audit_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `current_count` smallint(6) NOT NULL,
  `manual_count` smallint(6) NOT NULL,
  KEY `inventory_audit_id` (`inventory_audit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE  `foreup_timeclock_entries` ADD  `terminal_id` INT NOT NULL AFTER  `employee_id`;

-- 1/27/2014
-- Customer list query speedups
ALTER TABLE foreup_people DROP INDEX first_name, DROP INDEX last_name;
ALTER TABLE `foreup_people` ADD INDEX  `person_orderby` (  `last_name` ,  `first_name` ,  `person_id` );

--- 1/3/2014
--- Quickbooks Updates
ALTER TABLE `foreup_quickbooks_accounts` ADD `parent_quickbooks_id` VARCHAR( 64 ) NOT NULL AFTER `quickbooks_id` ,
ADD `description` VARCHAR( 255 ) NOT NULL AFTER `parent_quickbooks_id` ,
ADD `bank_number` VARCHAR( 64 ) NOT NULL AFTER `description` ,
ADD `sub_level` TINYINT UNSIGNED NOT NULL AFTER `bank_number` ,
ADD `account_number` VARCHAR( 12 ) NOT NULL AFTER `sub_level`;

ALTER TABLE `foreup_items` ADD `quickbooks_income` VARCHAR( 32 ) NOT NULL AFTER `kitchen_printer` ,
ADD `quickbooks_cogs` VARCHAR( 32 ) NOT NULL AFTER `quickbooks_income`;

ALTER TABLE `foreup_quickbooks_accounts`
  DROP `is_sales`,
  DROP `is_redemption`,
  DROP `is_tips`;

ALTER TABLE `foreup_quickbooks_accounts` ADD `map_to` VARCHAR( 32 ) NOT NULL AFTER `type`;

-- 1/6/2014
ALTER TABLE `foreup_quickbooks_accounts` ADD `full_name` VARCHAR( 1024 ) NOT NULL AFTER `name`;
ALTER TABLE `foreup_items` CHANGE `quickbooks_income` `quickbooks_income` VARCHAR( 1024 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
CHANGE `quickbooks_cogs` `quickbooks_cogs` VARCHAR( 1024 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `foreup_items` ADD `quickbooks_assets` VARCHAR( 1024 ) NOT NULL AFTER `quickbooks_cogs`;

-- 1/7/2014
ALTER TABLE `foreup_item_kits` ADD `quickbooks_income` VARCHAR( 1024 ) NOT NULL AFTER `cost_price` ,
ADD `quickbooks_cogs` VARCHAR( 1024 ) NOT NULL AFTER `quickbooks_income`,
ADD `quickbooks_assets` VARCHAR( 1024 ) NOT NULL AFTER `quickbooks_cogs`;

// 1/31/14
ALTER TABLE  `foreup_invoices` ADD  `overdue_total` DECIMAL( 15, 2 ) NOT NULL AFTER  `total`;


//2/7/14
ALTER TABLE  `foreup_invoices` ADD  `previous_payments` DECIMAL( 15, 2 ) NOT NULL AFTER  `overdue_total`

//2/10/14
ALTER TABLE  `foreup_register_log` ADD  `persist` TINYINT NOT NULL ,
ADD  `closing_employee_id` INT NOT NULL

-- 2/4/2014
ALTER TABLE  `foreup_teesheet` ADD  `require_credit_card` TINYINT( 1 ) UNSIGNED NOT NULL AFTER  `online_booking`;
ALTER TABLE `foreup_courses` ADD `no_show_policy` VARCHAR( 2048 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

-- 2/6/2014
ALTER TABLE  `foreup_courses` ADD  `reservation_email_text` TEXT NOT NULL ,
ADD  `reservation_email_photo` INT UNSIGNED NOT NULL;

//2/19/2014
ALTER TABLE  `foreup_terminals` ADD  `webprnt` TINYINT DEFAULT NULL AFTER  `auto_print_receipts`;

-- 2/21/2014
CREATE TABLE IF NOT EXISTS `foreup_table_customers` (
  `sale_id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`sale_id`,`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 2/24/2012
ALTER TABLE  `foreup_table_payments` ADD  `customer_id` INT UNSIGNED NOT NULL AFTER  `payment_amount`;

-- 1/22/2014
ALTER TABLE  `foreup_invoice_items` ADD  `item_id` INT UNSIGNED NOT NULL AFTER  `line_number` ,
ADD INDEX (  `item_id` );

ALTER TABLE  `foreup_customer_billing_items` ADD  `item_id` INT UNSIGNED NOT NULL AFTER  `line_number` ,
ADD INDEX (  `item_id` );

-- 1/24/2014
-- Removing constraint which allows us to place sales_items with an item_id of 0 (for custom invoice items)
ALTER TABLE  `foreup_sales_items` DROP FOREIGN KEY  `foreup_sales_items_ibfk_1` ;

-- 1/28/2014
-- Add invoice field to sales item kits
ALTER TABLE  `foreup_sales_item_kits` ADD  `invoice_id` INT UNSIGNED NOT NULL AFTER  `item_kit_id`;
customercredit
-- 2/10/2014
ALTER TABLE  `foreup_invoices` CHANGE  `employee` `employee_id` INT UNSIGNED NOT NULL;
ALTER TABLE  `foreup_member_account_transactions` ADD  `invoice_id` INT UNSIGNED NOT NULL AFTER  `course_id`;
ALTER TABLE  `foreup_member_account_transactions` ADD INDEX (  `invoice_id` );

-- 2/12/2014
-- Adjust foreup account transactions table to store transactions from all accounts
ALTER TABLE `foreup_account_transactions`
DROP `CID`,
ADD  `sale_id` INT UNSIGNED NOT NULL AFTER  `trans_id`,
ADD  `invoice_id` INT UNSIGNED NOT NULL AFTER  `sale_id`,
ADD `account_type` VARCHAR(24) FIRST;

ALTER TABLE  `foreup_account_transactions` ADD INDEX (  `sale_id` );
ALTER TABLE  `foreup_account_transactions` ADD INDEX (  `invoice_id` );

UPDATE `foreup_account_transactions` SET account_type = 'customer';

-- 2/12/2014
-- Merge member account transactions into account table
INSERT INTO `foreup_account_transactions`
(account_type, trans_id, course_id, trans_customer, trans_household, trans_user, trans_date, trans_comment, trans_description, trans_amount)
	SELECT 'member' AS account_type, NULL AS trans_id, 0 AS course_id, t.trans_customer,
		t.trans_household, t.trans_user, t.trans_date, t.trans_comment, t.trans_description,
		t.trans_amount
	FROM foreup_member_account_transactions AS t
	GROUP BY t.trans_id;

-- 2/12/2014
-- Extract out sale_ids from transaction comment and place them in their proper column
UPDATE foreup_account_transactions
SET sale_id = RIGHT(trans_comment, POSITION(' ' IN trans_comment))
WHERE trans_comment LIKE '%POS%';

-- 2/12/2014
-- Bug found: Transactions have no course_id associated with them, find
-- the course_id for each transaction and update the row
UPDATE foreup_account_transactions AS t
INNER JOIN foreup_employees AS e
ON e.person_id = t.trans_user
SET t.course_id = e.course_id;

-- 2/12/2014
ALTER TABLE  `foreup_invoices` DROP  `include_itemized_sales` ,
DROP  `show_overdue_items`;

ALTER TABLE  `foreup_invoices` ADD  `pay_customer_account` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT  '0',
ADD  `pay_member_account` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT  '0',
ADD  `show_account_transactions` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT  '0';

-- 2/14/2014
ALTER TABLE  `foreup_customer_billing_items` CHANGE  `tax`  `tax_percentage` FLOAT( 15, 2 ) NOT NULL;
ALTER TABLE  `foreup_customer_billing_items` ADD  `item_type` VARCHAR( 24 ) NOT NULL AFTER  `item_id`;

ALTER TABLE  `foreup_customer_billing_items` CHANGE  `item_type`  `item_type` VARCHAR( 24 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
CHANGE  `description`  `description` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE  `foreup_customer_billing_items` CHANGE  `amount`  `price` FLOAT( 15, 2 ) NOT NULL;

ALTER TABLE  `foreup_invoices`
ADD  `overdue` DECIMAL( 15, 2 ) NOT NULL AFTER  `paid`,
CHANGE  `total`  `total` DECIMAL( 15, 2 ) NOT NULL ,
CHANGE  `paid`  `paid` DECIMAL( 15, 2 ) NOT NULL,
ADD  `bill_start` DATETIME NOT NULL AFTER  `date`,
ADD  `bill_end` DATETIME NOT NULL AFTER  `bill_start`,
DROP `month_billed`;

--2/17/2014
ALTER TABLE  `foreup_customer_billing` ADD  `stop_after` SMALLINT UNSIGNED NULL AFTER  `frequency`;
ALTER TABLE  `foreup_customer_billing_items` CHANGE  `line_number`  `line` TINYINT( 4 ) UNSIGNED NOT NULL;

ALTER TABLE  `foreup_customer_billing` DROP  `start_month` ,
DROP  `end_month` ,
DROP  `pay_account_balance` ,
DROP  `pay_member_balance` ,
DROP  `month` ,
DROP  `day` ,
DROP  `generate_days_before` ,
DROP  `include_itemized_sales` ,
DROP  `show_overdue_items` ;

ALTER TABLE  `foreup_customer_billing` CHANGE  `frequency`  `frequency` SMALLINT( 6 ) UNSIGNED NOT NULL;

-- 2/18/2014
ALTER TABLE  `foreup_customer_billing`
ADD  `show_account_transactions` TINYINT(1) NOT NULL DEFAULT 0,
ADD  `end_date` DATE NOT NULL AFTER  `start_date`,
ADD  `frequency_period` VARCHAR( 24 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER  `frequency` ,
ADD  `frequency_on` VARCHAR( 24 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER  `frequency_period` ,
ADD  `frequency_on_date` DATE NOT NULL AFTER  `frequency_on`;

-- 2/20/2014
ALTER TABLE `foreup_customer_billing` ADD  `due_days` SMALLINT UNSIGNED NOT NULL AFTER  `frequency_on_date`;
-------- RUN UP TO HERE
-- Get some tasty indexes on our invoice tables
ALTER TABLE  `foreup_invoices`
ADD INDEX (  `credit_card_payment_id` ),
ADD INDEX (  `course_id` ),
ADD INDEX (  `person_id` ),
ADD INDEX (  `billing_id` ),
ADD INDEX (  `date` );

ALTER TABLE  `foreup_customer_billing`
ADD INDEX (  `course_id` ),
ADD INDEX (  `person_id` ),
ADD INDEX (  `start_date` ,  `end_date` );

-- 2/27/2014
-- Add a total field on customers row that will track outstanding invoice amount
ALTER TABLE  `foreup_customers` ADD  `invoice_balance` DECIMAL( 15, 2 ) NOT NULL AFTER  `member_account_balance_allow_negative`;

-- 2/28/2014
ALTER TABLE  `foreup_invoices` ADD  `sale_id` INT UNSIGNED NOT NULL;

 --- 10/9/13
 --- Seasonal Pricing Tables
  CREATE TABLE IF NOT EXISTS `foreup_seasonal_prices` (
 `price_id` int(11) NOT NULL AUTO_INCREMENT,
 `teesheet_id` int(11) NOT NULL,
 `schedule_id` int(11) NOT NULL,
 `season` varchar(255) NOT NULL,
 `start_time` smallint(4) NOT NULL,
 `end_time` smallint(4) NOT NULL,
`price_category` int(11) NOT NULL,
`price1` int(11) NOT NULL,
 `price2` int(11) NOT NULL,
 `price3` int(11) NOT NULL,
 `price4` int(11) NOT NULL,
 `price5` int(11) NOT NULL,
 `price6` int(11) NOT NULL,
 `monday` tinyint(1) NOT NULL,
 `tuesday` tinyint(1) NOT NULL,
 `wednesday` tinyint(1) NOT NULL,
 `thursday` tinyint(1) NOT NULL,
 `friday` tinyint(1) NOT NULL,
 `saturday` tinyint(1) NOT NULL,
 `sunday` tinyint(1) NOT NULL,
 PRIMARY KEY (`price_id`)
 ) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
 
 CREATE TABLE IF NOT EXISTS `foreup_seasons` (
 `season_id` int(11) NOT NULL AUTO_INCREMENT,
 `course_id` int(11) NOT NULL,
 `season_name` int(11) NOT NULL,
 `start_date` date NOT NULL,
 `end_date` date NOT NULL,
 PRIMARY KEY (`season_id`)
 ) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
 
ALTER TABLE  `foreup_courses` ADD  `seasonal_pricing` TINYINT( 1 ) NOT NULL DEFAULT '0' AFTER  `send_text_reminder`;
 
3/5/14
ALTER TABLE  `foreup_courses` ADD  `auto_split_teetimes` TINYINT NOT NULL AFTER  `send_text_reminder`;

3/6/14
ALTER TABLE  `foreup_courses` ADD  `e2e_account_id` VARCHAR( 255 ) NOT NULL AFTER  `ets_key` ,
ADD  `e2e_account_key` VARCHAR( 255 ) NOT NULL AFTER  `e2e_account_id`;

ALTER TABLE  `foreup_terminals` ADD  `e2e_account_id` VARCHAR( 255 ) NOT NULL AFTER  `ets_key` ,
ADD  `e2e_account_key` VARCHAR( 255 ) NOT NULL AFTER  `e2e_account_id`

3/28/14
ALTER TABLE  `foreup_courses` ADD  `use_ets_giftcards` TINYINT NOT NULL AFTER  `e2e_account_key`
-- NEW SEASONAL PRICING
-- 3/18/2014
DROP TABLE IF EXISTS foreup_price_classes;

CREATE TABLE IF NOT EXISTS `foreup_price_classes` (
  `class_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cart` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `color` char(7) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`class_id`),
  UNIQUE KEY `course_id` (`course_id`,`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 3/20/2014
DROP TABLE IF EXISTS foreup_seasonal_prices;
DROP TABLE IF EXISTS foreup_seasonal_timeframes;
DROP TABLE IF EXISTS foreup_seasons;

CREATE TABLE IF NOT EXISTS `foreup_seasonal_timeframes` (
  `timeframe_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_id` int(10) unsigned NOT NULL,
  `season_id` int(10) unsigned NOT NULL,
  `timeframe_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `monday` tinyint(1) unsigned NOT NULL,
  `tuesday` tinyint(1) unsigned NOT NULL,
  `wednesday` tinyint(1) unsigned NOT NULL,
  `thursday` tinyint(1) unsigned NOT NULL,
  `friday` tinyint(1) unsigned NOT NULL,
  `saturday` tinyint(1) unsigned NOT NULL,
  `sunday` tinyint(1) unsigned NOT NULL,
  `price1` decimal(15,2) NOT NULL DEFAULT '0.00',
  `price2` decimal(15,2) NOT NULL DEFAULT '0.00',
  `price3` decimal(15,2) NOT NULL DEFAULT '0.00',
  `price4` decimal(15,2) NOT NULL DEFAULT '0.00',
  `price5` decimal(15,2) NOT NULL DEFAULT '0.00',
  `price6` decimal(15,2) NOT NULL DEFAULT '0.00',
  `start_time` smallint(5) unsigned NOT NULL,
  `end_time` smallint(5) unsigned NOT NULL,
  `default` tinyint(1) unsigned NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`timeframe_id`),
  KEY `class_id` (`class_id`),
  KEY `season_id` (`season_id`),
  KEY `start_time` (`start_time`,`end_time`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `foreup_seasons` (
  `season_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(10) unsigned NOT NULL,
  `teesheet_id` int(10) unsigned NOT NULL,
  `season_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `holiday` tinyint(1) unsigned NOT NULL,
  `increment` decimal(15,2) NOT NULL,
  `front_nine` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `online_booking` tinyint(1) unsigned NOT NULL,
  `online_open_time` smallint(5) unsigned NOT NULL,
  `online_close_time` smallint(5) unsigned NOT NULL,
  `default` tinyint(1) unsigned NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`season_id`),
  KEY `start_date` (`start_date`,`end_date`),
  KEY `holiday` (`holiday`),
  KEY `course_id` (`course_id`),
  KEY `online_open_time` (`online_open_time`,`online_close_time`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 3/27/2014
ALTER TABLE  `foreup_rainchecks` ADD  `teetime_date` DATE NOT NULL DEFAULT  '0000-00-00' AFTER  `teetime_id` ,
ADD  `teetime_time` SMALLINT UNSIGNED NOT NULL DEFAULT  '0' AFTER  `teetime_date`;

-- END NEW SEASONAL PRICING CHANGES

-- 3/31/2014
ALTER TABLE  `foreup_courses` ADD  `last_invoices_sent` DATE NOT NULL AFTER  `billing_email`;

-- 4/3/2014
ALTER TABLE  `foreup_courses` ADD  `deduct_tips` TINYINT NOT NULL AFTER  `separate_courses`;
-- 4/4/2014
-- Seasonal pricing tweaks
-- New season price class table since price classes can be attached
-- to seasons without a default timeframe
DROP TABLE IF EXISTS `foreup_season_price_classes`;
CREATE TABLE IF NOT EXISTS `foreup_season_price_classes` (
  `season_id` int(10) unsigned NOT NULL,
  `class_id` int(10) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`season_id`,`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Delete all default timeframes that are attached to non default price classes
DELETE timeframe.*
FROM foreup_seasonal_timeframes AS timeframe
LEFT JOIN foreup_price_classes AS price_class
	ON price_class.class_id = timeframe.class_id
	AND price_class.default = 1
WHERE price_class.class_id IS NULL
	AND timeframe.default = 1;

-- Transfer any price classes attached to timeframes into new relationship table
-- since price classes can now be attached to a season without a timeframe
INSERT INTO foreup_season_price_classes	
SELECT season_id, class_id, NOW() AS date_created
FROM foreup_seasonal_timeframes
GROUP BY season_id, class_id;

ALTER TABLE  `foreup_seasonal_timeframes` CHANGE  `deleted`  `active` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT  '1';
UPDATE `foreup_seasonal_timeframes` SET active = 1;

-- END seasonal pricing tweaks

-- 4/7/2014
-- Tweaks to allow customers to pay from invoice account
ALTER TABLE  `foreup_customers` ADD  `invoice_balance_allow_negative` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT  '0' AFTER  `invoice_balance` ;

-- 4/14/2014
ALTER TABLE  `foreup_reservations` ADD  `send_confirmation` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT  '0' AFTER  `date_cancelled`;

-- 4/15/2014 (I hope you submitted your taxes...)
ALTER TABLE  `foreup_courses` ADD  `include_tax_online_booking` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT  '0' AFTER  `seasonal_pricing`;

-- 4/18/2014
-- F&B Table nickname feature
ALTER TABLE  `foreup_tables` ADD  `name` VARCHAR( 255 ) NOT NULL AFTER  `table_id`;

4/27/14
ALTER TABLE  `foreup_teesheet` ADD  `color` CHAR( 7 ) NOT NULL AFTER  `default_price_class`

4/29/14
ALTER TABLE  `foreup_sales_suspended` CHANGE  `table_id`  `table_id` INT NOT NULL

5/1/14
ALTER TABLE  `foreup_courses` ADD  `teesheet_refresh_rate` TINYINT NOT NULL DEFAULT  '90' AFTER  `auto_split_teetimes`;

5/6/14
CREATE TABLE  `ejacketa_pos`.`foreup_employee_permissions` (
`person_id` INT NOT NULL ,
`sales_stats` TINYINT NOT NULL ,
UNIQUE (
`person_id`
)
) ENGINE = INNODB;

ALTER TABLE  `foreup_items` ADD  `unit_price_includes_tax` TINYINT NOT NULL AFTER  `unit_price`;

ALTER TABLE  `foreup_sales_items` ADD  `unit_price_includes_tax` TINYINT NOT NULL AFTER  `item_unit_price`

// 5/12/14
CREATE TABLE  `foreup_invoice_changes` (
`invoice_id` INT NOT NULL ,
`course_id` INT NOT NULL ,
`person_id` INT NOT NULL ,
`date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
`previous_paid` FLOAT( 15, 2 ) NOT NULL ,
`paid` FLOAT( 15, 2 ) NOT NULL ,
`previous_overdue` FLOAT( 15, 2 ) NOT NULL ,
`overdue` FLOAT( 15, 2 ) NOT NULL ,
INDEX (  `invoice_id` ,  `course_id` ,  `person_id` )
) ENGINE = INNODB;

-- 5/9/2014 F&B Tweaks to allow any characters as table numbers
ALTER TABLE  `foreup_tables` CHANGE  `table_id`  `table_id` VARCHAR( 255 ) NOT NULL;
ALTER TABLE  `foreup_sales` CHANGE  `table_id`  `table_id` VARCHAR( 255 ) NOT NULL;


// 5/19/14
ALTER TABLE  `foreup_customer_credit_cards` ADD  `ets_key` VARCHAR( 255 ) NOT NULL AFTER  `credit_card_id` ,
ADD  `mercury_id` VARCHAR( 255 ) NOT NULL AFTER  `ets_key` ,
ADD  `mercury_password` VARCHAR( 255 ) NOT NULL AFTER  `mercury_id`;
// UPDATE EXISTING CREDIT CARDS
UPDATE `foreup_customer_credit_cards` AS ccc JOIN foreup_courses AS c ON ccc.course_id = c.course_id SET ccc.ets_key = c.ets_key, ccc.mercury_id = c.mercury_id, ccc.mercury_password = c.mercury_password WHERE 1


// 5/21/14
ALTER TABLE  `foreup_credit_card_transaction_steps` ADD  `course_id` INT NOT NULL FIRST;

ALTER TABLE  `foreup_sessions` CHANGE  `user_data`  `user_data` MEDIUMTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

//5/23/14
CREATE TABLE  `foreup_printers` (
`printer_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`course_id` INT NOT NULL ,
`label` VARCHAR( 255 ) NOT NULL ,
`ip_address` VARCHAR( 255 ) NOT NULL ,
INDEX (  `course_id` )
) ENGINE = INNODB;

CREATE TABLE  `foreup_item_printers` (
`item_id` INT NOT NULL ,
`printer_id` INT NOT NULL ,
INDEX (  `item_id` ,  `printer_id` )
) ENGINE = INNODB;

//5/26/14
ALTER TABLE  `foreup_courses` ADD  `maintenance_mode` TINYINT NOT NULL AFTER  `course_id`;


ALTER TABLE  `foreup_item_printers` ADD UNIQUE  `item_printer` (  `item_id` ,  `printer_id` );

//5/28/14
ALTER TABLE  `foreup_customer_credit_cards` ADD  `ets_key` VARCHAR( 255 ) NOT NULL AFTER  `credit_card_id` ,
ADD  `mercury_id` VARCHAR( 255 ) NOT NULL AFTER  `ets_key` ,
ADD  `mercury_password` VARCHAR( 255 ) NOT NULL AFTER  `mercury_id`;
//6/3/14
CREATE TABLE `foreup_sendhub_messages` (
`campaign_id` INT NOT NULL ,
`message_id` INT NOT NULL ,
`status` VARCHAR( 255 ) NOT NULL ,
`sent` DATETIME NOT NULL ,
`confirmed` TINYINT NOT NULL ,
INDEX (  `campaign_id` ,  `message_id` )
) ENGINE = INNODB;

ALTER TABLE `foreup_sendhub_messages` ADD  `id` INT NOT NULL AFTER  `campaign_id`;

CREATE TABLE `foreup_sendhub_message_contacts` (
`message_id` INT NOT NULL ,
`sendhub_id` INT NOT NULL ,
INDEX (  `message_id` ,  `sendhub_id` )
) ENGINE = INNODB;

//6/4/14
ALTER TABLE  `foreup_customers` ADD  `discount` FLOAT( 5, 2 ) NOT NULL AFTER  `taxable`;

-- 6/4/2014
-- iBeacon changes
-- API key for iBeacon calls
INSERT INTO `foreup_dev`.`foreup_api_keys` (`id`, `key`, `name`, `level`, `ignore_limits`, `date_created`) 
VALUES (NULL, MD5('iBeaconKey:6/4/2014'), 'iBeacon', '10', '1', UNIX_TIMESTAMP());

-- Table to track ibeacon calls
CREATE TABLE IF NOT EXISTS `foreup_ibeacon` (
  `beacon_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `major_id` mediumint(8) unsigned NOT NULL,
  `minor_id` mediumint(8) unsigned NOT NULL,
  `person_id` int(10) unsigned NOT NULL,
  `course_id` int(10) unsigned NOT NULL,
  `terminal_id` int(10) unsigned NOT NULL,
  `type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `received` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`beacon_id`),
  KEY `course_id` (`course_id`,`terminal_id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

-- Course settings to enable ibeacon interaction
ALTER TABLE  `foreup_courses` ADD  `ibeacon_major_id` MEDIUMINT UNSIGNED NOT NULL DEFAULT  '0',
ADD  `ibeacon_enabled` TINYINT( 1 ) NOT NULL DEFAULT  '0';

-- ibeacon terminal setting
ALTER TABLE  `foreup_terminals` ADD  `ibeacon_minor_id` MEDIUMINT UNSIGNED NOT NULL DEFAULT  '0' AFTER  `non_credit_card_receipt_count`;

//6/6/2014

ALTER TABLE `foreup_register_log_counts` CHANGE `change` `pennies` SMALLINT(6) NOT NULL;

ALTER TABLE `foreup_register_log_counts` ADD `nickels` SMALLINT(6) NOT NULL AFTER `pennies`, ADD `dimes` SMALLINT(6) NOT NULL AFTER `nickels`, ADD `quarters` SMALLINT(6) NOT NULL AFTER `dimes`;

//6/11/2014
ALTER TABLE `foreup_rainchecks` ADD `expiry_date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00';
    
//6/12/2014
ALTER TABLE `foreup_customers` ADD `status_flag` TINYINT(1) NOT NULL DEFAULT '3' COMMENT '1 = Red, 2 = Yellow, 3 = Green';

-- 6/13/2014
ALTER TABLE  `foreup_account_transactions` ADD  `running_balance` DECIMAL( 15, 2 ) NULL DEFAULT NULL AFTER  `trans_amount`;

//6/24/2014
ALTER TABLE `foreup_sales` ADD `deleted_by` INT NOT NULL AFTER `deleted`, ADD `deleted_at` DATETIME NOT NULL AFTER `deleted_by`;

//6/25/2014
ALTER TABLE `foreup_courses` ADD `auto_gratuity` DECIMAL(15,2) NOT NULL ;
ALTER TABLE `foreup_table_receipts` ADD `auto_gratuity` DECIMAL(15,2) NOT NULL AFTER `status`;
ALTER TABLE `foreup_sales` ADD `auto_gratuity` DECIMAL(15,2) NOT NULL AFTER `payment_type`;
//6/26/2014
CREATE TABLE IF NOT EXISTS `foreup_teesheet_notes` (
`note_id` int(11) NOT NULL,
  `teesheet_id` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `note` text NOT NULL
) ENGINE=InnoDB;
      
ALTER TABLE `foreup_teesheet_notes` ADD PRIMARY KEY (`note_id`), ADD KEY `teesheet_id` (`teesheet_id`);

ALTER TABLE `foreup_teesheet_notes` MODIFY `note_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `foreup_teesheet_notes` ADD UNIQUE (`teesheet_id` ,`date`);

ALTER TABLE `foreup_teesheet` ADD `online_rate_setting` TINYINT NOT NULL AFTER `online_booking`;

ALTER TABLE `foreup_teetime` CHANGE `price_class_1` `price_class_1` MEDIUMINT NOT NULL;
ALTER TABLE `foreup_teetime` CHANGE `price_class_2` `price_class_2` MEDIUMINT NOT NULL;
ALTER TABLE `foreup_teetime` CHANGE `price_class_3` `price_class_3` MEDIUMINT NOT NULL;
ALTER TABLE `foreup_teetime` CHANGE `price_class_4` `price_class_4` MEDIUMINT NOT NULL;
ALTER TABLE `foreup_teetime` CHANGE `price_class_5` `price_class_5` MEDIUMINT NOT NULL;
//6/30/2014
ALTER TABLE `foreup_courses` ADD `minimum_food_spend` INT( 11 ) NOT NULL;

ALTER TABLE `foreup_customers` ADD `require_food_minimum` SMALLINT( 6 ) NOT NULL;

CREATE TABLE IF NOT EXISTS `foreup_customer_food_spending` (
  `course_id` int(11) unsigned NOT NULL,
  `person_id` int(11) unsigned NOT NULL,
  `amount` decimal(15,2) DEFAULT '0.00',
  `month` int(2) unsigned NOT NULL,
  `year` int(4) unsigned NOT NULL,
  `transaction_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB;

//7/18/2014
ALTER TABLE `foreup_teetime` ADD `cart_num_1` VARCHAR( 20 ) NOT NULL ,ADD `cart_num_2` VARCHAR( 20 ) NOT NULL;


// PROMOTIONS SQL
//7/23/14
CREATE TABLE `foreup_promotions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `coupon_definition_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `expiration_date` date DEFAULT NULL,
  `redeemed` char(1) DEFAULT 'N',
  `deleted` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE `foreup_promotion_definitions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `number_used` int(255) DEFAULT '0',
  `number_issued` int(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `amount_type` varchar(100) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `valid_for` varchar(255) DEFAULT NULL,
  `Sunday` tinyint(1) DEFAULT '1',
  `Monday` tinyint(1) DEFAULT '1',
  `Tuesday` tinyint(1) NOT NULL DEFAULT '1',
  `Wednesday` tinyint(1) NOT NULL DEFAULT '1',
  `Thursday` tinyint(1) NOT NULL DEFAULT '1',
  `Friday` tinyint(1) NOT NULL DEFAULT '1',
  `Saturday` tinyint(1) NOT NULL DEFAULT '1',
  `valid_between_from` varchar(255) DEFAULT NULL,
  `valid_between_to` varchar(255) DEFAULT NULL,
  `limit` int(11) DEFAULT NULL,
  `buy_quantity` int(11) DEFAULT NULL,
  `get_quantity` int(11) DEFAULT NULL,
  `min_purchase` double DEFAULT NULL,
  `additional_details` text,
  `rules` text,
  `text_text` text,
  `email_text` text,
  `expiration_date` date DEFAULT '3000-01-01',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `bogo` enum('bogo','discount') NOT NULL,
  `coupon_limit` enum('one','unlimited') NOT NULL,
  `item_id` int(255) NOT NULL DEFAULT '0',
  `discount_type` tinyint(10) NOT NULL DEFAULT '0',
  `category_type` varchar(100) NOT NULL DEFAULT '',
  `subcategory_type` varchar(100) NOT NULL DEFAULT '',
  `department_type` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE  `foreup_marketing_campaigns` ADD  `promotion_id` INT NOT NULL;


ALTER TABLE `foreup_customer_food_spending` ADD `minimum` DECIMAL(15,2) NOT NULL AFTER `person_id`;
