<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Session Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Sessions
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/sessions.html
 */
class CI_Permissions {

	var $userdata					= array();
	var $CI;
	
	/**
	 * Session Constructor
	 *
	 * The constructor runs the session routines automatically
	 * whenever the class is instantiated.
	 */
	public function __construct($params = array())
	{
		//log_message('debug', "Session Class Initialized");

		// Set the super object to a local variable for use throughout the class
		$this->CI =& get_instance();
                
                // Load the string helper so we can use the strip_slashes() function
		$this->CI->load->helper('string');

		// Are we using a database?  If so, load it
	}
        
        function is_super_admin() {
            if ($this->CI->session->userdata('user_level') == 5)
                return true;
            else
                return false;
        }
        function is_admin() {
            if ($this->CI->session->userdata('user_level') == 3)
                return true;
            else
                return false;
        }
        function is_manager() {
            if ($this->CI->session->userdata('user_level') == 2)
                return true;
            else
                return false;
        }
        function is_employee() {
            if ($this->CI->session->userdata('user_level') < 3)
                return true;
            else
                return false;
        }
        function course_has_module($module_id) {
            if ($this->CI->session->userdata($module_id) == 1 || $module_id == '')
                return true;
            else
                return false;
        }
}
?>