var BookingClass = Backbone.Model.extend({
	idAttribute: 'booking_class_id',
	defaults: {
		name: '',
		price_class: ''
	}
});

var BookingClassCollection = Backbone.Collection.extend({
	url: function(){
		return BASE_API_URL + 'courses/' + App.data.filters.get('course_id') + '/schedules/' + App.data.filters.get('schedule_id') + '/booking_classes';
	},
	model: BookingClass
});
