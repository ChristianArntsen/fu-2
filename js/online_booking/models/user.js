var User = Backbone.Model.extend({
	idAttribute: 'user_id',
	
	url: function(){
		return BASE_API_URL + 'courses/' + App.data.course.get('course_id') + '/users'
	},
	
	initialize: function(){
		this.on('error', this.triggerError);
	},
	
	defaults: {
		logged_in: false,
		first_name: '',
		last_name: '',
		email: '',
		username: '',
		phone: '',
		address_1: '',
		address_2: '',
		city: '',
		state: '',
		zip: '',
		credit_cards: null,
		purchases: null,
		reservations: null
	},
	
	triggerError: function(model, response, options){
		App.vent.trigger('error', model, response);
	},
	
	set: function(attributes, options){
		if(attributes.credit_cards !== undefined && !(attributes.credit_cards instanceof CreditCardCollection)){
			attributes.credit_cards = new CreditCardCollection( attributes.credit_cards );
		}
		if(attributes.reservations !== undefined && !(attributes.reservations instanceof ReservationCollection)){
			attributes.reservations = new ReservationCollection( attributes.reservations );
		}
		if(attributes.purchases !== undefined && !(attributes.purchases instanceof PurchaseCollection)){
			attributes.purchases = new PurchaseCollection( attributes.purchases );
		}
		if(attributes.gift_cards !== undefined && !(attributes.gift_cards instanceof GiftCardCollection)){
			attributes.gift_cards = new GiftCardCollection( attributes.gift_cards );
		}
		if(attributes.invoices !== undefined && !(attributes.invoices instanceof InvoiceCollection)){
			attributes.invoices = new InvoiceCollection( attributes.invoices );
		}						
		
		return Backbone.Model.prototype.set.call(this, attributes, options);
	},
	
    // Overwrite save function
    save: function(attrs, options) {
        options || (options = {});
		
		var save_type = '';
		if(options && options.save_type){
			save_type = options.save_type;
		}
		
        // Filter the data to send to the server
        if(save_type == 'info'){
			attrs = _.pick(attrs, 'first_name', 'last_name', 'phone', 'email', 'birthday', 'address', 'city', 'state', 'zip'); 
		
		}else if(save_type == 'credentials'){
			attrs = _.pick(attrs, 'username', 'current_password', 'password');
		}
		
        options.data = JSON.stringify(attrs);
        options.contentType = 'application/json';

        // Proxy the call to the original save function
        Backbone.Model.prototype.save.call(this, attrs, options);
    },
	
	isLoggedIn: function(){
		if(this.get('logged_in')){
			return true;
		}else{
			return false;
		}
	},
	
	// Log the user in
	login: function(username, password, reservation){
		var model = this;
		
		$.post(this.url() + '/login', {username:username, password:password, api_key:API_KEY}, function(response){	
			// Update user information returned from successful login
			model.set(response);
			
			// Book reservation if one is passed		
			if(reservation && reservation.isValid()){
				
				// If tee time is available for purchase, show payment
				// method selection window
				if(reservation.canPurchase()){
					var paymentMethod = new PaymentSelectionView({model: reservation});
					paymentMethod.show();

				}else{				
					App.data.user.get('reservations').create(reservation.attributes, {wait: true});
				}					
			}
			
		},'json').fail(function(response){
			App.vent.trigger('error:login', model, response);
		});
	},
	
	logout: function(){
		var model = this;
		$.post(this.url() + '/logout', null, function(response){
			if(response.success){
				model.clear();
			}
		},'json');
	}
});
