var CreditCard = Backbone.Model.extend({
	idAttribute: 'credit_card_id',
	defaults: {
		masked_account: '',
		card_type: ''
	}
});

var CreditCardCollection = Backbone.Collection.extend({
	model: CreditCard,
	url: function(){
		return BASE_API_URL + 'courses/' + App.data.course.get('course_id') + '/users/credit_cards'
	}
});
