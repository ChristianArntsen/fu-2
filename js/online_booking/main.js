// Override ajax function to always pass API key
$(document).ajaxSend(function(e, xhr, options){
    xhr.setRequestHeader("Api-key", API_KEY);
});

// Enable fast click (to get rid of 300ms delay when tapping things on mobile devices)
window.addEventListener('load', function() {
    FastClick.attach(document.body);
}, false);

// Underscore init, custom functions
_.mixin({
	round: function(value, places) {	
		if(typeof(value) != 'number'){
			value = parseFloat(value);
		}
		if(isNaN(value)){
			value = 0;
		}
		if(!places){
			places = 2;
		}
		
		return +(Math.round(value + "e+" + places)  + "e-" + places);
	}
});

// Main router to handle page links
var BookingRouter = Backbone.Router.extend({

	routes: {
		"": 			"index",
		"teetimes":		"teetimes",
		"confirmation/:reservationId":	"confirmation",
		"course":		"course",
		"account":				"account"
	},
	
	index: function(){
		this.navigate('teetimes', {trigger: true});
	},
	
	initialize: function(){
		this.listenTo(App.data.user, 'change:logged_in', this.redirectLogout);
		this.on('route', this.trackPageView);
	},
	
	trackPageView: function(){
		var url = Backbone.history.root + Backbone.history.getFragment();
		ga('send', 'pageview', url);	
	},
	
	redirectLogout: function(user){
		if(!user.get('logged_in')){
			this.navigate('teetimes', {trigger: true});
			this.checkLogin();
		}
	},
	
	// If course has online booking password protected, show message 
	// on page for user to log in or register
	checkLogin: function(){
		if((App.settings.online_booking_protected == '1' || App.data.course.get('online_booking_protected') == '1') && !App.data.user.get('logged_in')){
			var page = new OneColLayout();
			App.page.show(page);
			page.content.show( new TeeTimeLoginView() );
			return false;		
		}
		return true;
	},
	
	// View teetimes page
	teetimes: function(){
		
		if(!this.checkLogin()){
			return false;
		}
		
		var schedule = App.data.schedules.findWhere({selected: true});
		
		// If a booking class is set, but doesn't belong to current schedule, reset it
		var bookingClasses = schedule.get('booking_classes');
		var bookingClass = bookingClasses.get( App.data.filters.get('booking_class') );
		if(!bookingClass){
			App.data.filters.set('booking_class', false);
		}
		
		// If booking class is selected, or not required, display teetimes
		if(bookingClasses.length == 0 || (App.data.filters.get('booking_class') && App.data.filters.get('booking_class') != 'false')){
			var page = new TwoColLayout();
			App.page.show(page);			
			
			page.nav.show( new FilterView({model: App.data.filters, collection: App.data.times}) );
			page.content.show( new TimeListView({collection: App.data.times}) );
			page.booking_class.show( new BookingClassFilterView({model: bookingClass}) );
			
		// If no booking class has been selected, show list of booking classes		
		}else{
			var page = new OneColLayout();
			App.page.show(page);			
			
			page.content.show( new BookingClassListView({collection: schedule.get('booking_classes')}) );
		}
	},
	
	// Reservation confirmation/thank you page
	confirmation: function(reservationId){
		var reservation = App.data.user.get('reservations').get(reservationId);
		$('#modal').modal('hide');
		
		var page = new OneColLayout();
		App.page.show(page);
		page.content.show( new ReservationConfirmationView({model:reservation}) );
	},
	
	course: function(){
		$('#modal').modal('hide');
		
		var page = new OneColLayout();
		App.page.show(page);
		page.content.show( new CourseInformationView({model:App.data.course}) );
	},
	
	account: function(){
		$('#modal').modal('hide');
		
		// If user is not logged in, redirect them to the teetimes page 
		// and show the login window
		if(!App.data.user.get('logged_in')){
			App.router.navigate('teetimes', {trigger:true});
			
			var loginWindow = new LoginView();
			loginWindow.show();
			return false;
		}
		
		var page = new OneColLayout();
		App.page.show(page);
		page.content.show( new UserProfileView({model: App.data.user}) );
	}
});

var App = new Backbone.Marionette.Application();

App.addRegions({
	nav: '#navigation',
	page: '#page'
});

App.addInitializer(function(options){
	this.data = {};
	this.data.status = new Backbone.Model({'loading': false});	
	this.data.course = new Course(COURSE);
	this.data.times = new TimeCollection();
	this.data.user = new User(USER);
	this.data.filters = new Filter(DEFAULT_FILTER);
	this.data.schedules = new ScheduleCollection(SCHEDULES);
	
	this.settings = SETTINGS;
	
	this.router = new BookingRouter();
	this.notify = new NotificationView();

	var navView =  new NavigationView({userModel: this.data.user, courseModel: this.data.course});
	navView.listenTo(this.router, 'route', navView.highlightLink);
	this.nav.show(navView);
});

App.on('initialize:after', function(){
	Backbone.history.start({pushState: false, root: BASE_URL + COURSE_ID});
	this.data.times.refresh();
});

App.vent.on('reservation', function(reservation){
		
	// If the user is not pre-paying for tee time, show the confirmation page
	if(!reservation.get('is_paying')){
		var reservationId = reservation.get('teetime_id');
		ga('send', 'event', 'Online Booking', 'Tee Time Reservation', App.data.course.getCurrentSchedule());
		App.router.navigate('confirmation/' + reservationId, {trigger: true});		
	}
});

$(document).ajaxComplete(function(event, response, options){
	
	if((response.code == 200 || response.code == 201) && options.url.indexOf('credit_card_window') == -1){
		var success_msg =  $('<span class="success-msg text-success"><span class="glyphicon glyphicon-ok"></span> Saved</span>');
		$('button.disabled').after(success_msg);
			
		_.delay(function(msg){
			msg.fadeOut();
		}, 2000, success_msg);		
	}
	
	$('button.disabled').button('reset');
});

App.start();
