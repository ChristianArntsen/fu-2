var LoginView = Backbone.View.extend({
	
	id: 'login',
	className: 'modal-dialog',
	template: _.template( $('#template_log_in').html() ),
	signupTemplate: _.template( $('#template_register').html() ),
	
	events: {
		'click .btn.login': 'login',
		'click .btn.register': 'showSignup',
		'click .btn.complete': 'register',
		'keypress': 'submitForm',		
	},
	
	submitForm: function(e){

		if(e.keyCode != 13){ return true }
		
		if(this.$el.find('#login_form').length > 0){
			this.login();
		}else{
			this.register();
		}
		
		e.preventDefault();
		return false;
	},
	
	initialize: function(options){
		
		this.listenTo(App.vent, 'error:login', this.showLoginError);
		this.listenTo(App.vent, 'error:signup', this.showSignupError);
		this.listenTo(App.vent, 'error', this.showError);
		
		// If attempting to book a reservation, store the reservation in the view
		if(options && options.reservation){
			this.reservation = options.reservation;			
		
		// If just wanting to log in to app, attach listener to redirect user after login
		}else{
			this.listenTo(App.data.user, 'change:logged_in', this.redirectLogin);
		}
		var view = this;
		
		$(document).on('hidden.bs.modal', function(){
			view.close();
		});
	},
	
	showError: function(model, response, options){
		this.showLoginError(model, response, options);
		this.showSignupError(model, response, options);
	},	
	
	showLoginError: function(model, response, options){
		if(this.$el.find('#login_form').data('bootstrapValidator')){
			this.$el.find('#login_form').data('bootstrapValidator').resetForm();
			this.$el.find('#login-error').text(response.responseJSON.msg).show();
		}
	},
	
	showSignupError: function(model, response, options){
		if(this.$el.find('#register_form').data('bootstrapValidator')){
			this.$el.find('#register_form').data('bootstrapValidator').resetForm();
			this.$el.find('#signup-error').text(response.responseJSON.msg).show();
		}
	},	
	
    close: function() {
		this.remove();
    },

    render: function() {
		var html = this.template();
		this.$el.html(html);	

		$('#modal').html(this.el);
		$('#modal').find('input').first().focus();
		ga('send', 'event', 'Online Booking', 'View Login Form', App.data.course.getCurrentSchedule());
		return this;
    },
    
    redirectLogin: function(){
		App.router.navigate('account', {trigger: true});
	},
    
    renderSignup: function(event){
		
		var html = this.signupTemplate();
		this.$el.html(html);	

		if($('#modal:hidden').length > 0){
			$('#modal').html(this.el);	
		}
		ga('send', 'event', 'Online Booking', 'View Signup Form', App.data.course.getCurrentSchedule());
		return this;		
	},

    show: function(form){
		if(!form){
			form = 'login';
		}

		if(form == 'login'){
			this.render();
		
		}else if(form == 'signup'){
			this.renderSignup();
		}
		$('#modal').modal();
	},
	
	showSignup: function(){
		this.renderSignup();
		return false;
	},
	
	// Collect username and password and log user in
	login: function(event){	
		var form = this.$el.find('#login_form');
		form.bootstrapValidator('validate');
		
		if(!form.data('bootstrapValidator').isValid()){
			return false;
		}		
		
		var email = this.$el.find('#login_email').val();
		var password = this.$el.find('#login_password').val();
		this.$el.find('button.login').button('loading');
					
		// Send request to server to log user in
		App.data.user.login(email, password, this.reservation);
		ga('send', 'event', 'Online Booking', 'Login', App.data.course.getCurrentSchedule());
		
		if(event){
			event.preventDefault();
		}
		return false;
	},
	
	// Register a new user account
	register: function(event){
		var view = this;
		var form = this.$el.find('#register_form');
		form.bootstrapValidator('validate');

		if(!form.data('bootstrapValidator').isValid()){
			return false;
		}

		var data = {};
		var reservation = false;
		this.$el.find('button.complete').button('loading');
		
		if(this.reservation){
			reservation = this.reservation;
		}

		data.first_name = $('#register_first_name').val();
		data.last_name = $('#register_last_name').val();
		data.phone = $('#register_phone').val();
		data.email = $('#register_email').val();
		data.password = $('#register_password').val();
		
		// Save the new user to the server, upon success, book reservation
		App.data.user.save(data, {success: function(){
			
			if(reservation && reservation.isValid()){
				
				view.$el.find('button.complete').button('loading');
				
				// If tee time is available for purchase, show payment
				// method selection window				
				if(reservation.get('can_purchase')){
					var paymentMethod = new PaymentSelectionView({model: reservation});
					paymentMethod.show();

				}else{					
					// Book reservation
					App.data.user.get('reservations').create(reservation.attributes, {wait: true});
					ga('send', 'event', 'Online Booking', 'Signup', App.data.course.getCurrentSchedule());	
				}
			}		
		},
		'error': function(model, response){
			App.vent.trigger('error:signup', model, response);
		}});
		
		if(event){
			event.preventDefault();
		}		
		return false;
	}
});
