var UserProfileView = Backbone.View.extend({
	tagName: 'div',
	className: 'row',
	template: _.template( $('#template_user_profile').html() ),

	events: {
		'click .save-account': 'saveAccountInfo',
		'click .save-login': 'saveLoginInfo',
		'click .add-card': 'addCreditCard'
	},
	
	initialize: function(options){
		this.children = new Backbone.ChildViewContainer();
		this.listenTo(App.vent, 'error', this.showError);
	},
	
	render: function(){
		this.$el.html(this.template(this.model.attributes));
		this.renderSettings();
		this.renderReservations();
		this.renderCreditCards();
		this.renderPurchases();
		this.renderGiftCards();
		this.renderInvoices();		
		return this;
	},
	
	renderSettings: function(){
		var settingsTemplate = _.template( $('#template_user_settings').html() );
		this.$el.find('#account-settings').html( settingsTemplate(this.model.attributes) );
		this.$el.find('#account_birthday').datepicker({
			format: 'mm/dd/yyyy'
		});
	},
	
	renderReservations: function(){
		var reservations = this.model.get('reservations');
		var view = new ReservationListView({collection: reservations});
		this.children.add(view);
		this.$el.find('#account-reservations').html(  view.render().el );
		this.$el.find('#account-reservations').prepend('<h2>Reservations</h2>');
	},
	
	renderCreditCards: function(){
		var creditCards = this.model.get('credit_cards');
		var view = new CreditCardListView({collection: creditCards});
		this.children.add(view);
		this.$el.find('#account-creditcards').html( view.render().el );
		this.$el.find('#account-creditcards').prepend('<div style="overflow: hidden;"><h2 class="pull-left">Credit Cards</h2> <button class="btn btn-success add-card pull-left" style="margin-top: 20px; margin-left: 15px;">Add Card</button></div>');
	},
	
	renderPurchases: function(){
		var purchases = this.model.get('purchases');
		var view = new PurchaseListView({collection: purchases});
		this.children.add(view);
		this.$el.find('#account-purchases').html( view.render().el );
		this.$el.find('#account-purchases').prepend('<h2>Purchases</h2>');
	},
	
	renderInvoices: function(){
		var invoices = this.model.get('invoices');
		var view = new InvoiceListView({collection: invoices});
		this.children.add(view);
		this.$el.find('#account-invoices').html( view.render().el );
		this.$el.find('#account-invoices').prepend('<h2>Invoices</h2>');
	},	
	
	renderGiftCards: function(){
		var giftCards = this.model.get('gift_cards');
		var view = new GiftCardListView({collection: giftCards});
		this.children.add(view);
		this.$el.find('#account-giftcards').html( view.render().el );
	},
	
	showError: function(model, response){
		if(model.changed.email){
			this.$el.find('#user-info-error').text(response.responseJSON.msg).show();
		}else{
			this.$el.find('#user-credentials-error').text(response.responseJSON.msg).show();
		}
	},
	
	saveAccountInfo: function(e){
		e.preventDefault();
		
		var form = this.$el.find('form.information');
		form.bootstrapValidator('validate');
		
		if(!form.data('bootstrapValidator').isValid()){
			return false;
		}
		this.$el.find('button.save-account').button('loading');		
				
		var params = {
			'first_name': this.$el.find('#account_first_name').val(),
			'last_name': this.$el.find('#account_last_name').val(),
			'email': this.$el.find('#account_email').val(),
			'phone': this.$el.find('#account_phone').val(),
			'address': this.$el.find('#account_address').val(),
			'city': this.$el.find('#account_city').val(),
			'state': this.$el.find('#account_state').val(),
			'zip': this.$el.find('#account_zip').val(),
			'birthday': this.$el.find('#account_birthday').val()
		};
		
		this.model.save(params, {wait: true, save_type: 'info'});
	},
	
	saveLoginInfo: function(e){
		e.preventDefault();
		
		var form = this.$el.find('form.account');
		form.bootstrapValidator('validate');
		
		if(!form.data('bootstrapValidator').isValid()){
			return false;
		}
		this.$el.find('button.save-login').button('loading');	
				
		var params = {
			'username': this.$el.find('#account_username').val(),
			'password': this.$el.find('#account_password').val(),
			'current_password': this.$el.find('#account_current_password').val()
		};
		
		this.model.save(params, {wait: true, save_type: 'credentials'});		
	},
	
	addCreditCard: function(e){
		e.preventDefault();
		var view = new AddCreditCardView();
		view.show();
	},
	
	remove: function(){
		this.children.call('remove');
		Backbone.View.prototype.remove.call(this);
	}
});
