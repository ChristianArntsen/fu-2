var InvoiceView = Backbone.View.extend({
	tagName: 'tr',
	className: '',
	template: _.template( $('#template_invoice').html() ),
	
	events: {
		'click button.view': 'view',
		'click button.pay': 'pay'
	},
	
	initialize: function(){
		this.listenTo(this.model, 'change', this.render);		
	},
	
	render: function(){
		this.$el.html(this.template(this.model.attributes));
		return this;
	},
	
	view: function(event){
		$(event.currentTarget).button('loading');
		var view = this;
		var model = this.model
		
		// Get invoice details from server
		this.model.fetch({success: function(){	
			var detailsView = new InvoiceDetailsView({model: model});
			detailsView.show();
		}});
	},
	
	pay: function(event){
		$(event.currentTarget).button('loading');
		var view = this;
		var model = this.model
		
		// Get invoice details from server
		this.model.fetch({success: function(){	
			var invoicePayment = new InvoiceSelectCreditCardView({model: model});
			invoicePayment.show();
		}});
	}	
});

var InvoiceDetailsView = Backbone.View.extend({
	
	id: 'invoice_details',
	className: 'modal-dialog',
	template: _.template( $('#template_invoice_details').html() ),
	
	events: {
		'click button.pay': 'pay'		
	},
	
	initialize: function(){
		var view = this;
		$(document).on('hidden.bs.modal', function(){
			view.close();
		});			
	},
	
    close: function() {
		this.remove();
    },

    render: function() {
		var attributes = this.model.attributes;	
		var html = this.template(attributes);
		this.$el.html(html);
		$('#modal').html(this.el);
		return this;
    },
    
    show: function(){
		this.render();
		$('#modal').modal();
	},
	
	pay: function(){
		var creditCardView = new InvoiceSelectCreditCardView({model: this.model});
		creditCardView.show();		
	}
});

var InvoiceListView = Backbone.View.extend({
	tagName: 'table',
	className: 'table table-striped invoices',
	
	initialize: function(){
		this.listenTo(this.collection, 'add remove reset', this.render);
	},
	
	render: function(){
		var self = this;
		if(!this.$el.html()){
			this.$el.html('');
		}
		this.$el.html('<thead>' +
			'<tr>' +
				'<th class="sale"><span class="hidden-xs">Invoice </span>#</th>' +
				'<th class="date">Date</th>' +
				'<th class="total">Total</th>' +
				'<th class="total">Due</th>' +
				'<th class="status">Status</th>' +
				'<th>&nbsp;</th>' +
			'</tr></thead><tbody></tbody>');
		
		if(this.collection.length == 0){
			this.renderEmpty();
		}else{
			_.each(this.collection.models, this.renderInvoice, this);
		}

		return this;
	},
	
	renderEmpty: function(){
		this.$el.find('tbody').append('<tr class="purchase empty muted"><td colspan="6">No invoices available</td></tr>');
	},
	
	renderInvoice: function(invoice){
		this.$el.append( new InvoiceView({model: invoice}).render().el );
	}
});
