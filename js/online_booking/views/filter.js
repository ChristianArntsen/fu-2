var Filter = Backbone.Model.extend({
	defaults: {
		'schedule_id': false,
		'time': '',
		'players': '',
		'holes': '',
		'date': '',
		'booking_class': false
	},
	
	initialize: function(){
		this.listenTo(this, 'change:schedule_id', this.setSelectedSchedule);
		this.listenTo(this, 'change:booking_class', this.setBookingClass);
	},
	
	// If schedule is changed, apply new settings
	setSelectedSchedule: function(){
		var id = this.get('schedule_id');
		var selected_schedule;
	
		_.each(App.data.schedules.models, function(schedule){
			
			if(schedule.get('teesheet_id') == id){
				schedule.set('selected', true);
				selected_schedule = schedule;
			}else{
				schedule.unset('selected');
			}
		});
			
		App.settings.booking_carts = selected_schedule.get('booking_carts');
		App.settings.minimum_players = selected_schedule.get('minimum_players');
		App.settings.limit_holes = selected_schedule.get('limit_holes');		
		App.settings.days_in_booking_window = parseInt(selected_schedule.get('days_in_booking_window')) + parseInt(selected_schedule.get('days_out')) - 1;
		App.settings.online_booking_protected = 0;
		App.settings.online_close_time = selected_schedule.get('online_close_time');
		
		this.updateFilterSettings();				
	},
	
	// If booking class is changed, apply new settings
	setBookingClass: function(){
		var booking_class_id = this.get('booking_class');
		var selected_schedule = App.data.schedules.get( this.get('schedule_id') );
		
		if(booking_class_id && booking_class_id != 0){
			var booking_class = selected_schedule.get('booking_classes').get(booking_class_id);
			
			App.settings.booking_carts = booking_class.get('booking_carts');
			App.settings.minimum_players = booking_class.get('minimum_players');
			App.settings.limit_holes = booking_class.get('limit_holes');
			App.settings.days_in_booking_window = parseInt(booking_class.get('days_in_booking_window')) + parseInt(selected_schedule.get('days_out')) - 1;	
			App.settings.online_booking_protected = booking_class.get('online_booking_protected');
			App.settings.online_close_time = booking_class.get('online_close_time');
		
			this.updateFilterSettings();
		
		// If booking class is just being reset. Clear out password protection
		// (in case last booking class was password protected)
		}else{
			App.settings.online_booking_protected = 0;
		}	
	},
	
	// Updates filter settings to match only those allowed
	// (called after change in schedule or booking class)
	updateFilterSettings: function(){
		
		if(App.settings.minimum_players != 0){
			this.set('players', App.settings.minimum_players);
		}
		if(App.settings.limit_holes != 0){
			this.set('holes', App.settings.limit_holes);
		}		

		var close_time = parseInt(App.settings.online_close_time);
		if(close_time > parseInt(App.data.course.get('close_time'))){
			close_time = parseInt(App.data.course.get('close_time'));
		}		
		
		var date_adjustment = 0;
		if((close_time - 100) <= parseInt(moment().format('H00'))){
			date_adjustment = 1;
		}
		
		var cur_set_date = moment(this.get('date'));
		var max_date = moment().add('days', App.settings.days_in_booking_window + date_adjustment);

		this.set('date', cur_set_date.max(max_date).format('MM-DD-YYYY'));					
	}
});

var FilterView = Backbone.View.extend({
	tagName: 'div',
	className: '',
	template: _.template( $('#template_filters').html() ),
	
	events: {
		'click .time': 'setTime',
		'click .players': 'setPlayers',
		'click .holes': 'setHoles',
		'click .prevday': 'prevDay',
		'click .nextday': 'nextDay',
		'change .schedules': 'setSchedule'
	},
	
	intitialize: function(){
		this.listenTo(this.model, 'change:schedule_id change:booking_class', this.renderCalendar);
	},
	
	getDateAdjustment: function(){
		var close_time = parseInt(App.settings.online_close_time);
		if(close_time > parseInt(App.data.course.get('close_time'))){
			close_time = parseInt(App.data.course.get('close_time'));
		}
		
		// Check if we need to skip a day in calendar (if we are past close time
		// of online booking)
		var date_adjustment = 0;
		if((close_time - 100) <= parseInt(moment().format('H00'))){
			date_adjustment = 1;
		}

		return date_adjustment;		
	},
	
	render: function(){
		var attributes = this.model.attributes;
		var schedule = App.data.schedules.findWhere({selected:true});
		
		attributes.limit_holes = App.settings.limit_holes;
		attributes.minimum_players = parseInt(App.settings.minimum_players);
		attributes.date_adjustment = this.getDateAdjustment();

		this.$el.html(this.template(attributes));
		this.renderCalendar();
		return this;
	},

	renderCalendar: function(){

		var view = this;
		var calendar = this.$el.find('#date');
		var schedule = App.data.schedules.findWhere({'selected': true});
		
		var date_adjustment = this.getDateAdjustment();
		
		var calStart = moment().add('days', date_adjustment);
		var bookingDays = App.settings.days_in_booking_window;
		var calEnd = moment().add('days', bookingDays + date_adjustment);
		
		calendar.datepicker('remove');
		calendar.find('input').val( this.model.get('date') );

		// Initialize calendar picker
		calendar.datepicker({
			format: 'mm-dd-yyyy',
			startDate: calStart.toDate(),
			endDate: calEnd.toDate()
		});
		
		// When date is changed, set date on model
		calendar.on('changeDate', function(e){
			if(!e.dates[0]){
				return false;
			}
			view.$el.find('#date-menu').val(e.format('mm-dd-yyyy'));
			view.setDate(e.format('mm-dd-yyyy'));
		});
			
		var dateMenu = this.$el.find('#date-menu');
		var options = '';
		for(var i = date_adjustment; i <= bookingDays + date_adjustment; i++){
			var dateOption = moment().add('days', i);
			options += '<option value="' +dateOption.format('MM-DD-YYYY')+ '">' +dateOption.format('dddd, MMM Do')+ '</option>';
		}
		dateMenu.html(options).val(this.model.get('date'));
		
		dateMenu.off().on('change', function(e){
			var date = $(this).val();
			calendar.datepicker('setDate', date);
			view.setDate(date);
		});
		
		return this;		
	},

	refreshTimes: function(){
		this.collection.refresh();
	},
	
	prevDay: function(){
		
		var date_adjustment = this.getDateAdjustment();
		var date = this.model.get('date');
		var prevDay = moment(date).subtract('days', 1).min( moment().add('days', date_adjustment) );
		
		this.$el.find('#date').datepicker('setDate', prevDay.toDate());
		this.$el.find('#date-menu').val(prevDay.format('MM-DD-YYYY'));
		this.setDate(prevDay.format('MM-DD-YYYY'));				
	},
	
	nextDay: function(){
		var date_adjustment = this.getDateAdjustment();
		var date = this.model.get('date');
		var nextDay = moment(date).add('days', 1).max(moment().add('days', App.settings.days_in_booking_window + date_adjustment));
		
		this.$el.find('#date').datepicker('setDate', nextDay.toDate());
		this.$el.find('#date-menu').val(nextDay.format('MM-DD-YYYY'));
		this.setDate(nextDay.format('MM-DD-YYYY'));	
	},
	
	setSchedule: function(event){
		var schedule_id = $(event.target).val();

		// If changing schedules, clear booking class
		this.model.set({schedule_id: schedule_id, booking_class: false});
		
		var maxDate = moment().add('days', App.settings.days_in_booking_window);
		var setDate = this.model.get('date');
		var date = moment(setDate).max(maxDate);
		this.model.set({'date': date.format('MM-DD-YYYY')});
				
		this.refreshTimes();
		App.router.teetimes();
		return true;
	},
	
	setDate: function(value){
		this.model.set('date', value);
		this.refreshTimes();	
	},
	
	setTime: function(event){
		var btn = $(event.target);
		btn.addClass('active').siblings().removeClass('active');
		var value = btn.data('value');
		this.model.set('time', value);
		this.refreshTimes();
	},
	
	setHoles: function(event){
		var btn = $(event.target);
		btn.addClass('active').siblings().removeClass('active');
		var value = btn.data('value');
		this.model.set('holes', value);	
		this.refreshTimes();	
	},
	
	setPlayers: function(event){
		var btn = $(event.target);
		btn.addClass('active').siblings().removeClass('active');
		var value = btn.data('value');
		this.model.set('players', value);
		this.refreshTimes();		
	}
});
