(function($) {
	$.fn.swipeable = function (options) {
		var settings = $.extend({
			numbers_only:false,
			allow_enter:false,
			character_limit:null,
			auto_complete:null,
			clear_additional_data:false,
			ready_to_clear:false
		}, options);
		
		this.blur(function(event){
			console.log('blurred');
		});
		this.focus(function(event){
			console.log('focussed');
			if ($(this).val() != '')
				settings.ready_to_clear = true;
		});
		this.keydown(function(event){
			var id = $(this).attr('id');
			if (settings.ready_to_clear && id.substr(0,13) == 'teetime_title')
			{
				console.log('clearing existing player data');
				$(this).val('');
				if (id == 'teetime_title') {
					$('#person_id').val('');
					$('#email').val('');
					$('#phone').val('');
					$('#zip').val('');
				}
				if (id == 'teetime_title_2') {
					$('#person_id_2').val('');
					$('#email_2').val('');
					$('#phone_2').val('');
				}
				if (id == 'teetime_title_3') {
					$('#person_id_3').val('');
					$('#email_3').val('');
					$('#phone_3').val('');
				}
				if (id == 'teetime_title_4') {
					$('#person_id_4').val('');
					$('#email_4').val('');
					$('#phone_4').val('');
				}
				if (id == 'teetime_title_5') {
					$('#person_id_5').val('');
					$('#email_5').val('');
					$('#phone_5').val('');
				}
				settings.ready_to_clear = false;
			}	
			var card_swipe = $(this).data('card_swipe') == undefined ? 0 : $(this).data('card_swipe');
			var card_scan = $(this).data('card_scan') == undefined ? 0 : $(this).data('card_scan');
			
			var kc = event.keyCode;

			console.log('-----------------------------------------------');
			console.log('swipeable character log');
			console.log('card_swipe '+card_swipe);
			console.log('card_scan '+card_scan);
			console.log('character '+kc+' - '+String.fromCharCode(kc));
			
	        // IF WE'RE DEALING WITH A CARD SCAN OR CARD SWIPE, ENTER SHOULD TRIGGER AUTO SELECTION OF FIRST SEARCH RESULT
            if (!settings.allow_enter && (card_scan || card_swipe) &&  kc == 13)
            {
                event.preventDefault();
            	 console.log('enter on card scan or swipe - 003');
//            	setTimeout(function(){
            		// if (settings.auto_complete != null)
            		// {
            			$(settings.auto_complete).autocomplete('enable');
            			$(settings.auto_complete).autocomplete({'autoFocus':true});
            			$(settings.auto_complete).autocomplete('search', $(settings.auto_complete).val());
            			//$(settings.auto_complete).autocomplete({open: function( event, ui ) {setTimeout(function(){$(settings.auto_complete).trigger(event);}, 2000); }});
            		//}
            		//$(element).data('card_swipe', 0);
//	            	},1000);
            	$(this).data('card_scan', 0);
            	$(this).data('card_swipe', 0);
    //    		setTimeout(function(){$($('.ui-menu-item a')[0]).trigger(event);}, 2000);
        		setTimeout(function(){$(settings.auto_complete).trigger(event);}, 2000);
            }
            // ALLOW: BACKSPACE, DELETE, TAB, ESCAPE, ENTER
	        else if ( kc == 46 || kc == 8 || kc == 9 || kc == 27 || (settings.allow_enter && kc == 13) || 
	             // Allow: Ctrl+A
	            (kc == 65 && event.ctrlKey === true) || 
	             // Allow: home, end, left, right
	            (kc >= 35 && kc <= 39)) {
	                 // let it happen, don't do anything
	                 console.log('allowed character - 001');
	                 return;
	        }
			// LIMITING BY CHARACTER LIMIT
			else if ($(this).val().length == settings.character_limit)// && card_swipe && settings.character_limit != null)
            {
            	console.log('blocking characters above character_limit '+settings.character_limit+' - 006');
				// THERE IS A DEFAULT LIMIT OF 16 CHARACTERS THIS PREVENTS MORE THAN THAT LIMIT
            	event.preventDefault(); 
            } 
            // FILTER OUT META CHARACTERS --- USUALLY INDICATIVE OF A CARD SWIPE, SO WE'RE GOING TO REGISTER IT AS A CARD SWIPE	
	        else if ((kc == 186 /*semi-colon*/ || kc == 187 /*equal sign*/|| kc == 191 /*forward slash*/
	       		|| (event.shiftKey && kc == 53) /*percentage sign*/))// || (kc >= 48 && kc <=57 && !event.shiftKey)/*NUMBER KEY RANGE*/ || (kc >= 96 && kc <= 105 && !event.shiftKey)/*NUMBER PAD KEY RANGE*/) 
            {
            	// IF AUTOCOMPLETE, STOP AUTOCOMPLETE UNTIL DONE
            	console.log('triggering card swipe - 005');
				if (settings.auto_complete != null)
            		$(settings.auto_complete).autocomplete('disable');
            	settings.character_limit = 16;
				settings.numbers_only = 1;
            	$(this).data('card_swipe', 1);
            	var element = this;
            	// SET TIMEOUT TO AUTOCOMPLETE AND SELECT FIRST CUSTOMER RESULT 
            	
                event.preventDefault(); 
            }
            // IF CARD SWIPE ISN'T ACTIVE, AND WE GET A NUMBER AS OUR FIRST CHARACTER, WE'RE ASSUMING A CARD SCAN
            else if ($(this).val() == '' && (!card_swipe && ((kc >= 48 && kc <=57 && !event.shiftKey)/*NUMBER KEY RANGE*/ 
            	|| (kc >= 96 && kc <= 105 && !event.shiftKey))))// A NUMBER AND FIRST CHARACTER
            {
            	// TREATING THIS AS A CARD SCAN BECAUSE WE'RE SEARCHING BY A NUMBER, ASSUMABLY AN ACCOUNT NUMBER
                 console.log('first character a number, triggering card scan - 002'+$(this).val());
            	if (settings.auto_complete != null)
            		$(settings.auto_complete).autocomplete('disable');
            	$(this).data('card_scan', 1);
            	
            }
            // IF WE GO BACK TO NO VALUE, THEN WE RESET SETTINGS... NO CARD SWIPE, NO CARD SCAN
           	else if ($(this).val() == '' && (card_scan || card_swipe) && !((kc >= 48 && kc <=57 && !event.shiftKey)/*NUMBER KEY RANGE*/ 
           		|| (kc >= 96 && kc <= 105 && !event.shiftKey)))// A NUMBER AND FIRST CHARACTER
            {
            	// TREATING THIS AS A CARD SCAN BECAUSE WE'RE SEARCHING BY A NUMBER, ASSUMABLY AN ACCOUNT NUMBER
                 console.log('canceling card swipe and card scann - 008'+$(this).val());
            	if (settings.auto_complete != null)
            		$(settings.auto_complete).autocomplete('enable');
            	//$(this).data('card_scan', 1);
            	$(this).data('card_scan', 0);
            	$(this).data('card_swipe', 0);
            	if (settings.numbers_only)
            		event.preventDefault();
            }
            // IF CARD SWIPE OR NUMBERS ONLY, THEN ONLY ALLOW NUMBERS
            else if ((settings.numbers_only || card_swipe || card_scan) 
            	&& !((kc >= 48 && kc <=57 && !event.shiftKey)/*NUMBER KEY RANGE*/ 
            	|| (kc >= 96 && kc <= 105 && !event.shiftKey)/*NUMBER PAD KEY RANGE*/ 
            	|| (!settings.allow_enter && kc == 13)/*ENTER*/)) //Allow numbers only and enter
            {
            	// BLOCKING EVERYTHING BUT NUMBERS BECAUSE IT IS A CARD SWIPE
                console.log('blocking non number characters - 004');
        		event.preventDefault();
        	}
	        else
	        { 
              	console.log('bypassed all ifs - 007');
            }
		});
	};
	
}(jQuery));
