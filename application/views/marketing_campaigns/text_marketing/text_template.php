<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!--html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?= $title ?></title-->
        <style type="text/css">
          div#text-container{
            width: 600px;
            height:515px;
            font-family: sans-serif;
            background: url("../images/marketing/phone.png") no-repeat center center;
          }
          div#cellphone-mockup{
            border: 1px solid transparent;
            height:515px;
            margin: 0 auto;
          }
          div#gc-name-upper,
          div#cp-date,
          div#cp-clock{
            width:100%;
            text-align: center;
            font-weight: bold;
          }

          div#cp-clock{
            font-size: 10px;
            height: 14px;
            margin-top: 88px;
            padding-top: 1px;
          }
          div#gc-name-upper{
            margin-top: 10px;
            font-size: 11px;
            width:94%;
            padding-left:6%;
          }
          div#cp-date{
            font-size: 11px;
            color: #73787c;
            line-height: 5px;
            margin-top: 45px;
          }
          div#cp-text{
            font-size: 12px;
            margin: 30px auto;
            width: 330px;
            word-wrap: break-word;
          }

          div#cp-text p#header{
            font-weight:bold;
            font-size: 13px;
            padding-left: 5px;
            position: absolute;
          }
          div#cp-text div{
            text-align:justify;
            padding-left: 5px;
            position: inherit;
            margin:270px auto 0px;
            width:350px;
            
          }
          div#cp-text div textarea {
          	height: 160px;
			background: transparent;
			border: none;
			box-shadow: none;
			resize:none;
			color:#333333;
			font-weight:lighter;
          }
          div.bubble-pack{
            padding-left: 10px;
            padding-right: 10px;
            border: 1px solid #000;
            width: 165px;
          }
          div.bubble-pack-header, .bubble-pack-footer{
            width: 185px;            
          }
          div.bubble-pack-header{
            height: 15px;
            background: url("<?= base_url('application/views/marketing_campaigns/text_marketing'); ?>/bubble-pack-header.png") no-repeat;
          }
          div.bubble-pack-body{
            background: url("<?= base_url('application/views/marketing_campaigns/text_marketing'); ?>/bubble-pack-body.png") repeat-y;
            padding-left: 10px;
            padding-right: 10px;
            width: 165px;
          }
          div.bubble-pack-footer{
            height: 20px;
            background: url("<?= base_url('application/views/marketing_campaigns/text_marketing'); ?>/bubble-pack-footer.png") no-repeat;
          }
          div#character_count_holder {
          	margin:15px 0px 0px 10px;
          	font-size:16px;
          }
          div#character_count_holder .bold {
          	font-weight:bold;
          }
          div#character_count_holder #character_count {
          	
          }
        </style>
    <!--/head>
    <body-->
      <div id="text-container">
      	<div id='character_count_holder'><span class='bold'>Message</span> <span id='character_count'>160</span>/160 Characters Remaining</div>
        <div id="cellphone-mockup">
            
          <!--div id="cp-clock"></div>
          <div id="gc-name-upper"><?= $name; ?></div>
          <div id="cp-date"><?= date('F d, Y'); ?></div-->
          <div id="cp-text">
          	 <div id="text-contents"><textarea id='text_contents' name='text_contents'><?= ($campaign_info->content == '')?$course_name:$campaign_info->content; ?></textarea></div>
            <!--div class="bubble-pack-header"></div>
            <div class="bubble-pack-body">
              <p id="header"></p>
             
            </div>
            <div class="bubble-pack-footer"></div-->
          </div>
        </div>
      </div>

      <script type="text/javascript">
      $(document).ready(function() {
      	$('#text_contents').keyup(function(e){
      		var length =$(this).val().length;
      		if (length <= 160)
	      		$('#character_count').html(160 - length);
	      	else if (e.keyCode != 8 && e.keyCode != 46 && e.keyCode != 37 && e.keyCode != 38 && e.keyCode != 39 && e.keyCode != 40)
	      	{
	      		$(this).val($(this).val().slice(0,160));
	      	}
      	})
      })
        function updateClock ( )
        {
          var currentTime = new Date ( );

          var currentHours = currentTime.getHours ( );
          var currentMinutes = currentTime.getMinutes ( );
          var currentSeconds = currentTime.getSeconds ( );

          // Pad the minutes and seconds with leading zeros, if required
          currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
          currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

          // Choose either "AM" or "PM" as appropriate
          var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";

          // Convert the hours component to 12-hour format if needed
          currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;

          // Convert an hours component of "0" to "12"
          currentHours = ( currentHours == 0 ) ? 12 : currentHours;

          // Compose the string for display
          var currentTimeString = currentHours + ":" + currentMinutes + " " + timeOfDay;

          // Update the time display
          var c = document.getElementById("cp-clock");
          if(c) c.innerHTML = currentTimeString;
        }
        
        setInterval('updateClock()', 60000 );

      </script>
    <!--/body>
</html-->
