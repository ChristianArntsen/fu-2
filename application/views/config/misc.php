<?php
	echo form_open('config/save/misc',array('id'=>'config_misc_form'));
?>

<!--div class="field_row clearfix">	
<?php echo form_label(lang('config_language').':', 'language',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_dropdown('language', array(
				'english'  => 'English',
				'indonesia'    => 'Indonesia',
				'spanish'   => 'Spanish'), $this->config->item('language'));
				?>
		</div>
</div-->

<div class="field_row clearfix">	
<?php echo form_label(lang('config_timezone').':', 'timezone',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_dropdown('timezone', 
		 array(
				'Pacific/Midway'=>'(GMT-11:00) Midway Island, Samoa',
				'America/Adak'=>'(GMT-10:00) Hawaii-Aleutian',
				'Etc/GMT+10'=>'(GMT-10:00) Hawaii',
				'Pacific/Marquesas'=>'(GMT-09:30) Marquesas Islands',
				'Pacific/Gambier'=>'(GMT-09:00) Gambier Islands',
				'America/Anchorage'=>'(GMT-09:00) Alaska',
				'America/Ensenada'=>'(GMT-08:00) Tijuana, Baja California',
				'Etc/GMT+8'=>'(GMT-08:00) Pitcairn Islands',
				'America/Los_Angeles'=>'(GMT-08:00) Pacific Time (US & Canada)',
				'America/Denver'=>'(GMT-07:00) Mountain Time (US & Canada)',
				'America/Chihuahua'=>'(GMT-07:00) Chihuahua, La Paz, Mazatlan',
				'America/Dawson_Creek'=>'(GMT-07:00) Arizona',
				'America/Belize'=>'(GMT-06:00) Saskatchewan, Central America',
				'America/Cancun'=>'(GMT-06:00) Guadalajara, Mexico City, Monterrey',
				'Chile/EasterIsland'=>'(GMT-06:00) Easter Island',
				'America/Chicago'=>'(GMT-06:00) Central Time (US & Canada)',
				'America/New_York'=>'(GMT-05:00) Eastern Time (US & Canada)',
				'America/Havana'=>'(GMT-05:00) Cuba',
				'America/Bogota'=>'(GMT-05:00) Bogota, Lima, Quito, Rio Branco',
				'America/Caracas'=>'(GMT-04:30) Caracas',
				'America/Santiago'=>'(GMT-04:00) Santiago',
				'America/La_Paz'=>'(GMT-04:00) La Paz',
				'Atlantic/Stanley'=>'(GMT-04:00) Faukland Islands',
				'America/Campo_Grande'=>'(GMT-04:00) Brazil',
				'America/Goose_Bay'=>'(GMT-04:00) Atlantic Time (Goose Bay)',
				'America/Glace_Bay'=>'(GMT-04:00) Atlantic Time (Canada)',
				'America/St_Johns'=>'(GMT-03:30) Newfoundland',
				'America/Araguaina'=>'(GMT-03:00) UTC-3',
				'America/Montevideo'=>'(GMT-03:00) Montevideo',
				'America/Miquelon'=>'(GMT-03:00) Miquelon, St. Pierre',
				'America/Godthab'=>'(GMT-03:00) Greenland',
				'America/Argentina/Buenos_Aires'=>'(GMT-03:00) Buenos Aires',
				'America/Sao_Paulo'=>'(GMT-03:00) Brasilia',
				'America/Noronha'=>'(GMT-02:00) Mid-Atlantic',
				'Atlantic/Cape_Verde'=>'(GMT-01:00) Cape Verde Is.',
				'Atlantic/Azores'=>'(GMT-01:00) Azores',
				'Europe/Belfast'=>'(GMT) Greenwich Mean Time : Belfast',
				'Europe/Dublin'=>'(GMT) Greenwich Mean Time : Dublin',
				'Europe/Lisbon'=>'(GMT) Greenwich Mean Time : Lisbon',
				'Europe/London'=>'(GMT) Greenwich Mean Time : London',
				'Africa/Abidjan'=>'(GMT) Monrovia, Reykjavik',
				'Europe/Amsterdam'=>'(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna',
				'Europe/Belgrade'=>'(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague',
				'Europe/Brussels'=>'(GMT+01:00) Brussels, Copenhagen, Madrid, Paris',
				'Africa/Algiers'=>'(GMT+01:00) West Central Africa',
				'Africa/Windhoek'=>'(GMT+01:00) Windhoek',
				'Asia/Beirut'=>'(GMT+02:00) Beirut',
				'Africa/Cairo'=>'(GMT+02:00) Cairo',
				'Asia/Gaza'=>'(GMT+02:00) Gaza',
				'Africa/Blantyre'=>'(GMT+02:00) Harare, Pretoria',
				'Asia/Jerusalem'=>'(GMT+02:00) Jerusalem',
				'Europe/Minsk'=>'(GMT+02:00) Minsk',
				'Asia/Damascus'=>'(GMT+02:00) Syria',
				'Europe/Moscow'=>'(GMT+03:00) Moscow, St. Petersburg, Volgograd',
				'Africa/Addis_Ababa'=>'(GMT+03:00) Nairobi',
				'Asia/Tehran'=>'(GMT+03:30) Tehran',
				'Asia/Dubai'=>'(GMT+04:00) Abu Dhabi, Muscat',
				'Asia/Yerevan'=>'(GMT+04:00) Yerevan',
				'Asia/Kabul'=>'(GMT+04:30) Kabul',
				'Asia/Yekaterinburg'=>'(GMT+05:00) Ekaterinburg',
				'Asia/Tashkent'=>'(GMT+05:00) Tashkent',
				'Asia/Kolkata'=>'(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi',
				'Asia/Katmandu'=>'(GMT+05:45) Kathmandu',
				'Asia/Dhaka'=>'(GMT+06:00) Astana, Dhaka',
				'Asia/Novosibirsk'=>'(GMT+06:00) Novosibirsk',
				'Asia/Rangoon'=>'(GMT+06:30) Yangon (Rangoon)',
				'Asia/Bangkok'=>'(GMT+07:00) Bangkok, Hanoi, Jakarta',
				'Asia/Krasnoyarsk'=>'(GMT+07:00) Krasnoyarsk',
				'Asia/Hong_Kong'=>'(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi',
				'Asia/Irkutsk'=>'(GMT+08:00) Irkutsk, Ulaan Bataar',
				'Australia/Perth'=>'(GMT+08:00) Perth',
				'Australia/Eucla'=>'(GMT+08:45) Eucla',
				'Asia/Tokyo'=>'(GMT+09:00) Osaka, Sapporo, Tokyo',
				'Asia/Seoul'=>'(GMT+09:00) Seoul',
				'Asia/Yakutsk'=>'(GMT+09:00) Yakutsk',
				'Australia/Adelaide'=>'(GMT+09:30) Adelaide',
				'Australia/Darwin'=>'(GMT+09:30) Darwin',
				'Australia/Brisbane'=>'(GMT+10:00) Brisbane',
				'Australia/Hobart'=>'(GMT+10:00) Hobart',
				'Asia/Vladivostok'=>'(GMT+10:00) Vladivostok',
				'Australia/Lord_Howe'=>'(GMT+10:30) Lord Howe Island',
				'Etc/GMT-11'=>'(GMT+11:00) Solomon Is., New Caledonia',
				'Asia/Magadan'=>'(GMT+11:00) Magadan',
				'Pacific/Norfolk'=>'(GMT+11:30) Norfolk Island',
				'Asia/Anadyr'=>'(GMT+12:00) Anadyr, Kamchatka',
				'Pacific/Auckland'=>'(GMT+12:00) Auckland, Wellington',
				'Etc/GMT-12'=>'(GMT+12:00) Fiji, Kamchatka, Marshall Is.',
				'Pacific/Chatham'=>'(GMT+12:45) Chatham Islands',
				'Pacific/Tongatapu'=>'(GMT+13:00) Nuku\'alofa',
				'Pacific/Kiritimati'=>'(GMT+14:00) Kiritimati'
				), $this->config->item('timezone') ? $this->config->item('timezone') : date_default_timezone_get());
				?>
		</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('config_date_format').':', 'date_format',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_dropdown('date_format', array(
				'middle_endian'    => '12/30/2000',
				'little_endian'  => '30-12-2000',
				'big_endian'   => '2000-12-30'), $this->config->item('date_format'));
				?>
		</div>
</div>

<div class="field_row clearfix">	
<?php echo form_label(lang('config_time_format').':', 'time_format',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_dropdown('time_format', array(
				'12_hour'    => '1:00 PM',
				'24_hour'  => '13:00'
				), $this->config->item('time_format'));
				?>
		</div>
</div>
<?php if ($this->permissions->course_has_module('marketing_campaigns')) { ?>
<!--div class="field_row clearfix">	
	<?php echo form_label(lang('config_mailchimp_api_key').':', 'mailchimp_api_key',array('class'=>'wide')); ?>
			<div class='form_field'>
			<?php echo form_input(array(
					'name'=>'mailchimp_api_key',
					'id'=>'mailchimp_api_key',
					'value'=>$this->config->item('mailchimp_api_key')));?>
			</div>
	</div-->
<?php } ?>

<div class="field_row clearfix">	
<?php echo form_label(lang('config_number_of_items_per_page').':', 'number_of_items_per_page',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_dropdown('number_of_items_per_page', 
		 array(
				'20'=>'20',
				'50'=>'50',
				'100'=>'100',
				'200'=>'200',
				'500'=>'500'
				), $this->config->item('number_of_items_per_page') ? $this->config->item('number_of_items_per_page') : '20');
				?>
		</div>
</div>
<div class="field_row clearfix">	
<?php echo form_label(lang('config_hide_back_nine').':', 'hide_back_nine',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php echo form_checkbox(array(
                'name'=>'hide_back_nine',
                'id'=>'hide_back_nine',
                'value'=>'1',
                'checked'=>$this->config->item('hide_back_nine')));?>
        </div>
</div>
<div class="field_row clearfix">	
<?php echo form_label(lang('config_use_ets_giftcards').':', 'use_ets_giftcards',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php echo form_checkbox(array(
                'name'=>'use_ets_giftcards',
                'id'=>'use_ets_giftcards',
                'value'=>'1',
                'checked'=>$this->config->item('use_ets_giftcards')));?>
        </div>
</div>
<div class="field_row clearfix">	
<?php echo form_label(lang('config_deduct_tips').':', 'deduct_tips',array('class'=>'wide')); ?>
        <div class='form_field'>
        <?php echo form_checkbox(array(
                'name'=>'deduct_tips',
                'id'=>'deduct_tips',
                'value'=>'1',
                'checked'=>$this->config->item('deduct_tips')));?>
        </div>
</div>
<!-- <h2 style="padding-bottom: 5px; margin-bottom: 10px; border-bottom: 1px solid #BBBBBB;">Customer Billing/Invoices</h2> -->
<div class="field_row clearfix">	
<?php echo form_label(lang('config_billing_email').':', 'billing_email',array('class'=>'wide')); ?>
		<div class='form_field'>
		<?php echo form_input(array('name'=>'billing_email', 'value'=>$this->config->item('billing_email'), 'size'=>40)); ?>
		</div>
</div>
<div class="centered_submit">
	<?php 
	echo form_submit(array(
		'name'=>'submit_misc',
		'id'=>'submit_misc',
		'value'=>lang('common_save'),
		'class'=>'submit_button float_right')
	);
	?>
</div>
</form>
<?php if ($this->session->userdata('quickbooks')) { ?>
<h2 style="padding-bottom: 5px; margin-bottom: 10px; border-bottom: 1px solid #BBBBBB;">QuickBooks Integration</h2>
<?php if(empty($quickbooks['qb_username'])){ $btnHidden = 'display: block;'; }else{ $btnHidden = 'display: none;'; } ?>
<button id="enable_quickbooks" class="submit_button" style="float: left; margin: 0px; <?php echo $btnHidden; ?>" href="#">Enable QuickBooks</button>
<form name="quickbooks_config" id="config_quickbooks_form" method="post" action="<?php echo site_url('qb/edit_account'); ?>">
	<div id="quickbooks_account" style="position: relative;">
	<?php if(!empty($quickbooks['qb_username'])){ ?>
	<?php $this->load->view('quickbooks/account_info'); ?>
	<?php } ?>
	</div>
</form>  
<?php } ?>