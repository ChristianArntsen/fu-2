<? //$this->load->helper('date');?>
<div class="teesheet_select">
	<div class="breadcrumbs">
		<span class="crumb_link"><a rel="crumb_teesheets" href="#">Teesheets</a></span>
		 > 
		<span class="crumb_link"><?=$teesheet->title?></span>
	</div>
</div>
<div class="columns_container">
	<div id="copy_season_window" style="display: none;">
		<div style="padding: 15px;">
			<p style="padding: 5px 0px 5px; 0px">Copy existing season</p>
			<input type="hidden" name="teesheet_id" id="teesheet_id" value="<?php echo $teesheet->teesheet_id; ?>" />
			<?php echo form_dropdown('duplicate_season_id', $seasons_dropdown); ?>
			<a href="#" id="submit_copy_season" style="margin-top: 15px;" class="submit terminal_button">Copy</a>
		</div>
	</div>
	<div class="column_left">
		<div class="details">
			<h2><?=$teesheet->title?></h2>
			<ul class="details">
				<li>Default: <span><?=$teesheet->default ? "Yes" : "No" ?></span></li>
				<li>Holes: <span><?=$teesheet->holes?></span></li>
				<li>Require Credit Card: <span><?=$teesheet->require_credit_card ? "Yes" : "No" ?></span></li>
				<li>Increment: <span><?=$teesheet->increment ?> mins</span></li>
				<?php $times = array(
					'140'=>'1 hour 40 min',
					'145'=>'1 hour 45 min',
					'150'=>'1 hour 50 min',
					'155'=>'1 hour 55 min',
					'200'=>'2 hours',
					'205'=>'2 hours 5 min',
					'210'=>'2 hours 10 min',
					'215'=>'2 hours 15 min',
					'220'=>'2 hours 20 min',
					'225'=>'2 hours 25 min',
					'230'=>'2 hours 30 min'
				); ?>
				<li>Time to finish 9 holes: <span><?=$times[$teesheet->frontnine]; ?></span></li>
				<li>Online Booking:<span><?=$teesheet->online_booking ? "Yes" : "No" ?></span></li>
				<?php if($teesheet->online_booking == 1){ ?>
				<li>Online Open Time: <span><?php echo date('g:ia', strtotime($teesheet->online_open_time)); ?></span></li>
				<li>Online Close Time: <span><?php echo date('g:ia', strtotime($teesheet->online_close_time)); ?></span></li>
				<?php } ?>
				<li>Days in Booking Window: <span><?php echo $teesheet->days_in_booking_window; ?></span></li>
				<li>Minimum Players: <span><?php echo $teesheet->minimum_players; ?></span></li>
				<li>Limit Holes: <span><?php if($teesheet->limit_holes == 0){ echo 'No Limit'; }else{ echo $teesheet->limit_holes.' only'; } ?></span></li>				
			</ul>
			<a href="#" rel="edit_tee_sheet" class="terminal_button inline_button" title="Edit Teesheet" style="float: left;">Edit Teesheet</a>
		</div>
	</div>
	<div class="column_main">
		<div class="list-header">
			<h2>Seasons</h2>
			<a class="terminal_button" id="new_season" style="float: left;" href="#">New Season</a> 
			<span style="float: left; height: 35px; width: 30px; text-align: center; line-height: 40px;">or</span>
			<a class="terminal_button" id="copy_season" style="float: left;" href="#">Copy Existing</a>
		</div>
		<ul class="seasons">
			<? foreach($seasons as $season){ ?>
			<li class="season<?=$season->holiday ? " holiday" : ""?>" data-season-id="<?php echo $season->season_id; ?>">
				<h3><?=$season->season_name?> <? if ($season->holiday){?><em style='font-size: 85%; font-weight: normal; color: #666;'>(Holiday)</em><? } ?></h3>
				<p><?=convert_to_short_date($season->start_date)?> - <?=convert_to_short_date($season->end_date)?></p>
				<? if ($season->default){?><em>Default</em><? } ?>
			</li>
			<? } ?>
		</ul>
	</div>
</div>
<script>
$(function(){	
	$('a[rel=edit_tee_sheet]').on('click',function(){
		$.colorbox({href: "index.php/teesheets/seasonal_teesheet/<?=$teesheet->teesheet_id?>", width: 675});
		return false;
	});
	
	$('ul.seasons').on('click', 'li', function(e){
		var id = $(this).data('season-id');
		$('#tab3').load('index.php/config/view/season/' + id);	
		e.preventDefault();
	});
	
	$('#new_season').on('click', function(e){
		$.colorbox({
			href: "index.php/seasons/view_season/new/<?=$teesheet->teesheet_id?>", 
			width: 650,
			title: 'New Season'
		});
		e.preventDefault();
	});
	
	$('#copy_season').on('click', function(e){
		$.colorbox({
			html: $('#copy_season_window').html(), 
			width: 400,
			title: 'Copy Season'
		});
		e.preventDefault();
	});
	
	$('body').off('click', '#submit_copy_season').on('click', '#submit_copy_season', function(e){
		var duplicate_season_id = $('#duplicate_season_id').val();
		var teesheet_id = $('#teesheet_id').val();
		
		$.post("<?php echo site_url('seasons/save'); ?>", {duplicate_from: duplicate_season_id, teesheet_id: teesheet_id}, function(response){
			if(response.success){
				$('#tab3').load('index.php/config/view/teesheet/' + teesheet_id);
				set_feedback(response.message, 'success_message', false, 1500);
				$.colorbox.close();
			}else{
				set_feedback(response.message, 'error_message', false, 2000);
			}
		},'json');
		
		e.preventDefault();
	});	
});	
</script>
