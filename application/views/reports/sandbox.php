<?php $this->load->view("partial/header"); ?>
<script src="<?php echo base_url();?>js/jquery-ui-timepicker-addon.min.js?<?php echo APPLICATION_VERSION; ?>"></script>
<link href="<?php echo base_url('css/jquery-ui-timepicker-addon.min.css'); ?>" type="text/css" rel="stylesheet" />
	
<table id="contents">
	<tr>
		<td id="commands" rowspan=2>
			<div id="new_button">
				<input type='hidden' id='last_generated_report' value=''/>
        <?php if (isset($my_allowed_modules['sales'])) {?>
			<a href="<?php echo site_url('reports/summary_z_out');?>" id='z_out_report' report_type='z_out'><?php echo lang('reports_z_out'); ?></a>
			<a href="<?php echo site_url('reports/detailed_sales');?>" id='sales' report_type='sales' class='selected_report_type'><?php echo lang('reports_sales'); ?></a>
			<a href="<?php echo site_url('reports/invoices');?>" id='invoices' report_type='invoices'> <?php echo lang('reports_invoices'); ?></a>
			<?php if ($this->config->item('track_cash')) { ?>
				<a href="<?php echo site_url('reports/detailed_register_log');?>" id='register_log_report' report_type='register_log'><?php echo lang('reports_register_log_title'); ?></a>
			<?php } ?>
				<a href="<?php echo site_url('reports/detailed_gl_code');?>" id='gl_code_report' report_type='gl_code'><?php echo lang('reports_gl_code'); ?></a>
				<a href="<?php echo site_url('reports/detailed_categories');?>" id='categories_report' report_type='categories'><?php echo lang('reports_departments'); ?></a>
				<a href="<?php echo site_url('reports/summary_categories');?>" id='summary_categories_report' report_type='summary_categories'><?php echo lang('reports_categories'); ?></a>
			<?php if (isset($my_allowed_modules['teesheets']) && false) { ?>
				<a href="<?php echo site_url('reports/detailed_teetimes');?>" id='teetimes_report' report_type='teetimes'><?php echo lang('reports_teetimes'); ?></a>
			<?php } ?>
			<a href="<?php echo site_url('reports/summary_discounts');?>" id='discounts_report' report_type='discounts'><?php echo lang('reports_discounts'); ?></a>
			<a href="<?php echo site_url('reports/summary_payments');?>" id='payments_report' report_type='payments'><?php echo lang('reports_payments'); ?></a>
			<a href="<?php echo site_url('reports/detailed_taxes');?>" id='taxes_report' report_type='taxes'><?php echo lang('reports_taxes'); ?></a>
			<a href="<?php echo site_url('reports/summary_taxes');?>" id='summary_taxes_report' report_type='summary_taxes'><?php echo lang('reports_taxes_summary'); ?></a>
			<a href="<?php echo site_url('reports/deleted_sales');?>" id='deleted_sales_report' report_type='deleted_sales'><?php echo lang('reports_deleted_sales'); ?></a>
		<?php } ?>
		<?php if (isset($my_allowed_modules['teesheets'])) { ?>
			<a href="<?php echo site_url('reports/summary_tee_times');?>" id='tee_times_report' report_type='tee_times'><?php echo lang('reports_tee_times'); ?></a>
			<a href="<?php echo site_url('reports/detailed_teesheets');?>" id='teesheets_report' report_type='teesheets'><?php echo lang('reports_teesheets'); ?></a>
			<a href="<?php echo site_url('reports/play_length');?>" id='play_length_report' report_type='play_length'><?php echo lang('reports_play_length'); ?></a>
		<?php } ?>
	    <?php if (isset($my_allowed_modules['customers'])) {?>
			<a href="<?php echo site_url('reports/specific_customer');?>" id='customers_report' report_type='customers'><?php echo lang('reports_customers'); ?></a>
        <?php } ?>
        <?php if (isset($my_allowed_modules['employees'])) {?>
			<a href="<?php echo site_url('reports/specific_employee');?>" id='employees_report' report_type='employees'><?php echo lang('reports_employees'); ?></a>
	    <?php } ?>
        <?php if (isset($my_allowed_modules['employees']) && !$this->permissions->is_employee()) {?>
			<a href="<?php echo site_url('reports/time_clock');?>" id='time_clock_report' report_type='time_clock'><?php echo lang('reports_time_clock'); ?></a>
	    <?php } ?>
        <?php if (isset($my_allowed_modules['items'])) {?>
			<a href="<?php echo site_url('reports/summary_items');?>" id='items_report' report_type='items'><?php echo lang('module_items'); ?></a>
			<a href="<?php echo site_url('reports/inventory_summary_2');?>" id='inventory_report' report_type='inventory'><?php echo lang('reports_inventory_levels'); ?></a>
		<?php } ?>
        <?php if (isset($my_allowed_modules['item_kits'])) {?>
			<a href="<?php echo site_url('reports/summary_item_kits');?>" id='item_kits_report' report_type='item_kits'><?php echo lang('module_item_kits'); ?></a>
		<?php } ?>
        <?php if (isset($my_allowed_modules['suppliers'])) {?>
			<a href="<?php echo site_url('reports/specific_supplier');?>" is='suppliers_report' report_type='suppliers'><?php echo lang('reports_suppliers'); ?></a>
		<?php } ?>
        <?php if (isset($my_allowed_modules['receivings'])) {?>
			<a href="<?php echo site_url('reports/detailed_receivings');?>" id='receivings_report' report_type='receivings'><?php echo lang('reports_receivings'); ?></a>
		<?php } ?>
        <?php if (isset($my_allowed_modules['giftcards'])) {?>
			<a href="<?php echo site_url('reports/summary_giftcards');?>" id='giftcards_report' report_type='giftcards'><?php echo lang('reports_giftcards'); ?></a>
	    <?php } ?>
	    <?php if (isset($my_allowed_modules['sales'])) {?>
			<a href="<?php echo site_url('reports/summary_account_balances');?>" id='account_balances_report' report_type='account_balances'><?php echo lang('reports_account_balances'); ?></a>
	    <?php } ?>
	    <?php if (isset($my_allowed_modules['sales'])) {?>
			<a href="<?php echo site_url('reports/detailed_account_transactions');?>" id='account_transactions_report' report_type='account_transactions'><?php echo lang('reports_account_transactions'); ?></a>
	    <?php } ?>
	    <?php if (isset($my_allowed_modules['sales'])) {?>
			<a href="<?php echo site_url('reports/summary_rainchecks');?>" id='rainchecks_report' report_type='rainchecks'><?php echo lang('reports_rainchecks'); ?></a>
	    <?php } ?>
	    <?php if (isset($my_allowed_modules['sales'])) {?>
			<a href="<?php echo site_url('reports/summary_tips');?>" id='tips_report' report_type='tips'><?php echo lang('reports_tips'); ?></a>
	    <?php } ?>
        <?php if (isset($my_allowed_modules['giftcards'])) {?>
            <a href="<?php echo site_url('reports/summary_punch_cards');?>" id='punch_cards_report' report_type='punch_cards'><?php echo lang('reports_punch_card'); ?></a>
	    <?php } ?>
        </div>
	    </td>
	    <td id='filters' style='height:77px;'>
	    	<div id='filters_holder'>
	    		<?php $this->load->view("reports/filters"); ?>
	    	</div>
	    </td>
    </tr>
    <tr id='report'>
    	<td>
    		<div id='report_holder'>
    			<div id='default1'></div>
    		</div>
    	</td>
    </tr>
</table>
<?php if ($this->config->item('print_after_sale') && !$this->config->item('webprnt')) { ?>
<div style='height:1px; width:1px; overflow: hidden'>
       <applet id='qz' name="qz" code="qz.PrintApplet.class" archive="<?php echo base_url()?>/qz-print.jar" width="100" height="100">
           <param name="printer" value="<?=$printer_name?>">
           <!-- <param name="sleep" value="200"> -->
       </applet>
</div>
<?php } ?>

<?php $this->load->view("partial/footer"); ?>

<script>

	setInterval(function(){webprnt.print_all("http://<?=$this->config->item('webprnt_ip')?>/StarWebPrint/SendMessage")}, 3000);
	var reports = {
		initialize:function() {
			$('#new_button a').click(function(e){
				e.preventDefault();
				reports.select_report_type(e.target);
				reports.change_filters();
			})
		},
		select_report_type:function(anchor) {
			var srt = $('.selected_report_type');
			var lgrt = $('#last_generated_report').val();
			srt.removeClass('selected_report_type');
			$(anchor).addClass('selected_report_type');

			if ($(anchor).attr('report_type') == lgrt)
			{
				console.log('match');
				$('#report_contents').show();
				$('#default1').hide();
			}
			else
			{
				console.log('no match');
				$('#report_contents').hide();
				if ($('#default1').length == 0)
					$('#report_holder').append("<div id='default1'></div>");
				else
					$('#default1').show();
			}
		},
		change_filters:function() {
			var filter_array = [];
			var report_types = [];
			var selected_report_type = $('.selected_report_type').attr('report_type');
			switch(selected_report_type) {
				case 'customers':
					filter_array = ['date_range','customer','sale_type'];
					report_types = ['html','csv','pdf'];
					break;
				case 'employees':
					filter_array = ['date_range','employee','sale_type'];
					report_types = ['html','csv'];
					break;
				case 'register_log':
					filter_array = ['date_range','terminal'];
					report_types = ['html','pdf'];
					break;
				case 'time_clock':
					filter_array = ['date_range','employee'];
					report_types = ['html', 'pdf'];
					break;
				case 'z_out':
					filter_array = ['date_range', 'cash_log', 'terminal', 'employee'];
					report_types = ['html','pdf','csv'];
					break;
				case 'sales':
					filter_array = ['sale_id','date_range','department','sale_type','terminal', 'employee'];
					report_types = ['html','csv','pdf'];
					break;
				case 'summary_categories':
					filter_array = ['date_range','department','sale_type', 'terminal'];
					report_types = ['html','csv','pdf'];
					break;
				case 'gl_code':
					filter_array = ['date_range','department','sale_type','terminal'];
					report_types = ['html','csv','pdf'];
					break;
				case 'categories':
					filter_array = ['date_range','department','sale_type','terminal'];
					report_types = ['html','csv','pdf'];
					break;
				case 'tee_times':
					filter_array = ['date_range','teesheet'];
					report_types = ['html','pdf'];
					break;
				case 'teesheets':
					filter_array = ['date','teesheet'];
					report_types = ['pdf'];
					break;
				case 'play_length':
					filter_array = ['date_range', 'teesheet'];
					report_types = ['html'];
					break;
				case 'discounts':
					filter_array = ['date_range','sale_type'];
					report_types = ['html','csv','pdf'];
					break;
				case 'payments':
					filter_array = ['date_range','department','sale_type','terminal','day_split','employee'];
					report_types = ['html','csv','pdf'];
					break;
				case 'taxes':
					filter_array = ['date_range','department','category','subcategory','sale_type'];
					report_types = ['html','csv','pdf'];
					break;
				case 'summary_taxes':
					filter_array = ['date_range','department','category','subcategory','sale_type'];
					report_types = ['html','csv','pdf'];
					break;
				case 'deleted_sales':
					filter_array = ['date_range','sale_type'];
					report_types = ['html','csv','pdf'];
					break;
				case 'items':
					filter_array = ['date_range','department','category','subcategory','sale_type'];
					report_types = ['html','csv','pdf'];
					break;
				case 'inventory':
					filter_array = ['department','category','subcategory','supplier'];
					report_types = ['html','csv','pdf'];
					break;
				case 'item_kits':
					filter_array = ['date_range','sale_type'];
					report_types = ['html','csv','pdf'];
					break;
				case 'suppliers':
					filter_array = ['date_range','supplier','sale_type'];
					report_types = ['html','csv'];
					break;
				case 'receivings':
					filter_array = ['date_range','sale_type'];
					report_types = ['html','csv','pdf'];
					break;
				case 'giftcards':
					filter_array = ['giftcard'];
					report_types = ['html','csv','pdf'];
					break;
				case 'account_balances':
					filter_array = [];
					report_types = ['html','csv','pdf'];
					break;
				case 'account_transactions':
					filter_array = ['date_range', 'customer', 'account_type'];
					report_types = ['html','csv','pdf'];
					break;
				case 'rainchecks':
					filter_array = [];
					report_types = ['html','csv'];
					break;
				case 'tips':
					filter_array = ['date_range','employee'];
					report_types = ['html','csv'];
					break;
				case 'invoices':
					filter_array = ['invoice'];
					report_types = ['html','pdf'];
					break;
				case 'punch_cards':
					filter_array = [];
					report_types = ['html','csv','pdf'];
					break;
			}
			$('.filter_col').hide();
			for (var i in filter_array)
			{
				$('.'+filter_array[i]+'_col').show();
			}
			$('.report_type').addClass('disabled');
			for (var i in report_types)
			{
				var selector = '';
				if (report_types[i] == 'html')
					selector = '#generate_report';
				else if (report_types[i] == 'csv')
					selector = '#csv a';
				else if (report_types[i] == 'pdf')
					selector = '#pdf a';
				$(selector).removeClass('disabled');
			}
		},
		generate:function(type) {
			$('#last_generated_report').val($('.selected_report_type').attr('report_type'));
			$('#report_holder').mask('Generating Report');
			console.log(this.create_url(0));
			$.ajax({
	           type: "POST",
	           url: this.create_url(0),
	           data: '',
	           success: function(response){
					$('#report_holder').html(response);
					$('#report_holder').unmask();
					resize_table();
					$('.colbox').colorbox();
	          	},
	            dataType:'html'
	         });
		},
		create_url:function(type){
			var dept_cat_subcat = 'department';
			var val = 'all';
			var url = 'index.php/reports/';
			var simple_date = $('#simple_radio').attr('checked');
			var complex_date = $('#complex_radio').attr('checked');
			var range_simple = $('#report_date_range_simple option:selected').val().split(' ');
			var sale_id = $('#sale_id').val();
			
			if(!complex_date){
				var start_date = range_simple[0] + ' 00:00:00';
				var end_date = range_simple[1] + ' 23:59:59';
			}else{
				var start_date = $('#start_date_actual').val();
				var end_date = $('#end_date_actual').val();		
			}
			
			var rd = new Date($('#report_date').val());
			var report_date = rd.getFullYear()+(rd.getMonth() < 10 ? '0' : '')+(rd.getMonth())+(rd.getDate() < 10 ? '0' : '')+rd.getDate();
			var true_report_date = rd.getFullYear()+((rd.getMonth()+1 < 10 ? '0' : '')+(rd.getMonth()+1))+(rd.getDate() < 10 ? '0' : '')+rd.getDate();
			var teesheet_id = $('#teesheetMenu').val();
			var customer_id = $('#customer_id').val();
			var account_type = $('#account_type').val();
			var giftcard_id = $('#giftcard_id').val();
			var employee_id = $('#employee_id').val();
			var supplier_id = $('#supplier_id').val();
			if(!supplier_id){
				supplier_id = 0;
			}
			var terminal = encodeURIComponent($('#terminal').val());
			var day_split = $('#day_split_checkbox').attr('checked') ? 1 : 0;
			var show_cash_log = $('#show_cash_log').attr('checked') ? 1 : 0;
			var sale_department = encodeURIComponent($('#sale_department').val());
			var sale_category = encodeURIComponent($('#sale_category').val());
			var sale_subcategory = encodeURIComponent($('#sale_subcategory').val());
			var sale_type = $('#sale_type').val();
			var selected_report_type = $('.selected_report_type').attr('report_type');
			var rm = new Date($('#report_month').val());
			var report_month = rm.getFullYear()+'-'+(rm.getMonth() < 9 ? '0' : '')+(rm.getMonth()+1)+'-'+(rm.getDate() < 10 ? '0' : '')+rm.getDate();
			console.log('sd '+start_date+' ed '+end_date);
			if (sale_department != 'all')
			{
				dept_cat_subcat = 'department';
				val = sale_department;
			}
			else if (sale_category != 'all')
			{
				dept_cat_subcat = 'category';
				val = sale_category;
			}
			else if (sale_subcategory != 'all')
			{
				dept_cat_subcat = 'subcategory';
				val = sale_subcategory;
			}

			switch(selected_report_type) {
				case 'customers':
					if (customer_id == '')
						url += 'summary_customers/'+start_date+'/'+end_date+'/'+sale_type+'/'+type;
					else
						url += 'specific_customer/'+start_date+'/'+end_date+'/'+customer_id+'/'+sale_type+'/'+type;
					break;
				case 'employees':
					if (employee_id == '')
						url += 'summary_employees/'+start_date+'/'+end_date+'/'+sale_type+'/'+type;
					else
						url += 'specific_employee/'+start_date+'/'+end_date+'/'+employee_id+'/'+sale_type+'/'+type;
					break;
				case 'time_clock':
					if (employee_id == '')
						url += 'time_clock/'+start_date+'/'+end_date+'/0/'+type;
					else
						url += 'time_clock/'+start_date+'/'+end_date+'/'+employee_id+'/'+type;
					break;
				case 'z_out':
					url += 'summary_z_out/'+start_date+'/'+end_date+'/'+type+'/'+show_cash_log+'/'+terminal+'/'+employee_id;
					break;
				case 'sales':
					if (sale_id != '')
						url += 'detailed_sales/0000-00-00/3000-01-01/sales/'+type+'/all/'+encodeURIComponent(sale_id);
					else
						url += 'detailed_sales/'+start_date+'/'+end_date+'/'+sale_type+'/'+type+'/'+sale_department+'/0/'+terminal+'/'+employee_id;
					break;
				case 'invoices':
					url += 'invoices/' + report_month +'/' + type;
					break;
				case 'register_log':
					url += 'detailed_register_log/'+start_date+'/'+end_date+'/'+type+'/'+terminal;
					break;
				case 'categories':
					url += 'detailed_categories/'+start_date+'/'+end_date+'/'+sale_type+'/'+sale_department+'/'+type+'/'+terminal;
					break;
				case 'gl_code':
					url += 'summary_gl_code/'+start_date+'/'+end_date+'/'+sale_type+'/'+type+'/'+sale_department;
					break;
				case 'summary_categories':
					url += 'summary_categories/'+start_date+'/'+end_date+'/'+sale_type+'/'+type+'/'+sale_department+'/'+terminal;
					break;
				case 'tee_times':
					url += 'summary_tee_times/'+teesheet_id+'/'+start_date+'/'+end_date+'/'+type;
					break;
				case 'teesheets':
					url += 'detailed_teesheets/'+report_date+'/'+teesheet_id;
					break;
				case 'play_length':
					url += 'play_length/'+start_date+'/'+end_date+'/'+teesheet_id+'/'+type;
					break;
				case 'discounts':
					url += 'summary_discounts/'+start_date+'/'+end_date+'/'+sale_type+'/'+type;
					break;
				case 'payments':
					url += 'summary_payments/'+start_date+'/'+end_date+'/'+sale_type+'/'+type+'/'+sale_department+'/'+terminal+'/'+day_split+'/'+employee_id;
					break;
				case 'taxes':
					url += 'detailed_taxes/'+start_date+'/'+end_date+'/'+sale_type+'/'+type+'/'+sale_department+'/'+sale_category+'/'+sale_subcategory;
					break;
				case 'summary_taxes':
					url += 'summary_taxes/'+start_date+'/'+end_date+'/'+sale_type+'/'+type+'/'+sale_department+'/'+sale_category+'/'+sale_subcategory;
					break;
				case 'deleted_sales':
					url += 'deleted_sales/'+start_date+'/'+end_date+'/'+sale_type+'/'+type;
					break;
				case 'items':
					url += 'summary_items/'+start_date+'/'+end_date+'/'+sale_type+'/'+type+'/'+dept_cat_subcat+'/'+encodeURIComponent(encodeURIComponent(val));
					break;
				case 'inventory':
					url += 'inventory_summary_2/'+type+'/'+dept_cat_subcat+'/'+val+'/'+supplier_id;
					break;
				case 'item_kits':
					url += 'summary_item_kits/'+start_date+'/'+end_date+'/'+sale_type+'/'+type;
					break;
				case 'suppliers':
					url += 'summary_suppliers/'+start_date+'/'+end_date+'/'+supplier_id+'/'+sale_type+'/'+type;
					break;
				case 'receivings':
					url += 'detailed_receivings/'+start_date+'/'+end_date+'/'+sale_type+'/'+type;
					break;
				case 'giftcards':
					url += 'summary_giftcards/'+giftcard_id+'/'+type;
					break;
				case 'account_balances':
					url += 'summary_account_balances/'+type;
					break;
				case 'account_transactions':
					if(!customer_id){
						customer_id = 0;
					}
					url += 'detailed_account_transactions/'+start_date+'/'+end_date+'/'+customer_id+'/'+account_type+'/'+type;
					break;
				case 'rainchecks':
					url += 'summary_rainchecks/'+type;
					break;
				case 'tips':
					url += 'summary_tips/'+start_date+'/'+end_date+'/'+employee_id+'/'+type;
					break;
				case 'punch_cards':
					url += 'summary_punch_cards/'+type;
					break;
			}
			return url;
		}
	};
	$(document).ready(function(){
		reports.initialize();
		if ('<?=$report_type?>' != '')
		{
			$('#<?=$report_type?>_report').click();
			reports.generate();
		}
	});
</script>
<style>
	#sale_id { width:75px;}
</style>