<div id='user_menu_contents'>
	<div id='user_menu_header'>
	</div>
	<div id='user_actions'>
		<?php
			$allowed_modules = ($this->Module->get_allowed_modules($this->session->userdata('person_id')));
			foreach($allowed_modules->result() as $module)
			{
				$module_list[] = $module->module_id;
			}
			?>
		<div id="home_module_list">
			<div class="module_item">
				<a id='timeclock_button' href="<?php echo site_url("timeclock");?>">
					<span class="module_icon timeclock_icon"></span>
					<div>
						<div><?php echo lang("module_timeclock") ?></div>
						<div class='module_desc'><?php echo lang('module_timeclock_desc');?></div>
					</div>
				</a>
			</div>
			<script>
				$('#timeclock_button').colorbox({width:500,title:'Time Clock'});
			</script>
			<?php
		        if ($this->permissions->is_super_admin() || ($this->permissions->course_has_module('employees') && in_array('config',$module_list)))
		        {
		
			?>
			<div class="module_item">
				<a href="<?php echo site_url("employees");?>">
					<span class="module_icon employees_icon"></span>
					<div>
						<div><?php echo lang("module_employees") ?></div>
						<div class='module_desc'><?php echo lang('module_employees_desc');?></div>
					</div>
				</a>
			</div>
			<?php
			}
			?>
			<?php
		        if ($this->permissions->is_super_admin() || ($this->permissions->course_has_module('config') && in_array('config',$module_list)))
		        {
		
			?>
			<div class="module_item">
				<a href="<?php echo site_url("config");?>">
					<span class="module_icon config_icon"></span>
					<div>
						<div><?php echo lang("module_config") ?></div>
						<div class='module_desc'><?php echo lang('module_config_desc');?></div>
					</div>
				</a>
			</div>
			<?php
			}
			?>
			<div class="module_item">
				<a href="<?php echo site_url("support");?>" target='_blank'>
					<span class="module_icon support_icon"></span>
					<div>
						<div>Support</div>
						<div class='module_desc'>Help section</div>
					</div>
				</a>
			</div>
			<div class="module_item">
				<a href="index.php/home/logout" id='log_out_link'>
					<span class="module_icon logout_icon"></span>
					<div>
						<div>Logout</div>
						<div class='module_desc'>Sign out of ForeUP</div>
					</div>
				</a>
			</div>
			<?php 
				$has_sales_permissions = false;
				$allowed_modules = $this->Module->get_allowed_modules($this->session->userdata('person_id'));
				foreach ($allowed_modules->result_array() as $allowed_module)
				{
					$has_sales_permission = ($has_sales_permission ? $has_sales_permission : $allowed_module['module_id'] == 'sales');
				}
				
				if ($this->config->item('track_cash') && $has_sales_permission) { ?>
				<script>
					$(document).ready(function(){
						$('#log_out_link').click(function(e){
							e.preventDefault();
							$.ajax({
							   type: "POST",
							   url: "index.php/home/logout_options/",
							   data: "",
							   success: function(response){
									if (response.open_log)
									{
										$.colorbox({html:response.html, 'width':350, 'title':'Log Out', onComplete:function(){
											$('#switch_user').click(function(e){
												e.preventDefault();
												system.switch_user();
											});
										}});
									}
									else
									{
										window.location = "index.php/home/logout";
									}
							   },
							   dataType:'json'
							});
							
						});
					});
					var system = {
						login: {
							submit:function(controller){
								controller = controller == undefined ? 'food_and_beverage' : controller;
								$.ajax({
								   type: "POST",
								   url: "index.php/"+controller+"/login/",
								   data: "pin_or_card=" + encodeURIComponent($('#pin_or_card').val().replace('%','!')),
								   success: function(response){
										if (response.success)
										{
											//$('#software_menu_contents').replaceWith(response.software_menu_contents); TODO
											//$('#user_menu_contents').replaceWith(response.user_menu_contents); TODO
											$('#user_button .menu_title').html(response.employee.first_name + ' ' + response.employee.last_name);
											$('#user_button').show();
					
											App.receipt.header.employee_name = response.employee.last_name + ', ' + response.employee.first_name;
											App.employee_id = response.employee.person_id;
											App.recent_transactions.reset(response.recent_transactions);
											App.register_log = new RegisterLog(response.register_log);
					
											// Show register log window, if no log has been set yet
											if(App.track_cash != 0 && App.register_log && !App.register_log.get('register_log_id')){
					
												var registerLog = new RegisterLogView({model: App.register_log });
												$.colorbox({
													title: 'Open Register',
													html: registerLog.render().el,
													width: 600,
													height: 400
												});
					
											// Show table select window
											}else{
												reload_tables(function(response){
													fnb.show_tables();
												});
											}
										}
										else
										{
											set_feedback(response.message,'warning_message',false, 5000);
										}
								   },
								   dataType:'json'
								});
							},
					
							show:function(){
								$.colorbox({
									transition: 'none',
									title: 'Switch User',
									escKey: false,
									closeButton: false,
									open: true,
									opacity: 1,
									overlayClose: false,
									href: 'index.php/sales/logout',
									onLoad:function(){
										$('#cboxClose').remove();
									},
									onComplete:function(){
										$('#cboxClose').remove();
										$('#pin').focus();
										fnb.login.initialize();
									},
									width: 400,
									height: 400
								});
							},
					
							initialize:function(){
								$('.number_one').click(function(){fnb.login.add_character(1)});
								$('.number_two').click(function(){fnb.login.add_character(2)});
								$('.number_three').click(function(){fnb.login.add_character(3)});
								$('.number_four').click(function(){fnb.login.add_character(4)});
								$('.number_five').click(function(){fnb.login.add_character(5)});
								$('.number_six').click(function(){fnb.login.add_character(6)});
								$('.number_seven').click(function(){fnb.login.add_character(7)});
								$('.number_eight').click(function(){fnb.login.add_character(8)});
								$('.number_nine').click(function(){fnb.login.add_character(9)});
								$('.number_zero').click(function(){fnb.login.add_character(0)});
								$('.number_backspace').click(function(){fnb.login.remove_character(true)});
								$('.number_login').click(function(){fnb.login.submit()});
					
								$('#pin').bind('keypress', function(e) {
									  e.preventDefault();
									 var code = (e.keyCode ? e.keyCode : e.which);
									 if(code == 13) { //Enter keycode
									   //Do something
									   e.preventDefault();
									   fnb.login.submit();
									   return;
									 }
									 var c = String.fromCharCode(code);
									 console.log('character '+c);
									 fnb.login.add_character(c);
								}).bind('keyup', function(e){
									 var code = (e.keyCode ? e.keyCode : e.which);
									 if (code == 8 || code == 46) {
										e.preventDefault();
										console.log('backspace');
										fnb.login.remove_character();
									 }
								});
					
								$('#pin').val('');
								$('#pin_or_card').val('');
							},
							add_character:function(number){
								console.log('adding_character');
								$('#pin').val($('#pin').val()+'*')
								$('#pin_or_card').val($('#pin_or_card').val()+''+number)
					
								$('#pin').focus();
							},
							remove_character:function(both){
								console.log('adding_character');
								if (both === true)
								{
									var str = $('#pin').val();
									$('#pin').val(str.substring(0, str.length - 1));
								}
								var str2 = $('#pin_or_card').val();
								$('#pin_or_card').val(str2.substring(0, $('#pin').val().length));
					
								$('#pin').focus();
							}
						},
						logout:function() {
							
						},
						switch_user:function() {
							system.login.show();
							// $.ajax({
							   // type: "POST",
							   // url: "index.php/home/switch_user/",
							   // data: "",
							   // success: function(response){
									// $('#menubar_stats').html('');
									// $('#user_button').hide();
// 									
							   // },
							   // dataType:'json'
							// });
						}
					};
				</script>
			<?php } ?>
			<div class='clear'></div>
		</div>
	</div>
</div>