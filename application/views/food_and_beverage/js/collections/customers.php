var CustomerCollection = Backbone.Collection.extend({
	url: App.api_table + 'customers',
	model: function(attrs, options){
		return new Customer(attrs, options);
	},
	
	initialize: function(){
		this.on('add', this.applyDiscount);
	},

	applyDiscount: function(){
		var customer = this.at(0);
		var discount = customer.get('discount');
		// Loop through items currently in cart and apply discount
		_.each(App.cart.models, function(item){
			item.set('discount', discount);
			item.save();
		});
	},
	
	byValidMemberBalance: function() {
		filtered = this.filter(function(customer) {
			return customer.get("member_account_balance") > 0 || customer.get("member_account_balance_allow_negative") == '1';
		});
		return new CustomerCollection(filtered);
	},

	byValidAccountBalance: function() {
		filtered = this.filter(function(customer) {
			return customer.get("account_balance") > 0 || customer.get("account_balance_allow_negative") == '1';
		});
		return new CustomerCollection(filtered);
	}
});