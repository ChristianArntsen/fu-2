var ItemSideCollection = Backbone.Collection.extend({
	model: function(attrs, options){
		return new ItemSide(attrs, options);
	},

	getTotalPrice: function(){
		var total = 0.00;

		if(this.models.length > 0){
			_.each(this.models, function(side){
				total += parseFloat(side.get('price'));
			});
		}
		return parseFloat(accounting.toFixed(total, 2));
	},

	getTotalTax: function(){
		var total = 0.00;

		if(this.models.length > 0){
			_.each(this.models, function(side){
				total += parseFloat(side.get('tax'));
			});
		}
		return parseFloat(accounting.toFixed(total, 2));
	}
});