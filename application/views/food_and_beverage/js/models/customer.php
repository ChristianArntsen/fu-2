var Customer = Backbone.Model.extend({
	idAttribute: "person_id",
	defaults: {
		"first_name": "",
		"last_name": "",
		"email": "",
		"phone": "",
		"photo": "",
		"account_number": "",
		"member_account_balance": 0.00,
		"account_balance": 0.00,
		"discount":0.00
	}
});
