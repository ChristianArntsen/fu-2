var AddTipView = Backbone.View.extend({
	
	tagName: "div",
	template: _.template( $('#template_add_tip').html() ),

	events: {
		"click a.save": "saveTip",
		"click button.type": "setType"
	},
	
	initialize: function(){
		this.setType('cash');
	},
	
	render: function() {
		var view = this;
		var attributes = this.model.attributes;
		attributes.tip = this.tip.attributes;

		this.$el.html(this.template(attributes));
		
		this.$el.find('#tip_amount').keypad({position: 'right'});	
		this.$el.find("#tip_employee_search").autocomplete({
			source: '<?php echo site_url("sales/employee_search"); ?>',
			delay: 50,
			autoFocus: true,
			minLength: 1,
			select: function(event, ui)
			{
				$(this).val(ui.item.label);
				view.$el.find("#tip_recipient").val(ui.item.value);
				return false;
			}
		});		
		return this;
	},
	
	setType: function(data){
		var invoice_id = 0;

		if(data.currentTarget){
			var button = $(data.currentTarget);
			var type = button.data('type');
			var payment_type = button.text();
			var invoice_id = button.data('invoice-id');

		}else{
			var type = data;
		}
		var tips = this.model.get('tips');
		
		// If a tip of the same type already exists, use it, otherwise
		// create a new tip
		if(invoice_id && tips.findWhere({'invoice_id': invoice_id})){
			tipModel = tips.findWhere({'invoice_id': invoice_id});	
		
		}else if(type != 'credit_card' && tips.findWhere({'type': type})){
			tipModel = tips.findWhere({'type': type});
		
		}else{	
			// Create more readable payment type
			if(type == 'cash'){
				payment_type = 'Cash Tip';
			}else if(type == 'check'){
				payment_type = 'Check Tip';
			}else if(type == 'member_balance'){
				payment_type = App.member_account_name + ' Tip - '+ this.model.get('last_name') +', '+ this.model.get('first_name');
			}else if(type == 'customer_balance'){
				payment_type = App.customer_account_name + ' Tip - '+ this.model.get('last_name') +', '+ this.model.get('first_name');
			}
			
			tipModel = new Tip({'customer_id': this.model.get('customer_id'), 'payment_type': payment_type, 'invoice_id':invoice_id, 'type':type, 'amount':0.00, 'tip_recipient':App.employee_id, 'tip_recipient_name':App.receipt.header.employee_name});			
		}

		this.tip = tipModel;			
		this.render();
	},
	
	saveTip: function(event){
		var data = {};
		data.type = $('#tip_type').val();
		data.amount = $('#tip_amount').val();
		data.invoice_id = $('#tip_invoice_id').val();
		data.customer_id = $('#tip_customer_id').val();
		data.tip_recipient = $('#tip_recipient').val();
		data.tip_recipient_name = $('#tip_employee_search').val();
		data.payment_type = $('#tip_payment_type').text();
		
		this.tip.set(data, {silent:true});
		$('#cbox2LoadedContent').mask('Applying Tip');
		
		this.model.get('tips').create(this.tip.attributes, {'wait':true, 'merge':true, 'success':function(){
			$('#cbox2LoadedContent').unmask();
			$.colorbox2.close();
		}});

		event.preventDefault();
	}
});
