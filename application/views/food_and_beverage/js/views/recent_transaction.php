var RecentTransactionView = Backbone.View.extend({
	tagName: "li",
	template: _.template( $('#template_recent_transaction').html() ),

	events: {
		"click button.add_tip": "addTip",
		"click button.print_receipt": "printReceipt",
		"click button.view_receipt": "viewReceipt"
	},

	initialize: function() {
		this.listenTo(this.model, "change", this.render);
		this.listenTo(this.model.get('tips'), "add remove change", this.render);
	},

	render: function() {
		this.$el.html(this.template(this.model.attributes));
		return this;
	},

	// Opens the receipt details in a new window
	viewReceipt: function(event){
		var recentTransactionDetails = new RecentTransactionDetailsView({model:this.model});
		$.colorbox2({
			title: 'Sale #' + this.model.get('sale_id'),
			html: recentTransactionDetails.render().el,
			width: 500
		});
	},

	printReceipt: function(event){

		var sale = this.model;
		var data = {
			cart: sale.get('items'),
			payments: sale.get('payments'),
			subtotal: sale.get('subtotal'),
			tax: sale.get('tax'),
			total: sale.get('total'),
		};

		var receipt_data = '';
		receipt_data = webprnt.build_itemized_receipt(receipt_data, data);

		if (App.receipt.return_policy != '')
		{
	    	receipt_data = webprnt.add_return_policy(receipt_data, App.receipt.return_policy);
	    }
	    var header = App.receipt.header;
	    header.table_number = '';
	    header.employee_name = sale.get('employee_name');
	    header.customer = sale.get('customer_name');

	    receipt_data = webprnt.add_itemized_header(receipt_data, header, sale.get('sale_id'))
	    receipt_data = webprnt.add_receipt_header(receipt_data, header)
	    receipt_data = webprnt.add_paper_cut(receipt_data);

	    webprnt.add_space(receipt_data);
	    webprnt.print(receipt_data, "http://"+App.receipt_ip+"/StarWebPrint/SendMessage");

		return false;
	},

	addTip: function(event){

		var addTipWindow = new AddTipView({model: this.model});

		$.colorbox2({
			title: 'Add/Edit Tip',
			html: addTipWindow.render().el,
			width: 600,
			onClosed: function(){
				addTipWindow.remove();
			}
		});

		return false;
	}
});
