<style>
div.keypad-container {
	padding: 15px;
	width: 400px;
	height: 300px;
}

div.keypad-container > h2 {
	text-align: center;
}

input.pin {
	display: block;
	width: auto;
	margin: 15px auto 10px auto;
	width: 240px;
}
</style>
<script type="text/html" id="template_authorize">
	<div class="keypad-container">
		<h2>Cancel Sale</h2>
		<div id="login_keypad">
			<input type="password" name="pin" class="pin" value="" />
			<div class="number_pad">
				<a data-char="1">1</a>
				<a data-char="2">2</a>
				<a data-char="3">3</a>
				<a data-char="4">4</a>
				<a data-char="5">5</a>
				<a data-char="6">6</a>
				<a data-char="7">7</a>
				<a data-char="8">8</a>
				<a data-char="9">9</a>
				<a class="special" data-char="enter">Enter</a>
				<a data-char="0">0</a>
				<a class="special" data-char="del">←</a>
			</div>		
		</div>
	</div>
</script>
