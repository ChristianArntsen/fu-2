<style>
html[xmlns] div.hidden {
	display:none;
}

ul.payments > li.payment {
	display: block;
	overflow: hidden;
	font-size: 18px;
	margin-top: 10px;
}

ul.payments > li.payment span, ul.payments > li.payment a  {
	padding: 5px;
	float: left;
	display: block;
}

ul.payments > li.payment span.amount {
	float: right;
	width: 50px;
	text-align: right;
	font-size: 18px;
}

ul.payments > li.payment span.type {
	width: 225px;
}

#make_payment {
	max-height: 500px;
	min-height: 200px;
	margin: 15px !important;
	padding: 0px;
	display: block;
	width: auto;
}

#make_payment a.payment-customer {
	margin: 0px 0px 5px 0px !important;
}

#make_payment div.left {
	width: 290px;
	overflow: hidden;
	float: left;
}

#make_payment div.left.max {
	float: none;
	display: block;
	width: auto;
}

#make_payment .disabled {
	background: #D9D9D9 !important;
	border: 1px solid #CCC !important;
	color: #AAA !important;
	text-shadow: none !important;
}

#make_payment .payment-amount {
	margin: 0px;
	width: 325px;
	float: right;
}

#make_payment div.due_amount {
	font-size: 20px;
	width: 80px;
}

#make_payment .due_amount_label {
	margin-top: 8px;
	padding: 5px;
	float: right;
}

#make_payment .show_payment_buttons {
	height: 35px;
	line-height: 35px;
	padding: 0px;
	width: 80px;
	display: block;
	float: none;
	margin: 0px 0px 10px 0px !important;
}

#payment_gift_card label {
	display: block;
	font-size: 14px;
	height: 16px;
	line-height: 16px;
}
</style>
<script type="text/html" id="template_payment_window">
<div id="make_payment" style="width: auto; display: block;">
	<div class="left">
		<!-- Payment Buttons -->
		<div id="payment_buttons" class="payment-types">
			<button id='use_credit_card' style="margin-left: 0px;" class='fnb_button'>Credit Card</button>
			<button id='use_cash' class='fnb_button'>Cash</button>
			<button id='use_check' style="margin-left: 0px;"  class='fnb_button'>Check</button>
			<button id='use_gift_card' class='fnb_button'>Gift Card</button>
			<button id='use_customer_account' style="margin-left: 0px;" class='fnb_button <% if(!enable_customer_balance){ print('disabled'); } %>'><%=App.customer_account_name %></button>
			<button id='use_member_account' class='fnb_button <% if(!enable_member_balance){ print('disabled'); } %>'><%=App.member_account_name %></button>
		</div>

		<!-- Giftcard window -->
		<div id='payment_gift_card' style='display: none'>
			<a class="fnb_button show_payment_buttons" href="#">Back</a>
			<label for="payment_giftcard_number">Giftcard #</label>
			<div class='form_field'>
				<input type="text" name="giftcard_number" value="" id="payment_giftcard_number" size="20" autocomplete="off" maxlength="16"  />
			</div>
			<div class='clear'>
				<a id="pay_gift_card" class="fnb_button float_right" href="#" style="margin: 0px">Pay</a>
			</div>
		</div>
		
		<!-- ETS gift card window -->
		<div id='payment_ets_gift_card' style="display: none"></div>
		
		<!-- Punch card window -->
		<div id='payment_punch_card' style="display: none">
			<label for="punch_card_number" class="required">Punch Card #:</label>
			<div class='form_field'>
				<input type="text" name="punch_card_number" value="" id="punch_card_number" size="20" autocomplete="off" maxlength="16"  />
			</div>
			<div class='clear'>
				<a class="fnb_button show_payment_buttons" href="#">Back</a>
				<input type="submit" name="submit" value="Pay" id="submit2" class="submit_button float_right"  />
			</div>
		</div>

		<!-- Credit card window -->
		<div id='payment_credit_card' style="display: none"></div>

		<!-- Member/customer account balance window -->
		<div id='payment_account' style="display: none"></div>
	</div>
	<div class="payment-amount">
		<div style="width: auto; display: block; overflow: hidden;">
			<div class='form_field'>
				<input type="text" name="amount_tendered" value="<%-accounting.formatMoney(amount_due, '') %>" id="payment_amount_tendered" autocomplete="off" style="text-align: right; width: 75px; float: right;"  />
				<label for="amount_tendered" style="float: right; width: 130px;">Amount Tendered</label>
			</div>
		</div>
		<div style="display: block; overflow: hidden; width: auto;">
			<ul class="payments"></ul>
			<div class='due_amount' style="padding: 5px;" id='due_amount'><%=accounting.formatMoney(amount_due) %></div>
			<div class="due_amount_label">Total Due:</div>
		</div>
	</div>
</div>
<input type="hidden" id="payment_type" name="payment_type" value="Cash" />
<input type="hidden" id="payment_gc_number" name="payment_gc_number" value="" />
<div class='clear'></div>
</script>
