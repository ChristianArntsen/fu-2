<?php if ($pdf) { ?>
	<style>
		#inventory_audit_table {
			font-size:10px;
		}
	</style>
<?php } ?>
<style>
	#inventory_audit_table {
		margin:20px 0px 20px 20px;
		width:95%;
	}
	#inventory_audit_table input.manual_count {
		width:50px;
		text-align:right;
	}
	.count_column {
		text-align:left;
	}
	.inv_data{
		padding: 0px !important;
	}
</style>
<?php 
$total_price_lost = 0; 
$total_cost_lost = 0; 
$total_count_lost = 0; 
?>
<table id='inventory_audit_table'>
	<?php if (!$pdf) { ?>
	<tr>
		<td></td>
		<?php if ($inventory_audit_id > 0) { ?>
			<td></td>
			<td></td>
			<td></td>
		<?php } ?>
		<td>
			<?php if ($inventory_audit_id > 0 && !$this->permissions->is_employee()) { ?>
				<a id='reset_item_counts_link' href="#">Reset Item Counts</a>
			<?php } ?>
		</td>
		<td>
			<a id='audit_export_link' target='_blank' href="index.php/items/pdf_inventory_audit/<?=$inventory_audit_id?>">Export to PDF</a>
		</td>
		<td>
			<?php if ($inventory_audit_id > 0 && !$this->permissions->is_employee()) { ?>
			<div class="saveEdit">
				<a id='audit_edit'>Edit</a>
			</div>
			<div style="display:none" class="update_count">
				<input type='submit' id='save_count' class='save_count' value='Save' />
			</div>
			<?php } ?>
		</td>
	</tr>
	<?php } ?>
	<tr>
		<td class='inv_data'>UPC/Item Number</td>
		<td class='inv_data'>Item Name</td>
		<?php if ($inventory_audit_id > 0) { ?>
			<td class='inv_data'>System Count</td>
		<?php } ?>
		<td class='inv_data'>Manual Count</td>
		<?php if ($inventory_audit_id > 0) { ?>
			<td class='inv_data'>Difference</td>
			<td class='inv_data'>Revenue Difference</td>
			<td class='inv_data'>Cost Difference</td>
		<?php } ?>
	</tr>
<?php 
$distinct_departments = array();
foreach($items as $item){
    $item_html = '';
    //Department
    if($item['department'] != '' && !in_array($item['department'], $distinct_departments)) {
        array_push($distinct_departments, $item['department']);
        $item_html = "<tr>";
        $item_html .= "<td class='inv_data'>{$item['department']}</td>";
        $item_html .= "<td></td><td></td>";
        if ($inventory_audit_id > 0) {
            $item_html .= "<td></td><td></td><td></td>";
        }
        $item_html .= "</tr>";
    }
	$item_html .= "<tr id='{$item['item_id']}'>
		<td>{$item['item_number']}</td>
		<td>{$item['name']}</td>";
	if ($inventory_audit_id > 0) {
		$item_html .= "<td class='count_column'>{$item['current_count']}</td>";	
	}		
	if ($inventory_audit_id > 0) {
		$item_html .= "<td id='m_count' class='count_column m_count'>{$item['manual_count']}</td>";	
	}
	else 
	{
		$item_html .= "<td>
			<input type='hidden' name='item_ids[]' value='{$item['item_id']}'/>
			<input type='text' name='manual_counts[]' class='manual_count' />
			<input type='hidden' name='system_counts[]' value='{$item['quantity']}'/>
		</td>";
	}		
	if ($inventory_audit_id > 0) {
		$item_html .= "<td class='count_column'>".($item['manual_count'] - $item['current_count'])."</td>";	
		$item_html .= "<td class='count_column'>".to_currency(($item['manual_count'] - $item['current_count']) * $item['unit_price'])."</td>";
		$item_html .= "<td class='count_column'>".to_currency(($item['manual_count'] - $item['current_count']) * $item['cost_price'])."</td>";
		
		$total_count_lost += ($item['manual_count'] - $item['current_count']);
		$total_price_lost += ($item['manual_count'] - $item['current_count']) * $item['unit_price'];
		$total_cost_lost += ($item['manual_count'] - $item['current_count']) * $item['cost_price'];
	}		
				
	$item_html .= "</tr>";	
	
	echo $item_html;
}
if ($inventory_audit_id > 0) {
		 echo "<tr>
				<td></td>
				<td></td>
				<td></td>
				<td>Total: </td>
				<td class='count_column'>$total_count_lost</td>
				<td class='count_column'>".to_currency($total_price_lost)."</td>
				<td class='count_column'>".to_currency($total_cost_lost)."</td>
			</tr>";
	}
?>
<?php if ($inventory_audit_id > 0 && !$this->permissions->is_employee()) { ?>
	<input type="hidden" name="inventory_audit_id" value="<?=$inventory_audit_id?>"/>
	<input type="hidden" name="status_flag" value="update"/>
<?php } ?>
</table>
<?php if (!$pdf) { ?>
<script>
	$(document).ready(function(){
		$('#audit_edit').click (function() {
			$('#audit_edit').hide();
			$( ".update_count" ).show();
        	$("table td .m_count").each(function() {
	            var item_id = $(this).closest('tr').attr('id');
	            var updated_manual_count = $(this).text();
	            $(this).html('<input type="text" name="updated_counts_' + item_id + '" class="edit_count" value="' + updated_manual_count + '" >');
        		$( ".save_count" ).removeClass( "save_count" );
        	});
    	});
    	$( "#save_count" ).live('click',function() {
   			$('#audit_edit').show();
   			$( ".update_count" ).hide();
    	});	
    	
		$('#reset_item_counts_link').click(function(e){
			e.preventDefault();
			if (confirm('Are you sure you want to reset your current inventory levels to match your manual counts from this audit?'))
			{
				$('#cboxLoadedContent').mask("<?php echo lang('common_wait'); ?>");
				$.ajax({
	               type: "POST",
	               url: "index.php/items/reset_item_counts/<?=$inventory_audit_id?>",
	               data: "",
	               success: function(response){
						if (response.success)
							set_feedback('Inventory counts successfully updated', 'success_message');
						else
							set_feedback('There was an error updating inventory counts', 'error_message');
							
						$('#cboxLoadedContent').unmask();
	               },
	               dataType:'json'
	            });
			}
		});
});
</script>
<?php } ?>