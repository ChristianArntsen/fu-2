<?php

/**
 * Result container object for the SOAP ->getLastError() method call
 * 
 * @author Keith Palmer <keith@consolibyte.com>
 * @license LICENSE.txt 
 * 
 * @package QuickBooks
 * @subpackage Server
 */

/**
 * Result base class
 */
require_once 'QuickBooks/Result.php';

/**
 * Result container object for the SOAP ->getLastError() method call
 */
class QuickBooks_Result_GetLastError extends QuickBooks_Result
{
	/**
	 * An error message
	 * 
	 * @param string $resp
	 */
	public $getLastErrorResult;
	
	/**
	 * Create a new result object
	 * 
	 * @param string $resp 		A message describing the last error that occured
	 */
	public function __construct($result, $status = null, $wait_before_next_update = null, $min_run_every_n_seconds = null)
	{
		$this->getLastErrorResult = $result;
	}
}
