<?php
/*************************************************
 * change   author    date          remarks
 * 0.1      MPogay   08-JUN-2012    add validation for user
 **************************************************/
class Login extends CI_Controller 
{
  private $mode;

	function __construct()
	{
		parent::__construct();
		$this->load->library('encrypt');
        $this->load->model('Member');
	}
	
	function index()
	{
	    // check login validation mode
	    $mobile = $_GET['mobile'] ? $_GET['mobile'] : $_POST['mobile'];
		$mode = $this->session->userdata('login_mode');
	    $this->mode = $mode !== false ? $mode : 'default';
	    
		if($this->Employee->is_logged_in())
		{
			redirect('home');
		}
	    // addition Check
	    if($this->Member->is_logged_in())
	    {
	      redirect('user');
	    }
		else
		{
			$this->form_validation->set_rules('username', 'lang:login_undername', 'callback_login_check');
	        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	
			if($this->form_validation->run() == FALSE)
			{
				if($mobile)
				{
					echo json_encode(array('status' => 'failed', 'message'=>'failed log in', 'session'=>'', 'terminals'=>''));
					return;
				}
				$data['welcome_message'] = 'login_welcome_message';
				$this->load->view('login/login', $data);
			}
			else
			{
				if($mobile)
				{
					$this->load->model('Course');
					$course_info = $this->Course->get_info($this->session->userdata('course_id'));
					$employee = $this->Employee->get_info($this->session->userdata('person_id'));
					$use_terminals = $course_info->use_terminals;
					$use_register_log = $course_info->track_cash;
					$blind_close = $course_info->blind_close;
					$integrated_processor = ($course_info->ets_key != '' ? 'ETS' : ($course_info->mercury_id != '' ? 'MPS' : ''));
					$course_name = $course_info->name;
					$terminals = array();
					if ($use_terminals)
					{
						$this->load->model('Terminal');
						$terminals = $this->Terminal->get_all()->result_array();
					}
					// ADD LOGGED IN TERMINALS (TO BLOCK ANYONE ALREADY LOGGED IN TO A SPECIFIC TERMINAL)
					$this->load->model('Sale');
					$terminal_array = $this->Sale->getUnfinishedRegisterLogs()->result_array();
					$logged_in_terminals = array();
					foreach($terminal_array as $terminal)
						$logged_in_terminals[] = array('terminal_id'=>$terminal['terminal_id'], 'logged_in_date'=>$terminal['shift_start']);  
						
					// CREATE SESSION DATA
					$session =  serialize(array(
						'session_id'=>$this->session->userdata('session_id'),
						'ip_address'=>$this->session->userdata('ip_address'),
						'user_agent'=>$this->session->userdata('user_agent'),
						'last_activity'=>$this->session->userdata('last_activity')
					));
					$hash = md5($session.$this->config->item('encryption_key'));
					// $session =  json_encode(array(
						// 'session_id'=>$this->session->userdata('session_id'),
						// 'ip_address'=>$this->session->userdata('ip_address'),
						// 'user_agent'=>$this->session->userdata('user_agent'),
						// 'last_activity'=>$this->session->userdata('last_activity')
					// ));
					// $hash = '';
					echo json_encode(array(
						'status' => 'success', 
						'message'=>'successfully logged in', 
						'session'=>$session.$hash, 
						'e2e_account_id'=>$course_info->e2e_account_id,
						'e2e_account_key'=>$course_info->e2e_account_key,
						'terminals'=>$terminals, 
						'logged_in_terminals'=>$logged_in_terminals,
						'use_terminals'=>$use_terminals, 
						'use_register_log'=>$use_register_log, 
						'blind_close'=>$blind_close,
						'customer_credit_nickname'=>$course_info->customer_credit_nickname,
						'member_balance_nickname'=>$course_info->member_balance_nickname,
						'course_info'=>array( 
							'name'=>$course_name,
							'address'=>$course_info->address,
							'city'=>$course_info->city,
							'state'=>$course_info->state,
							'zip'=>$course_info->zip,
							'phone'=>$course_info->phone,
							'email'=>$course_info->email,
							'website'=>$course_info->website
						),
						'employee_info'=>array(
							'employee_id'=>$employee->person_id,
							'first_name'=>$employee->first_name,
							'last_name'=>$employee->last_name,
							'user_level'=>$employee->user_level
						),
						'print_options'=>array(
							'auto_print_receipts'=>$course_info->print_after_sale,
							'receipt_ip'=>$course_info->webprnt_ip,
							'hot_webprnt_ip'=>$course_info->webprnt_hot_ip,
							'cold_webprnt_ip'=>$course_info->webprnt_cold_ip,
							'signature_slip_count'=>$course_info->print_credit_card_receipt ? ($course_info->print_two_signature_slips ? 2 : 1) : 0,
							'print_tip_line'=>$course_info->print_tip_line,
							'credit_card_receipt_count'=>$course_info->print_sales_receipt ? ($course_info->print_two_receipts ? 2 : 1) : 0,
							'non_credit_card_receipt_count'=>$course_info->print_sales_receipt ? ($course_info->print_two_receipts_other ? 2 : 1) : 0
						),
						'integrated_processor'=>$integrated_processor
					));
					return;
				}
				if($this->mode == 'default') redirect('home');
				if($this->mode == 'user') redirect('user');
			}
		}
	}
        
	function login_check($username)
	{
		$password = $this->input->post("password");	
		$valid = false;
	    if($this->mode == 'default') $valid = $this->Employee->login($username,$password);
	    if($this->mode == 'user') $valid = $this->Member->login($username,$password);

		if(!$valid)
		{
			$this->form_validation->set_message('login_check', lang('login_invalid_username_and_password'));
			return false;
		}
		return true;		
	}
	
	function reset_password()
	{
		$this->load->view('login/reset_password');
	}
	
	function do_reset_password_notify()
	{
		$employee = $this->Employee->get_employee_by_username_or_email($this->input->post('username_or_email'));
		if ($employee)
		{
			$data = array();
			$data['employee'] = $employee;
		    $data['reset_key'] = base64url_encode($this->encrypt->encode($employee->person_id.'|'.(time() + (2 * 24 * 60 * 60))));
			
			send_sendgrid(
				$employee->email,
				lang('login_reset_password'),
				$this->load->view("login/reset_password_email",$data, true),
				'support@foreup.com',
				'ForeUP Support'
			);
		}
		
		$this->load->view('login/do_reset_password_notify');	
	}
	
	function reset_password_enter_password($key=false)
	{
		if ($key)
		{
			$data = array();
		    list($employee_id, $expire) = explode('|', $this->encrypt->decode(base64url_decode($key)));			
			if ($employee_id && $expire && $expire > time())
			{
				$employee = $this->Employee->get_info($employee_id, true);
				$data['username'] = $employee->username;
				$data['key'] = $key;
				$this->load->view('login/reset_password_enter_password', $data);			
			}
		}
	}
	
	function do_reset_password($key=false)
	{
		if ($key)
		{
	    	list($employee_id, $expire) = explode('|', $this->encrypt->decode(base64url_decode($key)));
			
			if ($employee_id && $expire && $expire > time())
			{
				$password = $this->input->post('password');
				$confirm_password = $this->input->post('confirm_password');
				
				if (($password == $confirm_password) && strlen($password) >=8)
				{
					if ($this->Employee->update_employee_password($employee_id, md5($password)))
					{
						$this->load->view('login/do_reset_password');	
					}
				}
				else
				{
					$data = array();
					$employee = $this->Employee->get_info($employee_id);
					$data['username'] = $employee->username;
					$data['key'] = $key;
					$data['error_message'] = lang('login_passwords_must_match_and_be_at_least_8_characters');
					$this->load->view('login/reset_password_enter_password', $data);
				}
			}
		}
	}
}
?>