<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reservations extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    // TO DO: to verify if this module should be secured or not
  }

  public function index()
  {
    $data['allowed_modules']=$this->Module->get_allowed_modules(0);

    $ui = new stdClass();
    $ui->first_name = 'John';
    $ui->last_name  = 'Doe';
    $data['user_info'] = $ui;
    $am = array('reservations', 'directories', 'calendar', 'account',
        'newsletter', 'settings', 'information');
    $data['modules'] = $am;
    $data['controller_name'] = __CLASS__;
    $data['contents'] = $this->load->view('user/reservations/manage', $data, true);
    $this->load->view("user/home", $data);
  }
}