<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Courses extends Secure_area implements iData_controller
{
	function __construct()
	{
		parent::__construct('courses');
		$this->load->model('Course_message');
	}

	function index()
	{
            //if (!$this->permissions->is_super_admin())
              //  redirect('home', 'location');
		$config['base_url'] = site_url('courses/index');
		$config['total_rows'] = $this->Course->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		$this->pagination->initialize($config);

		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_courses_manage_table($this->Course->get_all($config['per_page'], $this->uri->segment(3)),$this);
		$this->load->view('courses/manage',$data);
	}

	function find_item_info()
	{
		$item_number=$this->input->post('scan_item_number');
		echo json_encode($this->Course->find_item_info($item_number));
	}

	function search($offset = 0)
	{
		$search=$this->input->post('search');
		$data_rows=get_courses_manage_table_data_rows($this->Course->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20, $offset),$this);
		$config['base_url'] = site_url('courses/index');
        $config['total_rows'] = $this->Course->search($search, 0);
        $config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['data_rows'] = $data_rows;
        echo json_encode($data);

	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
		$suggestions = $this->Course->get_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}

	function course_search()
	{
            	$suggestions = $this->Course->get_course_search_suggestions($this->input->get('term'),100);
		echo json_encode($suggestions);
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest_category()
	{
		$suggestions = $this->Course->get_category_suggestions($this->input->get('term'));
		echo json_encode($suggestions);
	}
	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest_teesheet()
	{
		$suggestions = $this->Course->get_teesheet_search_suggestions($this->input->get('term'));
		echo json_encode($suggestions);
	}

	function get_row()
	{
		$item_id = $this->input->post('row_id');
		$data_row=get_course_data_row($this->Course->get_info($item_id),$this);
		echo $data_row;
	}

	function get_info($item_id=-1)
	{
		echo json_encode($this->Course->get_info($item_id));
	}

	function view($item_id=-1)
	{
        $data['item_info'] = $this->Course->get_info($item_id);
		$data['groups']=$this->Course->get_group_info('', $item_id);
		$data['managed_groups']=$this->Course->get_managed_group_info('', $item_id);
		//print_r($data);
		$this->load->view("courses/form",$data);
	}
	function view_message($message_id=-1)
	{
        $data['message_info'] = $this->Course_message->get_info($message_id);
		//$data['groups']=$this->Course->get_group_info('', $item_id);
		//print_r($data);
		$this->load->view("courses/form_message",$data);
	}
	function manage_groups()
	{
		$data['groups']=$this->Course->get_group_info();
		//$data['last_query']=$this->db->last_query();
		$this->load->view('courses/manage_groups',$data);
	}
	function add_group()
	{
		$group_label = $this->input->post("group_label");
		$group_type = $this->input->post("group_type");
		echo json_encode(array('success'=>true,'group_id'=>$this->Course->add_group($group_label, $group_type)));
	}
	function delete_group($group_id)
	{
		$this->Course->delete_group($group_id);
		echo json_encode(array('success'=>true));
	}

	//Ramel Inventory Tracking
	function inventory($item_id=-1)
	{
		$data['item_info']=$this->Course->get_info($item_id);
		$this->load->view("courses/inventory",$data);
	}

	function count_details($item_id=-1)
	{
		$data['item_info']=$this->Course->get_info($item_id);
		$this->load->view("courses/count_details",$data);
	} //------------------------------------------- Ramel
	function save_message($message_id)
	{
		$message_data = array(
			'message' 	=> $this->input->post('message'),
			'read' 		=> 0
		);
		
		if ($this->input->post('all_courses'))
		{
			$original_message = $message_data;
			$this->db->from('courses');
			$this->db->where('sales', 1);
			$courses = $this->db->get()->result_array();
			foreach ($courses as $course)
			{
				$message_data = $original_message;
				$message_data['course_id'] = $course['course_id'];
				$this->Course_message->save($message_data);
			}
			echo json_encode(array('success'=>true));
			return;
		}
		else if ($this->input->post('employee_id'))
		{
			$message_data['person_id'] = $this->input->post('employee_id');
			$message_data['course_id'] = $this->input->post('employee_course_id');
			$this->Course_message->save($message_data);
			echo json_encode(array('success'=>true));
			return;
		}
		else if ($this->input->post('course_id'))
		{
			$message_data['course_id'] = $this->input->post('course_id');
			$this->Course_message->save($message_data);
			echo json_encode(array('success'=>true));
			return;
		}
			echo json_encode(array('success'=>false));
	}
	function save($item_id=-1)
	{
		$item_data = array(
		'name'=>$this->input->post('name'),
		'simulator'=>$this->input->post('simulator'),
		'teesheets'=>$this->input->post('teesheets'),
		'reservations'=>$this->input->post('reservations'),
		'schedules'=>$this->input->post('schedules'),
		'sales'=>$this->input->post('sales'),
		'food_and_beverage'=>$this->input->post('food_and_beverage'),
		'items'=>$this->input->post('items'),
		'item_kits'=>$this->input->post('item_kits'),
		'employees'=>$this->input->post('employees'),
		'customers'=>$this->input->post('customers'),
		'invoices'=>$this->input->post('invoices'),
		'reports'=>$this->input->post('reports'),
		'suppliers'=>$this->input->post('suppliers'),
		'receivings'=>$this->input->post('receivings'),
        'config'=>$this->input->post('config'),
        'giftcards'=>$this->input->post('giftcards'),
        'marketing_campaigns'=>$this->input->post('marketing_campaigns'),
        'promotions'=>$this->input->post('promotions'),
        'quickbooks'=>$this->input->post('quickbooks'),
        'address'=>$this->input->post('address'),
        'city'=>$this->input->post('city'),
        'state'=>$this->input->post('state'),
        'zip'=>$this->input->post('zip'),
        'phone'=>$this->input->post('phone'),
        //'fax'=>$this->input->post('fax'),
        'email'=>$this->input->post('email'),
        'website'=>$this->input->post('website'),
        'foreup_discount_percent'=>$this->input->post('foreup_discount_percent'),
        //'open_time'=>$this->input->post('open_time'),
        //'close_time'=>$this->input->post('close_time'),
        //'increment'=>$this->input->post('increment'),
        //'twilight_hour'=>$this->input->post('twilight_hour'),
        //'holes'=>$this->input->post('holes'),
        'mercury_id'=>$this->input->post('mercury_id'),
        'mercury_password'=>$this->input->post('mercury_password'),
		'ets_key'=>$this->input->post('ets_key'),
		'seasonal_pricing'=>$this->input->post('seasonal_pricing')
		);
		$this->Fee->rename_green_fees($item_id, $this->input->post('simulator'));
		$groups_data = $this->input->post("groups")!=false ? $this->input->post("groups"):array();
		$managed_groups = $this->input->post("managed_groups")!=false ? $this->input->post("managed_groups"):array();

        if($this->Course->save($item_data,$item_id, $groups_data, $managed_groups))
		{
			//New item
			if($item_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>lang('items_successful_adding').' '.
				$item_data['name'],'course_id'=>$item_data['course_id']));
				$item_id = $item_data['course_id'];
			}
			else //previous item
			{
				echo json_encode(array('success'=>true,'message'=>lang('items_successful_updating').' '.
				$item_data['name'],'course_id'=>$item_id));
			}
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('items_error_adding_updating').' '.
			$item_data['name'],'course_id'=>-1));
		}

	}


	function delete()
	{
		$items_to_delete=$this->input->post('ids');

		if($this->Course->delete_list($items_to_delete))
		{
			echo json_encode(array('success'=>true,'message'=>lang('items_successful_deleted').' '.
			count($items_to_delete).' '.lang('items_one_or_multiple')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('items_cannot_be_deleted')));
		}
	}

	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 550;
	}
}
?>
