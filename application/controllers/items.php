<?php
require_once ("secure_area.php");
require_once ("interfaces/idata_controller.php");
class Items extends Secure_area implements iData_controller
{
	function __construct()
	{
		parent::__construct('items');
		$this->load->model('modifier');

	}

	function index()
	{
		$config['base_url'] = site_url('items/index');
		$config['total_rows'] = $this->Item->count_all();
		$config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
		$this->pagination->initialize($config);

		$data['controller_name']=strtolower(get_class());
		$data['form_width']=$this->get_form_width();
		$data['manage_table']=get_items_manage_table($this->Item->get_all($config['per_page'], $this->uri->segment(3), true, false, 0),$this);
                $data['has_restaurant'] = $this->Item->has_restaurant();

                //log_message('error', "HAS RESTAURANT " . $data['has_restaurant']);
		$this->load->view('items/manage',$data);
	}

	function index_pdf($item_id=-1)
	{
		$data['item_info']=$this->Item->get_info($item_id);
		$data['item_tax_info']=$this->Item_taxes->get_info($item_id);
		$suppliers = array('' => lang('items_none'));
		foreach($this->Supplier->get_all()->result_array() as $row)
		{
			$suppliers[$row['person_id']] = $row['company_name'] .' ('.$row['first_name'] .' '. $row['last_name'].')';
		}

		$data['suppliers']=$suppliers;
		$data['selected_supplier'] = $this->Item->get_info($item_id)->supplier_id;
		$data['default_tax_1_rate']=($item_id==-1) ? $this->Appconfig->get('default_tax_1_rate') : '';
		$data['default_tax_2_rate']=($item_id==-1) ? $this->Appconfig->get('default_tax_2_rate') : '';
		$data['default_tax_2_cumulative']=($item_id==-1) ? $this->Appconfig->get('default_tax_2_cumulative') : '';
		$this->load->library('Html2pdf');
		$html2pdf = new Html2pdf('P','A4','fr');
		//$html2pdf->setModeDebug();
		$html2pdf->setDefaultFont('Arial');
		$html = $this->load->view('items/form',$data, true);
		$html = "<div style='height:300px; width:300px; background-color:blue'>Just testing it</div>";
		$html2pdf->writeHTML($html);
		$html2pdf->Output('example.pdf');
	}
	function create_teetimes() {
        //echo 'creating teetimes';
        $this->Item->create_teetimes();
		$item_data = array(
			'name'=>'Giftcard',
			'description'=>'',
			'department'=>'Giftcards',
			'category'=>'',
			'subcategory'=>'',
			'supplier_id'=>null,
			'item_number'=>null,
			'cost_price'=>0,
			'unit_price'=>0,
			'max_discount'=>0,
			'quantity'=>0,
			'is_unlimited'=>1,
			'reorder_level'=>0,
			'location'=>'',
			'allow_alt_description'=>0,
			'is_serialized'=>1,
	        'is_giftcard'=>1,
	        'course_id'=>$this->session->userdata('course_id')
		);
		$this->Item->save($item_data, -1);
    }
	function find_item_info()
	{
		$item_number=$this->input->post('scan_item_number');
		echo json_encode($this->Item->find_item_info($item_number));
	}
    function item_type_choices($offset = 0)
    {
        $data = array();
        $food_or_proshop = $this->input->post('item_type');
        $data_rows = get_items_manage_table_data_rows($this->Item->item_type_choice($food_or_proshop, $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20, true, $offset),$this);
        $data['sql']=$this->db->last_query();
        $config['base_url'] = site_url('items/index');
        $config['total_rows'] = $this->Item->item_type_choice($food_or_proshop, 0);
        $config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['data_rows'] = $data_rows;
        echo json_encode($data);
    }
    function search($offset = 0)
	{

		$data = array();
                $food_and_beverage = $this->input->post('search_type')?$this->input->post('search_type'):0;

                //log_message('error', "FOOD AND BEV: " . $food_and_beverage);
		$search=$this->input->post('search');
		$data_rows=get_items_manage_table_data_rows($this->Item->search($search,$this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20, true, $offset, $food_and_beverage),$this);
		$data['sql']=$this->db->last_query();
		$data['search']=$search;
		$config['base_url'] = site_url('items/index');
        $config['total_rows'] = $this->Item->search($search, 0);
        $config['per_page'] = $this->config->item('number_of_items_per_page') ? (int)$this->config->item('number_of_items_per_page') : 20;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['data_rows'] = $data_rows;
        echo json_encode($data);
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest()
	{
                $food_and_beverage = $this->input->get('food_and_beverage');
                //log_message('error', "SUGGEST: $food_and_beverage");
		$suggestions = $this->Item->get_search_suggestions($this->input->get('term'), 25, true, $food_and_beverage);
		echo json_encode($suggestions);
	}

	function item_search()
	{
		$suggestions = $this->Item->get_item_search_suggestions($this->input->get('term'), 100);
		echo json_encode($suggestions);
	}

	function view_side_options($item_id)
	{
		$data = array();
		$data['sides'] = $this->Item->get_item_sides($item_id)->result_object();
		$data['item_id'] = $item_id;
		//print_r($data['sides']);
		$this->load->view('items/sides', $data);
	}
	function save_side_options($item_id)
	{
		$side_ids = $this->input->post('side_id');
		$not_available = $this->input->post('not_available');
		$default = $this->input->post('default');
		$upgrade_price = $this->input->post('upgrade_price');

		$side_options_array = array();
		foreach ($side_ids as $index => $side_id)
		{
			if ($upgrade_price[$index] != '')
				$side_options_array[$side_ids[$index]]['upgrade_price'] = $upgrade_price[$index];
			if ($not_available[$index] > 0)
				$side_options_array[$not_available[$index]]['not_available'] = 1;
			if ($default[$index] > 0)
				$side_options_array[$default[$index]]['default'] = 1;
		}
		echo json_encode(array('success'=> $this->Item->save_side_options($side_options_array, $item_id)));
	}
	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest_department()
	{
		$suggestions = $this->Item->get_department_suggestions($this->input->get('term'));
		echo json_encode($suggestions);
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest_category()
	{
		$suggestions = $this->Item->get_category_suggestions($this->input->get('term'));
		echo json_encode($suggestions);
	}

	/*
	Gives search suggestions based on what is being searched for
	*/
	function suggest_subcategory()
	{
		$suggestions = $this->Item->get_subcategory_suggestions($this->input->get('term'));
		echo json_encode($suggestions);
	}

	function get_row()
	{
		$item_id = $this->input->post('row_id');
		$data_row=get_item_data_row($this->Item->get_info($item_id),$this);
		echo $data_row;
	}

	function get_info($item_id=-1)
	{
		echo json_encode($this->Item->get_info($item_id));
	}

	function view($item_id=-1)
	{
		$this->load->model('modifier');
		$this->load->model('quickbooks');
		$this->load->model('Image');
		$data['item_info']=$this->Item->get_info($item_id);
		$data['item_tax_info']=$this->Item_taxes->get_info($item_id);
		$suppliers = array('' => lang('items_none'));
		foreach($this->Supplier->get_all()->result_array() as $row)
		{
			$suppliers[$row['person_id']] = $row['company_name'] .' ('.$row['first_name'] .' '. $row['last_name'].')';
		}
		$data['image_thumb_url'] = $this->Image->get_thumb_url($data['item_info']->image_id);
		$data['suppliers']=$suppliers;
		$data['selected_supplier'] = $this->Item->get_info($item_id)->supplier_id;
		$data['default_tax_1_rate']=($item_id==-1) ? $this->Appconfig->get('default_tax_1_rate') : '';
		$data['default_tax_2_rate']=($item_id==-1) ? $this->Appconfig->get('default_tax_2_rate') : '';
		$data['default_tax_2_cumulative']=($item_id==-1) ? $this->Appconfig->get('default_tax_2_cumulative') : '';
		$data['modifiers'] = $this->modifier->get_by_item($item_id);
		$data['quickbooks_accounts'] = $this->quickbooks->get_accounts_menu();
		$data['quickbooks_accounts'][''] = '- Default -';

		$this->load->view("items/form", $data);
	}
        function view_new_item($item_id=-1, $test, $food_and_beverage)
        {
		$this->load->model('quickbooks');

		$data['item_info']=$this->Item->get_info($item_id);
		$data['item_tax_info']=$this->Item_taxes->get_info($item_id);
                $data['food_and_beverage'] = $food_and_beverage;
		$suppliers = array('' => lang('items_none'));
		foreach($this->Supplier->get_all()->result_array() as $row)
		{
			$suppliers[$row['person_id']] = $row['company_name'] .' ('.$row['first_name'] .' '. $row['last_name'].')';
		}

		$data['suppliers']=$suppliers;
		$data['selected_supplier'] = $this->Item->get_info($item_id)->supplier_id;
		$data['default_tax_1_rate']=($item_id==-1) ? $this->Appconfig->get('default_tax_1_rate') : '';
		$data['default_tax_2_rate']=($item_id==-1) ? $this->Appconfig->get('default_tax_2_rate') : '';
		$data['default_tax_2_cumulative']=($item_id==-1) ? $this->Appconfig->get('default_tax_2_cumulative') : '';
		$data['quickbooks_accounts'] = $this->quickbooks->get_accounts_menu();
		$data['quickbooks_accounts'][''] = '- Default -';

		$this->load->view("items/form",$data);
        }
	function view_quickbutton($quickbutton_id = -1)
	{
		$data = array();
		$data['quickbutton_info'] = $this->Quickbutton->get_info($quickbutton_id);
		$data['quickbutton_items'] = $this->Quickbutton->get_items($quickbutton_id);
		$item_array = array();
		foreach ($data['quickbutton_items'] as $item) {
			$item_array[$item['item_id']] = $item['i_name'];
		}
		$data['item_ids'] = json_encode($item_array);
		$data['green_fee_types'] = ($this->permissions->course_has_module('reservations')?$this->Fee->get_types():$this->Green_fee->get_types());

		$this->load->view("items/quickbutton_form", $data);
	}
	function save_quickbutton($quickbutton_id = -1)
	{
		$tab = $this->input->post('tab');
		$results = array('quickbutton_id' => $quickbutton_id);
		if ($this->input->post('delete_quickbutton'))
		{
			$this->Quickbutton->delete($quickbutton_id);
			$results['action'] = 'deleted';
			$results['success'] = true;
		}
		else
		{
			$item_ids = $this->input->post('quickbutton_items');
			$ids = explode('|', $item_ids);
			$quickbutton_data = array(
				'display_name'=>$this->input->post('name'),
				'course_id'=>$this->session->userdata('course_id'),
				'tab'=>$tab
			);

			if ($this->Quickbutton->save($quickbutton_data, $quickbutton_id))
			{
				$quickbutton_id = ($quickbutton_id != -1)?$quickbutton_id:$quickbutton_data['quickbutton_id'];
				$item_data_array = array();
				foreach ($ids as $index => $id)
					$item_data_array[] = array(
							'quickbutton_id'=>$quickbutton_id,
							'item_id'=>(strpos($id, 'KIT_') !== false) ? NULL : $id,
							'item_kit_id'=>(strpos($id, 'KIT_') !== false) ? str_replace('KIT_', '', $id) : NULL,
							'order'=>$index
						);
				if ($this->Quickbutton->delete_items($quickbutton_id) && $this->Quickbutton->save_items($item_data_array))
				{
					$results['action'] = 'saved';
					$results['success'] = true;
					$results['sql'] = $this->db->last_query();
					$results['item_array'] = $item_data_array;
					$quickbutton_info = $this->Quickbutton->get_all($quickbutton_id);
					foreach ($quickbutton_info as $quickbutton)
						$results['html'] = $this->Quickbutton->build($quickbutton_id, $quickbutton);
				}
			}
		}
		$results['last_sql'] = $this->db->last_query();
		echo json_encode($results);
	}
	function save_quickbutton_positions() {
		$positions_1 = $this->input->post('positions_1');
		$positions_1 = explode('|',$positions_1);
		$this->Quickbutton->save_positions($positions_1);

		$positions_2 = $this->input->post('positions_2');
		$positions_2 = explode('|',$positions_2);
		$this->Quickbutton->save_positions($positions_2);

		$positions_3 = $this->input->post('positions_3');
		$positions_3 = explode('|',$positions_3);
		$this->Quickbutton->save_positions($positions_3);

		echo json_encode($positions);
	}
	function save_image($item_id = null){
		$this->load->model('Image');
		$image_id = $this->input->post('image_id', null);
		if($image_id === null){
			$success = true;
		}else{
			$success = $this->Item->save_image($item_id, $image_id);
		}
		$url = $this->Image->get_thumb_url($image_id);

		echo json_encode(array('success'=>true, 'image_id'=>$image_id, 'thumb_url'=>$url));
	}
	//Ramel Inventory Tracking
	function inventory($item_id=-1)
	{
		$data['item_info']=$this->Item->get_info($item_id);
		$this->load->view("items/inventory",$data);
	}

	function count_details($item_id=-1)
	{
		$data['item_info']=$this->Item->get_info($item_id);
		$this->load->view("items/count_details",$data);
	} //------------------------------------------- Ramel
	function barcode_details($item_ids, $sheet_style = '')
	{
		$result = array();

		$item_ids = explode('~', $item_ids);
		foreach ($item_ids as $item_id)
		{
			$item_info = $this->Item->get_info($item_id);

			$result[] = array('name' =>$item_info->name, 'id'=> $item_id);
		}

		$data['items'] = $result;
		$data['sheet_style'] = $sheet_style;

		$this->load->view('items/form_barcode.php', $data);
	}
	function generate_barcodes($item_ids, $sheet_style = '', $start_index = 1, $top_margin = '')
	{
		$result = array();

		$data['items'] = $result;
		$data['sheet_style'] = $sheet_style;
		if ($sheet_style == 5267)
		{
			$data['scale'] = 1.5;
			$data['thickness'] = 20;
			$v_limit = 20;
			$h_limit = 4;
			$char_limit = 18;
		}
		else if ($sheet_style == 5160)
		{
			$data['scale'] = 1.5;
			$data['thickness'] = 20;
			$v_limit = 10;
			$h_limit = 3;
			$char_limit = 32;
		}
		else {
			$data['scale'] = 2;
			$v_limit = 20;
			$h_limit = 4;
		}
		$item_ids = explode('~', $item_ids);
		foreach ($item_ids as $item_id)
		{
			$item_info = $this->Item->get_info($item_id);
			$price = to_currency($item_info->unit_price);
			$result[] = array('name' =>character_limiter($item_info->name,$char_limit-strlen($price), '...').' '.$price, 'unit_price'=>to_currency($item_info->unit_price), 'id'=> number_pad($item_id, 11), 'number'=>$item_info->item_number);
		}

		/*$this->load->view('barcode_sheet', $data);
		$this->load->library("Html2pdf");
		$html2pdf = new Html2pdf('P','A4','fr', true, 'UTF-8', array(0, 8, 0, 3));
		//$html2pdf->setModeDebug();
		$html2pdf->setDefaultFont('Arial');
		//$html = $this->load->view('items/form',$data, true);
		$html = $this->load->view("barcode_sheet", $data, true);//"<div style='height:300px; width:300px; background-color:blue'>Just testing it</div>";
		$html2pdf->writeHTML($html);
		$html2pdf->Output('example.pdf');
		*/
		$this->load->library('fpdf');
		$pdf = new FPDF();
		$pdf->Open();
		$pdf->AddPage();
		$pdf->SetFont('Helvetica', 'B', 8);
		$pdf->SetMargins(0, 0);
		$pdf->SetAutoPageBreak(false);
		$x = $y = 0;

		while ($start_index > 1)
		{
			if ($sheet_style == 5267)
				$this->Item->Avery5267($x, $y, $pdf, '', '', '', $top_margin);
			else if ($sheet_style == 5160)
				$this->Item->Avery5160($x, $y, $pdf, '', '', '', $top_margin);

		    $y++; // next row
		    if($y == $v_limit) { // end of page wrap to next column
		        $x++;
		        $y = 0;
		        if($x == $h_limit) { // end of page
		            $x = 0;
		            $y = 0;
		            $pdf->AddPage();
		        }
		    }
			$start_index--;
		}
		foreach($result as $item) {
			if ($sheet_style == 5267)
				$this->Item->Avery5267($x, $y, $pdf, urlencode($item['name']), $item['id'], $item['number'], $top_margin);
			else if ($sheet_style == 5160)
				$this->Item->Avery5160($x, $y, $pdf, $item['name'], $item['id'], $item['number'], $top_margin);

		    $y++; // next row
		    if($y == $v_limit) { // end of page wrap to next column
		        $x++;
		        $y = 0;
		        if($x == $h_limit) { // end of page
		            $x = 0;
		            $y = 0;
		            $pdf->AddPage();
		        }
		    }
		}
		$pdf->Output();
	}

	function generate_barcode_labels($item_ids)
	{
		$result = array();

		$item_ids = explode('~', $item_ids);
		foreach ($item_ids as $item_id)
		{
			$item_info = $this->Item->get_info($item_id);

			$result[] = array('name' =>$item_info->name.': '.to_currency($item_info->unit_price), 'id'=> number_pad($item_id, 11), 'number'=>$item_info->item_number);
		}

		$data['items'] = $result;
		$data['scale'] = 1;
		//$data['thickness'] = 15;
		$this->load->view("barcode_labels", $data);
	}

	function bulk_edit()
	{
		$this->load->model('quickbooks');
		$data = array();
		$suppliers = array('' => lang('items_do_nothing'), '-1' => lang('items_none'));
		foreach($this->Supplier->get_all()->result_array() as $row)
		{
			$suppliers[$row['person_id']] = $row['company_name']. ' ('.$row['first_name'] .' '. $row['last_name'].')';
		}
		$data['suppliers'] = $suppliers;
		$data['allow_alt_desciption_choices'] = array(
			''=>lang('items_do_nothing'),
			1 =>lang('items_change_all_to_allow_alt_desc'),
			0 =>lang('items_change_all_to_not_allow_allow_desc'));

		$data['serialization_choices'] = array(
			''=>lang('items_do_nothing'),
			1 =>lang('items_change_all_to_serialized'),
			0 =>lang('items_change_all_to_unserialized'));
		$data['quickbooks_accounts'] = $this->quickbooks->get_accounts_menu();

		$data['quickbooks_accounts'][''] = '- Default -';
		$data['quickbooks_accounts'] = array('-nochange-'=>'- Do Nothing -') + $data['quickbooks_accounts'];

		$this->load->view("items/form_bulk", $data);
	}

	function save($item_id=-1)
	{
		$item_data = array(
		'name'=>$this->input->post('name'),
		'description'=>$this->input->post('description'),
		'department'=>$this->input->post('department'),
		'category'=>$this->input->post('category'),
		'subcategory'=>$this->input->post('subcategory'),
		'supplier_id'=>$this->input->post('supplier_id')=='' ? null:$this->input->post('supplier_id'),
		'item_number'=>$this->input->post('item_number')=='' ? null:$this->input->post('item_number'),
		'cost_price'=>$this->input->post('cost_price'),
		'unit_price'=>$this->input->post('unit_price'),
		'unit_price_includes_tax'=>$this->input->post('unit_price_includes_tax'),
		'max_discount'=>$this->input->post('max_discount'),
		'quantity'=>$this->input->post('quantity'),
		'is_unlimited'=>$this->input->post('is_unlimited'),
		'reorder_level'=>$this->input->post('reorder_level'),
		'location'=>$this->input->post('location'),
		'gl_code'=>$this->input->post('gl_code') ? $this->input->post('gl_code') : null,
		'allow_alt_description'=>$this->input->post('allow_alt_description'),
		'is_serialized'=>($this->input->post('is_giftcard'))?1:$this->input->post('is_serialized'),
        'is_giftcard'=>$this->input->post('is_giftcard'),
        'course_id'=>$this->session->userdata('course_id'),
        'food_and_beverage'=>$this->input->post('food_and_beverage'),
		'is_side'=>$this->input->post('is_side'),
		'add_on_price'=>$this->input->post('add_on_price'),
		'print_priority'=>$this->input->post('print_priority'),
		'kitchen_printer'=>$this->input->post('kitchen_printer'),
		'soup_or_salad'=>$this->input->post('soup_or_salad'),
		'number_of_sides'=>$this->input->post('number_of_sides'),
		'quickbooks_income'=>$this->input->post('quickbooks_income'),
		'quickbooks_cogs'=>$this->input->post('quickbooks_cogs'),
		'quickbooks_assets'=>$this->input->post('quickbooks_assets')
		);

		$itemModifiers = $this->input->post('modifiers');

		if(!empty($itemModifiers)){
			foreach($itemModifiers as $key => $itemModifier){
				$modifierId = $itemModifier['modifier_id'];
				unset($itemModifier['modifier_id']);
				$itemModifier['item_id'] = $item_id;

				if($itemModifier['override_price'] == ''){
					$itemModifier['override_price'] = null;
				}

				$data = array(
					'item' => $itemModifier
				);
				$this->modifier->save($data, $modifierId);
			}
		}

		$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
		$cur_item_info = $this->Item->get_info($item_id);


		if($this->Item->save($item_data,$item_id))
		{
			//New item
			if($item_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>lang('items_successful_adding').' '.
				$item_data['name'],'item_id'=>$item_data['item_id']));
				$item_id = $item_data['item_id'];
			}
			else //previous item
			{
				echo json_encode(array('success'=>true,'message'=>lang('items_successful_updating').' '.
				$item_data['name'],'item_id'=>$item_id));
			}

			$inv_data = array
			(
				'trans_date'=>date('Y-m-d H:i:s'),
				'trans_items'=>$item_id,
				'trans_user'=>$employee_id,
				'trans_comment'=>lang('items_manually_editing_of_quantity'),
				'trans_inventory'=>$cur_item_info ? $this->input->post('quantity') - $cur_item_info->quantity : $this->input->post('quantity'),
                'course_id'=>$this->session->userdata('course_id')
			);
			if (!$item_data->is_unlimited)
				$this->Inventory->insert($inv_data);

			$items_taxes_data = array();
			$tax_names = $this->input->post('tax_names');
			$tax_percents = $this->input->post('tax_percents');
			$tax_cumulatives = $this->input->post('tax_cumulatives');
			for($k=0;$k<count($tax_percents);$k++)
			{
				if (is_numeric($tax_percents[$k]))
				{
					$items_taxes_data[] = array('name'=>$tax_names[$k], 'percent'=>$tax_percents[$k], 'cumulative' => isset($tax_cumulatives[$k]) ? $tax_cumulatives[$k] : '0' );
				}
			}
			$this->Item_taxes->save($items_taxes_data, $item_id);
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>($item_data['error']?$item_data['error']:lang('items_error_adding_updating').' '.
			$item_data['name']),'item_id'=>-1));
		}

	}

	//Ramel Inventory Tracking
	function save_inventory($item_id=-1)
	{
		$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
		$cur_item_info = $this->Item->get_info($item_id);
		$inv_data = array
		(
			'trans_date'=>date('Y-m-d H:i:s'),
			'trans_items'=>$item_id,
			'trans_user'=>$employee_id,
			'trans_comment'=>$this->input->post('trans_comment'),
			'trans_inventory'=>$this->input->post('newquantity'),
                        'course_id'=>$this->session->userdata('course_id')
		);
		$this->Inventory->insert($inv_data);

		//Update stock quantity
		$item_data = array(
		'quantity'=>$cur_item_info->quantity + $this->input->post('newquantity')
		);
		if($this->Item->save($item_data,$item_id))
		{
			echo json_encode(array('success'=>true,'message'=>lang('items_successful_updating').' '.
			$cur_item_info->name,'item_id'=>$item_id));
		}
		else//failure
		{
			echo json_encode(array('success'=>false,'message'=>lang('items_error_adding_updating').' '.
			$cur_item_info->name,'item_id'=>-1));
		}

	}//---------------------------------------------------------------------Ramel

	function bulk_update()
	{
		$items_to_update=$this->input->post('item_ids');
		$item_data = array();

		foreach($_POST as $key=>$value)
		{
			if ($key == 'submit')
			{
				continue;
			}

			if($key == 'quickbooks_cogs' || $key == 'quickbooks_income' || $key == 'quickbooks_assets'){
				if($value != '-nochange-'){
					$item_data[$key] = $value;
				}
				continue;
			}

			//This field is nullable, so treat it differently
			if ($key == 'supplier_id')
			{
				if ($value!='')
				{
					$item_data["$key"]=$value == '-1' ? null : $value;
				}
			}
			elseif($value!='' and !(in_array($key, array('item_ids', 'tax_names', 'tax_percents', 'tax_cumulatives'))))
			{
				$item_data["$key"]=$value;
			}
		}

		//Item data could be empty if tax information is being updated
		if(empty($item_data) || $this->Item->update_multiple($item_data,$items_to_update))
		{
			$items_taxes_data = array();
			$tax_names = $this->input->post('tax_names');
			$tax_percents = $this->input->post('tax_percents');
			$tax_cumulatives = $this->input->post('tax_cumulatives');

			for($k=0;$k<count($tax_percents);$k++)
			{
				if (is_numeric($tax_percents[$k]))
				{
					$items_taxes_data[] = array('name'=>$tax_names[$k], 'percent'=>$tax_percents[$k], 'cumulative' => isset($tax_cumulatives[$k]) ? $tax_cumulatives[$k] : '0' );
				}
			}

			if (!empty($items_taxes_data))
			{
				$this->Item_taxes->save_multiple($items_taxes_data, $items_to_update);
			}

			echo json_encode(array('success'=>true,'message'=>lang('items_successful_bulk_edit')));
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>lang('items_error_updating_multiple')));
		}
	}

	function delete($confirmed = false)
	{
		if ($confirmed)
		{
			$items_to_delete=$this->input->post('item_ids');

			if($this->Item->delete_list($items_to_delete))
			{
				echo json_encode(array('success'=>true,'message'=>lang('items_successful_deleted').' '.
				count($items_to_delete).' '.lang('items_one_or_multiple')));
			}
			else
			{
				echo json_encode(array('success'=>false,'message'=>lang('items_cannot_be_deleted')));
			}
		}
		else {
			$items_to_delete=$this->input->post('ids');
			//print_r($items_to_delete);
			$result = array();
			foreach ($items_to_delete as $item_id)
			{
				$item_info = $this->Item->get_info($item_id);

				$result[] = array('name' =>$item_info->name, 'id'=> $item_id);
			}
			$data['items'] = $result;

			$this->load->view('items/confirm_delete', $data);
		}
	}

	function excel()
	{
		$data = file_get_contents("import_items.csv");
		$name = 'import_items.csv';
		force_download($name, $data);
	}

	/* added for excel expert */
	function excel_export($food_and_bev = 0) {
		ini_set('memory_limit', '500M');
		$data = $this->Item->get_all(10000, 0, true, false, $food_and_bev)->result_object();
		$this->load->helper('report');
		$rows = array();
		$row = array("UPC/EAN/ISBN", "Item Name", "Department", "Category", "Sub-Category", "Supplier ID", "Cost Price", "Unit Price", "Tax 1 Name", "Tax 1 Percent", "Tax 2 Name ", "Tax 2 Percent", "Tax 2 Cumulative", "Quantity", "Reorder Level", "Location", "Description", "Allow Alt Description", "Item has Serial Number");
		$rows[] = $row;
		foreach ($data as $r) {
			$taxdata = $this->Item_taxes->get_info($r->item_id);
			if (sizeof($taxdata) >= 2) {
				$r->taxn = $taxdata[0]['name'];
				$r->taxp = $taxdata[0]['percent'];
				$r->taxn1 = $taxdata[1]['name'];
				$r->taxp1 = $taxdata[1]['percent'];
				$r->cumulative = $taxdata[1]['cumulative'] ? 'y' : '';
			} else if (sizeof($taxdata) == 1) {
				$r->taxn = $taxdata[0]['name'];
				$r->taxp = $taxdata[0]['percent'];
				$r->taxn1 = '';
				$r->taxp1 = '';
				$r->cumulative = '';
			} else {
				$r->taxn = '';
				$r->taxp = '';
				$r->taxn1 = '';
				$r->taxp1 = '';
				$r->cumulative = '';
			}

			$row = array(
				'="'.$r->item_number.'"',
				$r->name,
				$r->department,
				$r->category,
				$r->subcategory,
				$r->supplier_id,
				$r->cost_price,
				$r->unit_price,
				$r->taxn,
				$r->taxp,
				$r->taxn1,
				$r->taxp1,
				$r->cumulative,
				$r->quantity,
				$r->reorder_level,
				$r->location,
				$r->description,
				$r->allow_alt_description,
				$r->is_serialized ? 'y' : ''
			);
			$rows[] = $row;
		}

		$content = array_to_csv($rows);
		force_download('items_export.csv', $content);
		exit;
	}

	function excel_import()
	{
		$this->load->view("items/excel_import", null);
	}

	function do_excel_import()
	{
        $this->db->trans_start();
		$msg = 'do_excel_import';
		$failCodes = array();
		if ($_FILES['file_path']['error']!=UPLOAD_ERR_OK)
		{
			$msg = lang('items_excel_import_failed');
			echo json_encode( array('success'=>false,'message'=>$msg) );
			return;
		}
		else
		{
			$path_info = pathinfo($_FILES['file_path']['name']);
	        if (($handle = fopen($_FILES['file_path']['tmp_name'], "r")) !== FALSE && strtolower($path_info['extension']) == 'csv')
			{
				//Skip first row
				fgetcsv($handle);
				$counter = 0;
				$inserting = '';
				$type = $this->input->post('inv_type');
				while (($data = fgetcsv($handle)) !== FALSE)
				{
					$item_data = array(
					'name'			=>	isset($data[1])?$data[1]:'',
					'description'	=>	isset($data[17])?$data[17]:'',
					'location'		=>	isset($data[16])?$data[16]:'',
					'department'	=>	isset($data[2])?$data[2]:'',
					'category'		=>	isset($data[3])?$data[3]:'',
					'subcategory'	=>	isset($data[4])?$data[4]:'',
					'cost_price'	=>	isset($data[6])?(float) str_replace(array('$',','), '', $data[6]):0,
					'unit_price'	=>	isset($data[7])?(float) str_replace(array('$',','), '', $data[7]):0,
					'max_discount'	=>	isset($data[20])?(float) str_replace(array('%'), '', $data[20]):100,
					'quantity'		=>	isset($data[13])?(int) $data[13]:0,
					'reorder_level'	=>	isset($data[14])?(int) $data[14]:0,
					'is_unlimited'	=>  isset($data[15])?(int) $data[15]:0,
					'supplier_id'	=>  $this->Supplier->exists($data[5]) ? $data[5] : $this->Supplier->find_supplier_id($data[5]),
					'allow_alt_description'=> (isset($data[18]) && $data[18] != '' )? '1' : '0',
					'is_serialized'=> (isset($data[19]) && $data[19] != '') ? '1' : '0',
                    'course_id'		=>  $this->session->userdata('course_id'),
					'food_and_beverage' => $type
					);
					$item_number = isset($data[0])?$data[0]:'';

					if ($item_number != "")
					{
						$item_data['item_number'] = $item_number;
					}

					if($this->Item->save($item_data))
					{
						$items_taxes_data = null;
						//tax 1
						if( isset($data[9]) && isset($data[8]) && $data[8]!='' )
						{
							$items_taxes_data[] = array('name'=>$data[8], 'percent'=>(float)$data[9], 'cumulative' => '0');
						}

						//tax 2
						if(isset($data[11]) && isset($data[10]) && $data[10]!='' )
						{
							$items_taxes_data[] = array('name'=>$data[10], 'percent'=>(float)$data[11], 'cumulative'=> (isset($data[12]) && $data[12] != '') ? '1' : '0', );
						}

						// save tax values
						if(count($items_taxes_data) > 0)
						{
							$this->Item_taxes->save($items_taxes_data, $item_data['item_id']);
						}

						$employee_id=$this->Employee->get_logged_in_employee_info()->person_id;
						$emp_info=$this->Employee->get_info($employee_id);
						$comment ='Qty CSV Imported';
						$excel_data = array
							(
							'trans_items'=>$item_data['item_id'],
							'trans_user'=>$employee_id,
							'trans_comment'=>$comment,
							'trans_inventory'=>isset($data[13])?$data[13]:'',
                            'course_id'=>$this->session->userdata('course_id')
							);
						$inserting = ($this->db->insert('inventory',$excel_data))?$inserting.'':$inserting.'-'.$counter;
						//------------------------------------------------Ramel
						$counter++;

					}
					else//insert or update item failure
					{
						echo json_encode( array('success'=>false,'message'=>lang('items_duplicate_item_ids')));
						return;
					}
				}
				//echo json_encode( array('success'=>false,'message'=>'reading file '.$counter.' i '.$inserting));
				//return;

			}
			else
			{
				echo json_encode( array('success'=>false,'message'=>lang('common_upload_file_not_supported_format')));
				return;
			}
		}

		$this->db->trans_complete();
		echo json_encode(array('success'=>true,'message'=>lang('items_import_successful')));
	}

	function cleanup()
	{
		$this->Item->cleanup();
		echo json_encode(array('success'=>true,'message'=>lang('items_cleanup_sucessful')));
	}

	/*
	get the width for the add/edit form
	*/
	function get_form_width()
	{
		return 550;
	}
    function manage_menus()
	{

        $this->load->model('menu');
		$data['menus'] = $this->menu->get_menu_info();
		//$data['last_query']=$this->db->last_query();
		$this->load->view('items/manage_menus', $data);
	}
	function view_inventory_audit()
	{
		$this->load->model('Inventory_audit');
		$data = array();
		$data['audits'] = $this->Inventory_audit->get_all()->result_array();
		$data['items'] = $this->Item->get_auditable()->result_array();
		$this->load->view('inventory_audits/manage', $data);
	}
	function get_audit_items ($inv_audit_id = false)
	{
		$this->load->model('Inventory_audit');
		$data = array();
		if (!$inv_audit_id)
		{
			$data['items'] = $this->Item->get_auditable()->result_array();
		}
		else 
		{
			$inv_audit_info = $this->Inventory_audit->get_info($inv_audit_id)->row_array();
			//print_r($inv_audit_info);
			$data['inventory_audit_id'] = $inv_audit_info['inventory_audit_id'];
			$data['items'] = $this->Inventory_audit->get_items($inv_audit_info['inventory_audit_id'])->result_array();
			//print_r($data);
		}

		echo json_encode(array('items_html'=> $this->load->view('inventory_audits/items', $data, true)));
	}
	function pdf_inventory_audit($inv_audit_id = false)
	{
		ini_set('memory_limit', '100M');
		$this->load->model('Inventory_audit');
		$data = array();
		$data['pdf'] = true;
		if (!$inv_audit_id)
		{
			$data['items'] = $this->Item->get_auditable()->result_array();
		}
		else 
		{
			$inv_audit_info = $this->Inventory_audit->get_info($inv_audit_id)->row_array();
			$data['inventory_audit_id'] = $inv_audit_info['inventory_audit_id'];
			$data['items'] = $this->Inventory_audit->get_items($inv_audit_info['inventory_audit_id'])->result_array();
			//print_r($data);
		}
		
		$this->load->library('Html2pdf');
		$html2pdf = new Html2pdf('P','A4','fr');
		//$html2pdf->setModeDebug();
		$html2pdf->setDefaultFont('Arial');
		$html = $this->load->view('inventory_audits/items', $data, true);
		//echo $html;
		$html2pdf->writeHTML($html);
		$html2pdf->Output('InventoryAudit.pdf');
	}
	function reset_item_counts($inv_audit_id = false)
	{
		$this->load->model('Inventory_audit');
		if ($this->permissions->is_employee())
		{
			echo json_encode(array('success'=>0));
		}
		
		$inv_audit_info = $this->Inventory_audit->get_info($inv_audit_id)->row_array();
		$items = $this->Inventory_audit->get_items($inv_audit_info['inventory_audit_id'])->result_array();
		foreach ($items as $item)
		{
			$data = array('quantity'=>$item['manual_count']);
			$this->Item->save($data, $item['item_id']);
		}
		echo json_encode(array('success'=>1));
	}
	function save_inventory_audit()
	{
		$this->load->model('Inventory_audit');
		$manual_count_submit = $this->input->post('status_flag');
		if($manual_count_submit == 'update'){
			$inventory_audit_id = $this->input->post('inventory_audit_id');
			$updated_counts = $_POST;
			foreach ($updated_counts as $intKey => $intValue) {
				$updated_manual_count = $_POST[$intKey];
				if(substr_count($intKey, 'updated_counts_') > 0){
					$updated_item_id = explode('_', $intKey);
	        	}
	        	$updated_count[] = array(
					'inventory_audit_id' => $inventory_audit_id,
					'item_id'=>$updated_item_id[2],
					'manual_count'=>$updated_manual_count
				);
			}	
			$update_status = $this->Inventory_audit->update_manual_count($updated_count);
			$return_data = array(
				'success'=>$update_status,
				'status'=>'update'
			);
			echo json_encode($return_data);
		}else{
			$this->db->trans_start();
			$item_ids = $this->input->post('item_ids');
			$manual_counts = $this->input->post('manual_counts');
			$system_counts = $this->input->post('system_counts');
			
			$inv_audit_data = array(
				'course_id'=>$this->session->userdata('course_id'),
				'employee_id'=>$this->session->userdata('person_id'),
				'date'=>date('Y-m-d')
			);
			// SAVE AUDIT
			$this->Inventory_audit->save($inv_audit_data);
			$inv_audit_items = array();
			foreach ($item_ids as $index => $item_id)
			{
				$inv_audit_items[] = array(
					'inventory_audit_id' => $inv_audit_data['inventory_audit_id'],
					'item_id'=>$item_id,
					'current_count'=>$system_counts[$index],
					'manual_count'=>$manual_counts[$index]
				);
			}
			// SAVE AUDIT ITEMS
			$this->Inventory_audit->save_items($inv_audit_items);
			$this->db->trans_complete();
			$audits_data = array(
				'audits'=>$this->Inventory_audit->get_all()
			);
			$data = array(
				'items'=>$inv_audit_items,
				'inventory_audit_id'=>$inv_audit_data['inventory_audit_id']
			);
			$return_data = array(
				'audit_list'=>$this->load->view('inventory_audits/audits', $audits_data, true),
				'audit_items'=>$this->load->view('inventory_audits/items', $data, true)
			);
			echo json_encode($return_data);
		}
	}
    function manage_menu_items($menu_id)
    {
        $this->load->model('menu');
        $data['menu_items'] = $this->menu->get_menu_data($menu_id);
        $this->load->view('items/manage_menu_items', $data);
    }
    function add_items_to_menu()
    {
        echo json_encode(array('success'=>true, 'menu_items'=>$this->Item->add_items_to_menu($menu_id_array)));
    }
	function add_menu()
	{
        $this->load->model('menu');
		$title = $this->input->post("title");
        $start_time = strtotime($this->input->post('start_time'));
        $end_time = strtotime($this->input->post('end_time'));
		echo json_encode(array('success'=>true,'menu_id'=>$this->menu->add_menu($start_time, $end_time, $title)));
	}
	function save_menu_name()
	{
        $this->load->model('menu');
        $start_time = strtotime($this->input->post('start_time'));
        $end_time = strtotime($this->input->post('end_time'));
		$title = $this->input->post('title');
		echo json_encode(array('success'=>$this->menu->save_menu_name($title, $start_time, $end_time)));
	}
	function delete_menu($menu_id)
	{
        $this->load->model('menu');
		$this->menu->delete_menu($menu_id);
		echo json_encode(array('success'=>true));
	}
    function add_take_out_menu_items()
    {
        $max_count = $this->input->post('max_num_items');
        $items_array = array();
        while ($max_count > 0)
        {
            $items_array[] = $this->input->post('item_'. $max_count--);
        }

        $menu_id = $this->input->post('menu_id');

        if($this->Item->add_take_out_menu_items($items_array, $menu_id))
        {
            echo json_encode(array('success'=>true));
        }
    }
    function has_restaurant()
    {
        return $this->Item->has_restaurant();
    }
}
?>
