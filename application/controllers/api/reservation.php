<?php
// Full CodeIgniter API documentation here
// http://net.tutsplus.com/tutorials/php/working-with-restful-services-in-codeigniter-2/
require_once(APPPATH.'libraries/REST_Controller.php');

class Reservation extends REST_Controller
{
	private $filter_time_range_presets = array(
        'morning'=> array('start'=>'0000','end'=>'1100'),
        'midday'=>  array('start'=>'1000','end'=>'1500'),
        'evening'=> array('start'=>'1400','end'=>'2359'),
        'full_day'=>array('start'=>'0000','end'=>'2359')
        );
		
	function __construct() 
	{
		parent::__construct();
		$this->load->model('Api_data');			
	}
	
	function available_times_get()
	{
		$start_time = $this->input->get('start_time');
		$end_time = $this->input->get('end_time');		
		$time_range = $this->input->get('time_range');		
		$date = $this->input->get('date');
		$teesheet_id = $this->input->get('schedule_id');
		$player_count = $this->input->get('guests') ? $this->input->get('guests') : 1;
		$player_count = $player_count > 4 ? 4 : $player_count;		
		$holes = $this->input->get('holes');
		$max_price = $this->input->get('max_price');
		
		if ($time_range && $date)
		{
			$value = $this->filter_time_range_presets[$time_range];			
			$date = date('Y-m-d', strtotime($date));		
			$start_time = "$date"."T".$value['start'];
			$end_time = "$date"."T".$value['end'];
		}
		
		$different_days = date('Ymd', strtotime($start_time)) == date('Ymd', strtotime($end_time)) ? false : true;

		$errors = array();
		if ($time_range && !isset($this->filter_time_range_presets[$time_range]) )
			$errors[] = 'Invalid parameter: time_range. Valid options are morning, midday, evening & full_day';
		if ($time_range && !$date)
			$errors[] = 'Missing parameter: time_range and date must be used together. Valid options for time_range are morning, midday, evening & full_day. Format for date is yyyy-mm-dd';
		if (!$time_range && $date)
			$errors[] = 'Missing parameter: time_range and date must be used together. Valid options for time_range are morning, midday, evening & full_day. Format for date is yyyy-mm-dd';
		if (!$start_time || $start_time == '')
			$errors[] = 'Missing parameter: start_time';
		if (!$end_time || $end_time == '')
			$errors[] = 'Missing parameter: end_time';
		if (!$teesheet_id || $teesheet_id == '')
			$errors[] = 'Missing parameter: schedule_id';	
		if ($different_days)
			$errors[] = 'Invalid parameters: start_time and end_time must be on the same day';
		if (count($errors)>0){			
			$this->response(
				array(
					'status'=>400,
					'message'=>'Missing or invalid parameters',
					'description'=>implode(', ', $errors),
					'code'=> 6040
				), 
				400
			);
		}
		$permissions = $this->Api_data->course_permissions(false, $teesheet_id, $this->rest->api_id);
		$available_for_sale = false;
        if ($permissions['tee_time_sales']) {
            $this->load->model('billing');
            $available_for_sale = $this->billing->have_sellable_teetimes(date('YmdHi', strtotime($start_time))-1000000, $teesheet_id);
        }
		$data = $this->Api_data->available_tee_times($start_time, $end_time, $teesheet_id, $player_count, $max_price, $permissions['extended_permissions'], $holes, $available_for_sale);
				
		if ($data) {
			$response =	array(
                'status'=>200,
				'available_reservation_times'=>$data					
			);
			if ($permissions['tee_time_sales']) {
				$this->load->model('course');
				$this->load->model('Teesheet');
				$teesheet_info = $this->Teesheet->get_info($teesheet_id);
				$course_info = $this->Course->get_info($teesheet_info->course_id);
				$response['available_for_sale'] = $available_for_sale;
				$response['maximum_discount'] = $course_info->foreup_discount_percent;
			}			
			$this->response(
				$response, 
				200
			);
		} 
		else 
		{			
			$this->response(
				array(
					'status'=>200,
					'message'=>'No Reservations',	
					'description'=>"No reservations are available at the specified time for schedule_id : {$teesheet_id}"
				),
				200
			);
		}		
		
	}
	
	// function booked_times_get()
	// {
		// $start_time = $this->input->get('start_time');
		// $end_time = $this->input->get('end_time');		
		// $time_range = $this->input->get('time_range');		
		// $date = $this->input->get('date');
		// $teesheet_id = $this->input->get('schedule_id');
		// //$player_count = $this->input->get('guests') ? $this->input->get('guests') : 1;
		// //$player_count = $player_count > 4 ? 4 : $player_count;		
		// //$max_price = $this->input->get('max_price');
// 		
		// if ($time_range && $date)
		// {
			// $value = $this->filter_time_range_presets[$time_range];			
			// $date = date('Ymd', strtotime($date))-100;		
			// $start_time = "$date".$value['start'];
			// $end_time = "$date".$value['end'];
		// }
// 		
		// $different_days = date('Ymd', strtotime($start_time)) == date('Ymd', strtotime($end_time)) ? false : true;
// 
		// $errors = array();
		// if ($time_range && !isset($this->filter_time_range_presets[$time_range]) )
			// $errors[] = 'Invalid parameter: time_range. Valid options are morning, midday, evening & full_day';
		// if ($time_range && !$date)
			// $errors[] = 'Missing parameter: time_range and date must be used together. Valid options for time_range are morning, midday, evening & full_day. Format for date is yyyy-mm-dd';
		// if (!$time_range && $date)
			// $errors[] = 'Missing parameter: time_range and date must be used together. Valid options for time_range are morning, midday, evening & full_day. Format for date is yyyy-mm-dd';
		// if (!$start_time || $start_time == '')
			// $errors[] = 'Missing parameter: start_time';
		// if (!$end_time || $end_time == '')
			// $errors[] = 'Missing parameter: end_time';
		// if (!$teesheet_id || $teesheet_id == '')
			// $errors[] = 'Missing parameter: schedule_id';	
		// if ($different_days)
			// $errors[] = 'Invalid parameters: start_time and end_time must be on the same day';
		// if (count($errors)>0){			
			// $this->response(
				// array(
					// 'status'=>400,
					// 'message'=>'Missing or invalid parameters',
					// 'description'=>implode(', ', $errors),
					// 'code'=> 6040
				// ), 
				// 400
			// );
		// }
// 				
		// $data = $this->Api_data->booked_tee_times($start_time, $end_time, $teesheet_id);
		// echo $this->db->last_query();
		// if ($data) {			
			// $this->response(
				// array(
					// 'status'=>200,
					// 'available_reservation_times'=>$data					
				// ), 
				// 200
			// );
		// } 
		// else 
		// {			
			// $this->response(
				// array(
					// 'status'=>200,
					// 'message'=>'No Reservations',	
					// 'description'=>"No reservations are booked at the specified time for schedule_id : {$teesheet_id}"			
				// ),
				// 200
			// );
		// }		
	// }
	
	function is_available_get()
	{
		//make it so they can only book between appropriate hours for the course in the teesheet table		
		$course_id = $this->input->get('golf_course_id');
		$tee_sheet_id = $this->input->get('schedule_id');
		$time = $this->input->get('time');		
		$player_count = $this->input->get('guests');
		$holes = $this->input->get('holes');
		
		$errors = array();
		if (!$course_id || $course_id == '')
			$errors[] = 'Missing parameter: golf_course_id';
		if (!$tee_sheet_id || $tee_sheet_id == '')
			$errors[] = 'Missing parameter: schedule_id';
		if (!$time || $time == '')
			$errors[] = 'Missing parameter: time';
		if (!$player_count || $player_count == '')
			$errors[] = 'Missing parameter: guests';		
		if (count($errors)>0){
			$this->response(
				array(
					'status'=>400,
					'message'=>'Missing or invalid parameters',
					'description'=>implode(', ', $errors),
					'code'=> 6040
				), 
				400
			);	
		}
		
		$is_available = $this->Api_data->tee_time_is_available($course_id, $tee_sheet_id, $time, $player_count, $holes);
		
		if ($is_available) {
			$permissions = $this->Api_data->course_permissions(false, $tee_sheet_id, $this->rest->api_id);
		
			$response =array(
				'status'=>200,
				'reservation_is_available'=>true					
			);
		
			if ($permissions['tee_time_sales']) {
				$this->load->model('billing');
				$response['available_for_sale'] = $this->billing->have_sellable_teetimes(date('YmdHi', strtotime($time))-1000000, $tee_sheet_id);
			}			
				
			$this->response(
				$response, 
				200
			);
		}
		else 
		{			
			$this->response(
				array(
					'status'=>200,
					'message'=>'Not Available',	
					'description'=>"This reservation time is no longer available : {$time}"			
				),
				200
			);
		}		
	}
	
	function book_get()
	{			
		$teesheet_id = $this->input->get('schedule_id');
		$start = $this->input->get('time');			
		$person_id = $this->input->get('guest_id');
		$guests = $this->input->get('guests');
		$pp = strtolower($this->input->get('prepaid'));
		$prepaid = ($pp === 'y' || $pp === 'yes' || $pp === 't' || $pp === 'true' || (int)$pp == 1)  ? 1 : 0;
		$holes = $this->input->get('holes');
		$ifa = strtolower($this->input->get('is_foreup_app'));
		$is_foreup_app = ($ifa === 'y' || $ifa === 'yes' || $ifa === 't' || $ifa === 'true' || (int)$ifa == 1)  ? 1 : 0;
		$purch = strtolower($this->input->get('purchased'));
		$purchased = ($purch === 'y' || $purch === 'yes' || $purch === 't' || $purch === 'true' || (int)$purch == 1)  ? 1 : 0;
		$amount_paid = $this->input->get('amount_paid');
		
		//optional parameters
		$notes = $this->input->get('notes');
		$carts = $this->input->get('carts') ? strtolower($this->input->get('carts')) : 0;
		
		//set the carts = to the number of guests
		if ($carts === 'y' || $carts === 'yes' || $carts === 't' || $carts === 'true' || (int)$carts == 1) 		
		{
			$carts = $guests;
		}
		else 
		{
			$carts = 0;			
		}
		
		$errors = array();
		if (!$teesheet_id || $teesheet_id == '')
			$errors[] = 'Missing parameter: schedule_id';
		if (!$start || $start == '')
			$errors[] = 'Missing parameter: time';	
		if (!$person_id || $person_id == '')
			$errors[] = 'Missing parameter: guest_id';		
		if (!$guests || $guests == '')
			$errors[] = 'Missing parameter: guests';	
		if (!$holes || $holes == '')
			$errors[] = 'Missing parameter: holes';		
		if ($purchased && (!$amount_paid || $amount_paid == '' || $amount_paid <= 0))
			$errors[] = 'Invalid parameter: amount paid';		
		if (count($errors)>0){
			$this->response(
				array(
					'status'=>400,
					'message'=>'Missing or invalid parameters',
					'description'=>implode(', ', $errors),
					'code'=> 6040
				), 
				400
			);	
		}			
		$permissions = $this->Api_data->course_permissions(false, $teesheet_id, $this->rest->api_id);
		$schedule_info = $this->Teesheet->get_info($teesheet_id);
		
		//if customer doesn't exist create customer with person_id, course_id, and some group name
		if (!$this->Customer->exists_for_course($person_id, $schedule_info->course_id)) {
			$person_data = array();
			$customer_data = array(
				'course_id'=>$schedule_info->course_id,
				'person_id'=>$person_id
			);
			// $giftcards=array();
	        // $groups=array('ForeUP Online Booking');
	        // $passes=array();
			
       		// $this->Customer->save($person_data,$customer_data,$person_id,$giftcards,$groups,$passes,true);
			$this->Api_data->save_customer($customer_data);       
		} 
		
		if ($purchased && $permissions['tee_time_sales']) {
			// SET PREPAID TO TRUE SO THAT THE TEE TIME IS MARKED AS PAID IN TEE SHEET	
			$prepaid = 1;
		}
		//I am not calling the reservation/is_available function first because the Teetime->save tells us if the time is unavailable			
		$response = array();	
		$save_response = $this->Api_data->book_teetime($teesheet_id, $start, $person_id, $guests, $this->rest->api_id, $holes, $carts, $this->rest->key_name, $response, $notes, $prepaid, $is_foreup_app);		
		if ($save_response['invalid_time']){
			$this->response(
				array(
					'status'=>200,
					'reservation_is_available'=>false,
					'message'=>'Invalid Time',
					'description'=> "The time : {$start} is not a valid time for schedule : {$teesheet_id}",
					'code'=>6021					
				), 
				200
			);
		}
		else if ($save_response['success'])
		{
			if ($purchased && $permissions['tee_time_sales']) {
				// SAVE PAYMENT IN SALES PAYMENTS CREDIT CARDS
				$this->load->model('Teesheet');
				$this->load->model('sale');
				$teesheet_info = $this->Teesheet->get_info($teesheet_id);
				$payment_data = array(
					'course_id' => $teesheet_info->course_id,
					'tran_type' => 'Sale',
					'trans_post_time' => date('Y-m-d H:i:s'),
					'amount' => $amount_paid,
					'auth_amount' => $amount_paid,
					'status' => 'Approved'
				);
				$invoice_id = $this->Sale->add_credit_card_payment($payment_data);
				// SAVE TEE TIME IN TEETIME_BARTERED
				$bartered_teetime = array(
					'teetime_id' => $response[0]['id'],
					'teesheet_id' => $teesheet_id,
					'start' => date("YmdHi", strtotime($start))-1000000,
					'end' => date("YmdHi", strtotime($start))-1000000,
					'player_count' => $guests,
					'holes' => $holes,
					'carts' => $carts,
					'booker_id' =>  $person_id,
					'invoice_id' => $invoice_id,
					'api_id' => $this->rest->api_id
				);
			
				// Save bartered tee time data
				$this->db->insert('teetimes_bartered', $bartered_teetime);
			}
					
			//echo 'echoing out success';
			$data = array(
				'status'=>200,
				'booked_reservation'=>true,
				'reservation_id'=>$response[0]['id']
			);
			if ($is_foreup_app)
			{
				//echo 'should be assigning all values';
				$data['discount_percent'] = $response['discount_percent'];
				$data['available_for_purchase'] = $response['available_for_purchase'];
				$data['min_required_carts'] = $response['min_required_carts'];
				$data['min_required_holes'] = $response['min_required_holes'];
				$data['min_required_players'] = $response['min_required_players'];
				// SEND EMAIL CONFIRMATION
				$this->Api_data->send_confirmation_email($response[0]['id'], $teesheet_info->course_id);
			}	
		
			$this->response(
				$data, 
				200
			);							
		}	
		else 
		{					
			$this->response(
				array(
					'status'=>200,
					'booked_reservation'=>false,
					'message'=>"Not Available",	
					'description'=>"This reservation time is no longer available : {$start}"	,
					'code'=>6020		
				),
				200
			);
		}		
	}

	function update_get()
	{
		$reservation_id = $this->input->get('reservation_id');
		$teesheet_id = $this->input->get('schedule_id');
		
		$errors = array();
		if (!$teesheet_id || $teesheet_id == '')
			$errors[] = 'Missing parameter: schedule_id';
		if (!$reservation_id || $reservation_id == '')
			$errors[] = 'Missing parameter: time';	
		if (count($errors)>0){
			$this->response(
				array(
					'status'=>400,
					'message'=>'Missing or invalid parameters',
					'description'=>implode(', ', $errors),
					'code'=> 6040
				), 
				400
			);	
		}		
		$permissions = $this->Api_data->course_permissions(false, $teesheet_id, $this->rest->api_id);
		if (!$permissions['extended_permissions']) {
			$this->response(
				array(
					'status'=>400,
					'message'=>'Restricted Access',
					'description'=>'You do not have permissions to access this function',
					'code'=> 6040
				), 
				400
			);	
		}		
		
		$data = (array)$this->Teetime->get_info($reservation_id);
		if ($this->input->get('time'))
			$data['start'] = $this->input->get('time');
		else 
			$data['start'] += 1000000;
		if ($this->input->get('guests'))
			$data['player_count'] = $this->input->get('guests');
		if ($this->input->get('prepaid'))
		{
			$pp = strtolower($this->input->get('prepaid'));
			$data['details'] = $data['details'].(($pp === 'y' || $pp === 'yes' || $pp === 't' || $pp === 'true' || (int)$pp == 1)  ? ' prepaid from '.$this->rest->key_name : '');
			$data['paid_player_count'] = (($pp === 'y' || $pp === 'yes' || $pp === 't' || $pp === 'true' || (int)$pp == 1)  ? $data['player_count'] : 0);
		}
		if ($this->input->get('holes'))
			$data['holes'] = $this->input->get('holes');
		if ($this->input->get('notes'))
			$data['details'] = $data['details'].' '.$this->input->get('notes');
		if ($this->input->get('carts'))
			$data['carts'] = $this->input->get('carts') ? strtolower($this->input->get('carts')) : 0;
		
		//$schedule_info = $this->Teesheet->get_info($teesheet_id);
		
		//if customer doesn't exist create customer with person_id, course_id, and some group name
		// if (!$this->Customer->exists_for_course($person_id, $schedule_info->course_id)) {
			// $person_data = array();
			// $customer_data = array(
				// 'course_id'=>$schedule_info->course_id,
				// 'person_id'=>$person_id
			// );
			// // $giftcards=array();
	        // // $groups=array('ForeUP Online Booking');
	        // // $passes=array();
// 			
       		// // $this->Customer->save($person_data,$customer_data,$person_id,$giftcards,$groups,$passes,true);
			// $this->Api_data->save_customer($customer_data);       
		// } 
		
		//I am not calling the reservation/is_available function first because the Teetime->save tells us if the time is unavailable	
		//print_r($data);
		if (count($data) == 0) {
			$this->response(
				array(
					'status'=>400,
					'message'=>'Missing or invalid parameters',
					'description'=>'You must have at least one update parameter.',
					'code'=> 6040
				), 
				400
			);	
		}		
				
		$response = array();	
		$save_response = $this->Api_data->update_teetime($teesheet_id, $reservation_id, $data, $response);		
		if ($save_response['invalid_time']){
			$this->response(
				array(
					'status'=>200,
					'reservation_is_available'=>false,
					'message'=>'Invalid Time',
					'description'=> "The time : {$data['start']} is not a valid time for schedule : {$teesheet_id}",
					'code'=>6021					
				), 
				200
			);
		}
		else if ($save_response['success'])
		{
			//echo 'echoing out success';
			$data = array(
				'status'=>200,
				'updated_reservation'=>true,
				'reservation_id'=>substr($response[0]['id'], 0, 20)
			);
			$this->response(
				$data, 
				200
			);							
		}	
		else 
		{					
			$this->response(
				array(
					'status'=>200,
					'updated_reservation'=>false,
					'message'=>"Not Available",	
					'description'=>"This reservation time is no longer available : {$data['start']}"	,
					'code'=>6020		
				),
				200
			);
		}		
	}

	function pay_get()
	{
		$amount = $this->input->get('amount');
		$course_id = $this->input->get('course_id');
		$mobile_pay = $this->input->get('mobile_pay');
		
		if ($course_id)
		{
			if ($course_id == 'test')
			{
				$mercury_id = config_item('test_mercury_id');
				$mercury_password = config_item('test_mercury_password');
			}
			else {
				$mercury_id = config_item('foreup_mercury_id');
				$mercury_password = config_item('foreup_mercury_password');
				//$this->load->model('course');
				//$course_info = $this->course->get_info($course_id);
				//$mercury_id = $course_info->mercury_id;
				//$mercury_password = $course_info->mercury_password;
			}
		}
		else {
			$mercury_id = config_item('foreup_mercury_id');
			$mercury_password = config_item('foreup_mercury_password');
		}
		
		$this->load->library('Hosted_checkout_2');
		
		$HC = new Hosted_checkout_2();
		$HC->set_merchant_credentials($mercury_id,$mercury_password);
		$HC->set_response_urls('api/payments/success', 'api/payments/fail');
		//$HC->set_default_swipe('Manual');
		$initialize_results = $HC->initialize_payment($amount,'0.00','Sale','eCOM','OneTime', true);
			
		//print_r($initialize_results);
		if ((int)$initialize_results->ResponseCode == 0)
		{
			$this->load->model('sale');
			$invoice = $this->sale->add_credit_card_payment(array('tran_type'=>'Sale','frequency'=>'OneTime'));
			$url_type = $mobile_pay ? 'mobile' : 'eCOM';
			$this->response(
				array(
					'status'=>200,
					'payment_id'=>(string)$initialize_results->PaymentID,
					'url'=>site_url("api/payments/mercury_mobile_window/".((string)$initialize_results->PaymentID)),//$HC->get_iframe_url($url_type, (string)$initialize_results->PaymentID),
					'invoice'=>$invoice	,
					'test_transaction'=>$course_id=='test'?true:false
				), 
				200
			);	
		}
		else {
			$this->response(
				array(
					'status'=>200,
					'payment_id'=>false
				),
				200
			);
		}						
	}
	
	function purchased_get()
	{
		$this->load->model('api_data');
		$this->load->model('teetime');
		$this->load->model('customer');
		$this->load->model('billing');
		$this->load->model('course');
		$reservation_id = $this->input->get('reservation_id');
		$guests = $this->input->get('guests');
		$holes = $this->input->get('holes');
		$carts = $this->input->get('carts');
		$payment_id = $this->input->get('payment_id');
		$invoice_id = $this->input->get('invoice');
		$course_id = $this->input->get('course_id');
		$subtotal = $this->input->get('subtotal');
		$discount = $this->input->get('discount');
		$course_info = $this->course->get_info($course_id);
		if ($carts === 'y' || $carts === 'yes' || $carts === 't' || $carts === 'true' || (int)$carts == 1) 		
		{
			$carts = $guests;
		}
		else 
		{
			$carts = 0;			
		}
		$ifa = strtolower($this->input->get('is_foreup_app'));
		$is_foreup_app = ($ifa === 'y' || $ifa === 'yes' || $ifa === 't' || $ifa === 'true' || (int)$ifa == 1)  ? 1 : 0;
		
		if ($course_id)
		{
			if ($course_id == 'test')
			{
				$mercury_id = config_item('test_mercury_id');
				$mercury_password = config_item('test_mercury_password');
			}
			else {
				$mercury_id = config_item('foreup_mercury_id');
				$mercury_password = config_item('foreup_mercury_password');
				//$mercury_id = $course_info->mercury_id;
				//$mercury_password = $course_info->mercury_password;
			}
		}
		else {
			$mercury_id = config_item('foreup_mercury_id');
			$mercury_password = config_item('foreup_mercury_password');
		}
		
		
		$this->load->library('Hosted_checkout_2');
		$HC = new Hosted_checkout_2();
		$HC->set_merchant_credentials($mercury_id,$mercury_password);
		$HC->set_payment_id($payment_id);
		$verify_results = $HC->verify_payment();
		$HC->complete_payment();	
		$payment_data = array (
			'course_id'=>$course_id,
			'mercury_id'=>$mercury_id,
			'tran_type'=>(string)$verify_results->TranType,
			'amount'=>(string)$verify_results->Amount,
			'auth_amount'=>(string)$verify_results->AuthAmount,
			'card_type'=>(string)$verify_results->CardType,
			'frequency'=>'OneTime',
			'masked_account'=>(string)$verify_results->MaskedAccount,
			'cardholder_name'=>(string)$verify_results->CardholderName,
			'ref_no'=>(string)$verify_results->RefNo,
			'operator_id'=>(string)$verify_results->OperatorID,
			'terminal_name'=>(string)$verify_results->TerminalName,
			'trans_post_time'=>(string)$verify_results->TransPostTime,
			'auth_code'=>(string)$verify_results->AuthCode,
			'voice_auth_code'=>(string)$verify_results->VoiceAuthCode,
			'payment_id'=>$payment_id,
			'acq_ref_data'=>(string)$verify_results->AcqRefData,
			'process_data'=>(string)$verify_results->ProcessData,
			'token'=>(string)$verify_results->Token,
			'response_code'=>(int)$verify_results->ResponseCode,
			'status'=>(string)$verify_results->Status,
			'status_message'=>(string)$verify_results->StatusMessage,
			'display_message'=>(string)$verify_results->DisplayMessage,
			'avs_result'=>(string)$verify_results->AvsResult,
			'cvv_result'=>(string)$verify_results->CvvResult,
			'tax_amount'=>(string)$verify_results->TaxAmount,
			'avs_address'=>(string)$verify_results->AVSAddress,
			'avs_zip'=>(string)$verify_results->AVSZip,
			'payment_id_expired'=>(string)$verify_results->PaymendIDExpired,
			'customer_code'=>(string)$verify_results->CustomerCode,
			'memo'=>(string)$verify_results->Memo
		);
		$this->sale->update_credit_card_payment($invoice_id, $payment_data);	
		log_message('error', $this->db->last_query());
		if ($payment_data['status'] == 'Approved')
		{
			$teetime_info = $this->teetime->get_info($reservation_id);
			$customer_info = $this->customer->get_info($teetime_info->person_id);
			$available_for_purchase = $this->billing->have_sellable_teetimes($teetime_info->start, $teetime_info->teesheet_id);
			$email_data = array(
				'course_name'=>$course_info->name,
				'booked_date'=>date('n/j/y', strtotime($teetime_info->start+1000000)),
				'booked_time'=>date('g:ia', strtotime($teetime_info->start+1000000)),
				'booked_holes'=>$holes,
				'booked_players'=>$guests,
				'booked_carts'=>($carts == 1)?$guests:0,
				'customer_name'=>$customer_info->first_name.' '.$customer_info->last_name,
				'customer_email'=>$customer_info->email,
				'card_type'=>$payment_data['card_type'],
				'current_date'=>date('g:ia n/j/y T'),
				'confirmation_number'=>substr($reservation_id, 5),
				'subtotal'=>$subtotal,
				'discount'=>$discount,
				'total'=>$payment_data['amount']
			);
			//$data = $HC->payment_made($email_data);
			if ($available_for_purchase) 
			{
				//Save teetime in the database
				$success = $this->Api_data->update_purchased_teetime($reservation_id, $guests, $carts, $holes, $invoice_id, $is_foreup_app);
				//Email receipt
				if ($success)
				{
					send_sendgrid(
						$customer_info->email,
						'Tee Time Receipt/Confirmation',
						$this->load->view("email_templates/purchase_receipt",$email_data, true),
						'booking@foreup.com',//$this->session->userdata('course_email'),
						'ForeUP'
					);
				}
				$this->response(
					array(
						'status'=>200,
						'payment_approved'=>true,
						'prepay_success'=>true
					),
					200
				);
			}
			else
			{
				$HC->cancel_payment($invoice_id);
				$this->response(
					array(
						'status'=>200,
						'payment_approved'=>true,
						'prepay_success'=>false
					),
					200
				);
			}
		}
		else
		{
			$this->response(
				array(
					'status'=>200,
					'payment_approved'=>false,
					'prepay_success'=>false,
					'message'=>'Credit Card was not approved'
				),
				200
			);
		}
	}
	function cancel_get($teetime_id)
	{		
		$TTID = $this->input->get('reservation_id');
		
		$errors = array();
		if (!$TTID || $TTID == '')
			$errors[] = 'Missing parameter: reservation_id';
		if (count($errors)>0){
			$this->response(
				array(
					'status'=>400,
					'message'=>'Missing or invalid parameters',
					'description'=>implode(', ', $errors),
					'code'=> 6040
				), 
				400
			);		
		}		
		
		$success = $this->Api_data->cancel_reservation($TTID, $this->rest->api_id);
		
		if ($success) 
		{			
			$this->response(
				array(
					'status'=>200,
					'reservation_cancelled'=>true					
				), 
				200
			);
		}
		else 
		{			
			$this->response(
				array(
					'status'=>200,
					'message'=>'Cancel Incomplete',	
					'description'=>"The reservation could not be cancelled "			
				),
				200
			);
		}
	}
	
	//tee time info
	function info_get()
	{
		$TTID = $this->input->get('reservation_id');
		$schedule_id = $this->input->get('schedule_id');
		
		$errors = array();
		if (!$TTID || $TTID == '')
			$errors[] = 'Missing parameter: reservation_id';
		if (!$schedule_id || $schedule_id == '')
			$errors[] = 'Missing parameter: schedule_id';
		if (count($errors)>0){
			$this->response(
				array(
					'status'=>400,
					'message'=>'Missing or invalid parameters',
					'description'=>implode(', ', $errors),
					'code'=> 6040
				), 
				400
			);
		}			
		$permissions = $this->Api_data->course_permissions(false, $schedule_id, $this->rest->api_id);
		
		$teetime_info = $this->Api_data->get_tee_time_info($TTID, $this->rest->api_id, $schedule_id, $permissions['extended_permissions']);		
		// echo $this->db->last_query();
		if ($teetime_info)			
			$this->response(
				array(
					'status'=>200,
					'reservation_info'=>$teetime_info					
				), 
				200
			);
		else {			
			$this->response(
				array(
					'status'=>200,
					'message'=>'No Information',	
					'description'=>"No information is available for the reservation_id : {$TTID} "			
				),
				200
			);			
		}
	}
				
	function list_get()
    {
        $schedule_id = $this->input->get('schedule_id');
        $permissions = $this->Api_data->course_permissions(false, $schedule_id, $this->rest->api_id);
        //print_r($permissions);
        if (!$schedule_id){
            $this->response(
                array(
                    'status'=>400,
                    'message'=>'Missing parameter',
                    'description'=>'schedule_id is a required parameter',
                    'code'=> 6040
                ),
                400
            );
        }
        else
        {

            $date = $this->input->get('date') ? $this->input->get('date') : date('Y-m-d');
            $results = $this->Api_data->get_booked_teetimes_list($course_id, $schedule_id, $date, $this->rest->api_id, $permissions['extended_permissions']);
            $tee_sheet_data = $this->Api_data->get_tee_sheet_info($schedule_id);
            if ($results->num_rows() > 0)
            {
                $reservations = array();
                foreach ($results->result_array() as $key => $teetime) {
                    $teetime['start_time'] = $this->Teesheet->format_time_string($teetime['start_time']);
                    $teetime['end_time'] = $this->Teesheet->format_time_string($teetime['end_time']);
                    $teetime['guests'] = (int)$teetime['guests'];
                    $reservations[] = $teetime;
                }

                $this->response(
                    array(
                        'status'=>200,
                         'tee_sheet_title'=>$tee_sheet_data['title'],
                         'schedule_id'=>$tee_sheet_data['teesheet_id'],
                         'increment'=>$tee_sheet_data['increment'],
                         'open_time'=>$tee_sheet_data['open_time'],
                         'close_time'=>$tee_sheet_data['close_time'],
                        'booked_reservations'=>$reservations
                    ),
                    200
                );
            }
            else
            {
                $this->response(
                    array(
                        'status'=>200,
                        'message'=>'No Reservations',
                        'description'=>"No reservation information is available for schedule_id : {$schedule_id} "
                    ),
                    200
                );
            }
        }
    }	
		
	/*
	 * FACEBOOK API
	 */		
	function join_get()
	{
		$name = $this->input->get('name');
		$email = $this->input->get('email');		
		$tee_time_id = $this->input->get('tee_time_id');		
		
		$errors = array();
		if (!$name || $name == '')
			$errors[] = 'Missing parameter: name';
		if (!$email || $email == '')
			$errors[] = 'Missing parameter: email';
		if (!$tee_time_id || $tee_time_id == '')
			$errors[] = 'Missing parameter: tee_time_id';		
		if (count($errors)>0)
			$this->response(array('success'=>false,'error'=>implode(', ', $errors)));
			
		//use the library to get first and last name
		$this->load->library('name_parser');
			$np = new Name_parser();
			$np->setFullName($name);
			$np->parse();
			if (!$np->notParseable()) {
					$first_name=$np->getFirstName();
					$last_name=$np->getLastName();
			}
			
        $person_data = array(
	        //Required 
			'first_name'=>$first_name,
			'last_name'=>$last_name,
			'email'=> $email,
			//Optional
			'phone_number'=>$phone_number = $this->input->get('phone_number'),						
		);
		
		
		$customer_data=array(
			'api_id' => $this->rest->api_id,			
		); 	
		
		$data = $this->Api_data->join_reservation($person_data, $customer_data, $tee_time_id);
		
		if ($data['success']) {
			$response = array(
				'success'=>true,
				'joined_tee_time'=>true,
				'person_id'=>$customer_data['person_id'], 
				'players'=>$data['players']
			);
			$this->response($response);
		}
		else 
		{
			$response = array(
				'success'=>true,
				'joined_tee_time'=>false,
				'error'=>$data['message']
			);
			$this->response($response);
		}
	}

	function search_get()
	{
		$course_id = $this->input->get('course_id');//$this->input->post('course_id');
		$schedule_id = $this->input->get('schedule_id');//$this->input->post('schedule_id');
		$search_value = $this->input->get('value');//$this->input->post('value');
		$type = $this->input->get('type');//$this->input->post('type');
		// CHECK PERMISSIONS ?
		$errors = array();
		if (!$course_id || $course_id == '')
			$errors[] = 'Missing parameter: course_id';
		if (!$schedule_id || $schedule_id == '')
			$errors[] = 'Missing parameter: schedule_id';
		if (!$search_value || $search_value == '')
			$errors[] = 'Missing parameter: value';		
		if (!$type || $type == '')
			$errors[] = 'Missing parameter: type';		
		if (count($errors)>0)
			$this->response(array('success'=>false,'error'=>implode(', ', $errors)));
		
		if ($course_id)
	    	$permissions = $this->Api_data->course_permissions($course_id, false, $this->rest->api_id);
		
		if (!$permissions['sales']) {
			$this->response(
				array(
					'status'=>400,
					'message'=>'Restricted Access',
					'description'=>'You do not have permissions to access this function',
					'code'=> 6040
				), 
				400
			);	
		}		
		// GET LIST OF VALID CUSTOMERS/GUESTS
		$customer_list = array();
		if ($type == 'last_name')
		{
			$customer_list = $this->Api_data->get_customer_search_suggestions($search_value,25,'name',$course_id,true);
		}
		else if ($type == 'account_number')
		{
			$customer_list = $this->Api_data->get_customer_search_suggestions($search_value,25,'account_number',$course_id,true);
		}
		else if ($type == 'email')
		{
			$customer_list = $this->Api_data->get_customer_search_suggestions($search_value,25,'email',$course_id,true);
		}
		else if ($type == 'phone_number')
		{
			$customer_list = $this->Api_data->get_customer_search_suggestions($search_value,25,'phone',$course_id,true);
		}
		else 
		{
			$this->response(
				array(
					'status'=>400,
					'message'=>'Invalid parameter',
					'description'=>'The type parameter requires one of the following values (last_name, account_number, email, phone_number)',
					'code'=> 6040
				), 
				400
			);
		}
		// echo 'customer_list<br/>';
		// print_r($customer_list);
		// GET LIST OF TEE TIMES AND PRICES
		$reservation_array = array();
		if (count($customer_list) > 0)
		{
			foreach($customer_list as $customer)
			{
				// GET RESERVATION LIST
				$reservations = $this->Api_data->get_reservations($customer['guest_id'], $schedule_id, true);
				// CLEAN UP DATA BEFORE PASSING IT BACK
				foreach($reservations as $reservation)
				{
					if (substr($reservation['reservation_id'], 20) == 'b')
					{
						// SKIP THE REROUNDS
					}
					else 
					{
						if (!isset($reservation_array[$reservation['reservation_id']]))
						{
							$reservation_array[$reservation['reservation_id']] = $reservation;
						}
						$reservation_array[$reservation['reservation_id']]['guest_info'][]  = array('guest_id'=>$customer['guest_id'], 'first_name'=>$customer['first_name'], 'last_name'=>$customer['last_name']);
					}
				}
			}
			if ($type == 'last_name')
			{
				$reservations = $this->Api_data->get_reservations(0, $schedule_id, true, $search_value);
				//echo $this->db->last_query();
				// CLEAN UP DATA BEFORE PASSING IT BACK
				foreach($reservations as $reservation)
				{
					if (substr($reservation['reservation_id'], 20) == 'b')
					{
						// SKIP THE REROUNDS
					}
					else 
					{
						if (!isset($reservation_array[$reservation['reservation_id']]))
						{
							$reservation_array[$reservation['reservation_id']] = $reservation;
							$reservation_array[$reservation['reservation_id']]['guest_info'][]  = array('guest_id'=>0, 'first_name'=>'Guest', 'last_name'=>'');
						}
					}
				}
			}
		}
		else
		{
			$this->response(
				array(
					'status'=>200,
					'message'=>'No reservations',
					'reservations'=>array()
				), 
				200
			);
		}
		
		// RETURN TEE TIME TIMES/PRICES/NAME
		$this->response(array('status'=>200,'reservations'=>$reservation_array), 200);
	}

	function price_details_get()
	{
		$reservation_id = $this->input->get('reservation_id');//$this->input->post('reservation_id');
		$schedule_id = $this->input->get('schedule_id');//$this->input->post('schedule_id');
		$course_id = $this->input->get('course_id');//$this->input->post('course_id');
		//$guests = $this->input->get('guests');//$this->input->post('guests'); // LIMITED TO THE SAME AS IN THE TEE TIME OR LESS
		//$carts = $this->input->get('carts');//$this->input->post('carts'); // LIMITED TO THE SAME AS IN THE TEE TIME OR LESS
		// CHECK PERMISSIONS AND PARAMETERS
		$errors = array();
		if (!$course_id || $course_id == '')
			$errors[] = 'Missing parameter: course_id';
		if (!$schedule_id || $schedule_id == '')
			$errors[] = 'Missing parameter: schedule_id';
		if (!$reservation_id || $reservation_id == '')
			$errors[] = 'Missing parameter: reservation_id';		
//		if (!$type || $type == '')
//			$errors[] = 'Missing parameter: type';		
		if (count($errors)>0)
			$this->response(array('success'=>false,'error'=>implode(', ', $errors)));
		
		if ($course_id)
	    	$permissions = $this->Api_data->course_permissions($course_id, false, $this->rest->api_id);
		if (!$permissions['sales']) {
			$this->response(
				array(
					'status'=>400,
					'message'=>'Restricted Access',
					'description'=>'You do not have permissions to access this function',
					'code'=> 6040
				), 
				400
			);	
		}		
		
		// GET RESERVATION INFORMATION
		$reservation_info = $this->Api_data->tee_time_details($reservation_id, '', true);
		//$reservation_info['player_count'] = 6;
		// GET PRICE AND TAX INFO
		$green_fee_tax_info = $this->Api_data->get_tee_time_taxes($course_id);
		$cart_tax_info = $this->Api_data->get_cart_taxes($course_id);
		
		$course_info = $this->Course->get_info($course_id);
		$index_array = $this->Api_data->get_tee_time_index($course_info, $reservation_info['holes']);
		//(float)$item_prices[$schedule_id][$course_id.'_'.$teetime_type_index]->price;
		$guest_info = array();
		$paid_green_fees = 0;
		$paid_carts = 0;
		// GET PRICE CLASS INFORMATION FOR TEE TIME CUSTOMERS
		if ($reservation_info['person_id'])
		{
			$customer_info = $this->Api_data->get_customer_info($reservation_info['person_id'], '', $course_id, true, true)->row_array();
			$customer_price = $this->Api_data->get_tee_time_prices($schedule_id, $customer_info['price_class']);
			$guest_info['guest_1'] = array(
				'guest_id'=>$customer_info['person_id'], 
				'first_name'=>$customer_info['first_name'],
				'last_name'=>$customer_info['last_name'],
				'paid_green_fee'=>($reservation_info['person_paid_1'] == 1),
				'paid_cart'=>($reservation_info['cart_paid_1'] == 1),
				'green_fee'=>to_currency_no_money($customer_price[$schedule_id][$course_id.'_'.$index_array['gf']]->price * (1 + $green_fee_tax_info[0]['percent']/100)), 
				'cart_fee'=>to_currency_no_money($customer_price[$schedule_id][$course_id.'_'.$index_array['cart']]->price * (1 + $cart_tax_info[0]['percent']/100))
				);
				$paid_green_fees += $reservation_info['person_paid_1'];
				$paid_carts += $reservation_info['cart_paid_1'];
    	}
		if ($reservation_info['person_id_2'])
		{
			$customer_info = $this->Api_data->get_customer_info($reservation_info['person_id_2'], '', $course_id, true, true)->row_array();
			$customer_price = $this->Api_data->get_tee_time_prices($schedule_id, $customer_info['price_class']);
			$guest_info['guest_2'] = array(
				'guest_id'=>$customer_info['person_id'], 
				'first_name'=>$customer_info['first_name'],
				'last_name'=>$customer_info['last_name'],
				'paid_green_fee'=>($reservation_info['person_paid_2'] == 1),
				'paid_cart'=>($reservation_info['cart_paid_2'] == 1),
				'green_fee'=>to_currency_no_money($customer_price[$schedule_id][$course_id.'_'.$index_array['gf']]->price * (1 + $green_fee_tax_info[0]['percent']/100)), 
				'cart_fee'=>to_currency_no_money($customer_price[$schedule_id][$course_id.'_'.$index_array['cart']]->price * (1 + $cart_tax_info[0]['percent']/100))
				);
				$paid_green_fees += $reservation_info['person_paid_2'];
				$paid_carts += $reservation_info['cart_paid_2'];
    	}
		if ($reservation_info['person_id_3'])
		{
			$customer_info = $this->Api_data->get_customer_info($reservation_info['person_id_3'], '', $course_id, true, true)->row_array();
			$customer_price = $this->Api_data->get_tee_time_prices($schedule_id, $customer_info['price_class']);
			$guest_info['guest_3'] = array(
				'guest_id'=>$customer_info['person_id'], 
				'first_name'=>$customer_info['first_name'],
				'last_name'=>$customer_info['last_name'],
				'paid_green_fee'=>($reservation_info['person_paid_3'] == 1),
				'paid_cart'=>($reservation_info['cart_paid_3'] == 1),
				'green_fee'=>to_currency_no_money($customer_price[$schedule_id][$course_id.'_'.$index_array['gf']]->price * (1 + $green_fee_tax_info[0]['percent']/100)), 
				'cart_fee'=>to_currency_no_money($customer_price[$schedule_id][$course_id.'_'.$index_array['cart']]->price * (1 + $cart_tax_info[0]['percent']/100))
				);
				$paid_green_fees += $reservation_info['person_paid_3'];
				$paid_carts += $reservation_info['cart_paid_3'];
		}
		if ($reservation_info['person_id_4'])
		{
			$customer_info = $this->Api_data->get_customer_info($reservation_info['person_id_4'], '', $course_id, true, true)->row_array();
			$customer_price = $this->Api_data->get_tee_time_prices($schedule_id, $customer_info['price_class']);
			$guest_info['guest_4'] = array(
				'guest_id'=>$customer_info['person_id'], 
				'first_name'=>$customer_info['first_name'],
				'last_name'=>$customer_info['last_name'],
				'paid_green_fee'=>($reservation_info['person_paid_4'] == 1),
				'paid_cart'=>($reservation_info['cart_paid_4'] == 1),
				'green_fee'=>to_currency_no_money($customer_price[$schedule_id][$course_id.'_'.$index_array['gf']]->price * (1 + $green_fee_tax_info[0]['percent']/100)), 
				'cart_fee'=>to_currency_no_money($customer_price[$schedule_id][$course_id.'_'.$index_array['cart']]->price * (1 + $cart_tax_info[0]['percent']/100))
				);
				$paid_green_fees += $reservation_info['person_paid_4'];
				$paid_carts += $reservation_info['cart_paid_4'];
    	}
		if ($reservation_info['person_id_5'])
		{
			$customer_info = $this->Api_data->get_customer_info($reservation_info['person_id_5'], '', $course_id, true, true)->row_array();
			$customer_price = $this->Api_data->get_tee_time_prices($schedule_id, $customer_info['price_class']);
			$guest_info['guest_5'] = array(
				'guest_id'=>$customer_info['person_id'], 
				'first_name'=>$customer_info['first_name'],
				'last_name'=>$customer_info['last_name'],
				'paid_green_fee'=>($reservation_info['person_paid_5'] == 1),
				'paid_cart'=>($reservation_info['cart_paid_5'] == 1),
				'green_fee'=>to_currency_no_money($customer_price[$schedule_id][$course_id.'_'.$index_array['gf']]->price * (1 + $green_fee_tax_info[0]['percent']/100)), 
				'cart_fee'=>to_currency_no_money($customer_price[$schedule_id][$course_id.'_'.$index_array['cart']]->price * (1 + $cart_tax_info[0]['percent']/100))
				);
				$paid_green_fees += $reservation_info['person_paid_5'];
				$paid_carts += $reservation_info['cart_paid_5'];
    	}
		$diff = $reservation_info['player_count'] - count($guest_info); 
		if ($diff > 0)
		{
			$price = $this->Api_data->get_tee_time_prices($schedule_id);
			for ($i = 1; $i <= $diff; $i++)
			{
				$paid_gf = ($reservation_info['paid_player_count'] - $paid_green_fees > 0);
				$paid_c = ($reservation_info['paid_carts'] - $paid_carts > 0);
				$guest_info['guest_'.(5+$i)] = array(
					'guest_id'=>'', 
					'first_name'=>'Guest',
					'last_name'=>'',
					'paid_green_fee'=>$paid_gf,
					'paid_cart'=>$paid_c,
					'green_fee'=>to_currency_no_money($price[$schedule_id][$course_id.'_'.$index_array['gf']]->price * (1 + $green_fee_tax_info[0]['percent']/100)), 
					'cart_fee'=>to_currency_no_money($price[$schedule_id][$course_id.'_'.$index_array['cart']]->price * (1 + $cart_tax_info[0]['percent']/100))
					);
				$paid_green_fees += $paid_gf ? 1 : 0;
				$paid_carts += $paid_c ? 1 : 0;
    		}
		}		
		unset($reservation_info['player_count']);
		unset($reservation_info['person_id']);
		unset($reservation_info['person_id_2']);
		unset($reservation_info['person_id_3']);
		unset($reservation_info['person_id_4']);
		unset($reservation_info['person_id_5']);
		unset($reservation_info['person_paid_1']);
		unset($reservation_info['person_paid_2']);
		unset($reservation_info['person_paid_3']);
		unset($reservation_info['person_paid_4']);
		unset($reservation_info['person_paid_5']);
		unset($reservation_info['cart_paid_1']);
		unset($reservation_info['cart_paid_2']);
		unset($reservation_info['cart_paid_3']);
		unset($reservation_info['cart_paid_4']);
		unset($reservation_info['cart_paid_5']);
		unset($reservation_info['paid_player_count']);
		unset($reservation_info['paid_carts']);
		$reservation_info['guest_info'] = $guest_info;
		
		$this->response(array('status'=>200,'reservation_info'=>$reservation_info), 200);
	}
	
	/*
	 * FACEBOOK API
	 */
	function tee_time_details_get()
	{
		$TTID = $this->input->get('tee_time_id');
		$errors = array();
		if (!$TTID || $TTID == '')
			$errors[] = 'Missing parameter: tee_time_id';
		if (count($errors)>0)
			$this->response(array('error', implode(', ', $errors)));
		
		$data = $this->Api_data->tee_time_details($TTID, $this->rest->api_id);
		
		if ($data) {
			$response = array('success'=>true,'tee_time_details'=>$data);
			$this->response($response);
		} 
		else 
		{
			$this->response(array('success'=>true,'tee_time_details'=>array(),'error'=>"No information is available for tee_time_id:{$TTID}"));
		}
		
	}			
}
