<?php
// Booking API to provide REST API functionality for online booking,
// Used internally (but could be used externally) for Backbone JS
require_once(APPPATH.'libraries/REST_Controller.php');

class Booking extends REST_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('teesheet');
		$this->load->model('schedule');
	    $this->load->model('teetime');
	    $this->load->model('course');
	    $this->load->model('user');
	    $this->load->helper('array');

	    header('Content-type: application/json');
	}

    private function _get_course($course_id){
		
		$this->course_id = (int) $course_id;
		$course_info = (array) $this->course->get_info($course_id);
		$course_info = $this->_filter_course_data($course_info);

		if(empty($course_id)){
			$this->response(array('success' => false, 'msg' => '{course_id} is required'), 400);
			return false;
		}

		$this->course_info = $course_info;
		$this->session->set_userdata('course_id', $course_id);
		$this->session->set_userdata('course_name', $course_info['name']);
		$this->session->set_userdata('course_phone', $course_info['phone']);
		$this->session->set_userdata('course_email', $course_info['email']);
		
		if($course_info['timezone']){
			date_default_timezone_set($course_info['timezone']);
		}

		return $course_info;
	}

    private function _get_schedule($schedule_id){

		if(empty($schedule_id)){
			$this->response(array('success' => false, 'msg' => 'Schedule ID is required'), 400);
			return false;
		}
		$this->schedule_id = (int) $schedule_id;

		$schedule_data = (array) $this->teesheet->get_info($schedule_id);
		$this->schedule = $schedule_data;
		
		// If selected schedule is deleted or does not have online booking enabled, return error
		if(empty($schedule_data) || $schedule_data['online_booking'] == 0 || $schedule_data['deleted'] == 1){
			$this->response(array('success' => false, 'msg' => 'Selected schedule does not exist, or is not available for booking'), 400);
			die();
			return false;
		}
		
		$this->session->set_userdata('teesheet_id', $schedule_data['teesheet_id']);
		$this->session->set_userdata('schedule_id', $schedule_data['teesheet_id']);
		$this->session->set_userdata('days_out', $schedule_data['days_out']);
		$this->session->set_userdata('online_open_time', $schedule_data['online_open_time']); 
		$this->session->set_userdata('online_close_time', $schedule_data['online_close_time']); 
		$this->session->set_userdata('days_in_booking_window', $schedule_data['days_in_booking_window']);
		$this->session->set_userdata('increment', $schedule_data['increment']);

		return $schedule_data;
	}
	
	private function _get_user($user_id){
		
		// Gather up users data to be sent back in response
		$this->load->model('customer');
		$this->load->model('invoice');
		$user_data = (array) $this->customer->get_info($user_id, $this->course_id);
		$user_data['user_id'] = (int) $this->session->userdata('customer_id');
		$user_data['logged_in'] = true;	
		
		$username = $this->user->get_username($this->session->userdata('customer_id'));
		$user_data['username'] = $username;
		
		// Get saved credit cards
		$this->load->model('Customer_credit_card');
		$user_data['credit_cards'] = $this->Customer_credit_card->get($user_data['user_id']);
		
		// Get user's current reservations
		$this->load->model('user');
		$user_data['reservations'] = $this->user->get_teetimes($user_data['user_id'], 'upcoming', $this->course_id);
		
		// Get user's current reservations
		$purchases = $this->user->get_purchases($user_data['user_id'],$this->course_id);	
		$user_data['purchases'] = $purchases['summary'];
		
		// Get user's gift cards
		$user_data['gift_cards'] = $this->user->get_giftcards($user_data['user_id'], $this->course_id);
		
		$invoices = $this->invoice->get_all($user_data['user_id']);	
		$user_data['invoices'] = $invoices->result_array();		
		
		return $user_data;		
	}

	private function _filter_course_data($course_data){
		$validParams = array(
			'course_id',
			'active',
			'area_id',
			'name',
			'address',
			'city',
			'state',
			'postal',
			'zip',
			'country',
			'area_code',
			'timezone',
			'latitude_centroid',
			'longitude_centrod',
			'phone',
			'online_booking',
			'online_booking_protected',
			'booking_rules',
			'email',
			'no_show_policy',
			'include_tax_online_booking',
			'foreup_discount_percent',
			'close_time',
			'open_time'
		);

		return elements($validParams, $course_data, '');
	}

    function index_get()
    {
		$this->response(null, 403);
		return true;
    }

	// GET Information about a specific course
	function courses_get($course_id, $method = null, $recordId = null, $method2 = null, $recordId2 = null)
	{
		$course_info = $this->_get_course($course_id);

		// Route schedules
		if($method == 'schedules'){
			$this->schedules_get($recordId, $method2);
			return true;
		}

		// Route login
		if($method == 'login'){
			$this->courses_login($recordId, $method2);
			return true;
		}
		
		if($recordId == 'invoices'){
			$this->users_invoices_get($method2);
			return true;
		}			

		$teesheets = $this->teesheet->get_teesheets($this->course_id, array('online_only' => 1));
		$course_info['schedules'] = $teesheets;

		$this->response($course_info);
	}

	// POST Log in to a specific course, or schedule a teetime on a course schedule
	function courses_post($course_id, $method = null, $recordId = null, $method2 = null, $recordId2 = null){
		$course_info = $this->_get_course($course_id);

		// Route schedules
		if($method == 'schedules'){
			$this->schedules_post($recordId, $method2);
			return true;
		}

		// Route login
		if($method == 'login'){
			$this->courses_login();
			return true;
		}
		
		// Route user signup
		if($method == 'users'){
			
			if($recordId == 'logout'){
				$this->users_logout();
			}
			if($recordId == 'login'){
				$this->users_login();
			}
			if($recordId == 'credit_cards'){
				$this->users_credit_cards_post();
			}
			if($recordId == 'reservations'){
				$this->users_reservations_post();
			}
			if($recordId == 'invoices'){
				$this->users_invoices_post($method2, $recordId2);
				return true;
			}																
			
			$this->users_post();
			return true;
		}		

		$this->response($course_info);
	}
	
	function courses_delete($course_id, $method = null, $recordId = null, $method2 = null, $recordId2 = null){
		$course_info = $this->_get_course($course_id);
		
		// Route user signup
		if($method == 'users'){
			
			if($recordId == 'reservations'){
				$this->users_reservations_delete($method2);
				return true;
			}
			if($recordId == 'credit_cards'){
				$this->users_credit_cards_delete($method2);
				return true;
			}														
		}		

		$this->response(null, 405);
	}
	
	function courses_put($course_id, $method = null, $recordId = null, $method2 = null, $recordId2 = null){
		$course_info = $this->_get_course($course_id);
		
		// Route user profile update
		if($method == 'users'){
			$this->users_put($method2);
			return true;										
		}		

		$this->response(null, 405);
	}		
		
	// Logs a user into a specific course, if the user is not yet on
	// the courses customer list, they will be added
	function courses_login(){

	}

	// GET List of teesheets from a specific course
	function schedules_get($teesheet_id = null, $method = null){

		// Route times
		if(!empty($teesheet_id) && $method == 'times'){
			$this->_get_schedule($teesheet_id);
			$this->schedules_times_get();
			return true;
		}

		$teesheets = $this->teesheet->get_teesheets($this->course_id, array('online_only'=>1));
		$this->response($teesheets);
    }

	// POST Schedules, only allow reserving of teetimes, nothing else
	function schedules_post($teesheet_id = null, $method = null){

		$this->_get_schedule($teesheet_id);

		// Route times
		if($method == 'times'){
			$this->schedules_times_post();
			return true;
		}

		// If trying to POST to root schedules URL, throw error
		$this->response(null, 405);
    }

    // GET Available teetimes from a specific schedule/teesheet
	function schedules_times_get(){
		
		if($this->course_info['online_booking_protected'] == 1 && !$this->session->userdata('customer_id')){
			$this->response(array('success' => false, 'msg' => 'You must log in to view tee times for this course'), 401);
			return false;
		}			
		$this->load->model('Booking_class');
			    
        $time_range = $this->input->get('time');
        $course_id = $this->input->get('course_id');
        $holes = $this->input->get('holes');
        $available_spots = $this->input->get('players');
        $booking_class_id = $this->input->get('booking_class');
        
        if($booking_class_id === 'false' || $booking_class_id === false){
			$booking_class_id = false;
		}
		
		// If booking class is passed, retrieve booking class info and
		// load it into session
        if($booking_class_id){
			$booking_class_info = $this->Booking_class->get_info($booking_class_id, $this->schedule_id);
			
			if(empty($booking_class_info) || $booking_class_info->teesheet_id != $this->schedule_id){
				$this->response(array('success' => false, 'msg' => 'Selected booking class is invalid'), 400);
				return false;				
			}
			
			$this->session->set_userdata('booking_class_id', (int) $booking_class_id);
			$this->session->set_userdata('booking_class_info', $booking_class_info);
		
		// If no booking class, clear out session data so it doesn't
		// screw up tee time retrieval
		}else{
			$this->session->unset_userdata('booking_class_id');
			$this->session->unset_userdata('booking_class_info');
		}
		$this->session->sess_write();

        // Subtract selected date from current date for a day number
        $date = $this->input->get('date');
        if(empty($date)){
			$date = date('m-d-Y');
		}
		$date = DateTime::createFromFormat('m#d#Y', $date);
		$curdate = new DateTime();
		$days_in_future = abs((int) $date->diff($curdate)->format('%a')) + 1;
		
		if($days_in_future < 0){
			$this->response(array(), 200);
			return true;
		}
		
		// Course close time takes precedence over tee sheet time
		$close_time = (int) $this->schedule['online_close_time'];
		if($this->session->userdata('booking_class_info')){
			$close_time = (int) $this->session->userdata('booking_class_info')->online_close_time;
		}

		// If it is past the close time, default date to tomorrow
        $days_in_future_adjustment = 0;
        if(($close_time - 100) <= (int) date('H').'00'){
			$days_in_future_adjustment = -1;
		}	
		
		// Set tee time API settings and retrieve available tee times
		$this->teesheet->load_booking_settings($this->course_id, $this->schedule_id);
		$this->teesheet->apply_booking_filters($holes, $available_spots, $time_range, '', '', '', '', $days_in_future + $days_in_future_adjustment);
		$this->teesheet->format = 'json';
		$teetimes = $this->teesheet->get_available_teetimes();
		
		$cart_setting = (int) $this->schedule['booking_carts'];
		if($booking_class_info->booking_carts){
			$cart_setting = (int) $booking_class_info->booking_carts;
		}
		
		// Check if there are available tee times to purchase
		$start = (int) $date->format('Ymd') - 100;
		$can_purchase = (bool) $this->billing->have_sellable_teetimes($start);

		if(!empty($teetimes)){
			
			foreach($teetimes as &$teetime){
				$teetime['time'] = date('Y-m-d H:i', strtotime($teetime['reservation_time']));
				$teetime['holes'] = $holes;
				$teetime['can_purchase'] = $can_purchase;
				
				// If walking only
				if($cart_setting == 0){		
					$teetime['green_fee'] = abs($teetime['green_fee']);
					unset($teetime['cart_fee']);
				
				// If both
				}else if($cart_setting == 1){
					$teetime['green_fee'] = abs($teetime['green_fee']);
					$teetime['cart_fee'] = abs($teetime['cart_fee']);
				
				// If Riding only
				}else{
					$teetime['green_fee'] = abs($teetime['green_fee']) + abs($teetime['cart_fee']);
					unset($teetime['cart_fee']);
				}
			}
		}
		
		$this->response($teetimes, 200);
    }

	function schedules_times_post(){
		$this->response(null, 405);
    }

    function users_get($userId){
		$this->response(null, 405);
	}

    // POST Register as a new user, or login as an existing user
    function users_post($method = null){

		if($method == 'login'){
			$this->users_login();
			return true;
		}

		if($method == 'logout'){
			$this->users_logout();
			return true;
		}
		
		if($method == 'credit_cards'){
			$this->users_credit_cards_post();
			return true;
		}		
		$this->load->model('customer');
		
		$first_name = $this->request->body['first_name'];	
		$last_name = $this->request->body['last_name'];	
		$phone = $this->request->body['phone'];
		$email = $this->request->body['email'];
		$password = $this->request->body['password'];
		
		if(empty($first_name)){
			$this->response(array('success' => false, 'msg' => 'First name is required'), 400);
			return false;			
		}
		if(empty($last_name)){
			$this->response(array('success' => false, 'msg' => 'Last name is required'), 400);
			return false;			
		}
		if(empty($phone)){
			$this->response(array('success' => false, 'msg' => 'Phone number is required'), 400);
			return false;			
		}	
		if(empty($email)){
			$this->response(array('success' => false, 'msg' => 'Email is required'), 400);
			return false;			
		}
		if(empty($password)){
			$this->response(array('success' => false, 'msg' => 'Password is required'), 400);
			return false;			
		}
		if(strlen($password) < 6){
			$this->response(array('success' => false, 'msg' => 'Password must be at least 6 characters'), 400);
			return false;			
		}		
		
		if($this->user->username_exists($email)){
			$this->response(array('success' => false, 'msg' => lang('customers_error_registering')), 409);
			return false;
		}
		
		$person_data = array(
			'first_name' => $first_name,
			'last_name' => $last_name,
			'email' => $email,
			'phone_number' => $phone
		);

		$customer_data = array(
			'account_number' => null,
            'course_id' => $this->course_id,
            'username' => $email,
            'password' => md5($password)
		);
		
		$giftcards = array();
		$groups = array('ForeUP Online Booking');
		$passes = array();
		$customer_id = false;	
		
		$saved = $this->customer->save($person_data, $customer_data, $customer_id, $giftcards, $groups, $passes, true);
		$customer_id = $person_data['person_id'];
		
		if($saved){
			$logged_in = $this->user->login($email, $password);
			
			// Get user data to send back with success message
			$user_data = $this->_get_user($customer_id);
			$user_data['logged_in'] = (bool) $logged_in;
			$user_data['username'] = $email;
			
			$this->response($user_data);	
		}else{
			$this->response(array('success' => false, 'msg' => 'Error completing registration'), 405);	
		}		
		
		return true;
	}
	
	// Updates a users profile
    function users_put($method = null){
	
		$this->load->model('person');
		$this->load->model('user');
		$this->load->model('customer');

		$first_name = $this->request->body['first_name'];	
		$last_name = $this->request->body['last_name'];	
		$phone = $this->request->body['phone'];
		$address = $this->request->body['address'];
		$city = $this->request->body['city'];
		$state = $this->request->body['state'];
		$zip = $this->request->body['zip'];
		$birthday = $this->request->body['birthday'];
		$email = $this->request->body['email'];
		
		$username = $this->request->body['username'];
		$password = $this->request->body['password'];
		$current_password = $this->request->body['current_password'];
		
		if(!$this->session->userdata('customer_id')){
			$this->response(array('success' => false, 'msg' => 'Please login to edit your account information'), 401);
			return false;			
		}
		$customer_id = $this->session->userdata('customer_id');
		$current_customer_info = (array) $this->customer->get_info($customer_id);
		$current_username = $this->user->get_username($customer_id);	
		
		if($username === null){
			
			$this->load->helper('email');			
			
			if(empty($first_name)){
				$this->response(array('success' => false, 'msg' => 'First name is required'), 400);
				return false;			
			}
			if(empty($last_name)){
				$this->response(array('success' => false, 'msg' => 'Last name is required'), 400);
				return false;			
			}
			if(empty($phone)){
				$this->response(array('success' => false, 'msg' => 'Phone number is required'), 400);
				return false;			
			}
			if(empty($email)){
				$this->response(array('success' => false, 'msg' => 'Email is required'), 400);
				return false;			
			}			
			if(!valid_email($email)){
				$this->response(array('success' => false, 'msg' => 'Email is invalid'), 400);
				return false;				
			}				
		
			if(!empty($birthday)){
				$birthday = date('Y-m-d', strtotime($birthday));
			}else{
				$birthday = '0000-00-00';
			}

			$person_data = array(
				'first_name' => $first_name,
				'last_name' => $last_name,
				'phone_number' => $phone,
				'address_1' => $address,
				'city' => $city,
				'state' => $state,
				'zip' => $zip,
				'birthday' => $birthday,
				'email' => $email
			);
			$saved = $this->person->save($person_data, $customer_id);
		
		}else{
			
			if(empty($username)){
				$this->response(array('success' => false, 'msg' => 'Username is required'), 400);
				return false;				
			}
			if($username != $current_username && $this->user->username_exists($username)){
				$this->response(array('success' => false, 'msg' => 'That username is taken, please use a different one'), 400);
				return false;				
			}
			
			$user_data = array();
			$user_data['email'] = $username;
			
			// If user is resetting their password
			if(!empty($current_password)){
				
				// If password is too short, return error
				if(strlen($password) < 6){
					$this->response(array('success' => false, 'msg' => 'New password must be at least 6 characters'), 400);
					return false;			
				}	
				
				// If their current password does not match, return error
				if(!$this->user->login($current_username, $current_password)){
					$this->response(array('success' => false, 'msg' => 'Current password does not match'), 400);
					return false;						
				}
						
				$user_data['password'] = md5($password);
			}
			
			$saved = $this->user->save($customer_id, $user_data);
		}
		
		if($saved){
			$user_data['success'] == true;
			$this->response(array('success'=>true), 200);	
		
		}else{
			$this->response(array('success'=>false, 'msg' => 'Error saving profile'), 400);	
		}		
		
		return true;
	}
	

	// POST Handle user login
	function users_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$logged_in = $this->user->login($username, $password);

		// If login failed, return error
		if(!$logged_in){
			$this->response(array('success' => false, 'msg' => 'Username or password is invalid'), 401);
			return false;
		}
		
		// Add user as a customer to this course (if they are not already)
		$user = $this->user->get_by_username($username);
		if(!empty($user->id) && !empty($this->course_id)){
			$this->user->add_to_course($user->id, $this->course_id);
		}		
		
		// Gather up users data to be sent back in response
		$user_data = $this->_get_user($this->session->userdata('customer_id'));

		$this->response($user_data);
	}

	function users_logout(){
		$this->session->sess_destroy();
		$this->response(array('msg'=>'You have been logged out successfully', 'success'=>true), 200);
	}
	
	function users_delete($method, $recordId){
		if($method == 'reservations'){
			$this->users_reservations_delete($recordId);
			return true;
		}

		if($method == 'credit_cards'){
			$this->users_credit_cards_delete($recordId);
			return true;
		}		
		$this->response(null, 405);
	}
	
	function users_credit_cards_post(){
		
		$customer_id = $this->session->userdata('customer_id');
		
		if(empty($customer_id)){
			$this->response(array('success' => false, 'msg' => 'Please log in to save credit card'), 401);
			return false;			
		}
		
		$number = $this->request->body['number'];
		$expiration = $this->request->body['expiration'];
		$type = $this->request->body['type'];
		$ets_id = $this->request->body['ets_id'];
		$ets_session_id = $this->request->body['ets_session_id'];
		$ets_customer_id = $this->request->body['ets_customer_id'];
		
		// Verify that the ETS ID is valid
		$this->load->library('Hosted_payments');
		$payment = new Hosted_payments();
		$payment->initialize($this->config->item('ets_key'));
		$response = $payment->set('action', 'verify')
				   ->set('sessionId', $ets_session_id)
				   ->set('customerId', $ets_customer_id)
				   ->set('transactionId', $ets_id)
				   ->send();
				   
		// If ETS failed to validate token, return error	   
		if(empty($response) || !$response->status || $response->status != 'success'){
			$this->response(array('success' => false, 'msg' => 'Error validating credit card, please try again'), 405);
			return false;				
		}
		
		switch($type){
			case 'MasterCard':
				$card_type = 'M/C';
			break;
			case 'Visa':
				$card_type = 'VISA';
			break;
			case 'Discover':
				$card_type = 'DCVR';
			break;
			case 'American Express':
				$card_type = 'AMEX';
			break;
			case 'Diners':
				$card_type = 'DINERS';
			break;
			case 'JCB':
				$card_type = 'JCB';
			break;
			default:
				$card_type = $type;
			break;
		}
		$number = str_replace('*', '', (string) $number);
		$expiration = DateTime::createFromFormat('my', (string) $expiration);			
		
		$this->load->model('Customer_credit_card');
		
		// Save credit card data to customers profile
		$credit_card_data = array(
			'course_id' 		=> $this->session->userdata('course_id'),
			'card_type' 		=> $card_type,
			'masked_account' 	=> $number,
			'cardholder_name' 	=> '',
			'customer_id'		=> $customer_id,
			'token' 			=> (string) $ets_customer_id,
			'expiration' 		=> $expiration->format('Y-m-01')
		);
		$credit_card_id = $this->Customer_credit_card->save($credit_card_data);
		$credit_card_data['credit_card_id'] = (int) $credit_card_id;
		
		$this->response($credit_card_data, 200);
	}
	
	function users_credit_cards_delete($creditCardId){
    	
    	if(!$creditCardId){
			$this->response(array('success' => false, 'msg' => 'Credit card ID is required'), 400);
			return false;
		}
		$this->load->model('Customer_credit_card');
		$this->Customer_credit_card->delete_card((int) $this->session->userdata('customer_id'), (int) $creditCardId);
		$success = $this->Customer_credit_card->db->affected_rows();

		if($success){
			$this->response(array('success'=>true, 'msg'=>'Credit card deleted'), 200);
		}else{
			$this->response(array('success'=>false, 'msg'=>'Error deleting credit card'), 500);
		}
	}
	
	// Make a new reservation
	function users_reservations_post(){
		
        // User must be logged in to register a teetime
    	if (!$this->user->is_logged_in()){
			$this->response(array('success' => false, 'msg' => 'Login is required'), 401);
			return false;
		}
		
		if($this->request->body['teesheet_id']){
			$schedule_id = $this->request->body['teesheet_id'];
		}else{
			$schedule_id = $this->request->body['schedule_id'];
		}
		
		// Make sure schedule ID was passed
    	if (empty($schedule_id)){
			$this->response(array('success' => false, 'msg' => 'Schedule ID is required'), 400);
			return false;
		}
		
    	$this->_get_schedule($schedule_id);		
		
        $time = $this->request->body['time'];
        $players = (int) $this->request->body['players'];
        $carts = $this->request->body['carts'];
        $holes = (int) $this->request->body['holes'];
        $credit_card_id = (int) $this->request->body['credit_card_id'];

		if($carts === false || $carts === 'false' || $carts === 0){
			$carts = 0;
		}else{
			$carts = $players;
		}
		
		// Parse reservation time, if time failed to parse throw error
        $time = DateTime::createFromFormat('Y-m-d H:i', $time);
        if(!$time){
			$this->response(array('success' => false, 'msg' => 'Invalid reservation time, format should be YYYY-MM-DD HH:MM'), 400);
			return false;
		}
		
		// Check number of holes in reservation
		if($this->schedule['limit_holes'] != 0 && $holes != $this->schedule['limit_holes']){
			$this->response(array('success' => false, 'msg' => 'Invalid number of holes, only '.$this->schedule['limit_holes'].' holes are allowed'), 400);
			return false;			
		}

        // Build timestamp for saving teetime
        $start = (int) $time->format('YmdHi') - 1000000;

        // If credit card is required and was not selected, return error
    	if ($this->schedule['require_credit_card'] == 1 && empty($credit_card_id)){
			$this->response(array('success' => false, 'msg' => 'Error reserving teetime, a valid credit card is required'), 405);
			return false;
		}
		
        // If carts are not allowed to be booked online, set them to 0
    	if ($this->schedule['booking_carts'] == 0){
			$carts = 0;
		}
	
        if($carts > 4){
			$this->response(array('success' => false, 'msg' => 'Invalid number of carts, maxiumum of 4 carts is allowed'), 400);
			return false;
		}		
		
		// Round time appropriately for 7/8 tee time
        $end = $start + $this->schedule['increment'];
    	if($this->schedule['increment'] == 7.5){
			$this->load->model('Teetime');
			$end = $this->Teetime->adjust_7_8_time($end);
		}
    	if ($end % 100 > 59){
            $end += 40;
		}

		$event_data = array(
			'teesheet_id' => $schedule_id,
			'start' => $start,
			'end' => $end,
			'side' => 'front',
			'allDay' => 'false',
			'holes' => $holes,
			'player_count' => $players,
			'type' => 'teetime',
			'carts' => $carts,
			'credit_card_id' => $credit_card_id,
			'person_id' => $this->session->userdata('customer_id'),
			'person_name' => $this->session->userdata('last_name').', '.$this->session->userdata('first_name'),
			'title' => $this->session->userdata('last_name'),
			'booking_source' => 'online',
			'booker_id' => $this->session->userdata('customer_id'),
			'details'=>'Reserved using online booking @ '.date('g:ia n/j T')
		);

		$response = array();
		$save_response = $this->teetime->save($event_data, false, $response, true, $this->course_id);
		
		// If there was an error making the reservation
		if($save_response['success'] !== true){
			$this->response(array('success' => false, 'msg' => 'The selected reservation time is already taken'), 409);
			return false;
		}
		
		// Send a confirmation email to customer
		$email_data = array(
			'TTID' => $save_response['teetime_id'],
			'teetime_id' => $save_response['teetime_id'],
			'new_booking' => true,		
			'cancelled' => false,	
			'person_id' => $this->session->userdata('customer_id'),
			'course_name' => $this->session->userdata('course_name'),
			'course_phone' => $this->session->userdata('course_phone'),
			'first_name' => $this->session->userdata('first_name'),
			'booked_date' => date('n/j/y', strtotime($start + 1000000)),
			'booked_time' => date('g:ia', strtotime($start + 1000000)),
			'booked_holes' => $holes,
			'booked_players' => $players,
			'course_id' => $this->course_id,
			'tee_sheet' => $this->schedule['title']
		);
		
		$this->teetime->send_confirmation_email(
			$this->session->userdata('customer_email'),
			'Tee Time Reservation Details',
			$email_data,
			($this->session->userdata('course_email') ? $this->session->userdata('course_email'):'booking@foreup.com'),
			$this->session->userdata('course_name')
		);		
		
		// Check if this tee time is available to purchase
		$this->load->model('billing');
		$can_purchase = (bool) $this->billing->have_sellable_teetimes($start);
		
		if($carts == 0){
			$carts = false;
		}else{
			$carts = true;
		}
		
		$reservation = array();
		$reservation['teetime_id'] = $save_response['teetime_id'];
		$reservation['holes'] = $holes;
		$reservation['carts'] = $carts;
		$reservation['player_count'] = $players;
		$reservation['reservation_time'] = $time->format('g:ia');
		$reservation['time'] = $time->format('Y-m-d H:i');
		$reservation['schedule_id'] = $schedule_id;
		$reservation['teesheet_title'] = $this->schedule['title'];
		$reservation['course_name'] = $this->course_info['name'];
		$reservation['course_id'] = $this->course_info['course_id'];
		$reservation['start_timestamp'] = $time->getTimestamp();
		$reservation['can_purchase'] = $can_purchase;
		$reservation['paid_player_count'] = 0;
		
		$this->response($reservation);		
	}
	
	// Cancel a user's reservation
	function users_reservations_delete($teetime_id){
		
        // User must be logged in to cancel a teetime
    	if (!$this->user->is_logged_in()){
			$this->response(array('success' => false, 'msg' => 'Login is required'), 401);
			return false;
		}
		$user_id = $this->session->userdata('customer_id');
		
        // User must be logged in to cancel a teetime
    	if (empty($teetime_id)){
			$this->response(array('success' => false, 'msg' => 'Teetime ID is required'), 400);
			return false;
		}
		$this->load->model('teetime');
		
		// Get information about teetime and course
		$reservation_info = (array) $this->teetime->get_info($teetime_id);
		$teesheet_info = (array) $this->teesheet->get_info($reservation_info['teesheet_id']);
		$this->_get_course($teesheet_info['course_id']);
		
		$success = $this->teetime->delete($teetime_id, $user_id);
		$this->db->trans_complete();

		$email_data = array(		
			'cancelled' => true,
			'person_id' => $user_id,
			'course_name' => $this->course_info['name'],
			'course_phone' => $this->course_info['phone'],
			'first_name' => $this->session->userdata('first_name'),
			'booked_date' => date('n/j/y', strtotime($reservation_info['start'] + 1000000)),
			'booked_time' => date('g:ia', strtotime($reservation_info['start'] + 1000000)),
			'booked_holes' => $reservation_info['holes'],
			'booked_players' => $reservation_info['player_count'],
			'tee_sheet' => $teesheet_info['title']
		);

		if ($success){
			
			send_sendgrid(
				$this->session->userdata('customer_email'),
				'Reservation Cancellation Details',
				$this->load->view("email_templates/reservation_made", $email_data, true),
				'booking@foreup.com',
				$this->course_info['name']
			);

			$this->response(array('success' => true, 'msg' => 'Reservation Cancelled'), 200);
		}else{
			$this->response(array('success '=> false, 'msg' => 'Error cancelling your reservation'), 400);
		}		
	}
	
	function users_invoices_get($invoice_id, $method = null){
		
		if($method && $method == 'payments'){
			$this->users_invoices_payments_post($invoice_id);
			return true;
		}
		
		$this->load->model('invoice');
		
		// If user tries to enter a random invoice ID, make sure they have access to it
		if(!$this->invoice->has_access($invoice_id, $this->session->userdata('customer_id'))){
			$this->response(null, 404);
			return false;
		}
		
		$invoice_data = $this->invoice->get_details($invoice_id);
		$this->response($invoice_data, 200);
	}
	
	function users_invoices_post($invoice_id, $method){
		
		if($method == 'payments'){
			$this->users_invoices_payments_post($invoice_id);
			return true;
		}

		$this->response(null, 405);		
	}
	
	function users_invoices_payments_post($invoice_id){
		
		// Make sure user has permission to access invoice
		if(!$this->Invoice->has_access($invoice_id, $this->session->userdata('customer_id'))){
			$this->response(null, 404);
			return false;
		}		
		$data = $this->request->body;
		
		if(empty($data['amount'])){
			$this->response(array('success '=> false, 'msg' => 'Amount is required'), 400);
			return false;			
		}
		
		if(empty($data['credit_card_id'])){
			$this->response(array('success '=> false, 'msg' => 'Existing credit card is required'), 400);
			return false;			
		}		
		
		$this->load->model('Customer_credit_card');
		$this->load->model('Sale');
		
		// Charge credit card for specified amount
		$charge_success = $this->Invoice->charge_invoice($invoice_id, $this->course_id, (int) $data['credit_card_id'], (float) $data['amount']);	
	
		if($charge_success){
			
			// Return updated invoice details
			$invoice_details = $this->Invoice->get_details($invoice_id);
			$this->response($invoice_details, 200);
			
		}else{
			$this->response(array('success' => false, 'msg' => 'Error charging credit card. Use a different card, or try again later.'), 400);
		}
	}
}
