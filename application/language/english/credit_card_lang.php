<?php
$lang['credit_card_credit_card_info']='Credit Card Info';
$lang['credit_card_name_on_card']='Name on Card';
$lang['credit_card_card_type']='Card Type';
$lang['credit_card_last_four']='Last Four';
$lang['credit_card_token_expiration']='Token Expiration';
$lang['credit_card_alert_select_course']='You must select a course before you can add a credit card';
$lang['credit_card_charge_card']='Charge Card';
$lang['credit_card_desc']='Description';
$lang['credit_card_amount']='Amount';
$lang['credit_card_add_item']='Add Item';
$lang['credit_card_subtotal']='Subtotal';
$lang['credit_card_taxes']='Taxes';
$lang['credit_card_total']='Total';
$lang['credit_card_capture']='Card capture successful';
$lang['credit_card_not_capture']='Card capture did not occur. Closing window.';

?>