<?php
$lang['invoices_new']='New Invoice';
$lang['invoices_batch_pdfs']='Batch PDFs';
$lang['invoices_templates']='Templates';
$lang['invoices_settings']='Billing Settings';
$lang['invoices_new_recurring_billing']='New Recurring Billing';
$lang['invoice_details']='Invoice Details';
$lang['invoices_basic_information'] ='Invoice Details';
$lang['invoices_invoice_number']='Invoice Number';
$lang['invoices_invoice_date_billed']='Date Billed';
$lang['invoices_invoice_date_created']='Date Created';
$lang['invoices_invoice_customer_name']='Customer Name';
$lang['invoices_invoice_total']='Total';
$lang['invoices_invoice_paid']='Paid';
$lang['invoices_invoice_amount_due']='Due';
$lang['invoices_no_batch_files'] = 'There are no batched invoice files available';
$lang['invoices_generate_on_message'] = 'If set, all invoices will be generated on this day of the month';
$lang['invoices_generate_on'] = 'Generate on';
$lang['invoices_paid'] = 'Paid';
$lang['invoices_overdue'] = 'Overdue';
?>