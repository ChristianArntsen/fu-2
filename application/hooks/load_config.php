<?php
//Loads configuration from database into global CI config
function load_config()
{
	$CI =& get_instance();
	foreach($CI->Appconfig->get_all()->result() as $app_config)
	{
            foreach($app_config as $key=>$value)
		$CI->config->set_item($key,$value);
	}
	
	$terminal_id = $CI->session->userdata('terminal_id');
	if ($terminal_id)
	{
		$CI->load->model('Terminal');
		$terminal_settings = $CI->Terminal->get_info($terminal_id);
		$terminal_settings = $terminal_settings[0];
		
		if ($terminal_settings['mercury_id'] != '')
		{
			$CI->config->set_item('mercury_id', $terminal_settings['mercury_id']);
			$CI->config->set_item('mercury_password', $terminal_settings['mercury_password']);
		}
		if ($terminal_settings['ets_key'])
			$CI->config->set_item('ets_key', $terminal_settings['ets_key']);
		if ($terminal_settings['e2e_account_id'] != '')
		{
			$CI->config->set_item('e2e_account_id', $terminal_settings['e2e_account_id']);
			$CI->config->set_item('e2e_account_key', $terminal_settings['e2e_account_key']);
		}
		if ($terminal_settings['auto_print_receipts'] != null)
			$CI->config->set_item('print_after_sale', $terminal_settings['auto_print_receipts']);
		if ($terminal_settings['webprnt'] != null)
			$CI->config->set_item('webprnt', $terminal_settings['webprnt']);
		if ($terminal_settings['receipt_ip'] != '')
			$CI->config->set_item('webprnt_ip', $terminal_settings['receipt_ip']);
		if ($terminal_settings['hot_webprnt_ip'])
			$CI->config->set_item('webprnt_hot_ip', $terminal_settings['hot_webprnt_ip']);
		if ($terminal_settings['cold_webprnt_ip'])
			$CI->config->set_item('webprnt_cold_ip', $terminal_settings['cold_webprnt_ip']);
		if ($terminal_settings['use_register_log'] != null)
			$CI->config->set_item('track_cash', $terminal_settings['use_register_log']);
		if ($terminal_settings['cash_register'] != null)
			$CI->config->set_item('cash_register', $terminal_settings['cash_register']);
		if ($terminal_settings['print_tip_line'] != null)
			$CI->config->set_item('print_tip_line', $terminal_settings['print_tip_line']);
		
		if ($terminal_settings['signature_slip_count'] != null)
			$CI->config->set_item('print_two_signature_slips', $terminal_settings['signature_slip_count'] > 1 ? 1 : 0);
		if ($terminal_settings['credit_card_receipt_count'] != null)
		{
			$CI->config->set_item('print_credit_card_receipt', $terminal_settings['credit_card_receipt_count'] > 0 ? 1 : 0);
			$CI->config->set_item('print_two_receipts', $terminal_settings['credit_card_receipt_count'] > 1 ? 1 : 0);
		}
		if ($terminal_settings['non_credit_card_receipt_count'] != null)
		{
			$CI->config->set_item('print_sales_receipt', $terminal_settings['non_credit_card_receipt_count'] > 0 ? 1 : 0);
			$CI->config->set_item('print_two_receipts_other', $terminal_settings['non_credit_card_receipt_count'] > 1 ? 1 : 0);
		}
		if ($terminal_settings['after_sale_load'] != null)
		{
			$CI->config->set_item('after_sale_load', $terminal_settings['after_sale_load']);
		}
	}
	
	if ($CI->config->item('language'))
	{
		$CI->lang->switch_to($CI->config->item('language'));
	}
	
	if ($CI->config->item('timezone'))
	{
		date_default_timezone_set($CI->config->item('timezone'));
	}
	else
	{
		date_default_timezone_set('America/New_York');
	}
	
	if ($CI->config->item('maintenance_mode'))
	{
		echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
			<meta http-equiv="content-type" content="text/html; charset=utf-8" />
			<link rel="icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon"/>
			<title>ForeUP Maintenance</title>
			</head>
			<body>
			
			
			</body>
			</html>
			<style>
			html {
				background:url(../images/maintenance.jpg)no-repeat center center fixed; 
				-webkit-background-size: cover;
				-moz-background-size: cover;
				-o-background-size: cover;
				background-size: cover;
			    font-family: Quicksand, Helvetica, Arial, sans-serif;
			}
			</style>"';
		die();
	}
}
?>
