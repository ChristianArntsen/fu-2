<?php
/*
Gets the html table to manage people.
*/
function get_people_manage_table($people,$controller)
{
	//print_r($people);
    	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
        $table='<table class="tablesorter" id="sortable_table">';
	
        if ($controller_name == 'employees') {
			if($CI->permissions->is_super_admin()) {
	            $headers = array('<div class="header_cell header">'.'<input type="checkbox" id="select_all" />'.'<span class="sortArrow">&nbsp;</span></div>', 
	            '<div class="header_cell header">'.lang('common_last_name').'<span class="sortArrow">&nbsp;</span></div>',
	            '<div class="header_cell header">'.lang('common_first_name').'<span class="sortArrow">&nbsp;</span></div>',
	            '<div class="header_cell header">'.lang('employees_position').'<span class="sortArrow">&nbsp;</span></div>',
	            '<div class="header_cell header">'.lang('common_golf_course').'<span class="sortArrow">&nbsp;</span></div>',
	            '<div class="header_cell header">'.lang('employees_last_login').'<span class="sortArrow">&nbsp;</span></div>',
	            '<div class="header_cell header">'.lang('common_email').'<span class="sortArrow">&nbsp;</span></div>',
	            '<div class="header_cell header">'.lang('common_phone_number').'<span class="sortArrow">&nbsp;</span></div>',
	            '<div class="header_cell header">'.lang('common_active').'<span class="sortArrow">&nbsp;</span></div>',
	            '<div class="header_cell header">'.'&nbsp'.'<span class="sortArrow">&nbsp;</span></div>',
	            '<div class="header_cell header">'.'&nbsp'.'<span class="sortArrow">&nbsp;</span></div>');
			}
			else {
			    $headers = array('<div class="header_cell header">'.'<input type="checkbox" id="select_all" />'.'<span class="sortArrow">&nbsp;</span></div>', 
	            '<div class="header_cell header">'.lang('common_last_name').'<span class="sortArrow">&nbsp;</span></div>',
	            '<div class="header_cell header">'.lang('common_first_name').'<span class="sortArrow">&nbsp;</span></div>',
	            '<div class="header_cell header">'.lang('employees_last_login').'<span class="sortArrow">&nbsp;</span></div>',
	            '<div class="header_cell header">'.lang('common_email').'<span class="sortArrow">&nbsp;</span></div>',
	            '<div class="header_cell header">'.lang('common_phone_number').'<span class="sortArrow">&nbsp;</span></div>',
	            '<div class="header_cell header">'.'&nbsp'.'<span class="sortArrow">&nbsp;</span></div>');
			}
        }
		else if ($controller_name == 'recipients') {
			$headers = array('<div class="header_cell header">'.'<input type="checkbox" id="select_all" />'.'<span class="sortArrow">&nbsp;</span></div>', 
            '<div class="header_cell header">'.lang('common_last_name').'<span class="sortArrow">&nbsp;</span></div>',
            '<div class="header_cell header">'.lang('common_first_name').'<span class="sortArrow">&nbsp;</span></div>',
            '<div class="header_cell header">'.lang('common_email').'<span class="sortArrow">&nbsp;</span></div>',
            '<div class="header_cell header">'.lang('common_auto_mailer').'<span class="sortArrow">&nbsp;</span></div>',
            '<div class="header_cell header">'.'&nbsp'.'<span class="sortArrow">&nbsp;</span></div>');
		}			
        else {
        	$cc_header = ($CI->config->item('customer_credit_nickname') == '') ? lang('customers_account_balance') : $CI->config->item('customer_credit_nickname');
        	$mb_header = ($CI->config->item('member_balance_nickname') == '') ? lang('customers_member_account_balance') : $CI->config->item('member_balance_nickname');
            $headers[] = '<div class="header_cell header">'.'<input type="checkbox" id="select_all" />'.'<span class="sortArrow">&nbsp;</span></div>'; 
            $headers[] = '<div class="header_cell header">'.lang('common_last_name').'<span class="sortArrow">&nbsp;</span></div>';
            $headers[] = '<div class="header_cell header">'.lang('common_first_name').'<span class="sortArrow">&nbsp;</span></div>';
            $headers[] = '<div class="header_cell header">'.lang('common_email').'<span class="sortArrow">&nbsp;</span></div>';
            $headers[] = '<div class="header_cell header">'.lang('common_phone_number').'<span class="sortArrow">&nbsp;</span></div>';
            $headers[] = '<div class="header_cell header">'.$cc_header.'<span class="sortArrow">&nbsp;</span></div>';
            $headers[] = '<div class="header_cell header">'.$mb_header.'<span class="sortArrow">&nbsp;</span></div>';
			$headers[] = '<div class="header_cell header">Billing Account<span class="sortArrow">&nbsp;</span></div>';
			if ($CI->config->item('use_loyalty'))
	            $headers[] = '<div class="header_cell header">'.lang('customers_loyalty_points').'<span class="sortArrow">&nbsp;</span></div>';
            $headers[] = '<div class="header_cell header">'.'&nbsp'.'<span class="sortArrow">&nbsp;</span></div>';
        }
	$table.='<thead><tr>';

	$count = 0;
	foreach($headers as $header)
	{
		$count++;
		
		if ($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";		
		}
	}
	$table.='</tr></thead><tbody>';
	$table.=get_people_manage_table_data_rows($people,$controller);
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the people.
*/
function get_people_manage_table_data_rows($people,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
        $table_data_rows='';
	$num_cols = 7;
        if ($controller_name == 'employees' && $CI->permissions->is_super_admin())
            $num_cols = 9;
		else if ($controller_name == 'auto_mailers')
            $num_cols = 6;
        
	foreach($people->result() as $person)
	{
		$table_data_rows.=get_person_data_row($person,$controller);
	}
	
	if($people->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='$num_cols'><div class='warning_message' style='padding:7px;'>".lang('common_no_persons_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_person_data_row($person,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = ($controller_name == 'employees' && $CI->permissions->is_employee() && $person->person_id != $CI->session->userdata('person_id'))?420:$controller->get_form_width();
	
	$start_of_time =  date('M j, Y 12:00\A\M', 0);
	$today = date('M j, Y 11:59\P\M');	
	$js_sot = date('Y-m-d H:i:s', 0);
	$js_t = date("Y-m-d H:i:s");
	$link = site_url('reports?report_type='.$controller_name.'&start='.$start_of_time.'&end='.$today.'&js_start='.$js_sot.'&js_end='.$js_t.'&'.$controller_name.'_id='.$person->person_id.'&'.$controller_name.'_name='.$person->last_name.', '.$person->first_name.'&sale_type=all');
	if ($controller_name == 'employees') {
		if ($CI->permissions->is_super_admin()) {
			$course_info = $CI->Course->get_info($person->course_id);
            $table_data_row='<tr>';	
            $table_data_row.="<td width='5%'><input type='checkbox' id='person_$person->person_id' value='".$person->person_id."'/></td>";
            $table_data_row.='<td width="12%"><a href="'.$link.'" target="_blank" class="underline">'.$person->last_name.'</a></td>';
            $table_data_row.='<td width="12%"><a href="'.$link.'" target="_blank" class="underline">'.$person->first_name.'</a></td>';
            $table_data_row.='<td width="10%">'.$person->position.'</td>';
            $table_data_row.='<td width="5%">'.$course_info->name.'</td>';
            $table_data_row.='<td width="10%">'.date('M j G:i',strtotime($person->last_login)).'</td>';
            $table_data_row.='<td width="20%">'.mailto($person->email,$person->email, array('class' => 'underline')).'</td>';
            $table_data_row.='<td width="18%">'.$person->phone_number.'</td>';		
            if ($person->activated == 0) {
                $width = $width/2;
                $table_data_row.='<td width="7%">'.anchor($controller_name."/activation_view/$person->person_id/width~$width", '<span class="mini_module_icons status'.$person->activated.'">'.$person->activated.'</span>',array('class'=>'colbox person_edit','title'=>lang($controller_name.'_activate'))).'</td>';
            }
            else
                $table_data_row.='<td width="7%"><span class="mini_module_icons status'.$person->activated.'">'.$person->activated.'</span></td>';
            $table_data_row.='<td width="5%" class="override_login">'.anchor($controller_name."/login_override/$person->person_id", lang('common_login'),array('title'=>lang('common_login'))).'</td>';
            $table_data_row.='<td width="5%" class="rightmost">'.anchor($controller_name."/view/$person->person_id/width~1100", lang('common_edit'),array('class'=>'colbox','title'=>lang($controller_name.'_update'))).'</td>';
            $table_data_row.='</tr>';
        }
		else {
			$table_data_row='<tr>';	
            $table_data_row.="<td width='5%'><input type='checkbox' id='person_$person->person_id' value='".$person->person_id."'/></td>";
            $table_data_row.='<td width="20%"><a href="'.$link.'" target="_blank" class="underline">'.$person->last_name.'</a></td>';
            $table_data_row.='<td width="20%"><a href="'.$link.'" target="_blank" class="underline">'.$person->first_name.'</a></td>';
            $table_data_row.='<td width="10%">'.date('M j G:i',strtotime($person->last_login)).'</td>';
            $table_data_row.='<td width="30%">'.mailto($person->email,$person->email, array('class' => 'underline')).'</td>';
            $table_data_row.='<td width="20%">'.$person->phone_number.'</td>';	
            $table_data_row.='<td width="5%" class="rightmost">'.anchor($controller_name."/view/$person->person_id/width~1100", lang('common_edit'),array('class'=>'colbox','title'=>lang($controller_name.'_update'))).'</td>';
            $table_data_row.='</tr>';
		}
    }
	else if ($controller_name == 'recipients') {
	                $table_data_row='<tr>';	
        $table_data_row.="<td width='5%'><input type='checkbox' id='person_$person->person_id' value='".$person->person_id."'/></td>";
        $table_data_row.='<td width="10%"><a href="'.$link.'" class="underline">'.$person->last_name.'</a></td>';
        $table_data_row.='<td width="10%"><a href="'.$link.'" class="underline">'.$person->first_name.'</a></td>';
        $table_data_row.='<td width="22%">'.mailto($person->email,$person->email, array('class' => 'underline')).'</td>';
        $table_data_row.='<td width="22%">'.$person->auto_mailer.'</td>';
        $table_data_row.='<td width="5%" class="rightmost">'.anchor($controller_name."/view/$person->person_id/width~$width", lang('common_edit'),array('class'=>'colbox','title'=>lang($controller_name.'_update'))).'</td>';
        $table_data_row.='</tr>';
    }
    else {
                    $table_data_row='<tr>';	
        $table_data_row.="<td width='5%'><input type='checkbox' id='person_$person->person_id' value='".$person->person_id."'/></td>";
        $table_data_row.='<td width="16%"><a href="'.$link.'" target="_blank" class="underline">'.$person->last_name.'</a></td>';
        $table_data_row.='<td width="16%"><a href="'.$link.'" target="_blank" class="underline">'.$person->first_name.'</a></td>';
        $table_data_row.='<td width="28%">'.mailto($person->email,$person->email, array('class' => 'underline')).'</td>';
        
        $phone_number = $person->phone_number;
		
		$phone_number = preg_replace('/[^0-9]/', '', $phone_number);
 
		$len = strlen($phone_number);
		if($len == 7)
		$phone_number = preg_replace('/([0-9]{3})([0-9]{4})/', '$1-$2', $num);
		elseif($len == 10)
		$phone_number = preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '($1) $2-$3', $phone_number);
		
        $table_data_row.='<td class="phone_number" width="16%">'.$phone_number.'</td>';	
        // $table_data_row.='<td class="phone_number" width="16%">'.$person->phone_number.'</td>';	
		$color_class = ($person->account_balance < 0)?'red':'';
		$person->account_balance = ($person->account_balance == '0.00' ? '0' : $person->account_balance);
        $table_data_row.='<td width="7%"><span class="account_balance '.$color_class.'">'.anchor($controller_name."/account_adjustment/$person->person_id/width~700", '<span style="float:left;">$'.$person->account_balance.'</span>',array('class'=>'colbox','title'=>'Account Adjustment')).'</span></td>';//inventory details	
        $color_class_2 = ($person->member_account_balance < 0)?'red':'';
		$color_class_3 = ($person->invoice_balance < 0)?'red':'';
		$person->member_account_balance = ($person->member_account_balance == '0.00' ? '0' : $person->member_account_balance);
        $table_data_row.='<td width="7%">'.($person->member?anchor($controller_name."/member_account_adjustment/$person->person_id/width~700", '<span class="account_balance '.$color_class_2.'"><span style="float:left;">$'.$person->member_account_balance.'</span>',array('class'=>'colbox','title'=>lang($controller_name.'_count'))):'<span style="float:right;">-</span>').'</span></td>';//inventory details	
        $table_data_row.='<td width="7%">'.anchor($controller_name."/billing_account_adjustment/$person->person_id/width~700", '<span class="account_balance '.$color_class_3.'"><span style="float:left;">$'.$person->invoice_balance.'</span>',array('class'=>'colbox','title'=>lang($controller_name.'_count')));	
        if ($CI->config->item('use_loyalty'))  
			$table_data_row.='<td width="7%">'.anchor($controller_name."/loyalty_adjustment/$person->person_id/width~700", '<span class="account_balance"><span style="float:left;">'.$person->loyalty_points.'</span>',array('class'=>'colbox','title'=>lang($controller_name.'_count')));//loyalty details	
        $table_data_row.='<td width="5%" class="rightmost">'.anchor($controller_name."/view/$person->person_id/width~$width", lang('common_edit'),array('class'=>'colbox','title'=>lang($controller_name.'_update'))).'</td>';
        $table_data_row.='</tr>';

    }
	
	return $table_data_row;
}



/*
Gets the html table to manage suppliers.
*/
function get_supplier_manage_table($suppliers,$controller)
{
	$CI =& get_instance();
	$table='<table class="tablesorter" id="sortable_table">';	
	$headers = array('<div class="header_cell header">'.'<input type="checkbox" id="select_all" />'.'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('suppliers_company_name').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('common_last_name').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('common_first_name').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('common_email').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('common_phone_number').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.'&nbsp'.'<span class="sortArrow">&nbsp;</span></div>');
	$table.='<thead><tr>';
	$count = 0;
	foreach($headers as $header)
	{
		$count++;
		
		if ($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";		
		}
	}
	
	$table.='</tr></thead><tbody>';
	$table.=get_supplier_manage_table_data_rows($suppliers,$controller);
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the supplier.
*/
function get_supplier_manage_table_data_rows($suppliers,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($suppliers->result() as $supplier)
	{
		$table_data_rows.=get_supplier_data_row($supplier,$controller);
	}
	
	if($suppliers->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='8'><div class='warning_message' style='padding:7px;'>".lang('common_no_persons_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_supplier_data_row($supplier,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<tr>';
	$table_data_row.="<td width='5%'><input type='checkbox' id='person_$supplier->person_id' value='".$supplier->person_id."'/></td>";
	$table_data_row.='<td width="17%">'.$supplier->company_name.'</td>';
	$table_data_row.='<td width="17%">'.$supplier->last_name.'</td>';
	$table_data_row.='<td width="17%">'.$supplier->first_name.'</td>';
	$table_data_row.='<td width="22%">'.mailto($supplier->email,$supplier->email).'</td>';
	$table_data_row.='<td width="17%">'.$supplier->phone_number.'</td>';		
	$table_data_row.='<td width="5%" class="rightmost">'.anchor($controller_name."/view/$supplier->person_id/width~$width", lang('common_edit'),array('class'=>'colbox','title'=>lang($controller_name.'_update'))).'</td>';				
	$table_data_row.='</tr>';
	return $table_data_row;
}
/*
Gets the html table to manage courses.
*/
function get_courses_manage_table($courses,$controller)
{
	$CI =& get_instance();
	$table='<table class="tablesorter course_table" id="sortable_table">';
	
	$headers = array('<div class="header_cell header">'.'<input type="checkbox" id="select_all" />'.'<span class="sortArrow">&nbsp;</span></div>', 
	'<div class="header_cell header">'.lang('courses_name').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.'<span class="mini_module_icons course_teesheets"></span>'.'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.'<span class="mini_module_icons course_sales"></span>'.'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.'<span class="mini_module_icons course_items"></span>'.'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.'<span class="mini_module_icons course_item_kits"></span>'.'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.'<span class="mini_module_icons course_employees"></span>'.'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.'<span class="mini_module_icons course_customers"></span>'.'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.'<span class="mini_module_icons course_reports"></span>'.'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.'<span class="mini_module_icons course_suppliers"></span>'.'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.'<span class="mini_module_icons course_receivings"></span>'.'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.'<span class="mini_module_icons course_config"></span>'.'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.'<span class="mini_module_icons course_giftcards"></span>'.'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.'&nbsp;'.'<span class="sortArrow">&nbsp;</span></div>'
	);
	
	$table.='<thead><tr>';
	$count = 0;
	foreach($headers as $header)
	{
		$count++;
		
		if ($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";		
		}
	}
	$table.='</tr></thead><tbody>';
	$table.=get_courses_manage_table_data_rows($courses,$controller);
	$table.='</tbody></table>';
	return $table;
}
/*
Gets the html data rows for the courses.
*/
function get_courses_manage_table_data_rows($courses,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($courses->result() as $course)
	{
		$table_data_rows.=get_course_data_row($course,$controller);
	}
	
	if($courses->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".lang('courses_no_courses_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_course_data_row($course,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='course_$course->course_id' value='".$course->course_id."'/></td>";
	$table_data_row.='<td width="60%">'.$course->name.'</td>';
	$table_data_row.='<td width="3%" style="text-align:center"><span class="mini_module_icons status'.$course->teesheets.'"></span></td>';
	$table_data_row.='<td width="3%" style="text-align:center"><span class="mini_module_icons status'.$course->sales.'"></span></td>';
	$table_data_row.='<td width="3%" style="text-align:center"><span class="mini_module_icons status'.$course->items.'"></span></td>';
	$table_data_row.='<td width="3%" style="text-align:center"><span class="mini_module_icons status'.$course->item_kits.'"></span></td>';
	$table_data_row.='<td width="3%" style="text-align:center"><span class="mini_module_icons status'.$course->employees.'"></span></td>';	
	$table_data_row.='<td width="3%" style="text-align:center"><span class="mini_module_icons status'.$course->customers.'"></span></td>';
	$table_data_row.='<td width="3%" style="text-align:center"><span class="mini_module_icons status'.$course->reports.'"></span></td>';
	$table_data_row.='<td width="3%" style="text-align:center"><span class="mini_module_icons status'.$course->suppliers.'"></span></td>';
	$table_data_row.='<td width="3%" style="text-align:center"><span class="mini_module_icons status'.$course->receivings.'"></span></td>';
	$table_data_row.='<td width="3%" style="text-align:center"><span class="mini_module_icons status'.$course->config.'"></span></td>';
	$table_data_row.='<td width="3%" style="text-align:center"><span class="mini_module_icons status'.$course->giftcards.'"></span></td>';
	$table_data_row.='<td width="4%" class="rightmost">'.anchor($controller_name."/view/$course->course_id/width~$width", lang('common_edit'),array('class'=>'colbox','title'=>lang($controller_name.'_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}
/*
Gets the html table to manage courses.
*/
function get_billings_manage_table($billings,$controller)
{
	$CI =& get_instance();
	$table='<table class="tablesorter course_table" id="sortable_table">';
	
	$headers = array('<div class="header_cell header">'.'<input type="checkbox" id="select_all" />'.'<span class="sortArrow">&nbsp;</span></div>', 
	'<div class="header_cell header">'.lang('courses_name').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('billings_product').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('billings_free').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('billings_annual').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('billings_monthly').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('billings_teetimes').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('billings_start_date').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.'&nbsp;'.'<span class="sortArrow">&nbsp;</span></div>'
	);
	
	$table.='<thead><tr>';
	$count = 0;
	foreach($headers as $header)
	{
		$count++;
		
		if ($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";		
		}
	}
	$table.='</tr></thead><tbody>';
	$table.=get_billings_manage_table_data_rows($billings,$controller);
	$table.='</tbody></table>';
	return $table;
}
/*
Gets the html data rows for the courses.
*/
function get_billings_manage_table_data_rows($billings,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($billings->result() as $billing)
	{
		$table_data_rows.=get_billing_data_row($billing,$controller);
	}
	
	if($billings->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='7'><div class='warning_message' style='padding:7px;'>".lang('billings_no_billings_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_billing_data_row($billing,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();
	$course_info = $CI->Course->get_info($billing->course_id);
	$products = array(1=>'Software', 2=>'Website', 3=>'Marketing');
	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='billing_$billing->billing_id' value='".$billing->billing_id."'/></td>";
	$table_data_row.='<td width="20%"><a href="index.php/billings/history/'.$course_info->course_id.'" target="_blank">'.$course_info->name.'</a></td>';
	$table_data_row.="<td width='3%' style='text-align:center'>{$products[$billing->product]}</td>";
	$table_data_row.="<td width='3%' style='text-align:center'>".($billing->free>0?'x':'-')."</td>";
	$table_data_row.="<td width='3%' style='text-align:center'>".($billing->annual_amount>0?'$'.$billing->annual_amount:'-')."</td>";
	$table_data_row.="<td width='3%' style='text-align:center'>".($billing->monthly_amount>0?'$'.$billing->monthly_amount:'-')."</td>";
	$table_data_row.="<td width='3%' style='text-align:center'>".($billing->teetimes_daily>0?$billing->teetimes_daily.'/day':'-').' <br/>'.($billing->teetimes_weekly>0?$billing->teetimes_weekly.'/week':'-')."</td>";
	$table_data_row.="<td width='3%' style='text-align:center'>$billing->start_date</td>";
	$table_data_row.='<td width="4%" class="rightmost">'.anchor($controller_name."/view/$billing->billing_id/width~$width", lang('common_edit'),array('class'=>'colbox','title'=>lang($controller_name.'_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}
/*
Gets the html table to manage courses.
*/
function get_customer_billings_manage_table($customer_billings,$controller)
{
	$CI =& get_instance();
	$table='<table class="tablesorter course_table" id="sortable_table">';
	
	$headers = array('<div class="header_cell header">'.'<input type="checkbox" id="select_all" />'.'<span class="sortArrow">&nbsp;</span></div>', 
	'<div class="header_cell header">'.lang('common_title').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('invoices_invoice_customer_name').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('invoices_frequency').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('invoices_last_run').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.'&nbsp;'.'<span class="sortArrow">&nbsp;</span></div>'
	);
	
	$table.='<thead><tr>';
	$count = 0;
	foreach($headers as $header)
	{
		$count++;
		
		if ($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";		
		}
	}
	$table.='</tr></thead><tbody>';
	$table.=get_customer_billings_manage_table_data_rows($customer_billings,$controller);
	$table.='</tbody></table>';
	return $table;
}
/*
Gets the html data rows for the courses.
*/
function get_customer_billings_manage_table_data_rows($customer_billings,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($customer_billings->result() as $customer_billing)
	{
		$table_data_rows.=get_customer_billing_data_row($customer_billing,$controller);
	}
	if($customer_billings->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='7'><div class='warning_message' style='padding:7px;'>".lang('billings_no_billings_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_customer_billing_data_row($customer_billing,$controller)
{
	//echo 'inside get customer billings';
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();
	//$course_info = $CI->Course->get_info($customer_billing->course_id);
	$products = array(1=>'Software', 2=>'Website', 3=>'Marketing');
	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='billing_$customer_billing->billing_id' value='".$customer_billing->billing_id."'/></td>";
	$table_data_row.='<td width="20%"><a href="index.php/customer/billing_history/'.$customer_billing->billing_id.'" target="_blank">'.$customer_billing->title.'</a></td>';
	$table_data_row.="<td width='3%' style='text-align:center'>".$customer_billing->last_name.', '.$customer_billing->first_name."</td>";
	$table_data_row.="<td width='3%' style='text-align:center'>".$customer_billing->frequency."</td>";
	$table_data_row.="<td width='3%' style='text-align:center'>".$customer_billing->last_billing_attempt."</td>";
	$table_data_row.='<td width="4%" class="rightmost">'.anchor($controller_name."/view/$customer_billing->billing_id/width~$width", lang('common_edit'),array('class'=>'colbox','title'=>lang($controller_name.'_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}
/*
Gets the html table to manage courses.
*/
function get_invoices_manage_table($invoices,$controller)
{
	$CI =& get_instance();
	$table='<table class="tablesorter course_table" id="sortable_table">';
	
	$headers = array('<div class="header_cell header">'.'<input type="checkbox" id="select_all" />'.'<span class="sortArrow">&nbsp;</span></div>', 
	'<div class="header_cell header">'.lang('invoices_invoice_number').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('invoices_invoice_customer_name').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('invoices_invoice_date_billed').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('invoices_invoice_total').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('invoices_invoice_paid').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('invoices_invoice_amount_due').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.'&nbsp;'.'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.'&nbsp;'.'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.'&nbsp;'.'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.'&nbsp;'.'<span class="sortArrow">&nbsp;</span></div>'
	);
	
	$table.='<thead><tr>';
	$count = 0;
	foreach($headers as $header)
	{
		$count++;
		
		if ($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";		
		}
	}
	$table.='</tr></thead><tbody>';
	$table.=get_invoices_manage_table_data_rows($invoices,$controller);
	$table.='</tbody></table>';
	return $table;
}
/*
Gets the html data rows for the courses.
*/
function get_invoices_manage_table_data_rows($invoices,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	foreach($invoices->result() as $invoice)
	{
		$table_data_rows.=get_invoice_data_row($invoice,$controller);
	}
	
	if($invoices->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='7'><div class='warning_message' style='padding:7px;'>".lang('billings_no_billings_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_invoice_data_row($invoice,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = 850;
	$course_info = $CI->Course->get_info($invoice->course_id);
	$products = array(1=>'Software', 2=>'Website', 3=>'Marketing');
	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='invoice_$invoice->invoice_id' value='".$invoice->invoice_id."'/></td>";
	$table_data_row.='<td width="4%">'.$invoice->invoice_number.'</td>';
	$table_data_row.="<td width='13%' style='text-align:left'>".$invoice->last_name.", ".$invoice->first_name."</td>";
	$table_data_row.="<td width='8%' style='text-align:center'>".$invoice->month_billed."</td>";
	$table_data_row.="<td width='3%' style='text-align:center'>".($invoice->total>0?'$'.$invoice->total:'-')."</td>";
	$table_data_row.="<td width='3%' style='text-align:center'>".($invoice->paid>0?'$'.$invoice->paid:'-')."</td>";
	$amount_due = $invoice->total-$invoice->paid;
	$am_due = $amount_due > 0;
	$amount_due = ($am_due ? "<span class='red'>".to_currency($amount_due)."</span>" : to_currency($amount_due));
	$pay_text = ($invoice->credit_card_id ? "Auto" : 'Pay'); 
	$table_data_row.="<td width='3%' style='text-align:center'>".$amount_due."</td>";
	$table_data_row.='<td width="4%">'.($am_due ? anchor("invoices/#", $pay_text,array('class'=>'pay_invoice', 'invoice-id'=>$invoice->invoice_number,'title'=>lang($controller_name.'_update'))) : '' ).'</td>';		
	$table_data_row.='<td width="4%">'.($invoice->email != '' ? anchor("invoices/#", 'Email',array('class'=>'email_invoice', 'invoice-id'=>$invoice->invoice_id,'title'=>lang($controller_name.'_update'))) : '' ).'</td>';		
	$table_data_row.='<td width="4%">'.anchor("customers/load_invoice/{$invoice->invoice_id}/0/1", 'PDF',array('target'=>'_blank','title'=>lang($controller_name.'_update'))).'</td>';		
	$table_data_row.='<td width="4%" class="rightmost">'.anchor($controller_name."/view/$invoice->invoice_id/width~$width", ($am_due ? lang('common_edit') : lang('common_view')),array('class'=>'colbox','title'=>($am_due ? lang($controller_name.'_update') : lang('common_view')))).'</td>';		
		
	$table_data_row.='</tr>';
	return $table_data_row;
}
/*
Gets the html table to manage items.
*/
function get_items_manage_table($items,$controller)
{
	$CI =& get_instance();
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array(
	'<div class="header_cell"><input type="checkbox" id="select_all" /></div>', 
	//lang('items_item_number'),
	'<div class="header_cell header">'.lang('items_name').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('items_department').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header"><span class="restaurant_label" style="display:none">Menu Sections</span><span class="inventory_label">Category</span><span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header"><span class="restaurant_label" style="display:none">Menu SubSect</span><span class="inventory_label">SubCat</span><span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('items_cost_price').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('items_unit_price').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('items_taxes').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('items_qty').'<span class="sortArrow">&nbsp;</span></div>',
	//lang('items_inventory'),
	'<div class="header_cell header">'.'&nbsp;'.'</div>'
	);
	
	$table.='<thead id="fixedHeader" class="fixedHeader"><tr>';
	$count = 0;
	foreach($headers as $header)
	{
		$count++;
		
		if ($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";		
		}
	}
	$table.='</tr></thead><tbody class="scrollContent">';
	$table.=get_items_manage_table_data_rows($items,$controller);
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the items.
*/
function get_items_manage_table_data_rows($items,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($items->result() as $item)
	{
		$table_data_rows.=get_item_data_row($item,$controller);
	}
	
	if($items->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".lang('items_no_items_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_item_data_row($item,$controller)
{
	$CI =& get_instance();
	$item_tax_info=$CI->Item_taxes->get_info($item->item_id);
	$tax_percents = '';
	foreach($item_tax_info as $tax_info)
	{
		$tax_percents.=$tax_info['percent']. '%, ';
	}
	$tax_percents=substr($tax_percents, 0, -2);
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='item_$item->item_id' value='".$item->item_id."'/></td>";
	//$table_data_row.='<td width="15%">'.$item->item_number.'</td>';
	$table_data_row.='<td width="29%">'.anchor($controller_name."/count_details/$item->item_id/width~$width",$item->name,array('class'=>'colbox','title'=>lang($controller_name.'_details_count'))).'</td>';
	$table_data_row.='<td width="11%">'.$item->department.'</td>';
	$table_data_row.='<td width="11%">'.$item->category.'</td>';
	$table_data_row.='<td width="11%">'.$item->subcategory.'</td>';
	$table_data_row.='<td width="9%" align="right">'.to_currency($item->cost_price).'</td>';
	$table_data_row.='<td width="9%" align="right">'.to_currency($item->unit_price).'</td>';
	$table_data_row.='<td width="8%">'.$tax_percents.'</td>';
	$quantity = ($item->is_unlimited)?'-':$item->quantity;	
	$table_data_row.='<td width="4%" align="right">'.anchor($controller_name."/inventory/$item->item_id/width~700", $quantity,array('class'=>'colbox','title'=>lang($controller_name.'_count'))).'</td>';
	//$table_data_row.='<td width="8%">'. lang('common_inv').'&nbsp;&nbsp;&nbsp;&nbsp;'. lang('common_det').'</td>';//inventory details	
	$table_data_row.='<td width="4%" class="rightmost">'.anchor($controller_name."/view/$item->item_id/width~1100", lang('common_edit'),array('class'=>'colbox','title'=>lang($controller_name.'_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}
/*
Gets the html table to manage items.
*/
function get_schedules_manage_table($schedules,$controller)
{
	$CI =& get_instance();
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array(
	'<div class="header_cell"><input type="checkbox" id="select_all" /></div>', 
	//lang('items_item_number'),
	'<div class="header_cell header">'.lang('schedules_name').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('schedules_default').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('schedules_type').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('schedules_increment').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('schedules_online_open_time').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('schedules_online_close_time').'<span class="sortArrow">&nbsp;</span></div>',
	//lang('schedules_inventory'),
	'<div class="header_cell header">'.'&nbsp;'.'</div>'
	);
	
	$table.='<thead id="fixedHeader" class="fixedHeader"><tr>';
	$count = 0;
	foreach($headers as $header)
	{
		$count++;
		
		if ($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";		
		}
	}
	$table.='</tr></thead><tbody class="scrollContent">';
	$table.=get_schedules_manage_table_data_rows($schedules,$controller);
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the items.
*/
function get_schedules_manage_table_data_rows($schedules,$controller)
{
	$CI =& get_instance();
	$table_data_rows='';
	//echo $CI->db->last_query().'<br/>';
	//print_r($schedules->result_object());
	foreach($schedules->result_object() as $schedule)
	{
		//print_r($schedule);
		//echo '<br/>Stuff';
		$table_data_rows.=get_schedule_data_row($schedule,$controller);
	}
	
	if($schedules->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".lang('schedules_no_schedules_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_schedule_data_row($schedule,$controller)
{
	$CI =& get_instance();
	//$CI->load->model('track');
	$tracks=$CI->track->get_all($schedule->schedule_id);
	$track_array = array();
	//echo $CI->db->last_query().'<br/>';
	//print_r($tracks->result_object());
	$controller_name=strtolower(get_class($CI));
	//$width = $controller->get_form_width();

	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='schedule_$schedule->schedule_id' value='".$schedule->schedule_id."'/></td>";
	//$table_data_row.='<td width="15%">'.$item->item_number.'</td>';
	$table_data_row.='<td width="30%">'.$schedule->title.'</td>';
	$table_data_row.='<td width="30%">'.($schedule->default?'Y':'').'</td>';
	$table_data_row.='<td width="30%">'.$schedule->type.'</td>';
	$table_data_row.='<td width="11%">'.$schedule->increment.'</td>';
	$table_data_row.='<td width="11%">'.$schedule->online_open_time.'</td>';
	$table_data_row.='<td width="11%">'.$schedule->online_close_time.'</td>';
	$table_data_row.='<td width="4%" class="rightmost">'.anchor("schedules/view/$schedule->schedule_id/width~1100", lang('common_edit'),array('class'=>'colbox','title'=>lang($controller_name.'_update'))).'</td>';		
	$table_data_row.='</tr>';
	
	$table_data_row.='<tr>'; 
	$table_data_row.='<th></th>'; 
	$table_data_row.='<th>'.lang('schedules_track_name').'</th>'; 
	$table_data_row.='<th>'.lang('schedules_reround').'</th>'; 
	$table_data_row.='<th>'.lang('schedules_online_booking').'</th>'; 
	$table_data_row.='<th></th>'; 
	$table_data_row.='<th></th>'; 
	$table_data_row.='<th></th>'; 
	$table_data_row.='<th></th>'; 
	$table_data_row.='</tr>'; 
	foreach($tracks->result_object() as $track) 
	{
		$track_array[$track->track_id] = $track->title;
	}
	foreach($tracks->result_object() as $track)
	{
		$table_data_row.='<tr>'; 
		$table_data_row.='<td></td>'; 
		$table_data_row.='<td>'.$track->title.'</td>'; 
		$table_data_row.='<td>'.($track->reround_track_id?$track_array[$track->reround_track_id]:'').'</td>'; 
		$table_data_row.='<td>'.($track->online_booking?'Y':'').'</td>'; 
		$table_data_row.='<td></td>'; 
		$table_data_row.='<td></td>'; 
		$table_data_row.='<td></td>'; 
		$table_data_row.='<td></td>'; 
		$table_data_row.='</tr>'; 
	}
	
	return $table_data_row;
}
/*
Gets the html table to manage tournaments
*/
function get_tournament_manage_table($tournaments, $controller )
{
	$CI =& get_instance();
	
	$table = '<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<input type="checkbox" id="select_all" />',
	lang('tournaments_tournament_name'),
	lang('tournaments_tournament_total_cost'),
	lang('tournaments_tournament_accumulated_pot'),
	lang('tournaments_tournament_remaining_pot_balance'),
	'&nbsp',//empty header for the edit column
	'&nbsp',//empty header for the award column
	);
	
	$table .= '<thead><tr>';
	$count = 0;
	foreach ($headers as $header) 
	{
		$count++;
		
		if($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";		
		}
	}

	$table.='</tr></thead><tbody>';
	$table .= get_tournament_manage_table_data_rows($tournaments, $controller);
	$table.='</tbody></table>';
	return $table;
	
	
}

/*
Gets the html data rows for the tournament.
*/
function get_tournament_manage_table_data_rows( $tournaments, $controller )
{
	$CI =& get_instance();
	$table_data_rows = '';
	
	foreach($tournaments->result() as $tournament)
	{
		$table_data_rows.=get_tournament_data_row($tournament, $controller);		
	}
	
	if($tournaments->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".lang('tournaments_no_tournaments_to_display')."</div></tr></tr>";
	}
 	
	return $table_data_rows;
}

function get_tournament_data_row($tournament,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();
	$link = site_url('reports/detailed_'.$controller_name.'/'.$tournament->tournament_id.'/0');	
	
	$table_data_row='<tr>';
	
	$tournament_id = "tournament_".$tournament->tournament_id;
	$tournament_value = $tournament->tournament_id;

	$table_data_row.="<td width='3%'><input type='checkbox' id=$tournament_id value=$tournament_value /></td>";	
	$table_data_row.='<td width="15%">'.$tournament->name.'</td>';
	$table_data_row.='<td width="15%">'.to_currency($tournament->total_cost).'</td>';
	$table_data_row.='<td width="15%">'.to_currency($tournament->accumulated_pot).'</td>';
	$table_data_row.='<td width="15%">'.to_currency($tournament->remaining_pot_balance).'</td>';
	$table_data_row.='<td width="5%">'.anchor($controller_name."/award/$tournament->tournament_id/width~500", lang('common_award'),array('class'=>'colbox','title'=>lang($controller_name.'_award'))).'</td>';
	$table_data_row.='<td width="5%" class="rightmost">'.anchor($controller_name."/view/$tournament->tournament_id/width~590", lang('common_edit'),array('class'=>'colbox','title'=>lang($controller_name.'_update'))).'</td>';				
	$table_data_row.='</tr>';
	return $table_data_row;
}

/*
Gets the html table to manage giftcards.
*/
function get_giftcards_manage_table( $giftcards, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<div class="header_cell header">'.'<input type="checkbox" id="select_all" />'.'<span class="sortArrow">&nbsp;</span></div>', 
	'<div class="header_cell header">'.lang('giftcards_giftcard_number').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('giftcards_card_value').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('giftcards_customer_name').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('giftcards_expiration_date').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('giftcards_issue_date').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('giftcards_department').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('giftcards_category').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('giftcards_notes').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.'&nbsp'.'<span class="sortArrow">&nbsp;</span></div>' 
	);
	
	$table.='<thead><tr>';
	$count = 0;
	foreach($headers as $header)
	{
		$count++;
		
		if ($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";		
		}
	}
	$table.='</tr></thead><tbody>';
	$table.=get_giftcards_manage_table_data_rows( $giftcards, $controller );
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the giftcard.
*/
function get_giftcards_manage_table_data_rows( $giftcards, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($giftcards->result() as $giftcard)
	{
		$table_data_rows.=get_giftcard_data_row( $giftcard, $controller );
	}
	
	if($giftcards->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".lang('giftcards_no_giftcards_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_giftcard_data_row($giftcard,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();
    $link = site_url('reports/giftcard_transactions/'.$giftcard->giftcard_id.'/width~1100');
	$cust_info = $CI->Customer->get_info($giftcard->customer_id);
	
	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='giftcard_$giftcard->giftcard_id' value='".$giftcard->giftcard_id."'/></td>";
	$table_data_row.='<td width="15%"><a class="underline colbox" href="'.$link.'" title="Giftcard # '.$giftcard->giftcard_number.'">'.$giftcard->giftcard_number.'</a></td>';
	$table_data_row.='<td width="10%">'.to_currency($giftcard->value).'</td>';
	$table_data_row.='<td width="20%"><a class="underline colbox" href="'.$link.'" title="Giftcard # '.$giftcard->giftcard_number.'">'.$cust_info->first_name. ' '.$cust_info->last_name.'</a></td>';
	$expiration_date = ($giftcard->expiration_date == '0000-00-00')?'No exp.':$giftcard->expiration_date;
	$class_name = ($giftcard->expiration_date != '0000-00-00' && strtotime($giftcard->expiration_date)<time())?'red':''; 
	$table_data_row.='<td width="10%"><span class="'.$class_name.'">'.$expiration_date.'</span></td>';
	$table_data_row.='<td width="10%">'.$giftcard->date_issued.'</span></td>';
	$table_data_row.='<td width="10%">'.$giftcard->department.'</span></td>';
	$table_data_row.='<td width="10%">'.$giftcard->category.'</span></td>';
	$table_data_row.='<td width="22%">'.$giftcard->details.'</span></td>';
	$table_data_row.='<td width="5%" class="rightmost">'.anchor($controller_name."/view/$giftcard->giftcard_id", lang('common_edit'),array('class'=>'colbox','title'=>lang($controller_name.'_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}
/*
Gets the html table to manage giftcards.
*/
function get_punch_card_manage_table( $punch_cards, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<div class="header_cell header">'.'<input type="checkbox" id="select_all" />'.'<span class="sortArrow">&nbsp;</span></div>', 
	'<div class="header_cell header">'.lang('giftcards_giftcard_number').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('giftcards_card_value').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('giftcards_customer_name').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('giftcards_expiration_date').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header"><span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header"><span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('giftcards_notes').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.'&nbsp'.'<span class="sortArrow">&nbsp;</span></div>' 
	);
	
	$table.='<thead><tr>';
	$count = 0;
	foreach($headers as $header)
	{
		$count++;
		
		if ($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";		
		}
	}
	$table.='</tr></thead><tbody>';
	$table.=get_punch_cards_manage_table_data_rows( $punch_cards, $controller );
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the giftcard.
*/
function get_punch_cards_manage_table_data_rows( $punch_cards, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($punch_cards->result() as $punch_card)
	{
		$table_data_rows.=get_punch_card_data_row( $punch_card, $controller );
	}
	
	if($punch_cards->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".lang('giftcards_no_giftcards_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_punch_card_data_row($punch_card,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();
	$link = site_url('reports/detailed_punch_cards/'.$punch_card->punch_card_number.'/width~1100');
	$cust_info = $CI->Customer->get_info($punch_card->customer_id);
	
	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='punch_card_$punch_card->punch_card_id' value='".$punch_card->punch_card_id."'/></td>";
	$table_data_row.='<td width="15%"><a class="underline colbox" href="'.$link.'" title="Punch Card # '.$punch_card->punch_card_number.'">'.$punch_card->punch_card_number.'</a></td>';
	$table_data_row.='<td width="2%"></td>';
	$table_data_row.='<td width="20%"><a class="underline colbox" href="'.$link.'" title="Punch Card # '.$punch_card->punch_card_number.'">'.$cust_info->first_name. ' '.$cust_info->last_name.'</a></td>';
	$expiration_date = ($punch_card->expiration_date == '0000-00-00')?'No exp.':$punch_card->expiration_date;
	$class_name = ($punch_card->expiration_date != '0000-00-00' && strtotime($punch_card->expiration_date)<time())?'red':''; 
	$table_data_row.='<td width="10%"><span class="'.$class_name.'">'.$expiration_date.'</span></td>';
	$table_data_row.='<td width="2%"></span></td>';
	$table_data_row.='<td width="2%"></span></td>';
	$table_data_row.='<td width="22%">'.$punch_card->details.'</span></td>';
	$table_data_row.='<td width="5%" class="rightmost">'.anchor($controller_name."/view_punch_card/$punch_card->punch_card_id", lang('common_view'),array('class'=>'colbox','title'=>lang($controller_name.'_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}

/*
Gets the html table to manage giftcards.
*/
function get_marketing_campaigns_manage_table( $campaigns, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<div class="header_cell header">'.'<input type="checkbox" id="select_all" />'.'<span class="sortArrow">&nbsp;</span></div>', 
	'<div class="header_cell header">'.lang('marketing_campaigns_name').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('marketing_campaigns_type').'<span class="sortArrow">&nbsp;</span></div>',
    '<div class="header_cell header">'.lang('marketing_campaigns_template').'<span class="sortArrow">&nbsp;</span></div>',
    '<div class="header_cell header">'.lang('marketing_campaigns_recepients').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('marketing_campaigns_delivery_date').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('marketing_campaigns_status').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.'&nbsp'.'<span class="sortArrow">&nbsp;</span></div>' 
	);
	
	$table.='<thead><tr>';
	$count = 0;
	foreach($headers as $header)
	{
		$count++;
		
		if ($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";		
		}
	}
	$table.='</tr></thead><tbody>';
	$table.=get_marketing_campaigns_manage_table_data_rows( $campaigns, $controller );
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the giftcard.
*/
function get_marketing_campaigns_manage_table_data_rows( $campaigns, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($campaigns->result() as $campaign)
	{
		$table_data_rows.=get_marketing_campaigns_data_row( $campaign, $controller );
	}
	
	if($campaigns->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='8'><div class='warning_message' style='padding:7px;'>".lang('marketing_campaigns_no_marketing_campaigns_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_marketing_campaigns_data_row($campaign,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();
	$link = site_url('marketing_campaigns/detailed_'.$controller_name.'/'.$campaign->campaign_id.'/0');
//	$camp_info = $CI->Marketing_campaign->get_info($campaign->campaign_id);
	$class = ($campaign->is_sent == 1) ? 'sent' : 'on-queue';
  $tpl   = empty($campaign->template) ? '--' : $campaign->template;
  $tpl   = strtolower($tpl);
  $tpl   = str_replace('_', ' ', $tpl);
  $tpl   = ucwords($tpl);
  $type  = $campaign->type;
  $type  = ($type=='text') ? 'txt msg' : $type;
  $type  = ucwords($type);
  $mrl  = $controller->marketing_recipients_lib;
  $mrl->reset();
  $rec = $campaign->recipients;
  $rec = unserialize($rec);
  if($rec['groups'] !== false ) $mrl->set_groups($rec['groups']);
  if($rec['individuals'] !== false) $mrl->set_individuals($rec['individuals']);
  
  $recpnts = $campaign->recipient_count;//$mrl->get_recipients($campaign->type);
  $count = $campaign->recipient_count;//count($recpnts);

  $table_data_row='<tr>';
	$table_data_row.="<td width='1%'><input type='checkbox' id='campaign_$campaign->campaign_id' value='".$campaign->campaign_id."'/></td>";
	$table_data_row.='<td width="28%">'.$campaign->name.'</td>';
	$table_data_row.='<td width="5%">'.$type.'</td>';
  $table_data_row.='<td width="22%">'.$tpl.'</td>';
  $table_data_row.='<td width="9%">'.$count.'</td>';
	$table_data_row.='<td width="24%">'.date('Y-m-d h:i a', strtotime($campaign->send_date)).'</td>';
	$table_data_row.='<td width="6%" class="'.$campaign->status.'">'.ucwords($campaign->status).'</td>';
	$table_data_row.='<td width="5%" class="rightmost">'.anchor($controller_name."/view/$campaign->campaign_id/width~700", lang('common_edit'),array('class'=>'colbox','title'=>$campaign->name)).'</td>';
	
	$table_data_row.='</tr>';
	return $table_data_row;
}
/*
Gets the html table to manage giftcards.
*/
function get_auto_mailers_manage_table( $auto_mailers, $controller )
{
	$CI =& get_instance();
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<div class="header_cell header">'.'<input type="checkbox" id="select_all" />'.'<span class="sortArrow">&nbsp;</span></div>', 
	'<div class="header_cell header">'.lang('common_name').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('auto_mailers_components').'<span class="sortArrow">&nbsp;</span></div>',
  	'<div class="header_cell header">'.lang('auto_mailers_recipients').'<span class="sortArrow">&nbsp;</span></div>',
  	'<div class="header_cell header">'.'&nbsp'.'<span class="sortArrow">&nbsp;</span></div>', 
	'<div class="header_cell header">'.'&nbsp'.'<span class="sortArrow">&nbsp;</span></div>' 
	);
	
	$table.='<thead><tr>';
	$count = 0;
	foreach($headers as $header)
	{
		$count++;
		
		if ($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";		
		}
	}
	$table.='</tr></thead><tbody>';
	$table.=get_auto_mailers_manage_table_data_rows( $auto_mailers, $controller );
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the giftcard.
*/
function get_auto_mailers_manage_table_data_rows( $auto_mailers, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($auto_mailers->result() as $auto_mailer)
	{
		$table_data_rows.=get_auto_mailers_data_row( $auto_mailer, $controller );
	}
	
	if($auto_mailers->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='8'><div class='warning_message' style='padding:7px;'>".lang('auto_mailers_no_auto_mailers_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_auto_mailers_data_row($auto_mailer,$controller)
{
	//echo 'get_auto_mailers_data_row';
	
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();
	$link = site_url('auto_mailers/detailed_'.$controller_name.'/'.$auto_mailer->campaign_id.'/0');
	//echo 'about to get components';
	$components = $CI->Auto_mailer_campaign->get_all($auto_mailer->auto_mailer_id)->result_array();
	$recipient_count = 0;
	$triggers = array(1=>'Subscribe');
	//echo 'got components';
  $table_data_row='<tr>';
	$table_data_row.="<td width='1%'><input type='checkbox' id='auto_mailer_$auto_mailer->auto_mailer_id' value='".$auto_mailer->auto_mailer_id."'/></td>";
	$table_data_row.='<td width="26%">'.$auto_mailer->name.'</td>';
	$table_data_row.='<td width="46%">';
	foreach($components as $component)
	{
		//print_r($component);
		$trigger = $triggers[$component['trigger']];
		$days_after_trigger = ($component['days_from_trigger'] == 0)?' the day of ':' '.$component['days_from_trigger'].' days after ';
		$table_data_row.="<div>Send '{$component['name']}' $days_after_trigger $trigger</div>";
	}
	$table_data_row.='</td>';
	$table_data_row.='<td width="10%">'.$recipient_count.'</td>';
    $table_data_row.='<td width="13%">'.anchor($controller_name."/view_recipients/$auto_mailer->auto_mailer_id/width~$width", lang('auto_mailers_add_recipients'),array('class'=>'colbox','title'=>$auto_mailer->name, 'style'=>'width:80px')).'</td>';
	$table_data_row.='<td width="5%" class="rightmost">'.anchor($controller_name."/view/$auto_mailer->auto_mailer_id/width~$width", lang('common_edit'),array('class'=>'colbox','title'=>$auto_mailer->name)).'</td>';
	
	$table_data_row.='</tr>';
	return $table_data_row;
}
/*
Gets the html table to manage giftcards.
*/
function get_teesheets_manage_table( $teesheets, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<div class="header_cell header">'.'<input type="checkbox" id="select_all" />'.'<span class="sortArrow">&nbsp;</span></div>', 
	'<div class="header_cell header">'.lang('teesheets_teesheet_title').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('teesheets_default').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('teesheets_holes').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('teesheets_increment').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('teesheets_frontnine').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('teesheets_online_booking').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.'&nbsp'.'<span class="sortArrow">&nbsp;</span></div>' 
	);
	
	$table.='<thead><tr>';
	$count = 0;
	foreach($headers as $header)
	{
		$count++;
		
		if ($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";		
		}
	}
	$table.='</tr></thead><tbody>';
	$table.=get_teesheets_manage_table_data_rows( $teesheets, $controller );
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the giftcard.
*/
function get_teesheets_manage_table_data_rows( $teesheets, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($teesheets->result() as $teesheet)
	{
		$table_data_rows.=get_teesheet_data_row( $teesheet, $controller );
	}
	
	if($teesheets->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".lang('teesheets_no_teesheets_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_teesheet_data_row($teesheet,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = 650;//$controller->get_form_width();
	$default = ($teesheet->default)?'yes':'no';
	$online_booking = ($teesheet->online_booking==1)?'on':'off';
	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='teesheet_$teesheet->teesheet_id' value='".$teesheet->teesheet_id."'/></td>";
	$table_data_row.='<td width="32%">'.$teesheet->title.'</td>';
	$table_data_row.='<td width="5%">'.$default.'</td>';
	$table_data_row.='<td width="10%">'.$teesheet->holes.'</td>';
	$table_data_row.='<td width="15%">'.$teesheet->increment.'</td>';
	$table_data_row.='<td width="25%">'.substr($teesheet->frontnine,0,1).'h '.substr($teesheet->frontnine,1).'m</td>';
	$table_data_row.='<td width="5%">'.$online_booking.'</td>';
	$table_data_row.='<td width="5%" class="rightmost">'.anchor("teesheets/view_teesheet/$teesheet->teesheet_id/width~$width", lang('common_edit'),array('class'=>'colbox','title'=>lang('teesheets_update'))).'</td>';		
	
	$table_data_row.='</tr>';
	return $table_data_row;
}
/*
Gets the html table to manage item kits.
*/
function get_item_kits_manage_table( $item_kits, $controller )
{
	$CI =& get_instance();
	
	$table='<table class="tablesorter" id="sortable_table">';
	
	$headers = array('<div class="header_cell header">'.'<input type="checkbox" id="select_all" />'.'<span class="sortArrow">&nbsp;</span></div>', 
	'<div class="header_cell header">'.lang('items_item_number').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell htd">'.lang('item_kits_name').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('item_kits_description').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('items_unit_price').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('items_tax_percents').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.'&nbsp'.'<span class="sortArrow">&nbsp;</span></div>' 
	);
	
	$table.='<thead><tr>';
	$count = 0;
	foreach($headers as $header)
	{
		$count++;
		
		if ($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";		
		}
	}
	$table.='</tr></thead><tbody>';
	$table.=get_item_kits_manage_table_data_rows( $item_kits, $controller );
	$table.='</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the item kits.
*/
function get_item_kits_manage_table_data_rows( $item_kits, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';
	
	foreach($item_kits->result() as $item_kit)
	{
		$table_data_rows.=get_item_kit_data_row( $item_kit, $controller );
	}
	
	if($item_kits->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='11'><div class='warning_message' style='padding:7px;'>".lang('item_kits_no_item_kits_to_display')."</div></tr></tr>";
	}
	
	return $table_data_rows;
}

function get_item_kit_data_row($item_kit,$controller)
{

	$CI =& get_instance();
	
	$item_kit_tax_info=$CI->Item_kit_taxes->get_info($item_kit->item_kit_id);
	$tax_percents = '';
	foreach($item_kit_tax_info as $tax_info)
	{
		$tax_percents.=$tax_info['percent']. '%, ';
	}
	$tax_percents=substr($tax_percents, 0, -2);

	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();

	$table_data_row='<tr>';
	$table_data_row.="<td width='3%'><input type='checkbox' id='item_kit_$item_kit->item_kit_id' value='".$item_kit->item_kit_id."'/></td>";
	$table_data_row.='<td width="15%">'.$item_kit->item_kit_number.'</td>';
	$table_data_row.='<td width="15%">'.$item_kit->name.'</td>';
	$table_data_row.='<td width="20%">'.character_limiter($item_kit->description, 40).'</td>';
	$table_data_row.='<td width="20%" align="right">'.(!is_null($item_kit->unit_price) ? to_currency($item_kit->unit_price) : '').'</td>';
	$table_data_row.='<td width="20%">'.$tax_percents.'</td>';
	$table_data_row.='<td width="5%" class="rightmost">'.anchor($controller_name."/view/$item_kit->item_kit_id/width~1100", lang('common_edit'),array('class'=>'colbox','title'=>lang($controller_name.'_update'))).'</td>';
	$table_data_row.='</tr>';
	return $table_data_row;
}

/**
 * promotions module
 */
function get_promotions_manage_table( $promotions, $controller )
{
	$CI =& get_instance();

	$table='<table class="tablesorter" id="sortable_table">';

	$headers = array('<div class="header_cell header">'.'<input type="checkbox" id="select_all" />'.'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('promotions_name').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('promotions_number_available').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('promotions_amount_type').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('promotions_amount').'<span class="sortArrow">&nbsp;</span></div>',
	'<div class="header_cell header">'.lang('promotions_rules').'<span class="sortArrow">&nbsp;</span></div>',
//	lang('promotions_expiration_date'),
	'<div class="header_cell header">'.'&nbsp'.'<span class="sortArrow">&nbsp;</span></div>'
	);

	$table.='<thead><tr>';
	$count = 0;
	foreach($headers as $header)
	{
		$count++;

		if ($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";
		}
	}
	$table .= '</tr></thead><tbody>';
	$table .= get_promotions_manage_table_data_rows( $promotions, $controller );
	$table .= '</tbody></table>';
	return $table;
}

/*
Gets the html data rows for the promotions.
*/
function get_promotions_manage_table_data_rows( $promotions, $controller )
{
	$CI =& get_instance();
	$table_data_rows='';

	foreach($promotions->result() as $promotion)
	{
		$table_data_rows.=get_promotions_data_row( $promotion, $controller );
	}

	if($promotions->num_rows()==0)
	{
		$table_data_rows.="<tr><td colspan='6'><div class='warning_message' style='padding:7px;'>".lang('promotions_no_data_to_display')."</div></tr></tr>";
	}

	return $table_data_rows;
}

function get_promotions_data_row($promotion,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));
	$width = $controller->get_form_width();
        
	$table_data_row='<tr>';
	$table_data_row.="<td width='1%'><input type='checkbox' id='campaign_$promotion->id' value='".$promotion->id."'/></td>";
	$table_data_row.='<td width="15%">' . $promotion->name . '</td>';       
        $table_data_row.='<td width="20%">' . $promotion->number_used . '/' . $promotion->number_issued . '</td>';       
        $table_data_row.='<td width="5%">' . $promotion->amount_type . '</td>';
        //log_message('error', 'Goober: ' . $promotion->buy_quantity);
        
        if ($promotion->amount_type === '%' && $promotion->bogo === 'bogo')
        {
            $table_data_row.='<td width="25%">Buy ' . $promotion->buy_quantity . ' Get ' . $promotion->get_quantity . ' ' . $promotion->amount .'% Off</td>';
        }
        else if ($promotion->amount_type !== '%' && $promotion->bogo === 'bogo')
        {
            $table_data_row.='<td width="25%">Buy ' . $promotion->buy_quantity . ' Get ' . $promotion->get_quantity . ' FREE</td>';
        }
        else if ($promotion->amount_type !== '%')
        {
	    $table_data_row.='<td width="25%">' . $promotion->amount_type . $promotion->amount . ' Off</td>';
        }     
        else if ($promotion->amount_type === '%')
        {
            $table_data_row.='<td width="25%">' . $promotion->amount . $promotion->amount_type . ' Off</td>';
        }
        $table_data_row.='<td width="15%">' . $promotion->rules . '</td>';
//  $table_data_row.='<td width="20%">'.date('Y-m-d h:i a', strtotime($promotion->expiration_date)).'</td>';
	$table_data_row.='<td width="5%" class="rightmost">'.anchor($controller_name."/view/$promotion->id/width~$width", lang('common_edit'),array('class'=>'colbox','title'=>lang($controller_name.'_update'))).'</td>';

	$table_data_row.='</tr>';
	return $table_data_row;
}
/**
 * EO promotions module
 */
 
/*
 * Dashboard module
 */
 function get_dashboard_genstat_manage_table( $general_stats, $controller )
 {
	$CI =& get_instance();

	$table='<table class="dashboard">';

  if($controller->permissions->is_super_admin())
  {
    $headers = array(
      '&nbsp;',
      lang('dashboard_genstat_course_count'),
      lang('dashboard_genstat_customer_count'),
      lang('dashboard_genstat_employee_count'),
      lang('dashboard_genstat_teetimes_booked'),
      lang('dashboard_genstat_online_bookings'),
      lang('dashboard_genstat_total_sales'),
      lang('dashboard_genstat_mercury_sales')
    );
  }
  else
  {
    $headers = array(
      '&nbsp;',
      lang('dashboard_genstat_customer_count'),
      lang('dashboard_genstat_employee_count'),
      lang('dashboard_genstat_teetimes_booked'),
      lang('dashboard_genstat_online_bookings'),
      lang('dashboard_genstat_total_sales')
    );
  }
	

	$table.='<thead><tr>';
	$count = 0;
	foreach($headers as $header)
	{
		$count++;

		if ($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";
		}
	}
	$table.='</tr></thead><tbody>';
	$table.=get_dashboard_genstat_manage_table_data_rows( $general_stats, $controller );
	$table.='</tbody></table>';
	return $table;
}

function get_dashboard_genstat_manage_table_data_rows( $general_stats, $controller )
{
  $CI =& get_instance();
	$table_data_rows='';

	foreach($general_stats as $general_stat)
//	foreach($general_stats->result() as $general_stat)
	{
		$table_data_rows.=get_dashboard_genstat_data_row( $general_stat, $controller );
	}
  
  $colspan = $controller->permissions->is_super_admin() ? 8 : 6;

	if(count($general_stats)==0)
	{
		$table_data_rows.="<tr><td colspan='{$colspan}'><div class='warning_message' style='padding:7px;'>".lang('dashboard_no_data_to_display')."</div></tr></tr>";
	}

	return $table_data_rows;
}

function get_dashboard_genstat_data_row($general_stat,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));

  if($controller->permissions->is_super_admin())
  {
    $table_data_row='<tr>';
    $table_data_row.='<td width="15%">'.$general_stat->pivot.'</td>';
    $table_data_row.='<td width="20%">'.$general_stat->course_count.'</td>';
    $table_data_row.='<td width="15%">'.$general_stat->customer_count.'</td>';
    $table_data_row.='<td width="15%">'.$general_stat->employee_count.'</td>';
    $table_data_row.='<td width="15%">'.$general_stat->teetimes_booked.'</td>';
    $table_data_row.='<td width="15%">'.$general_stat->online_bookings.'</td>';
    $table_data_row.='<td width="15%">'.$general_stat->total_sales.'</td>';
    $table_data_row.='<td width="15%">'.$general_stat->mercury_sales.'</td>';
    $table_data_row.='</tr>';
  }
  else
  {
    $table_data_row='<tr>';
    $table_data_row.='<td width="15%">'.$general_stat->pivot.'</td>';
    $table_data_row.='<td width="15%">'.$general_stat->customer_count.'</td>';
    $table_data_row.='<td width="15%">'.$general_stat->employee_count.'</td>';
    $table_data_row.='<td width="15%">'.$general_stat->teetimes_booked.'</td>';
    $table_data_row.='<td width="15%">'.$general_stat->online_bookings.'</td>';
    $table_data_row.='<td width="15%">'.$general_stat->total_sales.'</td>';
    $table_data_row.='</tr>';
  }
	return $table_data_row;
}

 function get_dashboard_courseinfo_manage_table( $course_infos, $controller )
 {
	$CI =& get_instance();

	$table='<table class="dashboard">';

  if($controller->permissions->is_super_admin())
  {
    $headers = array(
      lang('dashboard_courseinfo_course_name'),
      lang('dashboard_courseinfo_last_month_texts'),
      lang('dashboard_courseinfo_this_month_texts'),
      lang('dashboard_courseinfo_last_month_emails'),
      lang('dashboard_courseinfo_this_month_emails'),
      lang('dashboard_courseinfo_using_mercury')
    );
  }
  else
  {
    $headers = array(
      lang('dashboard_courseinfo_course_name'),
      lang('dashboard_courseinfo_last_month_texts'),
      lang('dashboard_courseinfo_this_month_texts'),
      lang('dashboard_courseinfo_last_month_emails'),
      lang('dashboard_courseinfo_this_month_emails')
    );
  }


	$table.='<thead><tr>';
	$count = 0;
	foreach($headers as $header)
	{
		$count++;

		if ($count == 1)
		{
			$table.="<th class='leftmost'>$header</th>";
		}
		elseif ($count == count($headers))
		{
			$table.="<th class='rightmost'>$header</th>";
		}
		else
		{
			$table.="<th>$header</th>";
		}
	}
	$table.='</tr></thead><tbody>';
	$table.=get_dashboard_courseinfo_manage_table_data_rows( $course_infos, $controller );
	$table.='</tbody></table>';
	return $table;
}

function get_dashboard_courseinfo_manage_table_data_rows( $course_infos, $controller )
{
  $CI =& get_instance();
	$table_data_rows='';

	foreach($course_infos as $course_info)
//	foreach($course_infos->result() as $course_info)
	{
		$table_data_rows.=get_dashboard_courseinfo_data_row( $course_info, $controller );
	}

  $colspan = $controller->permissions->is_super_admin() ? 6 : 5;

	if(count($course_infos)==0)
	{
		$table_data_rows.="<tr><td colspan='{$colspan}'><div class='warning_message' style='padding:7px;'>".lang('dashboard_no_data_to_display')."</div></tr></tr>";
	}

	return $table_data_rows;
}

function get_dashboard_courseinfo_data_row($course_info,$controller)
{
	$CI =& get_instance();
	$controller_name=strtolower(get_class($CI));

  if($controller->permissions->is_super_admin())
  {
    $table_data_row='<tr>';
    $table_data_row.='<td width="20%">'.$course_info['course_name'].'</td>';
    $table_data_row.='<td width="15%">'.$course_info['last_texts'].'</td>';
    $table_data_row.='<td width="15%">'.$course_info['current_texts'].'</td>';
    $table_data_row.='<td width="15%">'.$course_info['last_emails'].'</td>';
    $table_data_row.='<td width="15%">'.$course_info['current_emails'].'</td>';
    $table_data_row.='<td width="15%">'.$course_info['using_mercury'].'</td>';
    $table_data_row.='</tr>';
  }
  else
  {
    $table_data_row='<tr>';
    $table_data_row.='<td width="20%">'.$course_info['course_name'].'</td>';
    $table_data_row.='<td width="15%">'.$course_info['last_texts'].'</td>';
    $table_data_row.='<td width="15%">'.$course_info['current_texts'].'</td>';
    $table_data_row.='<td width="15%">'.$course_info['last_emails'].'</td>';
    $table_data_row.='<td width="15%">'.$course_info['current_emails'].'</td>';
    $table_data_row.='</tr>';
  }
	return $table_data_row;
}
/*
 * EO dashboard module
 */
?>