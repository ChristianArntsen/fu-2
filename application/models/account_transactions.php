<?php
class Account_transactions extends CI_Model
{
	function get_all_by_sale_id($sale_id)
	{
		$result = $this->db->query("SELECT * FROM (`foreup_account_transactions`) WHERE sale_id = {$sale_id} ORDER BY `trans_date` desc");
		return $result->result_array();		
	}
	function save($account_type, $person_id = -1, $trans_description='', $add_subtract='', $trans_details='', $sale_id = 0, $invoice_id = 0, $employee_id = 0, $course_id = false, $cron = false, $date = false)
	{
		if(empty($account_type)){
			return false;
		}
		
		if (empty($date))
		{
			$date = date('Y-m-d H:i:s');
		}
		
		if(empty($employee_id)){
			$employee_id = (int) $this->Employee->get_logged_in_employee_info()->person_id;
		}

		if ($add_subtract == ''){
			$add_subtract = $this->input->post('add_subtract');
		}
		if ($trans_description == ''){
			$trans_description = $this->input->post('trans_comment');
		}
		$manual_edit = $this->input->post('manual_edit');
		
		if(empty($course_id)){
			$course_id = $this->session->userdata('course_id');
		}

		// CHECK TO SEE IF CUSTOMER IS PART OF A HOUSEHOLD
		$this->load->model('Household');
		if (!$manual_edit && $this->Household->is_member($person_id))
		{
			$head = $this->Household->get_head($person_id);
			$cur_customer_info = $this->Customer->get_info($head['person_id']);
			$trans_household = $head['person_id'];
		}
		else
		{
			$cur_customer_info = $this->Customer->get_info($person_id);
			$trans_household  = $person_id;
		}
		
		$trans_data = array(
			'course_id' => $course_id,
			'invoice_id' => $invoice_id,
			'account_type' => strtolower($account_type),
			'sale_id' => $sale_id,
			'invoice_id' => $invoice_id,
			'trans_date' => date('Y-m-d H:i:s', strtotime($date)),
			'trans_customer' => $person_id,
			'trans_household' => $trans_household,
			'trans_user' => $employee_id,
			'trans_comment' => $trans_description,
			'trans_description' => $trans_details,
			'trans_amount' => $add_subtract
		);		
		
		// Update account balance
		if($account_type == 'customer' || $account_type == 'member' || $account_type == 'invoice'){

			if($account_type == 'customer'){
				$customer_data = array(
					'account_balance' => $cur_customer_info->account_balance + $add_subtract
				);
				$trans_data['running_balance'] = $customer_data['account_balance'];
			
			}else if($account_type == 'member'){
				$customer_data = array(
					'member_account_balance' => $cur_customer_info->member_account_balance + $add_subtract
				);
				$trans_data['running_balance'] = $customer_data['member_account_balance'];
			
			}else if($account_type == 'invoice'){
				$customer_data = array(
					'invoice_balance' => $cur_customer_info->invoice_balance + $add_subtract
				);
				$trans_data['running_balance'] = $customer_data['invoice_balance'];							
			}

			$this->db->where('person_id', $trans_household);
			$result = $this->db->update('customers', $customer_data);

		}else{
			$result = true;
		}

		if ($result)
		{
			$insert_id = $this->insert($account_type, $trans_data, $cron);
			if($cron) {
				return $insert_id;
			} else {
				return true;
			}
		}
		else
		{
			return false;
		}
	}

	function insert($account_type, $account_transaction_data, $cron = false)
	{
		if(empty($account_type)){
			return false;
		}
		$account_transaction_data['account_type'] = strtolower($account_type);
		$insert_id = $this->db->insert('account_transactions', $account_transaction_data);
		if($cron) {
			$insert_id = $this->db->insert_id();
		}
		return $insert_id;
	}

	function get_account_transaction_data_for_customer($account_type= false, $customer_id = false, $start_date = false, $end_date = false)
	{
		$this->load->model('course');
		$account_type_sql = '';
		if(!empty($account_type)){
			if(is_array($account_type)){
				$types = array();
				foreach($account_type as $type){
					$types[] = $this->db->escape(strtolower($type));
				}
				$account_type_list = implode(",", $types);
				$account_type_sql = " AND account_type IN(".$account_type_list.")";

			}else{
				$account_type_sql = " AND account_type = ".$this->db->escape(strtolower($account_type));
			}
		}

		$month_sql = "";
		$table_join = '';
		
		$customer_sql = '';
		if (!empty($customer_id))
		{
			$customer_sql = " AND (`trans_customer` = '{$customer_id}' OR `trans_household` = '{$customer_id}') ";
		}

		$table_join = ' LEFT JOIN foreup_customers ON foreup_customers.person_id = foreup_account_transactions.trans_customer 
			LEFT JOIN foreup_people ON foreup_people.person_id = foreup_customers.person_id ';
		
		if(empty($start_date)){
			$start_date = date('Y-m-d');
		}
		if(empty($end_date)){
			$end_date = date('Y-m-d');
		}
				
		$start_date_sql = '';
		if(!empty($start_date)){
			$start = date('Y-m-d 00:00:00', strtotime($start_date));
			$start_date_sql = "AND trans_date >= ". $this->db->escape($start);
			// REMOVED UNTIL FIXED
			// if(stripos($start_date,':') !== false){
				// $start = date('Y-m-d H:i:s', strtotime($start_date));
			// }else{
				// $start = date('Y-m-d 00:00:00', strtotime($start_date));
			// }
			// $start_date_sql = "AND trans_date >= ". $this->db->escape($start);
		}

		$end_date_sql = '';
		if(!empty($end_date)){
		    $end = date('Y-m-d 23:59:59', strtotime($end_date));
		    $end_date_sql = "AND trans_date <= ". $this->db->escape($end);
			// REMOVED UNTIL FIXED        
			// if(stripos($end_date,':') !== false){
				// $end = date('Y-m-d H:i:s', strtotime($end_date));
			// }else{
				// $end = date('Y-m-d 00:00:00', strtotime($end_date));
			// }
			// $end_date_sql = "AND trans_date <= ". $this->db->escape($end);
		}
				
		$course_ids = array();
		$this->course->get_linked_course_ids($course_ids, 'shared_customers', $this->session->userdata('course_id'));
		
		$result = $this->db->query("SELECT *, DATE_FORMAT(trans_date, '%c-%e-%y %l:%i %p') AS date
			FROM (`foreup_account_transactions`)
			{$table_join}
			WHERE trans_amount != 0
				AND foreup_customers.`course_id` IN (".implode(',', $course_ids).")
				{$account_type_sql}
				{$customer_sql}
				{$start_date_sql}
				{$end_date_sql}
			ORDER BY `trans_date` desc");

		return $result->result_array();
	}

	function get_transactions($account_type, $customer_id, $filters = array(), $course_id = false){
		$this->load->model('course');
		
		$account_type_sql = '';
		if(!empty($account_type)){
			if(is_array($account_type)){
				$types = array();
				foreach($account_type as $type){
					$types[] = $this->db->escape(strtolower($type));
				}
				$account_type_list = implode(",", $types);
				$account_type_sql = " AND account_type IN(".$account_type_list.")";

			}else{
				$account_type_sql = " AND account_type = ".$this->db->escape(strtolower($account_type));
			}
		}
		
		$customer_id = (int) $customer_id;
		
		if(empty($course_id)){
			$course_id = (int) $this->session->userdata('course_id');
		}
		$course_id = (int) $course_id;
		$course_ids = array();
		$this->course->get_linked_course_ids($course_ids, 'shared_customers', $course_id);		
		$excludeInvoice = '';
		if(!empty($filters['exclude_invoice_id'])){
			$excludeInvoice = "AND t.invoice_id != ". (int) $filters['exclude_invoice_id'];
		}
		
		$hidePositive = '';
		if(!empty($filters['hide_positive'])){
			$hidePositive = "AND t.trans_amount < 0";
		}
		
		$hideBalanceTransfers = '';
		$member_nickname = ($this->config->item('member_balance_nickname') == '' ? lang('customers_member_account_balance'):$this->config->item('member_balance_nickname'));
		$customer_credit_nickname = ($this->config->item('customer_credit_nickname') == '' ? lang('customers_account_balance'):$this->config->item('customer_credit_nickname'));
		if(!empty($filters['hide_balance_transfers'])){
			$hideBalanceTransfers = "AND t.trans_comment != 'Invoiced Balance Transfer' AND t.trans_description NOT LIKE '(1) {$member_nickname}%' AND t.trans_description NOT LIKE '(1) {$customer_credit_nickname}%' AND t.trans_comment NOT LIKE 'Deleted POS%'";
		}
		
		$onlySales = '';
		if(!empty($filters['only_sales'])){
			$onlySales = "AND t.sale_id > 0";
		}					

		$startDate = '';
		if(!empty($filters['date_start'])){
			$start = date('Y-m-d 00:00:00', strtotime($filters['date_start']));
			$startDate = "AND t.trans_date >= ". $this->db->escape($start);
		}

		$endDate = '';
		if(!empty($filters['date_end'])){
			$end = date('Y-m-d H:i:s', strtotime($filters['date_end']));
			$endDate = "AND t.trans_date <= ". $this->db->escape($end);
		}

		$result = $this->db->query("SELECT t.invoice_id, t.course_id, t.trans_customer, t.trans_user,
				DATE_FORMAT(t.trans_date, '%c/%e/%Y') AS date, t.trans_comment, t.trans_description,
				t.trans_amount AS total, t.running_balance
			FROM foreup_account_transactions AS t
			WHERE t.`course_id` IN (".implode(',', $course_ids).")
				{$hidePositive}
				{$hideBalanceTransfers}
				{$account_type_sql}
				AND (t.trans_customer = {$customer_id} OR t.trans_household = {$customer_id})
				{$excludeInvoice}
				{$startDate}
				{$endDate}
				{$onlySales}
			ORDER BY t.trans_date DESC");
			
		$transactions = $result->result_array();
		
		return $transactions;
	}

	function get_total($account_type, $customer_id, $filters = array(), $course_id = false){
		$this->load->model('course');
		
		$account_type_sql = '';
		if(!empty($account_type)){
			if(is_array($account_type)){
				$types = array();
				foreach($account_type as $type){
					$types[] = $this->db->escape(strtolower($type));
				}
				$account_type_list = implode(",", $types);
				$account_type_sql = " AND account_type IN(".$account_type_list.")";

			}else{
				$account_type_sql = " AND account_type = ".$this->db->escape(strtolower($account_type));
			}
		}

		$customer_id = (int) $customer_id;
		
		if(empty($course_id)){
			$course_id = $this->session->userdata('course_id');
		}
		$course_id = (int) $course_id;

		$excludeInvoice = '';
		if(!empty($filters['exclude_invoice_id'])){
			$excludeInvoice = "AND t.invoice_id != ". (int) $filters['exclude_invoice_id'];
		}

		$startDate = '';
		if(!empty($filters['date_start'])){
			$start = date('Y-m-d 00:00:00', strtotime($filters['date_start']));
			$startDate = "AND t.trans_date >= ". $this->db->escape($start);
		}

		$endDate = '';
		if(!empty($filters['date_end'])){
			$end = date('Y-m-d 23:59:59', strtotime($filters['date_end']));
			$endDate = "AND t.trans_date <= ". $this->db->escape($end);
		}
		
		$course_ids = array();
		$this->course->get_linked_course_ids($course_ids, 'shared_customers', $course_id);

		$result = $this->db->query("SELECT SUM(t.trans_amount) AS total
			FROM foreup_account_transactions AS t
			WHERE t.course_id  IN (".implode(',', $course_ids).")
				{$account_type_sql}
				AND (t.trans_customer = {$customer_id} OR t.trans_household = {$customer_id})
				{$excludeInvoice}
				{$startDate}
				{$endDate}
			ORDER BY t.trans_date DESC");

		$row = $result->row_array();
		return (float) $row['total'];
	}

	function get_month_total($account_type, $customer_id = false, $month = false)
	{
		if(empty($account_type)){
			return false;
		}
		$account_type_sql = "AND account_type = ".$this->db->escape($account_type);

		if (!$customer_id || ! $month){
			return 0;
		}

		$result = $this->db->query("
			SELECT SUM(trans_amount) AS total
			FROM foreup_account_transactions
			WHERE {$account_type_sql} (`trans_household` = '$customer_id')
				AND trans_date > '".date('Y-m-01 00:00:00', strtotime($month.' -1 month'))."'
				AND trans_date < '".date('Y-m-01 00:00:00', strtotime($month))."'
				AND  trans_amount < 0")->row_array();

		return $result['total'];
	}
}

?>
