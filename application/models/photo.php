<?php
class Photo extends CI_Model 
{
	/*Determines whether the given photo exists*/
	function exists($photo_id)
	{
		$this->db->from('photos');	
		$this->db->where('photos.photo_id',$photo_id);
		$this->db->limit(1);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}
	
	/*Gets all photos*/
	function get_all($limit=10000, $offset=0)
	{
		$this->db->from('photos');
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();		
	}
	
	function count_all()
	{
		$this->db->from('photos');
		$this->db->where('deleted',0);
		return $this->db->count_all_results();
	}
	
	/*
	Gets information about a photo as an array.
	*/
	function get_info($photo_id)
	{
		$query = $this->db->get_where('photos', array('photo_id' => $photo_id), 1);
		
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//create object with empty properties.
			$fields = $this->db->list_fields('photos');
			$photo_obj = new stdClass;
			
			foreach ($fields as $field)
			{
				$photo_obj->$field='';
			}
			
			return $photo_obj;
		}
	}
	
	/*
	Get photo with specific ids
	*/
	function get_multiple_info($photo_ids)
	{
		$this->db->from('photos');
		$this->db->where_in('photo_id',$photo_ids);
		return $this->db->get();		
	}
	
	/*
	Inserts or updates a photo
	*/
	function save(&$photo_data,$photo_id=false)
	{
		if (!$photo_id or !$this->exists($photo_id))
		{
			if ($this->db->insert('photos',$photo_data))
			{
				$person_data['photo_id']=$this->db->insert_id();
				return true;
			}
			return false;
		}
		
		$this->db->where('photo_id', $photo_id);
		$result = $this->db->update('people',$photo_data);
        return $result;
	}
	
	/*
	Deletes one Photo (doesn't actually do anything)
	*/
	function delete($photo_id)
	{
		return true; 
	}
	
	/*
	Deletes a list of photos (doesn't actually do anything)
	*/
	function delete_list($photos_ids)
	{	
		return true;	
 	}
}
?>
