<?php
require_once("report.php");
class Summary_account_balances extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		$customer_account_balance = ($this->config->item('customer_credit_nickname') ? $this->config->item('customer_credit_nickname'):lang('customers_account_balance'));
		$member_account_balance = ($this->config->item('member_balance_nickname') ? $this->config->item('member_balance_nickname'):lang('customers_member_account_balance'));
		return array(array('data'=>lang('giftcards_customer_name'), 'align'=>'left'), array('data'=>$customer_account_balance, 'align'=> 'left'), array('data'=>$member_account_balance, 'align'=> 'left'), array('data'=>'Invoice Balance', 'align'=> 'left'));
	}
	
	public function getData()
	{
		if (!$this->permissions->is_super_admin())
            $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->select('account_balance as account_balance, member_account_balance as member_balance, invoice_balance, CONCAT(last_name, ", ", first_name) as customer_name', false);
		$this->db->from('customers');
		$this->db->where('customers.deleted', 0);
		$this->db->join('people', 'customers.person_id = people.person_id', 'left');
		$this->db->order_by('last_name, first_name');

		return $this->db->get()->result_array();		
	}
	
	public function getSummaryData()
	{
		if (!$this->permissions->is_super_admin())
            $this->db->where('course_id', $this->session->userdata('course_id'));
        
        $this->db->select('SUM(IF(account_balance > 0, account_balance, 0)) as positive_balance, 
			SUM(IF(account_balance < 0, account_balance, 0)) as negative_balance, 
			SUM(account_balance) AS net, 
			
			SUM(IF(invoice_balance > 0, invoice_balance, 0)) as positive_invoice_balance, 
			SUM(IF(invoice_balance < 0, invoice_balance, 0)) as negative_invoice_balance,
			SUM(invoice_balance) AS invoice_net, 			
			
			SUM(IF(member_account_balance > 0, member_account_balance, 0)) as positive_member_balance, 	
			SUM(IF(member_account_balance < 0, member_account_balance, 0)) as negative_member_balance, 
			SUM(member_account_balance) AS mnet', false);
		$this->db->from('customers');
		$this->db->where('deleted', 0);
	
		$results = $this->db->get()->result_array();		
		return $results[0];
	}
}
?>
