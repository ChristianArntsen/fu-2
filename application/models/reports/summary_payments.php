<?php
require_once("report.php");
class Summary_payments extends Report
{
	public $payment_split = array('revenue'=>0,'non_revenue'=>0);
	
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array(array('data'=>lang('reports_payment_type'), 'align'=> 'left'), array('data'=>lang('reports_total'), 'align'=> 'right'));
	}
	
	public function getData($include_average_sales = false, $day_split = 0)
	{
		$deduct_tips = $this->config->item('deduct_tips');
		$sales_totals = array();

		$this->db->select('sale_id, SUM(total) as total', false);
		$this->db->from('sales_items_temp');
		$this->db->group_by('sale_id');
		if ($this->params['department'] != 'all')
			$this->db->where('department = "'.$this->params['department'].'"');
		if ($this->params['terminal'] != 'all')
			$this->db->where('terminal_id = "'.$this->params['terminal'].'"');
		if ($this->params['employee_id'])
			$this->db->where('employee_id', $this->params['employee_id']);	
		$from_z_out = ($this->params['from_z_out']) ? $this->params['from_z_out'] : 0;
		$data = $this->db->get();

		foreach($data->result_array() as $sale_total_row)
		{
			$sales_totals[$sale_total_row['sale_id']] = $sale_total_row['total'];
		}
		$this->db->select('sales_payments.sale_id, sale_time, sales_payments.payment_type, payment_amount', false);
		$this->db->from('sales_payments');
		$this->db->join('sales', 'sales.sale_id=sales_payments.sale_id and foreup_sales.course_id = "'.$this->session->userdata('course_id').'"');
		$this->db->where('sale_time BETWEEN '. $this->db->escape($this->params['start_date']).' AND '. $this->db->escape($this->params['end_date']));
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('payment_amount > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('payment_amount < 0');
		}
		if ($this->params['terminal'] != 'all')
			$this->db->where('terminal_id = "'.$this->params['terminal'].'"');
		if ($this->params['employee_id'])
			$this->db->where('employee_id', $this->params['employee_id']);	
		
		$this->db->where($this->db->dbprefix('sales').'.deleted', 0);
		$this->db->order_by('sale_time, sale_id, payment_type');
		$sales_payments = $this->db->get()->result_array();	
		
		//added by James in order to calculate the average sale price. 
		$average_sales = array(
			'total'=>0,
			'count'=>0
		);
		foreach($sales_payments as $row)
		{
			if ($row['payment_amount'] > 0) {				
				$average_sales['total'] += $row['payment_amount'];
				$average_sales['count'] += 1;
			}		

        	$payments_by_sale[$row['sale_id']][] = $row;
			$payment_totals_by_sale[$row['sale_id']] += $row['payment_amount'];
		}
		if ($this->params['department'] == 'all')
		{
			$payment_data = array();
			$payment_data_by_day = array();

			$non_cash_tips = 0;
			foreach($payments_by_sale as $sale_id => $payment_rows)
			{
				$total_sale_balance = $sales_totals[$sale_id];
				$cdn = $this->config->item('customer_credit_nickname')!=''?$this->config->item('customer_credit_nickname'):lang('customers_account_balance');
				$mbn = $this->config->item('member_balance_nickname')!=''?$this->config->item('member_balance_nickname'):lang('customers_member_account_balance');
				//echo $cdn.' - '.$mbn;	
				foreach($payment_rows as $row)
				{
					$sub_payment_type = '';
					$payment_amount = $row['payment_amount'];// <= $total_sale_balance ? $row['payment_amount'] : $total_sale_balance;

					if (strpos($row['payment_type'], 'Tip') !== false)
					{
						$dt_payment_type = '';
						if (strpos($row['payment_type'], 'M/C') !== false)
						{
								$sub_payment_type = 'M/C';
								$dt_payment_type = 'Credit Card';
						}
						else if (strpos($row['payment_type'], 'VISA') !== false)
						{
								$sub_payment_type = 'VISA';
								$dt_payment_type = 'Credit Card';
						}
						else if (strpos($row['payment_type'], 'AMEX') !== false)
						{
								$sub_payment_type = 'AMEX';
								$dt_payment_type = 'Credit Card';
						}
						else if (strpos($row['payment_type'], 'DCVR') !== false)
						{
								$sub_payment_type = 'DCVR';
								$dt_payment_type = 'Credit Card';
						}
						else if (strpos($row['payment_type'], 'DINERS') !== false)
						{
								$sub_payment_type = 'DINERS';
								$dt_payment_type = 'Credit Card';
						}
						else if (strpos($row['payment_type'], 'JCB') !== false)
						{
								$sub_payment_type = 'JCB';
								$dt_payment_type = 'Credit Card';
						}
						else if (strpos($row['payment_type'], 'Cash') !== false)
						{
								$sub_payment_type = 'Cash';
								$dt_payment_type = 'Cash';
						}
						else if (strpos($row['payment_type'], 'Check') !== false)
						{
								$sub_payment_type = 'Check';
								$dt_payment_type = 'Check';
						}
						else if (strpos($row['payment_type'], 'Credit Card') !== false)
						{
								$sub_payment_type = 'Credit Card';
								$dt_payment_type = 'Credit Card';
						}
						else if (strpos($row['payment_type'], $cdn) !== false)
						{
								$sub_payment_type = $cdn;
								$dt_payment_type = 'Account Balance Payments';
						}
						else if (strpos($row['payment_type'], $mbn) !== false)
						{
								$sub_payment_type = $mbn;
								$dt_payment_type = 'Account Balance Payments';
						}
						$row['payment_type'] = 'Tips';
						
						if ($sub_payment_type != 'Cash')
							$non_cash_tips += $payment_amount;
						
						if ($deduct_tips && !$from_z_out)
						{
							if ($dt_payment_type == 'Cash')
								continue;
							
							$row['payment_type'] = $dt_payment_type;
							$sub_payment_type .= " Tips";
						}
						else
							$this->payment_split['non_revenue'] += $payment_amount;
					}
					else if (strpos($row['payment_type'], 'M/C') !== false || strpos($row['payment_type'], 'VISA') !== false || strpos($row['payment_type'], 'AMEX') !== false || strpos($row['payment_type'], 'DCVR') !== false || strpos($row['payment_type'], 'DINERS') !== false || strpos($row['payment_type'], 'JCB') !== false)
					{

						if (strpos($row['payment_type'], 'M/C') !== false)
								$sub_payment_type = 'M/C';
						else if (strpos($row['payment_type'], 'VISA') !== false)
								$sub_payment_type = 'VISA';
						else if (strpos($row['payment_type'], 'AMEX') !== false)
								$sub_payment_type = 'AMEX';
						else if (strpos($row['payment_type'], 'DCVR') !== false)
								$sub_payment_type = 'DCVR';
						else if (strpos($row['payment_type'], 'DINERS') !== false)
								$sub_payment_type = 'DINERS';
						else if (strpos($row['payment_type'], 'JCB') !== false)
								$sub_payment_type = 'JCB';
				
						$row['payment_type'] = 'Credit Card';
						$this->payment_split['revenue'] += $payment_amount;
					}
					else if (strpos($row['payment_type'], $cdn) !== false || strpos($row['payment_type'], $mbn) !== false || stripos($row['payment_type'], 'Billing Account Balance') !== false)
					{
						if (strpos($row['payment_type'], $cdn) !== false)
								$sub_payment_type = $cdn;
						else if (strpos($row['payment_type'], $mbn) !== false)
								$sub_payment_type = $mbn;
						else if (stripos($row['payment_type'], 'Billing Account Balance') !== false)
								$sub_payment_type = 'Billing Account Balance';								
						$row['payment_type'] = lang('sales_account_balance_payments');
						$this->payment_split['non_revenue'] += $payment_amount;
					}
					else if (strpos($row['payment_type'], 'Bank Acct') !== false)
					{
						$row['payment_type'] = 'Bank Account';
						$this->payment_split['revenue'] += $payment_amount;
					}
					else if (strpos($row['payment_type'], 'Punch Card') !== false)
					{
						$row['payment_type'] = 'Punch Card';
						$this->payment_split['non_revenue'] += $payment_amount;
					}
					else if (strpos($row['payment_type'], 'Change') !== false)
					{
						$row['payment_type'] = 'Cash';
						$this->payment_split['revenue'] += $payment_amount;
						if ($deduct_tips && !$from_z_out)
							$sub_payment_type = 'Cash';
					}
					else if (strpos($row['payment_type'], 'Cash Refund') !== false)
					{
						$row['payment_type'] = 'Cash Refund';
						$this->payment_split['revenue'] += $payment_amount;
						if ($deduct_tips && !$from_z_out)
							$sub_payment_type = 'Cash';
					}					
					else if (strpos($row['payment_type'], 'Gift Card') !== false || strpos($row['payment_type'], 'Giftcard') !== false)
					{
						$row['payment_type'] = 'Gift Card';
						$this->payment_split['non_revenue'] += $payment_amount;
					}
					else if (strpos($row['payment_type'], 'Invoice Charge') !== false)
					{
						$row['payment_type'] = 'Invoice Charge';
						$this->payment_split['non_revenue'] += $payment_amount;
					}	
					else if (strpos($row['payment_type'], 'tournament') !== false)
					{
						$row['payment_type'] = 'Tournament';
						$this->payment_split['non_revenue'] += $payment_amount;
					}									
					else if (strpos($row['payment_type'], 'Loyalty') !== false)
						$this->payment_split['non_revenue'] += $payment_amount;
					else if (strpos($row['payment_type'], 'Check') !== false)
					{
						$this->payment_split['revenue'] += $payment_amount;
						if ($deduct_tips && !$from_z_out)
							$sub_payment_type = 'Check';
					}
					else if (strpos($row['payment_type'], 'CC Refund') !== false || strpos($row['payment_type'], 'Partial CC Refund') !== false)
						$this->payment_split['revenue'] += $payment_amount;
					else if (strpos($row['payment_type'], 'Cash') !== false)
					{
						$this->payment_split['revenue'] += $payment_amount;
						if ($deduct_tips && !$from_z_out)
							$sub_payment_type = 'Cash';
					}
					else if (strpos($row['payment_type'], 'Credit Card') !== false)
					{
						$this->payment_split['revenue'] += $payment_amount;
						$sub_payment_type = 'Credit Card';
					}

					$sale_day = date('Y-m-d', strtotime($row['sale_time']));
					if (!isset($payment_data_by_day[$sale_day]))
						$payment_data_by_day[$sale_day] = array();
					if (!isset($payment_data[$row['payment_type']]))
						$payment_data[$row['payment_type']] = array('payment_type' => $row['payment_type'], 'payment_amount' => 0 );
					if (!isset($payment_data_by_day[$sale_day][$row['payment_type']]))
						$payment_data_by_day[$sale_day][$row['payment_type']] = array('payment_type' => $row['payment_type'].' ', 'payment_amount' => 0 );
					// CAN'T REMEMBER WHY WE HAD TO PUT THIS IN THE PAYMENTS REPORT... WE'LL FIND OUT
					if (/*$total_sale_balance != 0 || */$row['payment_type'] == 'Tips' || (substr($sub_payment_type, 'Tip') !== false && $row['payment_type'] != 'Cash') || $row['payment_type'] == 'Cash Refund' || $row['payment_type'] == 'Cash')
					{
						$payment_data[$row['payment_type']]['payment_amount'] += $payment_amount;
						$payment_data_by_day[$sale_day][$row['payment_type']]['payment_amount'] += $payment_amount;
					}
					if (!isset($payment_data['Total']))
						$payment_data['Total'] = array('payment_type' => 'Total', 'payment_amount' => 0 );
					if (!isset($payment_data[$sale_day]['Total']))
						$payment_data_by_day[$sale_day]['Total'] = array('payment_type' => 'Total', 'payment_amount' => 0 );
					// CAN'T REMEMBER WHY WE HAD TO PUT THIS IN THE PAYMENTS REPORT... WE'LL FIND OUT
					if ( (/*$total_sale_balance != 0 || */$row['payment_type'] == 'Tips' || $row['payment_type'] == 'Cash Refund' || $row['payment_type'] == 'Cash'))
					{
						$payment_data['Total']['payment_amount'] += $payment_amount;
						$payment_data_by_day[$sale_day]['Total']['payment_amount'] += $payment_amount;
					}
					
					// Add sub payments into group
					if($sub_payment_type != '')
					{
						if (!isset($payment_data[$row['payment_type']]['sub_payments'][$sub_payment_type]))
							$payment_data[$row['payment_type']]['sub_payments'][$sub_payment_type] = array('payment_type' => $sub_payment_type, 'payment_amount' => 0 );
						if (!isset($payment_data_by_day[$sale_day][$row['payment_type']]['sub_payments'][$sub_payment_type]))
							$payment_data_by_day[$sale_day][$row['payment_type']]['sub_payments'][$sub_payment_type] = array('payment_type' => $sub_payment_type, 'payment_amount' => 0 );
						$payment_data[$row['payment_type']]['sub_payments'][$sub_payment_type]['payment_amount'] += $payment_amount;
						$payment_data_by_day[$sale_day][$row['payment_type']]['sub_payments'][$sub_payment_type]['payment_amount'] += $payment_amount;
					
						// SHOWING TIPS DEDUCTING FROM CASH WITH SUB PAYMENT
						if ($deduct_tips && !$from_z_out && strpos($sub_payment_type, 'Tip') !== false)
						{
							if (!isset($payment_data['Cash']['sub_payments'][$sub_payment_type]))
							{
								$payment_data['Cash']['payment_type'] = 'Cash';
								$payment_data['Cash']['sub_payments'][$sub_payment_type] = array('payment_type' => $sub_payment_type, 'payment_amount' => 0 );
							}
							if (!isset($payment_data_by_day[$sale_day]['Cash']['sub_payments'][$sub_payment_type]))
							{
								$payment_data_by_day[$sale_day]['Cash']['payment_type'] = 'Cash';
								$payment_data_by_day[$sale_day]['Cash']['sub_payments'][$sub_payment_type] = array('payment_type' => $sub_payment_type, 'payment_amount' => 0 );
							}
							$payment_data['Cash']['sub_payments'][$sub_payment_type]['payment_amount'] -= $payment_amount;
							$payment_data_by_day[$sale_day]['Cash']['sub_payments'][$sub_payment_type]['payment_amount'] -= $payment_amount;
						}
					}

					$total_sale_balance-=$payment_amount;
				}
			}
				
			if ($include_average_sales) {			
				$payment_data['average_sale'] = number_format($average_sales['total'] / $average_sales['count'], 2);
			}
			
			// ACCOUNTING FOR TIPS DEDUCTING FROM CASH
			if ($deduct_tips && !$from_z_out)
				$payment_data['Cash']['payment_amount'] = $payment_data['Cash']['payment_amount'] - $non_cash_tips;//$payment_data['Tips']['payment_amount'];
				//print_r($payment_data_by_day);

			if ($day_split)
				return $payment_data_by_day;
			else
				return $payment_data;
		}
		else {
			$payment_data = array();

			$non_cash_tips = 0;
			foreach($payments_by_sale as $sale_id => $payment_rows)
			{
				$total_sale_balance = $sales_totals[$sale_id];
				$cdn = $this->config->item('customer_credit_nickname')!=''?$this->config->item('customer_credit_nickname'):lang('customers_account_balance');
				$mbn = $this->config->item('member_balance_nickname')!=''?$this->config->item('member_balance_nickname'):lang('customers_member_account_balance');
			
				foreach($payment_rows as $row)
				{
					$sub_payment_type = '';
					$payment_amount = $row['payment_amount'];// <= $total_sale_balance ? $row['payment_amount'] : $total_sale_balance;

					if (strpos($row['payment_type'], 'Tip') !== false)
					{
						$dt_payment_type = '';
						if (strpos($row['payment_type'], 'M/C') !== false)
						{
								$sub_payment_type = 'M/C';
								$dt_payment_type = 'Credit Card';
						}
						else if (strpos($row['payment_type'], 'VISA') !== false)
						{
								$sub_payment_type = 'VISA';
								$dt_payment_type = 'Credit Card';
						}
						else if (strpos($row['payment_type'], 'AMEX') !== false)
						{
								$sub_payment_type = 'AMEX';
								$dt_payment_type = 'Credit Card';
						}
						else if (strpos($row['payment_type'], 'DCVR') !== false)
						{
								$sub_payment_type = 'DCVR';
								$dt_payment_type = 'Credit Card';
						}
						else if (strpos($row['payment_type'], 'DINERS') !== false)
						{
								$sub_payment_type = 'DINERS';
								$dt_payment_type = 'Credit Card';
						}
						else if (strpos($row['payment_type'], 'JCB') !== false)
						{
								$sub_payment_type = 'JCB';
								$dt_payment_type = 'Credit Card';
						}
						else if (strpos($row['payment_type'], 'Cash') !== false)
						{
								$sub_payment_type = 'Cash';
								$dt_payment_type = 'Cash';
						}
						else if (strpos($row['payment_type'], 'Check') !== false)
						{
								$sub_payment_type = 'Check';
								$dt_payment_type = 'Check';
						}
						else if (strpos($row['payment_type'], 'Credit Card') !== false)
						{
								$sub_payment_type = 'Credit Card';
								$dt_payment_type = 'Credit Card';
						}
						else if (strpos($row['payment_type'], $cdn) !== false)
						{
								$sub_payment_type = $cdn;
								$dt_payment_type = 'Account Balance Payments';
						}
						else if (strpos($row['payment_type'], $mbn) !== false)
						{
								$sub_payment_type = $mbn;
								$dt_payment_type = 'Account Balance Payments';
						}
						
						$row['payment_type'] = 'Tips';
						if ($sub_payment_type != 'Cash')
							$non_cash_tips += $payment_amount;
						
						if ($deduct_tips && !$from_z_out)
						{
							if ($dt_payment_type == 'Cash')
								continue;
							
							$row['payment_type'] = $dt_payment_type;
							$sub_payment_type .= " Tips";
						}
						else
							$this->payment_split['non_revenue'] += $payment_amount;
					}
					else if (strpos($row['payment_type'], 'M/C') !== false || strpos($row['payment_type'], 'VISA') !== false || strpos($row['payment_type'], 'AMEX') !== false || strpos($row['payment_type'], 'DCVR') !== false || strpos($row['payment_type'], 'DINERS') !== false || strpos($row['payment_type'], 'JCB') !== false)
					{
						if (strpos($row['payment_type'], 'M/C') !== false)
							$sub_payment_type = 'M/C';
						else if (strpos($row['payment_type'], 'VISA') !== false)
							$sub_payment_type = 'VISA';
						else if (strpos($row['payment_type'], 'AMEX') !== false)
							$sub_payment_type = 'AMEX';
						else if (strpos($row['payment_type'], 'DCVR') !== false)
							$sub_payment_type = 'DCVR';
						else if (strpos($row['payment_type'], 'DINERS') !== false)
							$sub_payment_type = 'DINERS';
						else if (strpos($row['payment_type'], 'JCB') !== false)
							$sub_payment_type = 'JCB';
						$row['payment_type'] = 'Credit Card';
						$this->payment_split['revenue'] += $payment_amount;
					}
					else if (strpos($row['payment_type'], $cdn) !== false || strpos($row['payment_type'], $mbn) !== false || stripos($row['payment_type'], 'Billing Account Balance') !== false)
					{
						if (strpos($row['payment_type'], $cdn) !== false)
							$sub_payment_type = $cdn;
						else if (strpos($row['payment_type'], $mbn) !== false)
							$sub_payment_type = $mbn;
						else if (stripos($row['payment_type'], 'Billing Account Balance') !== false)
							$sub_payment_type = 'Billing Account Balance';	
						$row['payment_type'] = lang('sales_account_balance_payments');
						$this->payment_split['non_revenue'] += $payment_amount;
					}
					else if (strpos($row['payment_type'], 'Bank Acct') !== false)
					{
						$row['payment_type'] = 'Bank Account';
						$this->payment_split['revenue'] += $payment_amount;
					}
					else if (strpos($row['payment_type'], 'Punch Card') !== false)
					{
						$row['payment_type'] = 'Punch Card';
						$this->payment_split['non_revenue'] += $payment_amount;
					}
					else if (strpos($row['payment_type'], 'Change') !== false)
					{
						$row['payment_type'] = 'Cash';
						$this->payment_split['non_revenue'] += $payment_amount;
						if ($deduct_tips && !$from_z_out)
	                        $sub_payment_type = 'Cash';
					}
					else if (strpos($row['payment_type'], 'Gift Card') !== false || strpos($row['payment_type'], 'Giftcard') !== false)
					{
						$row['payment_type'] = 'Gift Card';
						$this->payment_split['non_revenue'] += $payment_amount;
					}
					else if (strpos($row['payment_type'], 'Loyalty') !== false)
					{
						$this->payment_split['non_revenue'] += $payment_amount;
					}
					else if (strpos($row['payment_type'], 'Invoice Charge') !== false)
					{
						$row['payment_type'] = 'Invoice Charge';
						$this->payment_split['non_revenue'] += $payment_amount;
					}	
					else if (stripos($row['payment_type'], 'tournament') !== false)
					{
						$row['payment_type'] = 'Tournament';
						$this->payment_split['non_revenue'] += $payment_amount;
					}							
					else if (strpos($row['payment_type'], 'Check') !== false)
					{
						$this->payment_split['revenue'] += $payment_amount;
						if ($deduct_tips && !$from_z_out)
	                        $sub_payment_type = 'Check';
					}
					else if (strpos($row['payment_type'], 'CC Refund') !== false || strpos($row['payment_type'], 'Partial CC Refund') !== false)
					{
						$this->payment_split['revenue'] += $payment_amount;
					}	
					else if (strpos($row['payment_type'], 'Cash Refund') !== false)
					{
						$this->payment_split['revenue'] += $payment_amount;
						if ($deduct_tips && !$from_z_out)
	                        $sub_payment_type = 'Cash';
					}						
					else if (strpos($row['payment_type'], 'Cash') !== false)
					{				
						$this->payment_split['revenue'] += $payment_amount;
						if ($deduct_tips && !$from_z_out)
	                        $sub_payment_type = 'Cash';
					}	
					else if (strpos($row['payment_type'], 'Credit Card') !== false)
					{
						$this->payment_split['revenue'] += $payment_amount;
					}	
						
					if (!isset($payment_data[$row['payment_type']]))
						$payment_data[$row['payment_type']] = array('payment_type' => $row['payment_type'], 'payment_amount' => 0 );
					
					// CAN'T REMEMBER WHY WE HAD TO PUT THIS IN THE PAYMENTS REPORT... WE'LL FIND OUT
					//if ($total_sale_balance != 0)
						$payment_data[$row['payment_type']]['payment_amount'] += $payment_amount;
					
					if (!isset($payment_data['Total']))
						$payment_data['Total'] = array('payment_type' => 'Total', 'payment_amount' => 0 );
					// CAN'T REMEMBER WHY WE HAD TO PUT THIS IN THE PAYMENTS REPORT... WE'LL FIND OUT
					//if ($total_sale_balance != 0)
						$payment_data['Total']['payment_amount'] += $payment_amount;

					// Add sub payments into group
					if($sub_payment_type != '')
					{
				 		if (!isset($payment_data[$row['payment_type']]['sub_payments'][$sub_payment_type]))
				 			$payment_data[$row['payment_type']]['sub_payments'][$sub_payment_type] = array('payment_type' => $sub_payment_type, 'payment_amount' => 0 );
				 		$payment_data[$row['payment_type']]['sub_payments'][$sub_payment_type]['payment_amount'] += $payment_amount;
				 		
				 		// SHOWING TIPS DEDUCTING FROM CASH WITH SUB PAYMENT
                        if ($deduct_tips && !$from_z_out && strpos($sub_payment_type, 'Tip') !== false)
                        {
                                if (!isset($payment_data['Cash']['sub_payments'][$sub_payment_type]))
                                {
                                        $payment_data['Cash']['payment_type'] = 'Cash';
                                        $payment_data['Cash']['sub_payments'][$sub_payment_type] = array('payment_type' => $sub_payment_type, 'payment_amount' => 0 );
                                }
                                if (!isset($payment_data_by_day[$sale_day]['Cash']['sub_payments'][$sub_payment_type]))
                                {
                                        $payment_data_by_day[$sale_day]['Cash']['payment_type'] = 'Cash';
                                        $payment_data_by_day[$sale_day]['Cash']['sub_payments'][$sub_payment_type] = array('payment_type' => $sub_payment_type, 'payment_amount' => 0 );
                                }
                                $payment_data['Cash']['sub_payments'][$sub_payment_type]['payment_amount'] -= $payment_amount;
                                $payment_data_by_day[$sale_day]['Cash']['sub_payments'][$sub_payment_type]['payment_amount'] -= $payment_amount;
                        }
				
					}
					
					$total_sale_balance-=$payment_amount;
				}
			}
			
			if ($include_average_sales) {			
				$payment_data['average_sale'] = number_format($average_sales['total'] / $average_sales['count'], 2);
			}
			// ACCOUNTING FOR TIPS DEDUCTING FROM CASH
			if ($deduct_tips && !$from_z_out)
	            $payment_data['Cash']['payment_amount'] = $payment_data['Cash']['payment_amount'] - $non_cash_tips;//$payment_data['Tips']['payment_amount'];

			return $payment_data;
		}
	}
	
	public function getSummaryData()
	{
		$deduct_tips = $this->config->item('deduct_tips');
		$this->db->select('SUM(payment_amount) AS total', false);
		$this->db->from('sales_payments');
		$this->db->join('sales', 'sales.sale_id=sales_payments.sale_id and foreup_sales.course_id = "'.$this->session->userdata('course_id').'"');
		$this->db->where('date(sale_time) BETWEEN "'. $this->params['start_date']. '" and "'. $this->params['end_date'].'"');
		if ($this->params['sale_type'] == 'sales')
		{
			$this->db->where('payment_amount > 0');
		}
		elseif ($this->params['sale_type'] == 'returns')
		{
			$this->db->where('payment_amount < 0');
		}
		if ($this->params['terminal'] != 'all')
			$this->db->where('terminal_id = "'.$this->params['terminal'].'"');
		if ($this->params['employee_id'])
			$this->db->where('employee_id', $this->params['employee_id']);	
		
		$this->db->where($this->db->dbprefix('sales').'.deleted', 0);
		$sales_payments = $this->db->get();
		//echo $this->db->last_query();
		$sales_payments = $sales_payments->row_array();
		$return_values = array();
		if ($this->params['department'] == 'all')
		{
			$return_values['revenue_payments'] = $this->payment_split['revenue'];
			$return_values['non_revenue_payments'] = $this->payment_split['non_revenue'];
		}
		$return_values['total'] = $sales_payments['total'];
		
		return $return_values;		
	}
	
	public function getAllData() {
		$this->db->select('*');
		$this->db->from('sales_items_temp');
		//$this->db->limit(1000);
		$result = $this->db->get();
		foreach ($result->result() as $r)
		{
			echo "<br/>";
		}
		echo '<br/>'.$this->db->last_query();
		echo '<br/>';
		print_r($result);
		echo '<br/>';
		print_r($result->result());
		echo '<br/>';
		print_r($result->result_array());
		return ($result->result_array());
		//return $result->row_array();
	}
}
?>
