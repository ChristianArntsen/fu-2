<?php
require_once("report.php");
class Inventory_low extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array(array('data'=>lang('reports_item_name'), 'align'=>'left'), array('data'=>lang('reports_item_number'), 'align'=>'left'), array('data'=>lang('reports_description'), 'align'=>'left'), array('data' => lang('items_cost_price'), 'align' => 'right'),array('data' => lang('items_unit_price'), 'align' => 'right'), array('data'=>lang('reports_count'), 'align'=>'left'), array('data'=>lang('reports_reorder_level'), 'align'=>'left'));
	}
	
	public function getData()
	{
		if (!$this->permissions->is_super_admin())
                    $this->db->where('course_id', $this->session->userdata('course_id'));
                $this->db->select('name, item_number, cost_price, unit_price, quantity, reorder_level, description');
		$this->db->from('items');
		if ($this->params['value'] != 'all')
		{
			$this->db->where($this->params['filter'], urldecode(urldecode($this->params['value'])));
		}
		$this->db->where('invisible', 0);
		$this->db->where('deleted', 0);
		$this->db->where('quantity <= reorder_level and deleted=0');
		$this->db->order_by('name');
		
		return $this->db->get()->result_array();

	}
	
	public function getSummaryData()
	{
		return array();
	}
}
?>