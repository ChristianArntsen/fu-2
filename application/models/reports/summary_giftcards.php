<?php
require_once("report.php");
class Summary_giftcards extends Report
{
	function __construct()
	{
		parent::__construct();
	}
	
	public function getDataColumns()
	{
		return array(array('data'=>lang('giftcards_giftcard_number'), 'align'=>'left'), array('data'=>lang('giftcards_card_value'), 'align'=> 'left'), array('data'=>lang('giftcards_customer_name'), 'align'=> 'left'), array('data'=>lang('giftcards_issue_date'), 'align'=> 'left'));
	}
	public function getDataTransactionColumns()
	{
		return array('summary' => array( array('data'=>lang('reports_date'), 'align'=>'left'), array('data'=>lang('reports_customer'), 'align'=> 'left'), array('data'=>lang('reports_employee'), 'align'=> 'left'), array('data'=>lang('reports_Amount'), 'align'=> 'left'), array('data'=>lang('reports_comments'), 'align'=> 'left')) );
	}
	
	public function getData()
	{
		$this->db->select('giftcard_number, value, CONCAT(first_name, " ",last_name) as customer_name, date_issued', false);
		$this->db->from('giftcards');
		$this->db->where('deleted', 0);
		$this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->join('people', 'giftcards.customer_id = people.person_id', 'left');
		$this->db->order_by('giftcard_number');

		return $this->db->get()->result_array();		
	}
	
	public function getSummaryData()
	{
		$this->db->select('SUM(value) as outstanding_giftcard_value', false);
		$this->db->from('giftcards');
		$this->db->where('deleted', 0);
		$this->db->where('course_id', $this->session->userdata('course_id'));
        //$this->db->order_by('giftcard_number');

		return $this->db->get()->row_array();		
	}

	public function getTransactions($giftcard_id)
	{
		$this->db->select('trans_date, CONCAT(customer.first_name," ",customer.last_name) as customer_name, CONCAT(employee.first_name," ",employee.last_name) as employee_name, trans_amount, trans_comment', false);
		$this->db->from('giftcard_transactions');
		$this->db->join('people as customer', 'giftcard_transactions.trans_customer = customer.person_id', 'left');
		$this->db->join('people as employee', 'giftcard_transactions.trans_user = employee.person_id');
		$this->db->where('giftcard_transactions.trans_giftcard', $giftcard_id);
		$this->db->order_by('giftcard_transactions.trans_date', 'DESC');
		$this->db->group_by('giftcard_transactions.trans_id');

		$data = array();
		$data['summary'] = $this->db->get()->result_array();
		return $data;
	}
}
?>