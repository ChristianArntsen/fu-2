<?php
class Printer extends CI_Model
{
	// ADD/UPDATE NEW PRINTER
	public function save($printer_data, $printer_id = false)
	{
		if ($printer_id)
		{
			$this->db->where('printer_id', $printer_id);
			$this->db->update('printers', $printer_data);
			return $printer_id;
		}
		else {
			$this->db->insert($printer_data);
			return $this->db->insert_id();
		}
	}
	// DELETE PRINTER
	public function delete($printer_id)
	{
		$this->db->where('printer_id', $printer_id);
		$this->db->delete('printers');
	}
	// GET PRINTERS
	public function get_all()
	{
		$this->db->from('printers');
		$this->db->where('course_id', $this->session->userdata('course_id'));
		return $this->db->get()->result_array();
	}
	
	// DELETE ITEM PRINTERS
	public function delete_item_printers($item_id)
	{
		$this->db->where('item_id', $item_id);
		$this->db->delete('item_printers');
	}
	// SAVE ITEM PRINTERS
	public function save_item_printers($item_printer_data)
	{
		$this->db->insert_batch('item_printers', $item_printer_data);
	}
	// GET ITEM PRINTERS
	public function get_all_item_printers($item_id)
	{
		$this->db->from('item_printers');
		$this->db->where('item_id', $item_id);
		return $this->db->get()->result_array();
	}
	
}