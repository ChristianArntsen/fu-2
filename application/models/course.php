<?php
class Course extends CI_Model
{
	/*
	Determines if a given item_id is an item
	*/
	function exists($item_id)
	{
		$this->db->from('courses');
		$this->db->where("course_id = '$item_id'");
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}

	/*
	Returns all the items
	*/
	function get_all($limit=10000, $offset=0)
	{
        $this->db->from('courses');
		$this->db->order_by("name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	function get_group_info($course_id = '', $selected = '')
	{
		$this->db->select("foreup_course_groups.group_id AS group_id, label, type, course_id, count(course_id) AS member_count, sum(CASE WHEN course_id = '$selected' THEN 1 ELSE 0 END) AS is_member");
		$this->db->from('course_groups');
		if ($course_id == '')
			$this->db->join('course_group_members', "course_groups.group_id = course_group_members.group_id", 'LEFT');
		else
			$this->db->join('course_group_members', "course_groups.group_id = course_group_members.group_id AND foreup_course_group_members.course_id = '$course_id'", 'LEFT');
		$this->db->group_by('group_id');

		return $this->db->get()->result_array();
	}
	function get_managed_group_info($course_id = '', $selected = '')
	{
		$this->db->select("foreup_course_groups.group_id AS group_id, label, type, course_id, count(course_id) AS member_count, sum(CASE WHEN course_id = '$selected' THEN 1 ELSE 0 END) AS is_management");
		$this->db->from('course_groups');
		if ($course_id == '')
			$this->db->join('course_group_management', "course_groups.group_id = course_group_management.group_id", 'LEFT');
		else
			$this->db->join('course_group_management', "course_groups.group_id = course_group_management.group_id AND course_group_management.course_id = '$course_id'", 'LEFT');
		$this->db->group_by('group_id');

		return $this->db->get()->result_array();
	}
	function is_linked(&$group_ids, $search_type = false, $course_id = false) {
		$course_id = $course_id ? $course_id : $this->session->userdata('course_id');
    	//Gathering a list of 'linked' course_groups that this course belongs to and returning true if any are found
    	$this->db->from('course_groups');
    	$this->db->select('course_groups.group_id');
    	$this->db->join('course_group_members', 'course_group_members.group_id = course_groups.group_id');
    	$this->db->where ('type', 'linked');
		$this->db->where('course_group_members.course_id', $course_id);
		if ($search_type)
			$this->db->where("$search_type", 1);
		$this->db->group_by('group_id');
    	$query = $this->db->get();
		//echo $this->db->last_query();
		$ids = $query->result_array();
		foreach ($ids as $id)
			$group_ids[] = $id['group_id'];
		return ($query->num_rows() > 0);
    }
	// Checks to make sure course is managing another course
	function is_managing_course($course_id) {
		if($course_id == $this->session->userdata('course_id')){
			return true;
		}

    	$course_ids = array();
    	if ($this->get_managed_course_ids($course_ids))
		{
			return in_array($course_id, $course_ids);
		}

		return false;
    }
	function get_last_emailed_date($course_id)
	{
		$this->db->select('last_invoices_sent');
		$this->db->from('courses');
		$this->db->where('course_id', $course_id);
		$this->db->limit(1);
		$result = $this->db->get()->row_array();
		return $result['last_invoices_sent'];
	}
	function date_stamp_sent_invoices($course_id)
	{
		$this->db->where('course_id', $course_id);
		$this->db->limit(1);
		$this->db->update('courses', array('last_invoices_sent' => date('Y-m-d')));
	}
	
	function is_managing(&$group_ids) {
    	//Gathering a list of 'linked' course_groups that this course belongs to and returning true if any are found
    	$this->db->from('course_groups');
    	$this->db->select('course_groups.group_id');
    	$this->db->join('course_group_management', 'course_group_management.group_id = course_groups.group_id');
		$this->db->where('course_group_management.course_id', $this->session->userdata('course_id'));
		$this->db->group_by('group_id');
    	$query = $this->db->get();
		//echo $this->db->last_query();
		$ids = $query->result_array();
		foreach ($ids as $id)
			$group_ids[] = $id['group_id'];
		return ($query->num_rows() > 0);
    }
    function get_linked_course_ids(&$course_ids, $search_type = false, $course_id = false){
    	//echo 'getting_linked_course_ids';
    	//Gathering a list of 'linked' course_groups that this course belongs to and returning true if any are found
    	$group_ids = array();
    	if ($this->is_linked($group_ids, $search_type, $course_id))
		{
			$this->db->select('course_id');
	    	$this->db->from('course_group_members');
			$this->db->where_in('group_id', array_values($group_ids));
	    	$this->db->group_by('course_id');
	    	$query = $this->db->get();
			//echo $this->db->last_query();
			$ids = $query->result_array();
			foreach ($ids as $id)
				$course_ids[] = $id['course_id'];
			return ($query->num_rows() > 0);
		}
		$course_ids[] = $this->session->userdata('course_id')? $this->session->userdata('course_id') : $course_id;
    	return true;
    }
    function get_managed_course_ids(&$course_ids){
    	//Gathering a list of 'managed' course_groups that this course belongs to and returning true if any are found
    	$group_ids = array();
    	if ($this->is_managing($group_ids))
		{
			$this->db->select('courses.course_id, courses.name');
	    	$this->db->from('course_group_members');
	    	$this->db->join('courses', 'courses.course_id = course_group_members.course_id', 'inner');
			$this->db->where_in('course_group_members.group_id', array_values($group_ids));
	    	$this->db->group_by('course_group_members.course_id');
	    	$query = $this->db->get();
			//echo $this->db->last_query();
			$ids = $query->result_array();
			foreach ($ids as $id){
				$course_ids[$id['name']] = $id['course_id'];
			}
			return ($query->num_rows() > 0);
		}
    	return true;
    }

    function get_new_dashboard_drop_down_filter()
	{
		$courses = array();
		$this->db->select('c.name AS course, c.course_id');
    	$this->db->from('courses as c');
    	$this->db->join("billing as b", "c.course_id = b.course_id");
    	$this->db->where("b.teetimes_daily > 0" );
    	$this->db->where("b.teetimes_weekly > 0");
		$by_courses = $this->db->get();
		foreach($by_courses->result() as $row)
		{
			$courses[]=array('label' => $row->course, 'type'=>'category', 'value' => $row->course_id);
		}

		return $courses;
	}

	function save_group_memberships($groups_data, $course_id)
	{
		$this->db->delete('course_group_members', array('course_id'=>$course_id));
		foreach($groups_data as $group_id)
			$this->db->insert('course_group_members', array('course_id'=>$course_id, 'group_id'=>$group_id));
	}
	function save_managed_groups($groups_data, $course_id)
	{
		$this->db->delete('course_group_management', array('course_id'=>$course_id));
		foreach($groups_data as $group_id)
			$this->db->insert('course_group_management', array('course_id'=>$course_id, 'group_id'=>$group_id));
	}
	function add_group($group_label, $group_type)
	{
		$this->db->insert('course_groups', array('label'=>$group_label, 'type'=>$group_type));
		return $this->db->insert_id();
	}
	function delete_group($group_id)
	{
		$this->db->delete('course_groups', array('group_id'=>$group_id));
		$this->db->delete('course_group_members', array('group_id'=>$group_id));
	}
	function count_all()
	{
		$this->db->from('courses');
		$this->db->where('active', 1);
		return $this->db->count_all_results();
	}

	/*
	Gets information about a particular item
	*/
	function get_info($item_id)
	{
		$this->db->from('courses');
		$this->db->where("course_id = '$item_id'");
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('courses');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}
	/*
	Gets information about a particular item
	*/
	function get_info_from_teesheet_id($teesheet_id)
	{
		$this->db->from('courses');
		$this->db->join('teesheet', 'courses.course_id=teesheet.course_id');
		$this->db->where("teesheet_id", $teesheet_id);
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('courses');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}
	function get_info_from_track_id($track_id)
	{
		$this->db->from('courses');
		$this->db->join('schedules', 'courses.course_id=schedules.course_id');
		$this->db->join('tracks', 'schedules.schedule_id=tracks.schedule_id');
		$this->db->where("track_id", $track_id);
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $item_id is NOT an item
			$item_obj=new stdClass();

			//Get all the fields from items table
			$fields = $this->db->list_fields('courses');

			foreach ($fields as $field)
			{
				$item_obj->$field='';
			}

			return $item_obj;
		}
	}

	/*
	Get an item id given an item number
	*/
	function get_item_id($item_number)
	{
		$this->db->from('courses');
		$this->db->where("course_id = '$item_number'");
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row()->course_id;
		}

		return false;
	}

	/*
	Gets information about multiple items
	*/
	function get_multiple_info($item_ids)
	{
		$this->db->from('courses');
                $this->db->where_in('course_id',$item_ids);
		$this->db->order_by("course_id", "asc");
		return $this->db->get();
	}
	
	function init_seasonal_pricing($course_id){
		
		$this->load->model('Season');
		$this->load->model('Price_class');
		$this->load->model('Item');
		
		$this->Price_class->create_default($course_id);
		
		// Create special tee time/cart items
		$this->Item->create_teetimes($course_id);
	
		// Get a list of existing teesheets the course has
		$this->db->select('teesheet_id');
		$this->db->from('teesheet');
		$this->db->where('deleted', 0);
		$this->db->where('course_id', $course_id);
		$teesheets = $this->db->get()->result_array();
		
		// Create a default season for each teesheet
		foreach($teesheets as $teesheet){
			$season_id = $this->Season->create_default($teesheet['teesheet_id'], $course_id);
		}
	}
	
	/*
	Inserts or updates an item
	*/
	function save(&$item_data,$item_id=false, &$groups_data = array(), &$manage_groups_data = array())
	{
		if (!$item_id or !$this->exists($item_id))
		{
			if($this->db->insert('courses',$item_data))
			{
				$item_data['course_id']=$this->db->insert_id();
				$this->save_group_memberships($groups_data, $item_data['course_id']);
				$this->save_managed_groups($manage_groups_data, $item_data['course_id']);
				
				if($item_data['seasonal_pricing'] == '1'){
					$this->init_seasonal_pricing($item_id);
				}
				return true;
			}
			return false;
		}
		
		// If seasonal pricing was turned on, make sure default price 
		// classes/seasons are set up
		if($item_data['seasonal_pricing'] == '1'){
			$this->init_seasonal_pricing($item_id);
		}

    	$this->save_group_memberships($groups_data, $item_id);
    	$this->save_managed_groups($manage_groups_data, $item_id);
		$this->db->where('course_id', $item_id);
		$this->db->limit(1);
        return $this->db->update('courses',$item_data);
	}

	/*
	Updates multiple items at once
	*/
	function update_multiple($item_data,$item_ids)
	{
		$this->db->where_in('course_id',$item_ids);
		return $this->db->update('courses',$item_data);
	}

	/*
	Deletes one item
	*/
	function delete($item_id)
	{
		$this->db->where("course_id = '$item_id'");
		$this->db->limit(1);
		return $this->db->update('courses', array('deleted' => 1));
	}

	/*
	Deletes a list of items
	*/
	function delete_list($item_ids)
	{
		$this->db->where_in('course_id',$item_ids);
		return $this->db->update('courses', array('deleted' => 1));
 	}

 	/*
	Get search suggestions to find items
	*/
	function get_search_suggestions($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('courses');
		$this->db->like('name', $search);
		$this->db->order_by("name", "asc");
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=array('label' => $row->name, 'value' => $row->course_id);
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	function get_course_search_suggestions($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('courses');
		$this->db->like('name', $search);
		$this->db->order_by("name", "asc");
                $this->db->limit($limit);
		$by_name = $this->db->get();
		//echo json_encode(array('working'=>$by_name->num_rows()));
                foreach($by_name->result() as $row)
		{
			$suggestions[]=array('value' => $row->course_id, 'label' => $row->name.' - '.$row->city.', '.$row->state);
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}
        
	function get_teesheet_search_suggestions($search,$limit=25)
	{
		$suggestions = array();

		$this->db->from('teesheet');
		$this->db->join('courses', 'courses.course_id = teesheet.course_id');
		$this->db->like('courses.name', $search);
		$this->db->where('teesheet.deleted', 0);
		$this->db->order_by("courses.name", "asc");
                $this->db->limit($limit);
		$by_name = $this->db->get();
		//$suggestions[] = array('value'=>'id', 'label'=> $this->db->last_query());
		//echo json_encode(array('working'=>$by_name->num_rows()));
        foreach($by_name->result() as $row)
		{
			$suggestions[]=array('value' => $row->teesheet_id, 'label' => $row->name.' - '.$row->title);
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	/*
	Preform a search on items
	*/
	function search($search, $limit=20, $offset = 0)
	{
		$this->db->from('courses');
		$this->db->where("(name LIKE '%".$this->db->escape_like_str($search)."%' or
		city LIKE '%".$this->db->escape_like_str($search)."%' or
		state LIKE '%".$this->db->escape_like_str($search)."%')");
		$this->db->order_by("name", "asc");
		// Just return a count of all search results
        if ($limit == 0)
            return $this->db->get()->num_rows();
        // Return results
        $this->db->offset($offset);
		$this->db->limit($limit);
		return $this->db->get();
	}

  /**
   * get all course info
   */
  public function get_all_course_info($limit=10000, $offset=0)
  {

    $this->db->from('courses');
//    if (!$this->permissions->is_super_admin())
//           $this->db->where('course_id',$this->session->userdata('course_id'));

		$this->db->order_by("name", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);

		$res = $this->db->get();

    $data = array();

    if($res->num_rows() > 0)
    {
      foreach($res->result() as $course)
      {
        $d = array(
          'course_name' => '',
          'last_texts' => 0,
          'current_texts' => 0,
          'last_emails' => 0,
          'current_emails' => 0,
          'using_mercury'     => 'NO'
        );

        if(!empty($course->mercury_id)) $d['using_mercury'] = 'YES';

        $d['course_name'] = $course->name;

        // get all emails and text for last month
        $this->db->select('type, count(*) as count');
        $this->db->from('marketing_campaigns');
        $this->db->where('is_sent', 1);
        $this->db->where('course_id', $course->course_id);
        $this->db->where("month(send_date) = (month(now()) - 1)");
        $this->db->group_by('type');

        $last_res = $this->db->get();

        if($last_res->num_rows() > 0)
        {
          foreach($last_res->result() as $lr)
          {
            if($lr->type == 'email')
            {
              $d['last_emails'] = $lr->count;
            }
            else
            {
              $d['last_texts'] = $lr->count;
            }
          }
        }
        // get all emails and text for this month
        $this->db->select('type, count(*) as count');
        $this->db->from('marketing_campaigns');
        $this->db->where('is_sent', 1);
        $this->db->where('course_id', $course->course_id);
        $this->db->where("month(send_date) = month(now())");
        $this->db->group_by('type');

        $this_res = $this->db->get();

        if($this_res->num_rows() > 0)
        {
          foreach($this_res->result() as $tr)
          {
            if($tr->type == 'email')
            {
              $d['current_emails'] = $tr->count;
            }
            else
            {
              $d['current_texts'] = $tr->count;
            }
          }
        }

        $data[] = $d;
      }
    }

    return $data;
  }

}
?>
