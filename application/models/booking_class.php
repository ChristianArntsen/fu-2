<?php
class Booking_class extends CI_Model
{
	/*
	 * Gets avaialble booking classes by teesheet id
	 */
	function get_all($teesheet_id)
	{
		$this->db->from('booking_classes');
		$this->db->where("(teesheet_id = {$teesheet_id})");
		$this->db->where('deleted', 0);
        $result = $this->db->get()->result_array();

		return $result;
	}
	
	/*
	 * Gets info on a single booking class
	 */
	function get_info($booking_class_id, $teesheet_id)
	{
		$this->db->from('booking_classes');
		$this->db->where('booking_class_id',$booking_class_id);
		$this->db->where('teesheet_id', $teesheet_id);
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get all the fields from booking_classes table
			$fields = $this->db->list_fields('booking_classes');

			//append those fields to base parent object, we we have a complete empty object
			foreach ($fields as $field)
			{
				$booking_class_obj->$field='';
			}

			return $booking_class_obj;
		}
	}
	function load_info($booking_class_id, $teesheet_id)
	{
		$booking_info = $this->get_info($booking_class_id, $teesheet_id);
		if ($booking_info->booking_class_id != '')
		{
			$this->session->set_userdata('booking_class_id', $booking_info->booking_class_id);
			$this->session->set_userdata('booking_class_info', $booking_info);
			
			return true;
		}
		else {
			return false;
		}
	}
	/*
	Save booking class
	*/
	function save($data, $booking_class_id = false)
	{
		if ($booking_class_id)
		{
			// UPDATE BOOKING CLASS
			$this->db->where('booking_class_id', $booking_class_id);
			$this->db->update("booking_classes", $data);
		}
		else 
		{
			// INSERT BOOKING CLASS
			$this->db->insert("booking_classes", $data);
		}
	}
	function validate_user($booking_class_id, $type, $person_id)
	{
		$this->db->from('booking_class_validators');
		$this->db->join('customers', 'booking_class_validators.value = customers.price_class');
		$this->db->where('person_id', $person_id);
		$this->db->where('booking_class_id', $booking_class_id);
		$this->db->where('type', $type);
		$this->db->limit(1);
		
		$result = $this->db->get();
		return $result->num_rows() == 1;
	}
	function save_validators($booking_class_id, $validators)
	{
		$this->delete_validators($booking_class_id);
		$this->db->insert_batch($validators);
	}
	function delete_validators($booking_class_id)
	{
		$this->db->where('booking_class_id', $booking_class_id);
		$this->db->delete('booking_class_validators');
	}
	function delete($teesheet_id, $booking_class_id)
	{
		$this->db->where('teesheet_id', $teesheet_id);
		$this->db->where('booking_class_id', $booking_class_id);
		return $this->db->update('booking_classes', array('deleted'=>1));
	}
	
}
?>
