<?php
class Pricing extends CI_Model 
{
    private $prices_cache;
	private $default_price_cache;
    
    function __construct(){
		$this->load->model('Price_class');
		$this->prices_cache = array();
		$this->default_price_cache = '';
	}
    
    /*
	Returns all the timeframes for a given price category
	*/
	function get_timeframes($season_id, $class_id, $limit = 10000, $offset = 0)
	{
		$course_id = '';
        $this->db->from('seasonal_timeframes')
			->where('class_id', $class_id)
			->where('season_id', $season_id)
        	->order_by("default", "ASC")
        	->order_by("start_time", "ASC")
        	->order_by("timeframe_name", "DESC")   	
			->limit($limit)
			->offset($offset);
		
		$results = $this->db->get()->result();
		return $results;
	}

	/*
	Adds a new timeframe for a given price category
	*/
	function add_timeframe($timeframe_data)
	{
		// If the timeframe is a default, first check if default already exists
		if($timeframe_data['default'] == 1){
			$query = $this->db->get_where('seasonal_timeframes', array('season_id' => $timeframe_data['season_id'], 'default' => 1));
			
			if($query->num_rows() > 0){
				return false;
			}
		}
		
		if ($this->db->insert('seasonal_timeframes', $timeframe_data))
		{
			$timeframe_id = $this->db->insert_id();
			return $timeframe_id;
		}
		return false;
	}

	/*
	 Gets a timeframe with a given timeframe ID
	*/
	function get_timeframe($timeframe_id)
	{
		if (!$timeframe_id || $timeframe_id < 1) return false;
		
		$this->db->from('seasonal_timeframes');
        $this->db->where("timeframe_id", $timeframe_id);

        $query = $this->db->get();

        if($query->num_rows()==1)
        {
            return $query->row();
        }
        else return false;
	}
	/*
	Deletes a timeframe
	*/
	function delete_timeframe($timeframe_id)
	{
		if (!$timeframe_id){
			return false;
		}
		return $this->db->delete('seasonal_timeframes', array('timeframe_id' => (int) $timeframe_id));
	}
	/*
	Inserts or updates a price category
	*/
	function save(&$pricing_data, $price_id=false)
	{
		if (!$price_id || !$this->exists($price_id))
		{
			if($this->db->insert('seasonal_prices',$pricing_data))
			{
				$price_id = $this->db->insert_id();
				if (is_array($pricing_data))
					$pricing_data['price_id'] = $price_id;
				elseif (is_object($pricing_data))
					$pricing_data->price_id = $price_id;
				$this->create_default_timeframe($price_id,$pricing_data['season_id']);
				return true;
			}
			return false;
		}
		$this->db->where('price_id', $price_id);
		return $this->db->update('seasonal_prices',$pricing_data);
	}

	function get_overall_default()
	{
		$this->load->model('Teesheet');
		$teesheet_id = $this->Teesheet->get_default();
		$season_id = $this->Season->get_default($teesheet_id);
		$price_id = $this->get_default($season_id);
		$timeframe = $this->get_default_timeframe($price_id);
		if ($timeframe !== false)
			return timeframe;
		else return false;
	}
	
	/*
	Updates a timeframe
	*/
	function save_timeframe($timeframe_id, $timeframe_data)
	{
		$this->db->where('timeframe_id', $timeframe_id);
		return $this->db->update('seasonal_timeframes', $timeframe_data);
	}
	
	/*
	 Determines if a given price_id is a price category
	*/
	function exists($price_id)
	{
		$this->db->from('seasonal_prices');
		$this->db->where('price_id',$price_id);
		$this->db->limit(1);
		$query = $this->db->get();
	
		return ($query->num_rows()==1);
	}
	
	/*
	 * Get list of prices for the teesheet and day specified
	 */
	function get_price_timeframes($teesheet_id, $price_class = false, $date = false, $filter_days = true, $course_id = false)
	{
		$date_filter_sql = '';
		if($date){
			$day_of_week = strtolower(date('l', strtotime($date)));
			$date = substr($date, 5);
			$season_date = $this->db->escape("0000-" . $date);
			
			$date_filter_sql = "AND (season.start_date <= {$season_date} AND season.end_date >= {$season_date})";
		}else{
			$day_of_week = date('l');
		}
		
		$default_price = $this->Price_class->get_default($course_id);
		$default_class_id = (int) $default_price['class_id'];

		$teesheet_id = (int) $teesheet_id;
		$course_id =  $course_id ? $course_id : (int) $this->session->userdata('course_id');
		
		// Order by the selected price class first, then by the default price class
		if($price_class){
			$price_class_id = (int) $price_class;
			$price_class_sql = "AND (timeframe.class_id = {$price_class_id} OR timeframe.class_id = {$default_class_id})";
			
			if($price_class_id == $default_class_id){
				$price_class_sql = "AND timeframe.class_id = {$default_class_id}";
			}
		}
		
		$order_sql = 'season.season_name, price_class.name, timeframe.start_time';
		
		$filter_days_sql = '';
		if($filter_days){
			$filter_days_sql = "AND timeframe.{$day_of_week} = 1";			
		}else{
			$order_sql = "IF(timeframe.{$day_of_week} = 1, 1, 0) DESC, ".$order_sql;
		}
			
		$query = $this->db->query("SELECT timeframe.timeframe_id, timeframe.price1, 
				timeframe.price2, timeframe.price3, timeframe.price4, 
				timeframe.price5, timeframe.price6, timeframe.start_time, 
				timeframe.end_time, timeframe.default AS default_timeframe, timeframe.season_id,
				season.start_date, season.end_date, season.default AS default_season, 
				season.holiday AS season_holiday, timeframe.timeframe_name, season.season_name, 
				price_class.name AS price_class_name, price_class.class_id AS price_class_id,
				IF(timeframe.{$day_of_week} = 1, 1, 0) AS is_today
			FROM foreup_seasonal_timeframes AS timeframe
			INNER JOIN foreup_seasons AS season
				ON season.season_id = timeframe.season_id
			LEFT JOIN foreup_price_classes AS price_class
				ON price_class.class_id = timeframe.class_id
			WHERE season.course_id = {$course_id}
				AND season.teesheet_id = {$teesheet_id}
				{$date_filter_sql}
				{$price_class_sql}
				{$filter_days_sql}
				AND timeframe.active = 1
			GROUP BY timeframe.timeframe_id
			ORDER BY {$order_sql}");

		$rows = $query->result_array();
		return $rows;
	}
	
	// Get a price by price class, date, and time
	function get_price($teesheet_id, $price_class = false, $date = false, $time = false, $holes = '9', $cart = false, $course_id = false){
		$timeframe = $this->get_price_timeframe($teesheet_id, $price_class, $date, $time, $course_id);
		
		$price = 0.00;
		if($holes == '9' && empty($cart)){
			$price = $timeframe['price2'];
		
		}else if($holes == '9' && !empty($cart)){
			$price = $timeframe['price4'];
		
		}else if($holes == '18' && empty($cart)){
			$price = $timeframe['price1'];
		
		}else if($holes == '18' && !empty($cart)){
			$price = $timeframe['price3'];
		}
		
		return (float) $price;		
	}
	
	// Get a price by specific timeframe_id
	function get_price_by_timeframe($timeframe_id, $holes = '9', $cart = false){
		
		$this->db->select('price1, price2, price3, price4, price5, price6');
		$this->db->from('seasonal_timeframes');
		$this->db->where('timeframe_id', (int) $timeframe_id);
		$timeframe = $this->db->get()->row_array();
		$price = 0.00;
		if($holes == '9' && empty($cart)){
			$price = $timeframe['price2'];
		
		}else if($holes == '9' && !empty($cart)){
			$price = $timeframe['price4'];
		
		}else if($holes == '18' && empty($cart)){
			$price = $timeframe['price1'];
		
		}else if($holes == '18' && !empty($cart)){
			$price = $timeframe['price3'];
		}
		
		return (float) $price;		
	}
	
	// Get timeframe data for a specific price class, date and time
	function get_price_timeframe($teesheet_id, $price_class = false, $date = false, $time = false, $course_id = false)
	{
		if ($time == false){
			$time = date('Hi');
		}
		$time = sprintf('%04d', $time);
		$time_hours = substr($time, 0, 2);
		$time_min = substr($time, 2, 2);
		if((int) $time_min < 30){
			$time_min = '00';
		}else{
			$time_min = '30';
		}
		$time = $time_hours.$time_min;
		
		// If date is empty, default to today
		if ($date == false){
			$date = date('Y-m-d');
		}
		
		// If price class is empty, use default price class
		if(empty($price_class)){
			if (empty($this->default_price_cache)) {
				$default_price = $this->Price_class->get_default($course_id);
				$this->default_price_cache = $default_price;
			}
			else {
				$default_price = $this->default_price_cache;
			}
			$price_class = (int) $default_price['class_id'];			
		}	

		// Get possible timeframes for this particular price class, date, and teesheet
		$cache_key = md5($teesheet_id . $price_class . $date);
		
		if(!empty($this->prices_cache[$cache_key])){
			$price_array = $this->prices_cache[$cache_key];
		}else{
			$price_array = $this->get_price_timeframes($teesheet_id, $price_class, $date, true, $course_id);
			$this->prices_cache[$cache_key] = $price_array;
		}
		
		$ordered_prices = array();
		foreach($price_array as $timeframe){
			
			if((int) $timeframe['start_time'] <= (int) $time && (int) $timeframe['end_time'] > (int) $time){	
				
				// Create a list of possible prices based on distance from reservation time
				$time_difference = abs((int) ((int) $timeframe['start_time'] - (int) $time));
				$time_difference = sprintf('%04d', $time_difference);
				
				$start_date = str_replace('0000', date('Y'), $timeframe['start_date']);
				$start_date = DateTime::createFromFormat('Y-m-d', $start_date);
				$date_selected = DateTime::createFromFormat('Y-m-d', $date);

				$days_difference = $start_date->diff($date_selected);
				$days_difference = abs((int) $days_difference->format('%a'));
				
				if($timeframe['holiday'] == 1){ $holiday_sort = 0; }else{ $holiday_sort = 1; }
				if($timeframe['price_class_id'] == (int) $price_class){ $class_sort = 0; }else{ $class_sort = 1; }
				
				// Build key to sort pricing
				$key = $holiday_sort.$timeframe['default_season'].$class_sort.$days_difference.$timeframe['default_timeframe'].$time_difference;
				$ordered_prices[$key] = $timeframe;
			}
		}
		
		// Sort array by key from lowest to highest
		ksort($ordered_prices);
		
		// First timeframe in list is the correct one
		$timeframe = array_shift($ordered_prices);
		return $timeframe;
	}
	
	// Gets a list of prices by time for a particular day (used for online booking)
	function get_prices($teesheet_id, $price_class = false, $date = false){
		
		$this->load->helper('date');
		$times = time_array();
		$time_prices = array();
		$price_array = $this->get_price_timeframes($teesheet_id, $price_class, $date);

		// Loop through list of times throughout the daty
		foreach($times as $time => $time_data){
			
			// Loop through possible prices for each time
			foreach($price_array as $key => $timeframe){
				
				if((int) $timeframe['start_time'] <= (int) $time && (int) $timeframe['end_time'] > (int) $time){
					$time_prices[(int) $time] = $timeframe;
					break 1;
				}
			}
		}
		
		return $time_prices;
	}
	
	// Returns all price classes for 9 holes, and all price classes
	// for 18 holes. Used in select menu dropdown for sales page
	function get_teetime_price_classes($teesheet_id = false, $date = false){
		
		$filter_days = true;
		if($date == false){
			$filter_days = false;
		}
		if(empty($teesheet_id)){
			$teesheet_id = (int) $this->session->userdata('teesheet_id');
		}
		$timeframes = $this->get_price_timeframes($teesheet_id, false, $date, $filter_days);
		$num_price_classes = count($timeframes);
		
		$rows_9 = array();
		$rows_18 = array();
		$rows_9_today = array();
		$rows_18_today = array();
		
		// Load list of 9 hole price classes
		for($x = 0; $x < $num_price_classes; $x++){
			$t = $timeframes[$x];
			
			$timeframe_name = $t['timeframe_name'] != 'Default' ? $t['timeframe_name'] : '';
			
			$season_name = '';
			if($t['season_holiday'] == 1){
				$season_name = '['.$t['season_name'].']';
			}			
			
			$key_9 = $t['price_class_id'].'_'.$t['timeframe_id'].'_2';
			$key_18 = $t['price_class_id'].'_'.$t['timeframe_id'].'_1';
			
			if($t['is_today'] == 1){		
				$rows_9_today[$key_9] = '9 - '.$season_name.' '.$t['price_class_name'].' '.$timeframe_name;
				$rows_18_today[$key_18] = '18 - '.$season_name.' '.$t['price_class_name'].' '.$timeframe_name;				
			}else{
				$rows_9[$key_9] = '9 - '.$season_name.' '.$t['price_class_name'].' '.$timeframe_name;
				$rows_18[$key_18] = '18 - '.$season_name.' '.$t['price_class_name'].' '.$timeframe_name;
			}	
		}
		
		return array_merge($rows_9_today, $rows_18_today, $rows_9, $rows_18);
	}
	
	// Returns all price classes for 9 hole cart, and all price classes
	// for 18 hole cart. Used in select menu dropdown for sales page
	function get_cart_price_classes($teesheet_id = false, $date = false){
		
		$filter_days = true;
		if($date == false){
			$filter_days = false;
		}
		if(empty($teesheet_id)){
			$teesheet_id = (int) $this->session->userdata('teesheet_id');
		}
		$timeframes = $this->get_price_timeframes($teesheet_id, false, $date, $filter_days);
		$num_price_classes = count($timeframes);
		
		$rows_9 = array();
		$rows_18 = array();
		$rows_9_today = array();
		$rows_18_today = array();
		
		// Load list of 9 hole price classes
		for($x = 0; $x < $num_price_classes; $x++){
			$t = $timeframes[$x];
			
			$timeframe_name = $t['timeframe_name'] != 'Default' ? $t['timeframe_name'] : '';
			
			$season_name = '';
			if($t['season_holiday'] == 1){
				$season_name = '['.$t['season_name'].']';
			}			
			
			$key_9 = $t['price_class_id'].'_'.$t['timeframe_id'].'_4';
			$key_18 = $t['price_class_id'].'_'.$t['timeframe_id'].'_3';
			
			if($t['is_today'] == 1){		
				$rows_9_today[$key_9] = '9 Cart - '.$season_name.' '.$t['price_class_name'].' '.$timeframe_name;
				$rows_18_today[$key_18] = '18 Cart - '.$season_name.' '.$t['price_class_name'].' '.$timeframe_name;				
			}else{
				$rows_9[$key_9] = '9 Cart - '.$season_name.' '.$t['price_class_name'].' '.$timeframe_name;
				$rows_18[$key_18] = '18 Cart - '.$season_name.' '.$t['price_class_name'].' '.$timeframe_name;
			}	
		}
		
		return array_merge($rows_9_today, $rows_18_today, $rows_9, $rows_18);
	}	

	/*
	 Determines if a price class has already been applied to a season
	*/
	function used($class_id, $season_id)
	{
		$query = $this->db
			->from('price_classes')
			->join('seasonal_timeframes', 'seasonal_timeframes.class_id = price_classes.class_id')
			->where('seasonal_timeframes.class_id',$class_id)
			->where('seasonal_timeframes.season_id',$season_id)
			->limit(1)
			->get();
	
		return ($query->num_rows()==1);
	}
}
?>
