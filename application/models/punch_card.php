<?php
class Punch_card extends CI_Model
{
	/*
	Determines if a given giftcard_id is an punch_card
	*/
	function exists( $punch_card_id )
	{
		$course_ids = array();
		$this->get_linked_course_ids($course_ids);
        
        $this->db->from('punch_cards');
		$this->db->where('punch_card_id',$punch_card_id);
		$this->db->where_in('course_id', array_values($course_ids));
		$this->db->where('deleted',0);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}
	/*
	Determines if a given punch_card_id is expired
	*/
	function is_expired( $punch_card_id )
	{
		//echo 'is expired';
		$this->db->select('expiration_date');
		$this->db->from('punch_cards');
		$this->db->where('punch_card_id',$punch_card_id);
		$this->db->where('deleted',0);
		$this->db->limit(1);
		$result = $this->db->get()->result_array();
		//print_r($result);
		return ($result[0]['expiration_date'] != '0000-00-00' && strtotime($result[0]['expiration_date']) < time());
	}

	/*
	Determines if a given punch_card_id is has punches
	*/
	function has_punches( $punch_card_id, $item_id )
	{
		//echo 'is expired';
		$this->db->from('punch_card_items');
		$this->db->where('punch_card_id',$punch_card_id);
		$this->db->where('item_id', $item_id);
		$this->db->where('(foreup_punches - foreup_used > 0)');
		$this->db->where('deleted',0);
		$this->db->limit(1);
		$result = $this->db->get()->row_array();
		//print_r($result);
		return ($result[0]['expiration_date'] != '0000-00-00' && strtotime($result[0]['expiration_date']) < time());
	}
	function get_items($punch_card_id, $view_all = false)
	{
		$this->db->select('name, punch_card_items.item_id AS item_id, punches, used, punches - used AS quantity');
		$this->db->from('punch_card_items');
		$this->db->where('punch_card_id',$punch_card_id);
		$this->db->join('items', 'items.item_id = punch_card_items.item_id');
		
		//$this->db->where('deleted',0);
        if(!$view_all) {
            $this->db->limit(1);
        }
		$result = $this->db->get();
		
		return $result->result();
	}
	function get_punch_card_value($punch_card_id, $view_all = false)
	{
		$existing_punch_card = false;
		$payments = $this->sale_lib->get_payments();
		//print_r($payments);
		// foreach($payments as $payment_type => $payment)
		// {
			// echo 'finding Punch Card in '.$payment_type.' - '.((strpos('Punch', $payment_type) !== false)?'y':'n');
			// $existing_punch_card = ($existing_punch_card ? true : (strpos('Punch', $payment_type) !== false));
		// }
		// CHECK PAYMENTS FOR PUNCH CARDS
		if (!$existing_punch_card)
		{
			$tax_included = $this->config->item('unit_price_includes_tax');
			$punch_card = $this->get_info($punch_card_id);
			$punch_card_items = $this->get_items($punch_card_id, $view_all);
			$punch_items = array('valid'=>array(),'invalid'=>array());
			foreach($punch_card_items as $punch_card_item)
			{
				if ($punch_card_item->punches > $punch_card_item->used)
					$punch_items['valid'][] = $punch_card_item->item_id;
				else
					$punch_items['invalid'][] = $punch_card_item->item_id;
			}
			$cart = $this->sale_lib->get_basket();
			
			$invalid = false;
			//print_r($cart);
			foreach($cart as $item)
			{
				if (in_array($item['item_id'], $punch_items['valid']))
				{
					// GET ITEM TAXES TO ADD TO TOTAL
					$taxes = $tax_included ? array() : $this->Item_taxes->get_info($item['item_id']);
					$price = $item['price'];
					foreach ($taxes as $tax)
						$price += $item['price'] * $tax['percent'] / 100;

					return array('success'=>true,'item_id'=>$item['item_id'], 'value'=>$price);
				}
				else if (in_array($item['item_id'], $punch_items['invalid']))
					$invalid = true;
			}
			if ($invalid)
				return array('success'=>false,'message'=>lang('sales_item_punches_used'));
			else
				return array('success'=>false,'message'=>lang('sales_punch_card_not_valid_for_items'));
		}
		return array('success'=>true);
	}
	/*
	Returns all the punch_cards
	*/
	function get_all($limit=10000, $offset=0)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
        {
	        //$course_id = "AND foreup_customers.course_id = '{$this->session->userdata('course_id')}'";
			//$this->db->where('customers.course_id', $this->session->userdata('course_id'));
			$course_ids = array();
			$this->get_linked_course_ids($course_ids);
		    $this->db->where_in('course_id', array_values($course_ids));
	    }
        $this->db->from('punch_cards');
		$this->db->where("deleted = 0 $course_id");
		$this->db->order_by("punch_card_number", "asc");
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
	
	function count_all()
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
        {
	        //$course_id = "AND foreup_customers.course_id = '{$this->session->userdata('course_id')}'";
			//$this->db->where('customers.course_id', $this->session->userdata('course_id'));
			$course_ids = array();
			$this->get_linked_course_ids($course_ids);
		    $this->db->where_in('course_id', array_values($course_ids));
	    }
        $this->db->from('punch_cards');
		$this->db->where("deleted = 0 $course_id");
		return $this->db->count_all_results();
	}
	function cleanup()
	{
		$punch_card_data = array('punch_card_number' => null);
		$this->db->where('deleted', 1);
		return $this->db->update('punch_cards',$punch_card_data);
	}
    
	/*
	Gets information about a particular punch_card
	*/
	function get_info($punch_card_id)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
        {
	        //$course_id = "AND foreup_customers.course_id = '{$this->session->userdata('course_id')}'";
			//$this->db->where('customers.course_id', $this->session->userdata('course_id'));
			$course_ids = array();
			$this->get_linked_course_ids($course_ids);
		    $this->db->where_in('course_id', array_values($course_ids));
	    }
        $this->db->from('punch_cards');
		$this->db->where('punch_card_id',$punch_card_id);
		$this->db->where("deleted = 0 $course_id");
		
		$query = $this->db->get();
		if($query->num_rows()==1)
		{
			$result = $query->row();
			
			return $result;
		}
		else
		{
			//Get empty base parent object, as $punch_card_id is NOT an punch_card
			$punch_card_obj=new stdClass();

			//Get all the fields from punch_cards table
			$fields = $this->db->list_fields('punch_cards');

			foreach ($fields as $field)
			{
				$punch_card_obj->$field='';
			}

			return $punch_card_obj;
		}
	}

	/*
	Get an punch_card id given an punch_card number
	*/
	function get_punch_card_id($punch_card_number)
	{
		$course_ids = array();
		$this->get_linked_course_ids($course_ids);
		$this->db->where_in('course_id', array_values($course_ids));
	    
	    $this->db->from('punch_cards');
		$this->db->where('punch_card_number',$punch_card_number);
		$this->db->where("deleted", 0);
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows()==1)
		{
			return $query->row()->punch_card_id;
		}

		return false;
	}

	/*
	Gets information about multiple punch_cards
	*/
	function get_multiple_info($punch_card_ids)
	{
		$this->db->from('punch_cards');
		$this->db->where_in('punch_card_id',$punch_card_ids);
		$this->db->where('deleted',0);
		$this->db->order_by("punch_card_number", "asc");
		return $this->db->get();
	}

	/*
	Inserts or updates a punch_card
	*/
	function save(&$punch_card_data,$punch_card_id=false)
	{
		if (strtotime($punch_card_data['expiration_date']))
			$punch_card_data['expiration_date'] = date('Y-m-d ', strtotime($punch_card_data['expiration_date']));
		else 
			$punch_card_data['expiration_date'] = '0000-00-00';
		//echo $punch_card_data['expiration_date'];
		if ($punch_card_data['customer_id'] == '' || $this->Customer->exists($punch_card_data['customer_id']))
        {
			if (!$punch_card_id or !$this->exists($punch_card_id))
			{
				if($this->db->insert('punch_cards',$punch_card_data))
				{
					$punch_card_data['punch_card_id']=$this->db->insert_id();
					return true;
				}
				return false;
			}
	
			$this->db->where('punch_card_id', $punch_card_id);
			return $this->db->update('punch_cards',$punch_card_data);
		}
		else {
			return false;
		}
	}
	
	/*
	Inserts or updates a punch_card
	*/
	function save_items(&$punch_card_items,$punch_card_id=false)
	{
		foreach($punch_card_items as $punch_card_item)
		{
			$this->db->insert('punch_card_items', $punch_card_item);
			//echo $this->db->last_query();
		}
		return true;
	}

	/*
	Updates multiple punch_cards at once
	*/
	function update_multiple($punch_card_data,$punch_card_ids)
	{
		$this->db->where_in('punch_card_id',$punch_card_ids);
		return $this->db->update('punch_cards',$punch_card_data);
	}
	function update_punch_card_value($punch_card_number, $punches_used = 1, $item_id = false)
	{
        $where = '';
		$punch_card_id = $this->get_punch_card_id($punch_card_number);
        if($item_id) {
           $where = " AND item_id = $item_id";
        }

        return $this->db->query("UPDATE foreup_punch_card_items SET used = used + $punches_used WHERE punch_card_id = $punch_card_id $where");
	}
	

	/*
	Deletes one punch_card
	*/
	function delete($punch_card_id)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
            $course_id = "AND course_id = '{$this->session->userdata('course_id')}'";
        $this->db->where("punch_card_id = '$punch_card_id' $course_id");
		return $this->db->update('punch_cards', array('deleted' => 1, 'punch_card_number'=>null));
	}

	/*
	Deletes a list of punch_cards
	*/
	function delete_list($punch_card_ids)
	{
		if (!$this->permissions->is_super_admin())
            $this->db->where('course_id', $this->session->userdata('course_id'));
        $this->db->where_in('punch_card_id',$punch_card_ids);
		return $this->db->update('punch_cards', array('deleted' => 1, 'punch_card_number'=>null));
 	}

	function get_linked_course_ids(&$course_ids)
	{
		$this->load->model('course');
		return $this->course->get_linked_course_ids($course_ids, 'shared_giftcards');
	}
 	/*
	Get search suggestions to find punch_cards
	*/
	function get_search_suggestions($search,$limit=25, $ids = false)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
        {
	        //$course_id = "AND foreup_customers.course_id = '{$this->session->userdata('course_id')}'";
			//$this->db->where('customers.course_id', $this->session->userdata('course_id'));
			$course_ids = array();
			$this->get_linked_course_ids($course_ids);
		    $course_ids = join(',',$course_ids); 
			$course_id = "AND course_id IN ({$course_ids})";
	    }
        $suggestions = array();

		$this->db->from('punch_cards');
		$this->db->where("(punch_card_number LIKE '$search%' OR details LIKE '%$search%')");
		$this->db->where("deleted = 0 $course_id");
		$this->db->order_by("punch_card_number", "asc");
		$by_number = $this->db->get();
		foreach($by_number->result() as $row)
		{
			$suggestions[]=array('label' => $row->punch_card_number, 'value' => ($ids ? $row->punch_card_id : $row->punch_card_number));
		}

		$this->db->from('punch_cards');
		$this->db->join('people','punch_cards.customer_id=people.person_id');	
		$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%') and ".$this->db->dbprefix('punch_cards').".deleted=0 $course_id");
		$this->db->order_by("last_name", "asc");		
		$by_name = $this->db->get();
		foreach($by_name->result() as $row)
		{
			$suggestions[]=array('label'=> $row->first_name.' '.$row->last_name.($ids ? ' - '.$row->punch_card_number : ''), 'value' => ($ids ? $row->punch_card_id : $row->punch_card_number));		
		}

		//only return $limit suggestions
		if(count($suggestions > $limit))
		{
			$suggestions = array_slice($suggestions, 0,$limit);
		}
		return $suggestions;

	}

	/*
	Preform a search on punch_cards
	*/
	function search($search, $limit=20, $offset = 0)
	{
		$course_id = '';
        if (!$this->permissions->is_super_admin())
        {
	        //$course_id = "AND foreup_customers.course_id = '{$this->session->userdata('course_id')}'";
			//$this->db->where('customers.course_id', $this->session->userdata('course_id'));
			$course_ids = array();
			$this->get_linked_course_ids($course_ids);
		    $course_ids = join(',',$course_ids); 
			$course_id = "AND course_id IN ({$course_ids})";
	    }
        $this->db->from('punch_cards');
		$this->db->join('people','punch_cards.customer_id=people.person_id', 'left');	
		$this->db->where("(first_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		last_name LIKE '%".$this->db->escape_like_str($search)."%' or 
		CONCAT(`first_name`,' ',`last_name`) LIKE '%".$this->db->escape_like_str($search)."%' or 
                punch_card_number LIKE '%".$this->db->escape_like_str($search)."%' or 
                details LIKE '%".$this->db->escape_like_str($search)."%') and ".$this->db->dbprefix('punch_cards').".deleted=0 $course_id");
		$this->db->order_by("punch_card_number", "asc");
		// Just return a count of all search results
        if ($limit == 0)
            return $this->db->get()->num_rows();
        // Return results
        $this->db->offset($offset);
		$this->db->limit($limit);
		return $this->db->get();	
	}
}
?>
