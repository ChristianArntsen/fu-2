<?php
class Queue_invoice extends CI_Model
{
	/*
	Returns the count of all queue_invoices
	*/
	function count_all(){}
	function get_all($limit=10000, $offset=0){}
	
	/*
	Get search suggestions to find queue_invoices
	*/
	function get_search_suggestions($search,$limit=25){}
	
	/*
	Preform a search on queue_invoices
	*/
	function search($search, $limit=20){}
	
	/*
	Gets information about a particular queue_invoice
	*/
	function get_info($queue_invoice_id){}
	
	/*
	Inserts or updates a queue_invoice
	*/
	function save(&$queue_invoice_data,$queue_invoice_id=false)
	{		
			if (!$queue_invoice_id or !$this->exists($queue_invoice_id))
			{
				if($this->db->insert('queue_invoices',$queue_invoice_data))
				{
					$queue_invoice_data['queue_id']=$this->db->insert_id();
					return true;
				}
				return false;
			}
	
			$this->db->where('queue_id', $queue_invoice_id);
			
			return $this->db->update('queue_invoices',$queue_invoice_data);		
	}
	
	function get_jobs()
	{
		$this->db->from('queue_invoices');
		$this->db->where('completed', 0);
		$this->db->where('attempts < 3');
		$this->db->order_by('queue_id');
		$this->db->limit(1);
		$result = $this->db->get()->result_array();
		return $result;
	}
	
	
	/*
	Determines if a given queue_invoice_id is a queue_invoice
	*/
	function exists($queue_invoice_data)
	{
        $this->db->from('queue_invoices');
		$this->db->where('course_id',$queue_invoice_data['course_id']);
		$this->db->where('member_balance', $queue_invoice_data['member_balance']);
		$this->db->where('customer_credit', $queue_invoice_data['customer_credit']);
		$this->db->where('start_date', $queue_invoice_data['start_date']);
		$this->db->where('end_date', $queue_invoice_data['end_date']);
		$this->db->limit(1);
		$query = $this->db->get();

		return ($query->num_rows()==1);
	}	
	
	function delete_list($queue_invoice_ids){}	
}
?>
